﻿using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.FilterAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace FourgTV_OTT2.Api2
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 設定和服務
            var cors = new EnableCorsAttribute("*", "*", "*");
            //var cors = new EnableCorsAttribute("https://www.4gtv.tv,https://embed.4gtv.tv,https://webcache.googleusercontent.com", "*", "*");
            config.EnableCors(cors);

            // Web API 路由
            config.MapHttpAttributeRoutes();

            // Web API ActionFilter
            config.Filters.Add(new IsRestrictUserAgentAttribute());

            config.Filters.Add(new ExceptionFilter());
            config.Filters.Add(new LogFilter());
            // GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();


            var format = GlobalConfiguration.Configuration.Formatters;
            format.JsonFormatter.SerializerSettings.DateFormatString = "yyyy.MM.dd";

            //清除默认xml
            format.XmlFormatter.SupportedMediaTypes.Clear();

            //通过参数设置返回格式
           // format.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("t", "json", "application/json"));
            format.XmlFormatter.MediaTypeMappings.Add(new QueryStringMapping("t", "xml", "application/xml"));

            ////設定忽略值為 null 的屬性
            //GlobalConfiguration.Configuration.Formatters
            //    .JsonFormatter.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings()
            //    {
            //        NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore 
            //    };

            config.Routes.MapHttpRoute(
                name: "swagger_root",
                routeTemplate: "",
                defaults: null,
                constraints: null,
                handler: new Swashbuckle.Application.RedirectHandler((message => message.RequestUri.ToString()), "swagger/ui/index")
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
