﻿using AutoMapper;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Common
{
    public class clsTOOL
    {
        private static readonly string fsSMS_URL = WebConfigurationManager.AppSettings["fsSMS_URL"];

        private static readonly string fsMAIL_DISPLAYNAME = WebConfigurationManager.AppSettings["fsMAIL_DISPLAYNAME"];
        private static readonly string fsMAIL_FROM = WebConfigurationManager.AppSettings["fsMAIL_FROM"];
        private static readonly string fsMAIL_HOST = WebConfigurationManager.AppSettings["fsMAIL_HOST"];
        private static readonly string fnMAIL_PORT = WebConfigurationManager.AppSettings["fnMAIL_PORT"];
        


        /// <summary>
        /// 發送簡訊
        /// </summary>
        /// <param name="fsPHONE">手機號碼</param>
        /// <param name="fsBODY">簡訊內容</param>
        public bool fnSEND_SMS(string fsPHONE, string fsBODY)
        {
            try
            {
                string fsURL = fsSMS_URL.Replace("<phone>", fsPHONE).Replace("<smsbody>", HttpUtility.UrlEncode(fsBODY, System.Text.Encoding.GetEncoding(950)));
                string fsRESULT = string.Empty;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(fsURL);
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                httpWebRequest.GetResponse();
            }
            catch (Exception)
            {
                return false;
            }


            return true;
        }

        /// <summary>
        /// 發送EMAIL
        /// </summary>
        /// <param name="fsEMAIL">EMAIL</param>
        /// <param name="fsSUBJECT">主旨</param>
        /// <param name="fsBODY">EMAIL內容</param>
        /// <returns></returns>
        public bool fnSEND_EMAIL(string fsEMAIL, string fsSUBJECT, string fsBODY)
        {
            string MailFrom = fsMAIL_FROM;
            string MailFromDisplayName = fsMAIL_DISPLAYNAME;
            string MailTo = fsEMAIL;

            try
            {
                MailMessage objMail = new MailMessage();
                objMail.From = new MailAddress(MailFrom, MailFromDisplayName);
                objMail.Subject = fsSUBJECT;
                objMail.SubjectEncoding = Encoding.UTF8;
                objMail.Body = fsBODY;
                objMail.BodyEncoding = Encoding.UTF8;
                objMail.IsBodyHtml = true;
                objMail.Priority = MailPriority.Normal;
                objMail.To.Add(fsEMAIL);

                SmtpClient client = new SmtpClient()
                {
                    Host = fsMAIL_HOST,
                    Port = int.Parse(fnMAIL_PORT),
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential("edm@ftv.com.tw", "Ftv85118503"),
                    EnableSsl = true
                };

                client.Send(objMail);
                client.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        ///// <summary>
        ///// 發送EMAIL
        ///// </summary>
        ///// <param name="fsEMAIL">EMAIL</param>
        ///// <param name="fsSUBJECT">主旨</param>
        ///// <param name="fsBODY">EMAIL內容</param>
        ///// <returns></returns>
        //public bool fnSEND_EMAIL2(string fsEMAIL, string fsSUBJECT, string fsBODY)
        //{
        //    string MailFrom = fsMAIL_FROM;
        //    string MailFromDisplayName = fsMAIL_DISPLAYNAME;
        //    string MailTo = fsEMAIL;

        //    try
        //    {
        //        MailMessage objMail = new MailMessage();
        //        objMail.From = new MailAddress(MailFrom, MailFromDisplayName);
        //        objMail.Subject = fsSUBJECT;
        //        objMail.SubjectEncoding = Encoding.UTF8;
        //        objMail.Body = fsBODY;
        //        objMail.BodyEncoding = Encoding.UTF8;
        //        objMail.IsBodyHtml = true;
        //        objMail.Priority = MailPriority.Normal;
        //        objMail.To.Add(fsEMAIL);

        //        string[] aa = 
        //        {
        //            "07c6a2d2-ef42-4c04-8248-7b3d0f7e36c2.png",
        //            "368cec67-a06d-4ec2-a462-fcb6ed244ca1.png",
        //            "45716187-ba66-430f-b093-f6880dcbdf0f.png",
        //            "59cdcbad-4146-41ea-bdf2-4e4ae9a8bfcf.png",
        //            "663b20fa-bdb4-4776-96eb-5dbcd207b5dc.png",
        //            "a56a9fea-22a9-4955-834e-f278343bc43a.png",
        //            "b3592378-597c-4b06-a284-c3bea5bac45e.png",
        //            "b711c85f-05c9-4e4a-8be3-cdc19aa5cb54.png",
        //            "bbc15965-cb2c-4868-a7f4-932b4143da41.png",
        //            "c2606d3d-201b-458a-8ece-bacbd0a53ca1.png",
        //            "e08c29af-3c91-4ab2-ab0a-a068005a35e5.png",
        //            "ee613bb1-25ec-49e7-9b72-ea7bcc823e7a.png",
        //            "fe9fbb6f-e2da-4a2d-85d7-f40f1578d25a.png"
        //        };

        //        foreach (var a in aa)
        //        {
        //            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(fsBODY, null, "text/html");
        //            imgResource(htmlView, a, "image/png");
        //            // add the views
        //            objMail.AlternateViews.Add(htmlView);
        //        }



        //        SmtpClient client = new SmtpClient()
        //        {
        //            Host = fsMAIL_HOST,
        //            Port = int.Parse(fnMAIL_PORT),
        //            UseDefaultCredentials = false,
        //            Credentials = new NetworkCredential("edm@ftv.com.tw", "Ftv85118503"),
        //            EnableSsl = true
        //        };

        //        client.Send(objMail);
        //        client.Dispose();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}
        public void imgResource(AlternateView htmlView, string imgName, string imgType)
        {
            // create image resource from image path using LinkedResource class..   
            LinkedResource imageResource = new LinkedResource(@"\\192.168.100.179\FourgTV_Website\FourgTV_OTT2\API2\Staging\emailimage\" + imgName, imgType);
            string[] imgArr = imgName.Split('.');
            imageResource.ContentId = imgArr[0];
            imageResource.TransferEncoding = TransferEncoding.Base64;
            htmlView.LinkedResources.Add(imageResource);

        }

     

        /// <summary>
        /// 取得通行碼
        /// </summary>
        /// <param name="fnTYPE">取得通行碼方式(1:Email、2:簡訊)</param>
        /// <param name="fsUSER">EMAIL / PHONE</param>
        /// <param name="fsVALIDATE_TYPE">驗證類型(1:註冊 2:忘記密碼)</param>
        /// <returns></returns>
        public string fnGET_PASSCODE(int fnTYPE, string fsUSER, string fsVALIDATE_TYPE)
        {
            bool fbRESULT = true;

            var clsPASSCODE = new repACCOUNT().fnGET_PASSCODE(fsUSER, fsVALIDATE_TYPE);
            if (clsPASSCODE != null)
            {
                if (string.IsNullOrEmpty(clsPASSCODE.fsMESSAGE))
                {
                    if (fnTYPE == 1)
                    {
                        //EMAIL驗證
                        if (fsVALIDATE_TYPE == "1")
                            fbRESULT = fnSEND_EMAIL(fsUSER, "四季會員帳號開通", "您的4gTV會員通行碼為【" + clsPASSCODE.fsPASSCODE + "】，請在畫面輸入通行碼，註冊成功級成為4gTV會員並可免費體驗，免費內容請詳見網站說明。");
                        else
                            fbRESULT = fnSEND_EMAIL(fsUSER, "四季會員忘記密碼", "您的4gTV會員通行碼為【" + clsPASSCODE.fsPASSCODE + "】");
                    }
                    else
                    {
                        //傳送簡訊
                        if (fsVALIDATE_TYPE == "1")
                            fbRESULT = fnSEND_SMS(fsUSER, "您的4gTV會員通行碼為【" + clsPASSCODE.fsPASSCODE + "】，請在畫面輸入通行碼，註冊成功級成為4gTV會員並可免費體驗，免費內容請詳見網站說明。");
                        else
                            fbRESULT = fnSEND_SMS(fsUSER, "您的4gTV會員通行碼為【" + clsPASSCODE.fsPASSCODE + "】");
                    }

                    if (fbRESULT)
                        return string.Empty;
                    else
                        return "ERROR:發送通行碼失敗!";
                }
                else
                {
                    return "ERROR:" + clsPASSCODE.fsMESSAGE;
                }
            }
            else
            {
                return "ERROR:取得通行碼失敗!";
            }
        }

        /// <summary>
        /// 獲取作業系統的名字
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns></returns>
        public static string GetOSNameByUserAgent(string userAgent)
        {
            string osVersion = "";
            try
            {
                List<clsCODE> listCODE = new repCODE().fnGET_CODE("USERAGENT", "", "");
                clsCODE clsCODE = new clsCODE();
                clsCODE = listCODE.Where(x => userAgent.Contains(x.fsCODE)).FirstOrDefault();
                if (clsCODE != null)
                {
                    osVersion = clsCODE.fsNAME;
                }
                else
                {
                    osVersion = System.Web.HttpContext.Current.Request.Browser.Platform;
                }
            }
            catch (Exception e)
            {
                osVersion = System.Web.HttpContext.Current.Request.Browser.Platform;
            }
            return osVersion;
        }
        /// <summary>
        /// Model自動Mapper
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static TModel_2 ConvertModelToModel<TModel_1, TModel_2>(TModel_1 list)
        {

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<TModel_1, TModel_2>().ReverseMap();
            });
            var mapper = config.CreateMapper();
            var res = mapper.Map<TModel_1, TModel_2>(list);

            return res;
        }

    }
}