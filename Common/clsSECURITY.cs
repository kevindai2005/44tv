﻿//==============================================================
//<2016/08/31><David.Sin><新增本程式><加密方法，由STD_MAM3複製>
//==============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using FourgTV_OTT2.Api2.Models;
using System.Data;
using System.Web.Security;
using MaxMind.Db;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Common
{
    public class clsSECURITY
    {
        private static readonly string fsIP_DB = WebConfigurationManager.AppSettings["fsIP_DB"];
        private static readonly string fsAPP_HEADER_ENC_KEY = WebConfigurationManager.AppSettings["fsAPP_HEADER_ENC_KEY"];
        private static readonly string fsENC_KEY = WebConfigurationManager.AppSettings["fsENC_KEY"];
        private static readonly string fsNEW_MOZAI_KEY = WebConfigurationManager.AppSettings["fsNEW_MOZAI_KEY"];
        

        // 加密
        public static string Encrypt(string clearText, string key)
        {
            try
            {
                SHA512 sha512 = new SHA512CryptoServiceProvider();
                clearText = Convert.ToBase64String(sha512.ComputeHash(sha512.ComputeHash(Encoding.Default.GetBytes(clearText + key))));
            }
            catch (Exception)
            {
                clearText = string.Empty;
            }

            return clearText;
        }

        // 驗證(APP用)
        public static bool AppEncryptValidate(string validateText)
        {
            try
            {
                if (validateText == Convert.ToBase64String(new SHA512CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(DateTime.UtcNow.ToString("yyyyMMdd") + fsAPP_HEADER_ENC_KEY))))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        // 驗證是否已登入
        public static bool IsLogin(string cookieValue)
        {
            try
            {
                //cookie 解密
                string fsVALUE = CookieDecrypt(cookieValue);

                string fsACCOUNT_ID = fsVALUE.Split('-')[0];
                string fsTOKEN = fsVALUE.Split('-')[1];
                string fsUSER = fsVALUE.Split('-')[2];
                string fdEXPIRED_TIME = fsVALUE.Split('-')[3];
                string fsVALIDATE_CODE = fsVALUE.Split('-')[4];

                if (clsSECURITY.Encrypt(fsACCOUNT_ID + fsTOKEN + fsUSER + fdEXPIRED_TIME, fsENC_KEY) == fsVALIDATE_CODE)
                {
                    if (DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds > double.Parse(fdEXPIRED_TIME))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        // 驗證是否已登入(手機用)
        public static bool IsLogin(string cookieValue, string fsENC_KEY)
        {
            try
            {
                //cookie 解密
                string fsVALUE = CookieDecrypt(cookieValue, fsENC_KEY);

                string fsACCOUNT_ID = fsVALUE.Split('-')[0];
                string fsTOKEN = fsVALUE.Split('-')[1];
                string[] sArray = fsVALUE.Split(new string[] { fsACCOUNT_ID, fsTOKEN }, StringSplitOptions.RemoveEmptyEntries);
                string fsUSERTS = sArray[1].TrimStart(new Char[] { '-' });
                string fsUSERT = fsUSERTS.Remove(fsUSERTS.Length - 100, 100);
                //string fsUSER = fsVALUE.Split('-')[2];
                //string fdEXPIRED_TIME = fsVALUE.Split('-')[3];
                //string fsVALIDATE_CODE = fsVALUE.Split('-')[4];
                string[] sArray2 = fsVALUE.Split(new string[] { fsACCOUNT_ID, fsTOKEN, fsUSERT }, StringSplitOptions.RemoveEmptyEntries);
                string fdEXPIRED_TIMES = sArray2[2].TrimStart(new Char[] { '-' });
                string fdEXPIRED_TIMESS = fdEXPIRED_TIMES.Split('-')[0];
                string fsVALIDATE_CODES = fdEXPIRED_TIMES.Split('-')[1];

                if (clsSECURITY.Encrypt(fsACCOUNT_ID + fsTOKEN + fsUSERT + fdEXPIRED_TIMESS, fsENC_KEY) == fsVALIDATE_CODES)
                {
                    if (DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds > double.Parse(fdEXPIRED_TIMESS))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //Cookie加密
        public static string CookieEncrypt(string cookieValue)
        {
            return Convert.ToBase64String(MachineKey.Protect(Encoding.UTF8.GetBytes(cookieValue), fsENC_KEY));
        }

        //Cookie加密(手機用)
        public static string CookieEncrypt(string cookieValue, string fsENC_KEY)
        {
            return Convert.ToBase64String(MachineKey.Protect(Encoding.UTF8.GetBytes(cookieValue), fsENC_KEY));
        }

        //Cookie解密
        public static string CookieDecrypt(string encryptValue)
        {
            return Encoding.UTF8.GetString(MachineKey.Unprotect(Convert.FromBase64String(encryptValue), fsENC_KEY));
        }

        //Cookie解密(手機用)
        public static string CookieDecrypt(string encryptValue, string fsENC_KEY)
        {
            return Encoding.UTF8.GetString(MachineKey.Unprotect(Convert.FromBase64String(encryptValue), fsENC_KEY));
        }

        //Mozai URL加密
        public static string MozaiEncrypt(string content, uint unixTimeStamp)
        {
            string key = fsNEW_MOZAI_KEY;
            key = unixTimeStamp.ToString() + key.Substring(unixTimeStamp.ToString().Length);

            byte[] keyBytes = UTF8Encoding.UTF8.GetBytes(key);
            RijndaelManaged rm = new RijndaelManaged();
            rm.Key = keyBytes;
            rm.Mode = CipherMode.ECB;
            rm.Padding = PaddingMode.PKCS7;
            ICryptoTransform ict = rm.CreateEncryptor();
            byte[] contentBytes = UTF8Encoding.UTF8.GetBytes(content);
            byte[] resultBytes = ict.TransformFinalBlock(contentBytes, 0, contentBytes.Length);
            return Convert.ToBase64String(resultBytes, 0, resultBytes.Length);
        }

        //取得IP是否位於台澎金馬
        public static bool GetIpIsInTaiwan(string fsIP)
        {
            try
            {
                //string[] items = fsIP.Split('.');

                //long fnIP_LONG = long.Parse(items[0]) << 24 | long.Parse(items[1]) << 16 | long.Parse(items[2]) << 8 | long.Parse(items[3]);

                //Dictionary<string, string> dicParameters = new Dictionary<string, string>();
                //dicParameters.Add("fnIP", fnIP_LONG.ToString());
                //DataTable dtIP = clsDB.Do_Query("spAPI_GET_IP_IS_TAIWAN", dicParameters);

                //if (dtIP != null && dtIP.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dtIP.Rows.Count; i++)
                //    {
                //        if (dtIP.Rows[i]["fsCOUNTRY"].ToString() == "tw")
                //            return true;
                //    }
                //}

                using (var reader = new Reader(fsIP_DB, FileAccessMode.MemoryMapped))
                {
                    System.Net.IPAddress ip = System.Net.IPAddress.Parse(fsIP);
                    Dictionary<string, object> data = reader.Find<Dictionary<string, object>>(ip);
                    //
                    if (((Dictionary<string, object>)data["country"])["iso_code"].ToString().ToLower() == "tw")
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception)
            {
                return true;
            }

            return false;
        }

        //取得IP是否位於可播映地區
        public static string GetIpIsAllowArea(string fsIP, bool fcOVERSEAS, List<string> lstEXCLUDEs)
        {
            try
            {
                using (var reader = new Reader(fsIP_DB, FileAccessMode.MemoryMapped))
                {
                    System.Net.IPAddress ip = System.Net.IPAddress.Parse(fsIP);
                    Dictionary<string, object> data = reader.Find<Dictionary<string, object>>(ip);
                    string fsCOUNTRY = ((Dictionary<string, object>)data["country"])["iso_code"].ToString().ToUpper();

                    //允許海外
                    if (fcOVERSEAS)
                    {
                        if(lstEXCLUDEs != null && lstEXCLUDEs.Count > 0 && lstEXCLUDEs.FirstOrDefault(f => f == fsCOUNTRY) != null)
                        {
                            return "EXCEPT_COUNTRY";
                        }
                        else
                        {
                            return "IsAllowArea";
                        }
                    }
                    else
                    {
                        if (fsCOUNTRY == "TW")
                        {
                            return "IsAllowArea";
                        }
                        else
                        {
                            return "OVERSEAS";
                        }
                    }

                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        //取得IP所在國家
        public static string GetIpCountry(string fsIP)
        {
            try
            {
                using (var reader = new Reader(fsIP_DB, FileAccessMode.MemoryMapped))
                {
                    System.Net.IPAddress ip = System.Net.IPAddress.Parse(fsIP);
                    Dictionary<string, object> data = reader.Find<Dictionary<string, object>>(ip);
                    return ((Dictionary<string, object>)data["country"])["iso_code"].ToString().ToUpper();
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        //取得中華支付要用的code
        public static string GetChtCheckSum(string version, string chtProductId, string curl, string eurl, int fee, string others)
        {
            //string version = "3.1";
            //string backurl = "https://xxxx.xxx.xxx/"; //從中華支付頁面點畫面上「回上一頁」的按鈕回來的頁面網址。 如有客製的回傳參數，可以寫在網址後的 query 參數
            //string curl = "https://xxxx.xxx.xxx/c?chtsuccess=true&"; //當中華支付判定成功購買時，導回的頁面網址。 如有客製的回傳參數，可以寫在網址後的 query 參數
            //string eurl = "https://xxxx.xxx.xxx/e?chtfail=true&"; //當中華支付判定購買失敗時，導回的頁面網址。 如有客製的回傳參數，可以寫在網址後的 query 參數
            //string fee = "99"; //Price
            //string others = "";
            string fsRESULT = "";
            byte[] hashmessage = new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(version + chtProductId + curl + eurl + fee.ToString() + others));

            for (int i = 0; i < hashmessage.Count(); i++)
            {
                fsRESULT += String.Format("{0:X}", Convert.ToInt32((hashmessage[i] & 240) >> 4));
                fsRESULT += String.Format("{0:X}", Convert.ToInt32((hashmessage[i] & 15)));
            }

            return fsRESULT.ToLower();
        }

        //取得中華支付月付訂閱要用的code
        public static string GetChtSubscribeCheckSum(string authority, string alias, string productId, string mixcode, string action_date, string total_month, string setting_charge, string install_charge, string first_charge, string amount, string rent)
        {
            string fsRESULT = "";
            byte[] hashmessage = null;

            if (authority.ToLower() == "hinet" || authority.ToLower() == "chtld" || authority.ToLower() == "chtn")
            {
                hashmessage = new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(alias + productId + mixcode + action_date + total_month + setting_charge + install_charge + first_charge));
            }
            else if (authority.ToLower() == "credit")
            {
                hashmessage = new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(alias + productId + mixcode + action_date + total_month + setting_charge + install_charge + first_charge + amount + rent));

            }

            for (int i = 0; i < hashmessage.Count(); i++)
            {
                fsRESULT += String.Format("{0:X}", Convert.ToInt32((hashmessage[i] & 240) >> 4));
                fsRESULT += String.Format("{0:X}", Convert.ToInt32((hashmessage[i] & 15)));
            }

            return fsRESULT.ToLower();
        }

        //取得中華支付退租訂閱要用的code
        public static string GetChtUnSubscribeCheckSum(string alias, string subscribeno, string productId, string action_date, string amount)
        {
            string fsRESULT = "";
            byte[] hashmessage = null;

            hashmessage = new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(alias + subscribeno + productId + action_date + amount));

            for (int i = 0; i < hashmessage.Count(); i++)
            {
                fsRESULT += String.Format("{0:X}", Convert.ToInt32((hashmessage[i] & 240) >> 4));
                fsRESULT += String.Format("{0:X}", Convert.ToInt32((hashmessage[i] & 15)));
            }

            return fsRESULT.ToLower();
        }

        //取得中華支付查詢月租要用的code
        public static string GetChtQueryMonthCheckSum(string alias, string subscribeno, string productId)
        {
            string fsRESULT = "";
            byte[] hashmessage = null;

            hashmessage = new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(alias + subscribeno + productId));

            for (int i = 0; i < hashmessage.Count(); i++)
            {
                fsRESULT += String.Format("{0:X}", Convert.ToInt32((hashmessage[i] & 240) >> 4));
                fsRESULT += String.Format("{0:X}", Convert.ToInt32((hashmessage[i] & 15)));
            }

            return fsRESULT.ToLower();
        }

        //取得ECPAY要用的Parameter String
        public static string GetEcpayParameter(SortedDictionary<string, string> PostCollection)
        {
            string HashKey = new Repositorys.repCONFIG().fnGET_CFG("ECPAY_HASHKEY")[0].fsVALUE;
            string HashIV = new Repositorys.repCONFIG().fnGET_CFG("ECPAY_HASHIV")[0].fsVALUE;

            //計算檢查碼
            string str = string.Empty;
            string str_pre = string.Empty;
            foreach (var test in PostCollection)
            {
                str += string.Format("&{0}={1}", test.Key, test.Value);
            }

            str_pre += string.Format("HashKey={0}" + str + "&HashIV={1}", HashKey, HashIV);

            string urlEncodeStrPost = HttpUtility.UrlEncode(str_pre);
            string ToLower = urlEncodeStrPost.ToLower();
            string sCheckMacValue = GetSHA256(ToLower);

            PostCollection.Add("CheckMacValue", sCheckMacValue);
            
            return string.Join("&", PostCollection.Select(p => p.Key + "=" + p.Value));
        }

        //取得ECPAY檢查碼是否正確
        public static bool IsEcpayCheckMacValueCorrect(SortedDictionary<string, string> PostCollection, string CheckMacValue)
        {
            string HashKey = new Repositorys.repCONFIG().fnGET_CFG("ECPAY_HASHKEY")[0].fsVALUE;
            string HashIV = new Repositorys.repCONFIG().fnGET_CFG("ECPAY_HASHIV")[0].fsVALUE;

            //計算檢查碼
            string str = string.Empty;
            string str_pre = string.Empty;
            foreach (var test in PostCollection)
            {
                str += string.Format("&{0}={1}", test.Key, test.Value);
            }

            str_pre += string.Format("HashKey={0}" + str + "&HashIV={1}", HashKey, HashIV);

            string urlEncodeStrPost = HttpUtility.UrlEncode(str_pre);
            string ToLower = urlEncodeStrPost.ToLower();
            string sCheckMacValue = GetSHA256(ToLower);

            if (sCheckMacValue == CheckMacValue)
                return true;
            else
                return false;
        }

        //ECPAY加密
        public static string GetSHA256(string ToLower)
        {
            SHA256 SHA256Hasher = SHA256.Create();
            byte[] data = SHA256Hasher.ComputeHash(Encoding.Default.GetBytes(ToLower));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("X2"));//MD5碼 大小寫
            }

            return sBuilder.ToString();
        }
        
        /// <summary>
        ///  AES 加密
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AesEncrypt(string str)
        {
            string key = new Repositorys.repCONFIG().fnGET_CFG("API_KEY")[0].fsVALUE;
            string iv = new Repositorys.repCONFIG().fnGET_CFG("API_IV")[0].fsVALUE;
            if (string.IsNullOrEmpty(str)) return null;
            Byte[] toEncryptArray = Encoding.UTF8.GetBytes(str);

            RijndaelManaged rm = new RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                IV = Encoding.UTF8.GetBytes(iv),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            ICryptoTransform cTransform = rm.CreateEncryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        ///  AES 解密
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AesDecrypt(string str)
        {
            string key = new Repositorys.repCONFIG().fnGET_CFG("API_KEY")[0].fsVALUE;
            string iv = new Repositorys.repCONFIG().fnGET_CFG("API_IV")[0].fsVALUE;
            if (string.IsNullOrEmpty(str)) return null;
            Byte[] toEncryptArray = Convert.FromBase64String(str);

            RijndaelManaged rm = new RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                IV = Encoding.UTF8.GetBytes(iv),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            ICryptoTransform cTransform = rm.CreateDecryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Encoding.UTF8.GetString(resultArray);
        }


        /// <summary>
        ///  AES 加密(APP)
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AesEncrypt_APP(string str)
        {
            string key = new Repositorys.repCONFIG().fnGET_CFG("API_APP_KEY")[0].fsVALUE;
            string iv = new Repositorys.repCONFIG().fnGET_CFG("API_APP_IV")[0].fsVALUE;
            if (string.IsNullOrEmpty(str)) return null;
            Byte[] toEncryptArray = Encoding.UTF8.GetBytes(str);

            RijndaelManaged rm = new RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                IV = Encoding.UTF8.GetBytes(iv),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            ICryptoTransform cTransform = rm.CreateEncryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        ///  AES 解密(APP)
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AesDecrypt_APP(string str)
        {
            string key = new Repositorys.repCONFIG().fnGET_CFG("API_APP_KEY")[0].fsVALUE;
            string iv = new Repositorys.repCONFIG().fnGET_CFG("API_APP_IV")[0].fsVALUE;
            if (string.IsNullOrEmpty(str)) return null;
            Byte[] toEncryptArray = Convert.FromBase64String(str);

            RijndaelManaged rm = new RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                IV = Encoding.UTF8.GetBytes(iv),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            ICryptoTransform cTransform = rm.CreateDecryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Encoding.UTF8.GetString(resultArray);
        }
    }
}