﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Common
{
    public class clsM3U8
    {
        private static readonly string fsSECURITY_KEY = WebConfigurationManager.AppSettings["fsSECURITY_KEY"];

        private static readonly string fsNEW_MOZAI_URL = WebConfigurationManager.AppSettings["fsNEW_MOZAI_URL"];
        private static readonly string fsNEW_MOZAI_URL_B = WebConfigurationManager.AppSettings["fsNEW_MOZAI_URL_B"];
        private static readonly string fsNEW_CRYP_SRC = WebConfigurationManager.AppSettings["fsNEW_CRYP_SRC"];
        private static readonly string fsNEW_CRYP_SRC_B = WebConfigurationManager.AppSettings["fsNEW_CRYP_SRC_B"];
        

        /// <summary>
        /// 取得CHT-CHANNELURL
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <returns></returns>
        public static string GetChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP)
        {
            string fsFOLDER = string.Empty;

            //付費的一律用4bitrate
            if (fbIS_PAY_MEMBER)
            {
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/litv-pc/";
            }
            else
            {
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/litv-mobile/";
            }

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            // https://4gtv{fsTYPE}{fsDEVICE}-cds.cdn.hinet.net/{fsFOLDER}/index.m3u8?token={fsTOKEN}&expires={fdEXPIRED_TIME}&token1={fsTOKEN1}&expires1={fdEXPIRED_TIME1}

            if (fbIS_PAY_MEMBER)
            {
                fsURL = fsURL.Replace("{fsTYPE}", "");
            }
            else
            {
                fsURL = fsURL.Replace("{fsTYPE}", "free");
            }

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);

            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-NEW-CHANNELURL(新profile)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <returns></returns>
        public static string GetNewChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP)
        {
            string fsFOLDER = string.Empty;

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

            //付費的一律用4bitrate
            if (fbIS_PAY_MEMBER)
            {
                fsURL = fsURL.Replace("{fsTYPE}", "");
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
            }
            else
            {
                fsURL = fsURL.Replace("{fsTYPE}", "free");
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-mid/";
            }

            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            // https://4gtv{fsTYPE}{fsDEVICE}-cds.cdn.hinet.net/{fsFOLDER}/index.m3u8?token={fsTOKEN}&expires={fdEXPIRED_TIME}&token1={fsTOKEN1}&expires1={fdEXPIRED_TIME1}

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);

            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-NEW-CHANNELURL(新profile+設定free profile)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fsFREE_PROFILE">免費的Profile</param>
        /// <returns></returns>
        public static string GetNewChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP, string fsFREE_PROFILE)
        {
            string fsFOLDER = string.Empty;

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

            //付費的一律用4bitrate
            if (fbIS_PAY_MEMBER)
            {
                fsURL = fsURL.Replace("{fsTYPE}", "");
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
            }
            else
            {
                fsURL = fsURL.Replace("{fsTYPE}", "free");
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
            }

            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            // https://4gtv{fsTYPE}{fsDEVICE}-cds.cdn.hinet.net/{fsFOLDER}/index.m3u8?token={fsTOKEN}&expires={fdEXPIRED_TIME}&token1={fsTOKEN1}&expires1={fdEXPIRED_TIME1}

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);

            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-NEW-CHANNELURL(新profile+設定free profile+fnALLOWTIME)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fsFREE_PROFILE">免費的Profile</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <returns></returns>
        public static string GetNewChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP, string fsFREE_PROFILE, int fnALLOWTIME)
        {
            string fsFOLDER = string.Empty;

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

            //付費的一律用4bitrate
            if (fbIS_PAY_MEMBER)
            {
                fsURL = fsURL.Replace("{fsTYPE}", "");
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
            }
            else
            {
                fsURL = fsURL.Replace("{fsTYPE}", "free");
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
            }

            string fdEXPIRE_TIME;
            string fdEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
            else
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            // https://4gtv{fsTYPE}{fsDEVICE}-cds.cdn.hinet.net/{fsFOLDER}/index.m3u8?token={fsTOKEN}&expires={fdEXPIRED_TIME}&token1={fsTOKEN1}&expires1={fdEXPIRED_TIME1}

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);

            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }
        /// <summary>
        /// 取得CHT-NEW-CHANNELURL(新profile+設定free profile+fnALLOWTIME+fsCDN_ROUTE)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fsFREE_PROFILE">免費的Profile</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <param name="fsCDN_ROUTE">路線</param>
        /// <returns></returns>
        public static string GetNewChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP, string fsFREE_PROFILE, int fnALLOWTIME, string fsCDN_ROUTE)
        {
            string fsFOLDER = string.Empty;

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = string.Empty;


            switch (fsCDN_ROUTE)
            {
                case "A":

                    fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;
                case "B":

                    fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8_B")[0].fsVALUE;

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;
                default:

                    fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;

            }
          
            string fdEXPIRE_TIME;
            string fdEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
            else
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            // https://4gtv{fsTYPE}{fsDEVICE}-cds.cdn.hinet.net/{fsFOLDER}/index.m3u8?token={fsTOKEN}&expires={fdEXPIRED_TIME}&token1={fsTOKEN1}&expires1={fdEXPIRED_TIME1}

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);

            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-CHANNELURL-TV用
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <returns></returns>
        public static string GetTvChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP)
        {
            string fsFOLDER = string.Empty;

            //付費的一律用4bitrate
            if (fbIS_PAY_MEMBER)
            {
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/litv-pc/";
            }
            else
            {
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/litv-mobile/";
            }

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

            //過期時間=試看時間*1.5倍
            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);


            // https://4gtv{fsTYPE}{fsDEVICE}-cds.cdn.hinet.net/{fsFOLDER}/index.m3u8?token={fsTOKEN}&expires={fdEXPIRED_TIME}&token1={fsTOKEN1}&expires1={fdEXPIRED_TIME1}

            if (fbIS_PAY_MEMBER)
            {
                fsURL = fsURL.Replace("{fsTYPE}", "");
            }
            else
            {
                fsURL = fsURL.Replace("{fsTYPE}", "free");
            }

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);
            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-NEW-CHANNELURL-TV用(新profile)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <returns></returns>
        public static string GetTvNewChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP)
        {
            string fsFOLDER = string.Empty;

            //付費的一律用4bitrate
            if (fbIS_PAY_MEMBER)
            {
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
            }
            else
            {
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-mid/";
            }

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

            //過期時間=試看時間*1.5倍
            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);


            // https://4gtv{fsTYPE}{fsDEVICE}-cds.cdn.hinet.net/{fsFOLDER}/index.m3u8?token={fsTOKEN}&expires={fdEXPIRED_TIME}&token1={fsTOKEN1}&expires1={fdEXPIRED_TIME1}

            if (fbIS_PAY_MEMBER)
            {
                fsURL = fsURL.Replace("{fsTYPE}", "");
            }
            else
            {
                fsURL = fsURL.Replace("{fsTYPE}", "free");
            }

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);
            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-NEW-CHANNELURL-TV用(新profile+設定free profile)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fsFREE_PROFILE">免費的profile</param>
        /// <returns></returns>
        public static string GetTvNewChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP, string fsFREE_PROFILE)
        {
            string fsFOLDER = string.Empty;

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

            //付費的一律用4bitrate
            if (fbIS_PAY_MEMBER)
            {
                fsURL = fsURL.Replace("{fsTYPE}", "");
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
            }
            else
            {
                fsURL = fsURL.Replace("{fsTYPE}", "free");
                fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
            }

            //過期時間=試看時間*1.5倍
            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);
            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-NEW-CHANNELURL-TV用(新profile+設定free profile)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fsFREE_PROFILE">免費的profile</param>
        /// <param name="fsCDN_ROUTE">路線</param>
        /// <returns></returns>
        public static string GetTvNewChannelUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP, string fsFREE_PROFILE, string fsCDN_ROUTE)
        {
            string fsFOLDER = string.Empty;

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsURL = string.Empty;


            switch (fsCDN_ROUTE)
            {
                case "A":

                    fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;
                case "B":

                    fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8_B")[0].fsVALUE;

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;
                default:

                    fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_CHANNEL_M3U8")[0].fsVALUE;

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;

            }

            //過期時間=試看時間*1.5倍
            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);
            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);
            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRE_TIME);
            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);
            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-VODURL
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <returns></returns>
        public static string GetVodUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP)
        {
            string fsFOLDER = string.Empty;
            //string fsIP = GetUserIP_API();
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;
            //VOD目前一律全開
            if (fbIS_PAY_MEMBER)
            {
                //付費會員
                fsFOLDER = @"/vod/4gtv/4gtv-" + fsCDNFILE_ID + @"/4gtv-hls/";
            }
            else
            {
                fsFOLDER = @"/vod/4gtv/4gtv-" + fsCDNFILE_ID + @"/4gtv-hls/";
            }

            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);
            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);
            string fsURL = GetVodM3U8(fbIS_PAY_MEMBER, fsDEVICE, fsFOLDER, fsTOKEN, fdEXPIRE_TIME, fsTOKEN1, fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-VODURL(+fnALLOWTIME)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <returns></returns>
        public static string GetVodUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsCDNFILE_ID, string fsIP, int fnALLOWTIME)
        {
            string fsFOLDER = string.Empty;
            //string fsIP = GetUserIP_API();
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;
            //VOD目前一律全開
            if (fbIS_PAY_MEMBER)
            {
                //付費會員
                fsFOLDER = @"/vod/4gtv/4gtv-" + fsCDNFILE_ID + @"/4gtv-hls/";
            }
            else
            {
                fsFOLDER = @"/vod/4gtv/4gtv-" + fsCDNFILE_ID + @"/4gtv-hls/";
            }

            string fdEXPIRE_TIME;
            string fdEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
            else
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);
            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);
            string fsURL = GetVodM3U8(fbIS_PAY_MEMBER, fsDEVICE, fsFOLDER, fsTOKEN, fdEXPIRE_TIME, fsTOKEN1, fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得LiTV-VODURL
        /// </summary>
        /// <param name="fcTYPE">方案</param>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsWRITTEN_BY">館別(litv-drama、litv-show)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <returns></returns>
        public static string GetLiTVVodUrl(string fcTYPE, bool fbIS_PAY_MEMBER, string fsDEVICE, string fsWRITTEN_BY, string fsCDNFILE_ID, string fsIP)
        {
            // 4bitrate 
            // https://{HiNET CDN Domain}/vod/content/litv-drama-vod8696-000002M001/litv-hls-cl-pcb4/
            // https://{HiNET CDN Domain}/vod/content01/litv-drama-vod8696-000002M001/litv-hls-cl-pcb4/

            // 3bitrate
            // https://{HiNET CDN Domain}/vod/content/litv-drama-vod8696-000002M001/litv-hls-cl-mobile/
            // https://{HiNET CDN Domain}/vod/content01/litv-drama-vod8696-000002M001/litv-hls-cl-mobile/

            // /vod/content/litv-{assetId}/litv-hls-cl-{platform}/    platfor => tv, mobile, pc
            // https://4gtv[service][platform]-cds.cdn.hinet.net[path]index.m3u8?[authstring]

            //https://4gtvfreemobile-cds.cdn.hinet.net/vod/content/litv-drama-vod8696-000010M001/litv-hls-cl-mobile/litv-drama-vod8696-000010M001-audio=129000-video=399000-1.ts
            //https://4gtvfreemobile-cds.cdn.hinet.net/vod/content/litv-drama-vod8696-000010M001/litv-hls-cl-mobile/index.m3u8?token=0KhgfWZ99kBxi2WGQc2nmA&expires=1548362586
            string fsFOLDER = string.Empty;
            //string fsIP = GetUserIP_API();
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;
            //VOD目前一律全開
            if (fcTYPE == "A")
            {
                //付費會員
                fsFOLDER = @"/vod/content/" + fsWRITTEN_BY + @"-" + fsCDNFILE_ID + @"/litv-hls-cl-pc/";

            }
            else if (fcTYPE == "C")
            {
                //付費會員
                fsFOLDER = @"/vod/content01/" + fsWRITTEN_BY + @"-" + fsCDNFILE_ID + @"/litv-hls-cl-pc/";
            }

            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            string fsURL = string.Empty;

            fsURL = GetVodM3U8(fbIS_PAY_MEMBER, fsDEVICE, fsFOLDER, fsTOKEN, fdEXPIRE_TIME, fsTOKEN1, fdEXPIRE_TIME1);

            return fsURL;
        }


        /// <summary>
        /// 取得LiTV-VODURL(+fnALLOWTIME)
        /// </summary>
        /// <param name="fcTYPE">方案</param>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsWRITTEN_BY">館別(litv-drama、litv-show)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <returns></returns>
        public static string GetLiTVVodUrl(string fcTYPE, bool fbIS_PAY_MEMBER, string fsDEVICE, string fsWRITTEN_BY, string fsCDNFILE_ID, string fsIP, int fnALLOWTIME)
        {
            // 4bitrate 
            // https://{HiNET CDN Domain}/vod/content/litv-drama-vod8696-000002M001/litv-hls-cl-pcb4/
            // https://{HiNET CDN Domain}/vod/content01/litv-drama-vod8696-000002M001/litv-hls-cl-pcb4/

            // 3bitrate
            // https://{HiNET CDN Domain}/vod/content/litv-drama-vod8696-000002M001/litv-hls-cl-mobile/
            // https://{HiNET CDN Domain}/vod/content01/litv-drama-vod8696-000002M001/litv-hls-cl-mobile/

            // /vod/content/litv-{assetId}/litv-hls-cl-{platform}/    platfor => tv, mobile, pc
            // https://4gtv[service][platform]-cds.cdn.hinet.net[path]index.m3u8?[authstring]

            //https://4gtvfreemobile-cds.cdn.hinet.net/vod/content/litv-drama-vod8696-000010M001/litv-hls-cl-mobile/litv-drama-vod8696-000010M001-audio=129000-video=399000-1.ts
            //https://4gtvfreemobile-cds.cdn.hinet.net/vod/content/litv-drama-vod8696-000010M001/litv-hls-cl-mobile/index.m3u8?token=0KhgfWZ99kBxi2WGQc2nmA&expires=1548362586
            string fsFOLDER = string.Empty;
            //string fsIP = GetUserIP_API();
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;
            //VOD目前一律全開
            if (fcTYPE == "A")
            {
                //付費會員
                fsFOLDER = @"/vod/content/" + fsWRITTEN_BY + @"-" + fsCDNFILE_ID + @"/litv-hls-cl-pc/";

            }
            else if (fcTYPE == "C")
            {
                //付費會員
                fsFOLDER = @"/vod/content01/" + fsWRITTEN_BY + @"-" + fsCDNFILE_ID + @"/litv-hls-cl-pc/";
            }

            string fdEXPIRE_TIME;
            string fdEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
            else
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            string fsURL = string.Empty;

            fsURL = GetVodM3U8(fbIS_PAY_MEMBER, fsDEVICE, fsFOLDER, fsTOKEN, fdEXPIRE_TIME, fsTOKEN1, fdEXPIRE_TIME1);

            return fsURL;
        }
        /// <summary>
        /// 取得CHT-VODURL TYPEC方式
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fsIP">Client IP</param>
        /// <returns></returns>
        public static string GetTypeCVodUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, string fsIP)
        {

            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-high.smil/playlist.m3u8
            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-mid.smil/playlist.m3u8
            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-low.smil/playlist.m3u8
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY_TYPEC")[0].fsVALUE;
            string fsFOLDER = string.Empty;
            //VOD目前一律全開
            if (fbIS_PAY_MEMBER)
            {
                //付費會員
                fsFOLDER = @"/vod_4gtv/_definst_/smil:4gtv/" + fnYEAR.ToString() + @"/" + fsCDNFILE_ID + @"/4gtv-hls-high.smil/";
            }
            else
            {
                //非付費會員
                fsFOLDER = @"/vod_4gtv/_definst_/smil:4gtv/" + fnYEAR.ToString() + @"/" + fsCDNFILE_ID + @"/4gtv-hls-mid.smil/";
            }

            string fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            string fsURL = GetTypeCVodM3U8(fbIS_PAY_MEMBER, fsDEVICE, fsTYPE, fsFOLDER, fsTOKEN, fdEXPIRE_TIME, fsTOKEN1, fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得CHT-VODURL TYPEC方式(+fnALLOWTIME)
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <returns></returns>
        public static string GetTypeCVodUrl(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, string fsIP, int fnALLOWTIME)
        {

            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-high.smil/playlist.m3u8
            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-mid.smil/playlist.m3u8
            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-low.smil/playlist.m3u8
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY_TYPEC")[0].fsVALUE;
            string fsFOLDER = string.Empty;
            //VOD目前一律全開
            if (fbIS_PAY_MEMBER)
            {
                //付費會員
                fsFOLDER = @"/vod_4gtv/_definst_/smil:4gtv/" + fnYEAR.ToString() + @"/" + fsCDNFILE_ID + @"/4gtv-hls-high.smil/";
            }
            else
            {
                //非付費會員
                fsFOLDER = @"/vod_4gtv/_definst_/smil:4gtv/" + fnYEAR.ToString() + @"/" + fsCDNFILE_ID + @"/4gtv-hls-mid.smil/";
            }

            string fdEXPIRE_TIME;
            string fdEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
            else
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            string fsURL = GetTypeCVodM3U8(fbIS_PAY_MEMBER, fsDEVICE, fsTYPE, fsFOLDER, fsTOKEN, fdEXPIRE_TIME, fsTOKEN1, fdEXPIRE_TIME1);

            return fsURL;
        }
        
        /// <summary>
        /// 取得CHT-VODURL TYPEC方式(+fnALLOWTIME)(720P)
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN ID</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fsIP">Client IP</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <returns></returns>
        public static string GetTypeCVodUrlFor720P(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, string fsIP, int fnALLOWTIME)
        {

            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-high.smil/playlist.m3u8
            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-mid.smil/playlist.m3u8
            //http://4gtvfreepcvod-cds.cdn.hinet.net/vod_4gtv/_definst_/smil:4gtv/201806040001-12-A-20180712_0010002/4gtv-hls-low.smil/playlist.m3u8
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY_TYPEC")[0].fsVALUE;
            string fsFOLDER = string.Empty;
            //VOD目前一律全開
            if (fbIS_PAY_MEMBER)
            {
                //付費會員
                fsFOLDER = @"/vod_4gtv/_definst_/smil:4gtv/" + fnYEAR.ToString() + @"/" + fsCDNFILE_ID + @"/4gtv-hls-mid.smil/";
            }
            else
            {
                //非付費會員
                fsFOLDER = @"/vod_4gtv/_definst_/smil:4gtv/" + fnYEAR.ToString() + @"/" + fsCDNFILE_ID + @"/4gtv-hls-mid.smil/";
            }

            string fdEXPIRE_TIME;
            string fdEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
            else
            {
                fdEXPIRE_TIME = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }

            string fsTOKEN = GetMD5(fsFOLDER + fdEXPIRE_TIME + fsIP + fsKEY);

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            string fsURL = GetTypeCVodM3U8(fbIS_PAY_MEMBER, fsDEVICE, fsTYPE, fsFOLDER, fsTOKEN, fdEXPIRE_TIME, fsTOKEN1, fdEXPIRE_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 新版Mozai
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <returns></returns>
        public static string GetUrlFromNewMozai(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR)
        {
            string fsURL = fsNEW_MOZAI_URL;
            string fsCRYP = fsNEW_CRYP_SRC;

            Int32 fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate
            if (fsTYPE == "live")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-mid";
                }
            }
            else if (fsTYPE == "vod")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-high";
                }

            }

            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }

        /// <summary>
        /// 新版Mozai(+fnALLOWTIME)
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <returns></returns>
        public static string GetUrlFromNewMozai(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, int fnALLOWTIME)
        {
            string fsURL = fsNEW_MOZAI_URL;
            string fsCRYP = fsNEW_CRYP_SRC;


            Int32 fnEXPIRE_TIME;
            string fdEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
            else
            {
                fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                fdEXPIRE_TIME1 = ((Int32)(DateTime.Now.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }




            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate
            if (fsTYPE == "live")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-mid";
                }
            }
            else if (fsTYPE == "vod")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-high";
                }

            }

            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;

            string fsTOKEN1 = GetMD5(fsFOLDER + fdEXPIRE_TIME1 + fsKEY);

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$TOKEN1}", fsTOKEN1).Replace("{$ExpireTime1}", fdEXPIRE_TIME1).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }

        /// <summary>
        /// 新版Mozai(+fnALLOWTIME)(720P)
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <returns></returns>
        public static string GetUrlFromNewMozaiFor720P(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, int fnALLOWTIME)
        {
            string fsURL = fsNEW_MOZAI_URL;
            string fsCRYP = fsNEW_CRYP_SRC;


            Int32 fnEXPIRE_TIME;
            if (fnALLOWTIME == -1)
            {
                fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }
            else
            {
                fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }

            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate
            if (fsTYPE == "live")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-mid";
                }
            }
            else if (fsTYPE == "vod")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-mid";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-mid";
                }

            }

            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }

        /// <summary>
        /// 新版Mozai(設定free profile)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fsFREE_PROFILE">免費的profile</param>
        /// <returns></returns>
        public static string GetUrlFromNewMozai(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, string fsFREE_PROFILE)
        {
            string fsURL = fsNEW_MOZAI_URL;
            string fsCRYP = fsNEW_CRYP_SRC;

            Int32 fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate
            if (fsTYPE == "live")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                }
            }
            else if (fsTYPE == "vod")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-high";
                }

            }

            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }

        /// <summary>
        /// 新版Mozai(設定free profile + fnALLOWTIME)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fsFREE_PROFILE">免費的profile</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <returns></returns>
        public static string GetUrlFromNewMozai(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, string fsFREE_PROFILE, int fnALLOWTIME)
        {
            string fsURL = fsNEW_MOZAI_URL;
            string fsCRYP = fsNEW_CRYP_SRC;
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;
            Int32 fnEXPIRE_TIME;
            Int32 fnEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                fnEXPIRE_TIME1 = (Int32)(DateTime.UtcNow.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }
            else
            {
                fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                fnEXPIRE_TIME1 = (Int32)(DateTime.UtcNow.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }
            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate
            if (fsTYPE == "live")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                }
            }
            else if (fsTYPE == "vod")
            {
                if (fbIS_PAY_MEMBER)
                {
                    //付費會員
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    //非付費會員
                    fsFREE = "free";
                    fsPROFILE = "4gtv-high";
                }

            }



            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");
            //跟CHT規則一樣
            string fsTOKEN1 = GetMD5(fsFOLDER + fnEXPIRE_TIME1.ToString() + fsKEY);

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$TOKEN1}", fsTOKEN1).Replace("{$ExpireTime1}", fnEXPIRE_TIME1.ToString()).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }
        /// <summary>
        /// 新版Mozai(設定free profile + fnALLOWTIME+ fsCDN_ROUTE))
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fsFREE_PROFILE">免費的profile</param>
        /// <param name="fnALLOWTIME">觀看時間</param>
        /// <param name="fsCDN_ROUTE">路線</param>
        /// <returns></returns>
        public static string GetUrlFromNewMozai(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, string fsFREE_PROFILE, int fnALLOWTIME, string fsCDN_ROUTE)
        {

            string fsURL = string.Empty;
            string fsCRYP = string.Empty;
            string fsFOLDER1 = string.Empty;
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;
            Int32 fnEXPIRE_TIME;
            Int32 fnEXPIRE_TIME1;
            if (fnALLOWTIME == -1)
            {
                fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                fnEXPIRE_TIME1 = (Int32)(DateTime.UtcNow.AddHours(5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }
            else
            {
                fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                fnEXPIRE_TIME1 = (Int32)(DateTime.UtcNow.AddSeconds(fnALLOWTIME + 300).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }

            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate


            switch (fsCDN_ROUTE)
            {
                case "A":

                    fsURL = fsNEW_MOZAI_URL;
                    fsCRYP = fsNEW_CRYP_SRC;

                    if (fsTYPE == "live")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                        }
                    }
                    else if (fsTYPE == "vod")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-high";
                        }
                    }

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;
                case "B":
                    if (fsTYPE == "live")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                        }
                    }
                    else if (fsTYPE == "vod")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-high";
                        }
                    }

                    fsURL = fsNEW_MOZAI_URL_B;
                    fsCRYP = fsNEW_CRYP_SRC_B;
                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;
                default:

                    fsURL = fsNEW_MOZAI_URL;
                    fsCRYP = fsNEW_CRYP_SRC;

                    if (fsTYPE == "live")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                        }
                    }
                    else if (fsTYPE == "vod")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-high";
                        }
                    }

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;

            }


            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            //跟CHT規則一樣
            string fsTOKEN1 = GetMD5(fsFOLDER1 + fnEXPIRE_TIME1.ToString() + fsKEY);

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$TOKEN1}", fsTOKEN1).Replace("{$ExpireTime1}", fnEXPIRE_TIME1.ToString()).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }
      
        /// <summary>
        /// 新版Mozai-TV用
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <returns></returns>
        public static string GetTvUrlFromNewMozai(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR)
        {
            string fsURL = fsNEW_MOZAI_URL;
            string fsCRYP = fsNEW_CRYP_SRC;

            Int32 fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate
            if (fsTYPE == "live")
            {
                if (fbIS_PAY_MEMBER)
                {
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    fsFREE = "free";
                    fsPROFILE = "4gtv-mid";
                }
            }
            else if (fsTYPE == "vod")
            {
                if (fbIS_PAY_MEMBER)
                {
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    fsFREE = "free";
                    fsPROFILE = "4gtv-mid";
                }

            }

            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }

        /// <summary>
        /// 新版Mozai-TV用(設定free profile)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fsFREE_PROFILE">免費的profile</param>
        /// <returns></returns>
        public static string GetTvUrlFromNewMozai(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, string fsFREE_PROFILE)
        {
            string fsURL = fsNEW_MOZAI_URL;
            string fsCRYP = fsNEW_CRYP_SRC;

            Int32 fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate
            if (fsTYPE == "live")
            {
                if (fbIS_PAY_MEMBER)
                {
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    fsFREE = "free";
                    fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                }
            }
            else if (fsTYPE == "vod")
            {
                if (fbIS_PAY_MEMBER)
                {
                    fsPROFILE = "4gtv-high";
                }
                else
                {
                    fsFREE = "free";
                    fsPROFILE = "4gtv-mid";
                }

            }

            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }

        /// <summary>
        /// 新版Mozai-TV用(設定free profile+fsCDN_ROUTE)
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(pc / mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsCDNFILE_ID">CDN編號</param>
        /// <param name="fnYEAR">年份</param>
        /// <param name="fsFREE_PROFILE">免費的profile</param>
        /// <param name="fsCDN_ROUTE">路線</param>
        /// <returns></returns>
        public static string GetTvUrlFromNewMozai(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsCDNFILE_ID, int fnYEAR, string fsFREE_PROFILE ,string fsCDN_ROUTE)
        {
            string fsURL = string.Empty;
            string fsCRYP = string.Empty;
            string fsFOLDER1 = string.Empty;
            string fsKEY = new Repositorys.repCONFIG().fnGET_CFG("VOD_SECURITY_KEY")[0].fsVALUE;


            Int32 fnEXPIRE_TIME = (Int32)(DateTime.UtcNow.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            Int32 fnEXPIRE_TIME1 = (Int32)(DateTime.UtcNow.AddMinutes(15 * 1.5).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string fsREFER = Convert.ToBase64String(Encoding.Default.GetBytes(Guid.NewGuid().ToString())).Replace('+', '-').Replace('/', '_').Replace("=", "");
            string fsFREE = string.Empty;
            string fsPROFILE = string.Empty;

            uint fnUNXITIME = Convert.ToUInt32(fnEXPIRE_TIME);

            //profile = '4gtv-high' => 4bitrate
            //profile = '4gtv-mid' => 3bitrate
            //profile = '4gtv-low' => 2bitrate


            switch (fsCDN_ROUTE)
            {
                case "A":

                    fsURL = fsNEW_MOZAI_URL;
                    fsCRYP = fsNEW_CRYP_SRC;

                    if (fsTYPE == "live")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                        }
                    }
                    else if (fsTYPE == "vod")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-mid";
                        }
                    }

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;
                case "B":
                    if (fsTYPE == "live")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                        }
                    }
                    else if (fsTYPE == "vod")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-mid";
                        }
                    }

                    fsURL = fsNEW_MOZAI_URL_B;
                    fsCRYP = fsNEW_CRYP_SRC_B;
                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;
                default:

                    fsURL = fsNEW_MOZAI_URL;
                    fsCRYP = fsNEW_CRYP_SRC;

                    if (fsTYPE == "live")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-" + fsFREE_PROFILE;
                        }
                    }
                    else if (fsTYPE == "vod")
                    {
                        if (fbIS_PAY_MEMBER)
                        {
                            //付費會員
                            fsPROFILE = "4gtv-high";
                        }
                        else
                        {
                            //非付費會員
                            fsFREE = "free";
                            fsPROFILE = "4gtv-mid";
                        }
                    }

                    //付費的一律用4bitrate
                    if (fbIS_PAY_MEMBER)
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-high/";
                    }
                    else
                    {
                        fsURL = fsURL.Replace("{fsTYPE}", "free");
                        fsFOLDER1 = @"/live/pool/" + fsCDNFILE_ID + @"/4gtv-live-" + fsFREE_PROFILE + "/";
                    }

                    break;

            }

            

            string fsFOLDER = ("{$TYPE}/{$PROFILE}/{$ASSET_ID}").Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            fsCRYP = fsCRYP.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}", fsTYPE).Replace("{$PROFILE}", fsPROFILE).Replace("{$ASSET_ID}", fsCDNFILE_ID);

            string fsTOKEN = Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(fsCRYP + fnEXPIRE_TIME.ToString() + fsSECURITY_KEY + fsREFER))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            //跟CHT規則一樣
            string fsTOKEN1 = GetMD5(fsFOLDER1 + fnEXPIRE_TIME1.ToString() + fsKEY);

            fsURL = fsURL.Replace("{$FREE}", fsFREE).Replace("{$DEVICE}", fsDEVICE).Replace("{$TYPE}/{$PROFILE}/{$ASSET_ID}", System.Web.HttpUtility.UrlEncode(clsSECURITY.MozaiEncrypt(fsFOLDER, fnUNXITIME).Replace('+', '-').Replace('/', '_'))).Replace("{$TOKEN}", fsTOKEN).Replace("{$ExpireTime}", fnEXPIRE_TIME.ToString()).Replace("{$TOKEN1}", fsTOKEN1).Replace("{$ExpireTime1}", fnEXPIRE_TIME1.ToString()).Replace("{$REFER}", fsREFER).Replace("{$YEAR}", fnYEAR.ToString());

            return fsURL;
        }

        /// <summary>
        /// 取得 MD5 編碼驗證
        /// </summary>
        /// <param name="Info">資訊</param>
        private static string GetMD5(string Info)
        {
            MD5 md5 = MD5.Create();

            string Token = Convert.ToBase64String(md5.ComputeHash(Encoding.Default.GetBytes(Info))).Replace('+', '-').Replace('/', '_').Replace("=", "");

            return Token;
        }

        /// <summary>
        /// 取得 VOD M3U8 網址
        /// </summary>
        /// <param name="fbFREE">是否免費</param>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsFOLDER">路徑</param>
        /// <param name="fsTOKEN">驗證碼</param>
        /// <param name="fdEXPIRED_TIME">有效時間</param>
        /// <param name="fsTOKEN1">驗證碼1</param>
        /// <param name="fdEXPIRED_TIME1">有效時間1</param>
        private static string GetVodM3U8(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsFOLDER, string fsTOKEN, string fdEXPIRED_TIME, string fsTOKEN1, string fdEXPIRED_TIME1)
        {
            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_VOD_M3U8")[0].fsVALUE;

            if (fbIS_PAY_MEMBER)
            {
                //付費會員
                fsURL = fsURL.Replace("{fsTYPE}", "");
            }
            else
            {
                //非付費會員
                fsURL = fsURL.Replace("{fsTYPE}", "free");
            }

            fsURL = fsURL.Replace("{fsDEVICE}", fsDEVICE);

            fsURL = fsURL.Replace("{fsFOLDER}", fsFOLDER);

            fsURL = fsURL.Replace("{fsTOKEN}", fsTOKEN);

            fsURL = fsURL.Replace("{fdEXPIRED_TIME}", fdEXPIRED_TIME);

            fsURL = fsURL.Replace("{fsTOKEN1}", fsTOKEN1);

            fsURL = fsURL.Replace("{fdEXPIRED_TIME1}", fdEXPIRED_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得 VOD M3U8 網址 TypeC方式
        /// </summary>
        /// <param name="fbIS_PAY_MEMBER">是否為付費會員</param>
        /// <param name="fsDEVICE">裝置(tv、pc、pad、mobile)</param>
        /// <param name="fsTYPE">類型(vod / live)</param>
        /// <param name="fsFOLDER">路徑</param>
        /// <param name="fsTOKEN">驗證碼</param>
        /// <param name="fdEXPIRED_TIME">有效時間</param>
        /// <param name="fsTOKEN1">驗證碼1</param>
        /// <param name="fdEXPIRED_TIME1">有效時間1</param>
        private static string GetTypeCVodM3U8(bool fbIS_PAY_MEMBER, string fsDEVICE, string fsTYPE, string fsFOLDER, string fsTOKEN, string fdEXPIRED_TIME, string fsTOKEN1, string fdEXPIRED_TIME1)
        {
            //https://4gtv{$fsFREE}{$fsDEVICE}{$fsTYPE}-cds.cdn.hinet.net{$fsFOLDER}playlist.m3u8?token={$fsTOKEN}&expires={$fdEXPIRED_TIME}
            string fsURL = new Repositorys.repCONFIG().fnGET_CFG("4GTV_VOD_M3U8_TYPEC")[0].fsVALUE;
            //string fsDEVICE_FOLDER = string.Empty;

            if (fbIS_PAY_MEMBER)
                fsURL = fsURL.Replace("{$fsFREE}", "free");
            else
                fsURL = fsURL.Replace("{$fsFREE}", "");

            fsURL = fsURL.Replace("{$fsDEVICE}", fsDEVICE);

            fsURL = fsURL.Replace("{$fsTYPE}", fsTYPE);

            fsURL = fsURL.Replace("{$fsFOLDER}", fsFOLDER);

            //fsURL = fsURL.Replace("{$fsDEVICE_FOLDER}", fsDEVICE_FOLDER);

            fsURL = fsURL.Replace("{$fsTOKEN}", fsTOKEN);

            fsURL = fsURL.Replace("{$fdEXPIRED_TIME}", fdEXPIRED_TIME);

            fsURL = fsURL.Replace("{$fsTOKEN1}", fsTOKEN1);

            fsURL = fsURL.Replace("{$fdEXPIRED_TIME1}", fdEXPIRED_TIME1);

            return fsURL;
        }

        /// <summary>
        /// 取得 User IP(外部)
        /// </summary>
        public static string GetUserIP_API()
        {
            HttpWebRequest request = HttpWebRequest.Create("http://api.ipify.org/?format=json") as HttpWebRequest;
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.UserAgent = "Mozilla/5.0";

            string _IP = String.Empty;
            string _JSON = String.Empty;

            WebResponse response = request.GetResponse();

            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                _JSON = reader.ReadToEnd();
            }

            JObject obj = JObject.Parse(_JSON);

            _IP = obj["ip"].ToString();

            return _IP;
        }
    }
}