﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace FourgTV_OTT2.Api2.Common
{
    public class clsFILTER
    {
    }

    //判斷是否為限制的瀏覽器
    public class IsRestrictUserAgentAttribute : ActionFilterAttribute
    {
        private static readonly string lstRESTRICT_USERAGENT = WebConfigurationManager.AppSettings["lstRESTRICT_USERAGENT"];

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //base.OnActionExecuting(actionContext);
            string[] lstUSER_AGENT = lstRESTRICT_USERAGENT.Split(';');

            if (lstUSER_AGENT != null && lstUSER_AGENT.Count() > 0)
            {
                foreach (var item in lstUSER_AGENT)
                {
                    if (actionContext.Request.Headers.UserAgent.ToString().ToLower().IndexOf(item) > -1)
                    {
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
                    }
                }
            }
        }
    }
}