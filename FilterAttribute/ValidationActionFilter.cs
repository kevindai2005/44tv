﻿using FourgTV_OTT2.Api2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace FourgTV_OTT2.Api2.FilterAttribute
{
    public class ValidationActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {            
            var modelState = actionContext.ModelState;
            if (!modelState.IsValid)
            {
                string ErrorMessage = string.Empty;
                foreach (var model in modelState)
                {
                    ErrorMessage += model.Value.Errors[0].ErrorMessage + " ";
                }

                var res = new clsRETURN() { Success = false, ErrMessage = ErrorMessage };

                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, res);


            }
        }
    }
}