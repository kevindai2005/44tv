﻿using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.FilterAttribute
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var httpStatus = HttpStatusCode.Accepted;
        
            var std = new StdResp(ResponseCodeEnum.InternalError);

            if (typeof(MyException).IsAssignableFrom(actionExecutedContext.Exception.GetType()))
            {
                var customException = (MyException)actionExecutedContext.Exception;
                httpStatus = HttpStatusCode.OK;
                std.Status = customException.ResponseCode;
            }
            else
            {
               //LOG
            }
            // std.ErrMessage = string.Format("Message:{0}", std.ErrMessage)+ Environment.NewLine + string.Format("StackTrace:{0}",actionExecutedContext.Exception.StackTrace);
            std.ErrMessage = std.ErrMessage;
            actionExecutedContext.Response = new HttpResponseMessage
            {
                Content = new StringContent(SerializeToJSON(std), Encoding.GetEncoding("UTF-8"), "application/json"),
                StatusCode = httpStatus
            };
            OutputResponse(actionExecutedContext, std, httpStatus);
        }

        public static string OutputResponse<TResp>(HttpActionExecutedContext actionContext,
                                               TResp resp,
                                               HttpStatusCode status)
        {
            var str = "";
            actionContext.Response = GetHttpRespMsg(resp, out str, status);
            LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Response, str);
            return str;
        }
        public static HttpResponseMessage GetHttpRespMsg(string resp,
                                                      HttpStatusCode status = HttpStatusCode.OK,
                                                      string contentType = "application/json")
        {
            HttpResponseMessage result = new HttpResponseMessage
            {
                Content = new StringContent(resp, Encoding.GetEncoding("UTF-8"), contentType),
                StatusCode = status
            };
            return result;
        }
        public static HttpResponseMessage GetHttpRespMsg<TResp>(TResp resp,
                                                               out string outResp,
                                                               HttpStatusCode status = HttpStatusCode.OK)
        {
            var str = SerializeToJSON(resp);
            var result = GetHttpRespMsg(str, status);
            outResp = str;
            return result;
        }

        public static string SerializeToJSON(object value, bool shouldCamelCase = true, params JsonConverter[] converters)
        {
            if (value == null)
            {
                return null;
            }
            var str = "";
            if (shouldCamelCase)
            {
                var jsonSetting = new JsonSerializerSettings();
                jsonSetting.ContractResolver = new CamelCasePropertyNamesContractResolver();
                jsonSetting.Converters = converters;
                str = JsonConvert.SerializeObject(value);
            }
            else
            {
                str = JsonConvert.SerializeObject(value, converters);
            }
            return str;
        }

    }
}