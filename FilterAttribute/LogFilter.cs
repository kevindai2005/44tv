﻿using FourgTV_OTT2.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace FourgTV_OTT2.Api2.FilterAttribute
{
    public class LogFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            try
            {
                var response = string.Empty;
                if (actionExecutedContext.Response != null && actionExecutedContext.Response.Content != null)
                {
                    response = actionExecutedContext.Response.Content.ReadAsStringAsync().Result;
                }
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Response, response);

            }
            catch (Exception)
            {
#if DEBUG
                throw;
#endif 
            }
        }
    }
}