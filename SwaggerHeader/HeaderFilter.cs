﻿using Swashbuckle.Swagger;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Description;

namespace FourgTV_OTT2.Api2.SwaggerHeader
{
    public class HeaderFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (!apiDescription.ActionDescriptor.GetCustomAttributes<HeaderAttribute>().Any())
            {
                return;
            }

            if (operation.parameters == null)
            {
                operation.parameters = new List<Parameter>();
            }

            HeaderAttribute attr = apiDescription.ActionDescriptor.GetCustomAttributes<HeaderAttribute>().FirstOrDefault();
            // 取出Headers，並進行切割
            List<string> strHeaders = attr.headersConfig.Split("|".ToCharArray()).ToList();
            for (int i = 0; i < strHeaders.Count; i++)
            {
                // 切割參數
                List<string> strHeaderParam = strHeaders[i].Split(",".ToCharArray()).ToList();

                // 將設定的欄位及屬性加入到Header的輸入欄位之中
                Parameter objParam = new Parameter()
                {
                    name = strHeaderParam[0],
                    @in = "header",
                    type = strHeaderParam[1],
                    required = false,
                };

                if (strHeaderParam.Count > 2)
                    objParam.@enum = strHeaderParam[2].Split("/".ToCharArray()).ToList<object>();

                operation.parameters.Add(objParam);
            }
        }
    }
}