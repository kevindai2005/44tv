﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.SwaggerHeader
{
    public class HeaderAttribute : Attribute
    {
        public string headersConfig { get; set; }
    }
}