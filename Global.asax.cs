﻿using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace FourgTV_OTT2.Api2
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        private static readonly string fsLOG_PATH = WebConfigurationManager.AppSettings["fsLOG_PATH"];

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //This id will be used for tracing the request from gateway to following services
            LogHelper.RequestVariables.BeginInitVariables();
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {

            Account_BLL _Account_BLL = new Account_BLL();

            try
            {
                

                if (Request.Path.ToLower().IndexOf("/heran/") >= 0)
                {
                    var request = LogHelper.RequestVariables[LogRequestVariableType.Request] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.Request].ToString();
                    var RequestBody = LogHelper.RequestVariables[LogRequestVariableType.RequestBody] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.RequestBody].ToString();
                    var response = LogHelper.RequestVariables[LogRequestVariableType.Response] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.Response].ToString();
                    var MachineID = LogHelper.RequestVariables[LogRequestVariableType.MachineID] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.MachineID].ToString();
                    var ApiName = LogHelper.RequestVariables[LogRequestVariableType.ApiName] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.ApiName].ToString();
                    var Activity = LogHelper.RequestVariables[LogRequestVariableType.Activity] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.Activity].ToString();
                    var ClientIP = LogHelper.RequestVariables[LogRequestVariableType.ClientIP] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.ClientIP].ToString();
                    var UserAgent = LogHelper.RequestVariables[LogRequestVariableType.UserAgent] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.UserAgent].ToString();

                    Heran_BLL.InsertHeranLog(Activity, MachineID, RequestBody, response, ClientIP, UserAgent, ApiName);

                }

                if (Request.Path.ToLower().IndexOf("/tv/membermachinebinding") >= 0)
                {
                    var request = LogHelper.RequestVariables[LogRequestVariableType.Request] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.Request].ToString();
                    var RequestBody = LogHelper.RequestVariables[LogRequestVariableType.RequestBody] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.RequestBody].ToString();
                    var response = LogHelper.RequestVariables[LogRequestVariableType.Response] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.Response].ToString();
                    var MachineID = LogHelper.RequestVariables[LogRequestVariableType.MachineID] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.MachineID].ToString();
                    var ApiName = LogHelper.RequestVariables[LogRequestVariableType.ApiName] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.ApiName].ToString();
                    var Activity = LogHelper.RequestVariables[LogRequestVariableType.Activity] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.Activity].ToString();
                    var ClientIP = LogHelper.RequestVariables[LogRequestVariableType.ClientIP] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.ClientIP].ToString();
                    var UserAgent = LogHelper.RequestVariables[LogRequestVariableType.UserAgent] == null ? string.Empty : LogHelper.RequestVariables[LogRequestVariableType.UserAgent].ToString();


                    if (!string.IsNullOrEmpty(MachineID))
                    {
                        Heran_BLL.InsertHeranLog(Activity, MachineID, RequestBody, response, ClientIP, UserAgent, ApiName);
                    }


                }
                if (Request.Path.ToLower().IndexOf("/appaccount/") >= 0 || Request.Path.ToLower().IndexOf("/account/") >= 0)
                {
                    _Account_BLL.InsertMemberLog();
                }

                if (Request.Path.ToLower().IndexOf("/tv/getchannelurl") >= 0)
                {
                    _Account_BLL.InsertLog();
                }
                if (Request.Path.ToLower().IndexOf("/wsapc/") >= 0)
                {
                    _Account_BLL.InsertLog();
                }

                if (Request.Path.ToLower().IndexOf("usecouponcode") >= 0)
                {
                    _Account_BLL.InsertLog();
                }

                if (Request.Path.ToLower().IndexOf("/fb/datadeletioncallback") >= 0)
                {
                    _Account_BLL.InsertLog();
                }
            }
            catch (Exception ex)
            {
                if (!Directory.Exists(fsLOG_PATH))
                    Directory.CreateDirectory(fsLOG_PATH);

                System.IO.File.AppendAllText(fsLOG_PATH + DateTime.Now.ToString("yyyyMMdd") + ".txt", "發生時間:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "，錯誤訊息:" + ex + "=>" + ex.Message + "\r\n");
            }

        }
    }
}
