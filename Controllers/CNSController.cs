﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Http;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Req.CNS;
using System.Transactions;
using FourgTV_OTT2.Api2.SwaggerHeader;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("CNS")]
    public class CNSController : ApiController
    {
        CNS_BLL _CNS_BLL = new CNS_BLL();

        [Route("InsertOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostInsertOrder(InsertOrderReq req)
        {
            try
            {
                CNS_BLL.CheckHeader(Request);

                CNS_BLL.CheckIpInWhiteList(Request);

                CNS_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    _CNS_BLL.InsertOrder(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        [Route("CancelOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostCancelOrder(CancelOrderReq req)
        {
            try
            {
                CNS_BLL.CheckHeader(Request);

                CNS_BLL.CheckIpInWhiteList(Request);

                CNS_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    CNS_BLL.CancelOrder(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }
    }
}