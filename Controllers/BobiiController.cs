﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.BLL.Model.Req.Bobii;
using FourgTV_OTT2.BLL.Model.Res.Bobii;
using System.Web.Configuration;
using System.Transactions;
using FourgTV_OTT2.Api2.SwaggerHeader;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Bobii")]
    public class BobiiController : ApiController
    {
        private static readonly string fsENC_KEY = WebConfigurationManager.AppSettings["fsENC_KEY"];
        Bobii_BLL _Bobii_BLL = new Bobii_BLL();

        /// <summary>
        /// A01.新增會員
        /// </summary>
        /// <param name="req">新增會員</param>
        /// <response code="EA02">會員帳號已存在。</response>
        /// <returns></returns>
        [Route("SignUp")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public BobiiResp PostSignUp(BobiiReq req)
        {
            try
            {
                //return new BobiiResp(ResponseCodeEnum.BobiiServerSideError, Bobii_BLL.AesEncrypt(Newtonsoft.Json.JsonConvert.SerializeObject(new AccountAuthReq { fsDEVICE="test002", fsUSER_ID="bobiitestuser002", fsPASSWORD="88888888" })));
                Bobii_BLL.CheckHeader(Request);
                //Bobii_BLL.CheckIpInWhiteList(Request); // 完成前先不鎖IP
                AccountAuthReq signUpReq = Bobii_BLL.DeserializeReq<AccountAuthReq>(req.value);

                //檢查傳入參數
                Bobii_BLL.CheckParam(signUpReq);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    Bobii_BLL.InsertMember(signUpReq);

                    _TransactionScope.Complete();

                    return new BobiiResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// A02.帳號登入
        /// </summary>
        /// <param name="clsBOBII_LOGIN__REQ">登入資訊</param>
        /// <returns></returns>
        //[Route("SignIn")]
        //[Header(headersConfig = "From,string")]
        //[HttpPost]
        //public BobiiResp PostSignIn(BobiiReq bobiiReq)
        //{
        //    try
        //    {
        //        //return new BobiiResp(ResponseCodeEnum.BobiiServerSideError, Bobii_BLL.AesEncrypt(Newtonsoft.Json.JsonConvert.SerializeObject(new AccountAuthReq { fsDEVICE="test0", fsUSER_ID="0917330666", fsPASSWORD="0917330666" })));
        //        Bobii_BLL.CheckHeader(Request);
        //        //Bobii_BLL.CheckIpInWhiteList(Request); // 完成前先不鎖IP
        //        AccountAuthReq loginReq = Bobii_BLL.DeserializeReq<AccountAuthReq>(bobiiReq.value);



        //        BobiiLoginData bobiiLoginData = new repBOBII().fnBOBII_SIGNIN(loginReq.fsUSER_ID, loginReq.fsPASSWORD);

        //        if (string.IsNullOrEmpty(bobiiLoginData.fsACCOUNT_ID))
        //        {
        //            //登入失敗
        //            throw new MyException(ResponseCodeEnum.BobiiMemberOrPasswordNotMatch);
        //            //return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "登入失敗，請確認帳密是否正確。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //        }

        //        if (string.IsNullOrEmpty(loginReq.fsDEVICE) || loginReq.fsDEVICE != bobiiLoginData.fsDEVICE)
        //        {
        //            throw new MyException(ResponseCodeEnum.BobiiMemberAndDeviceNotMatch);
        //            //return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "登入失敗，會員帳號與綁定的裝置不匹配。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //        }

        //        //取得Token
        //        string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
        //        //取得有效的TimeSpan日期
        //        long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddYears(100).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
        //        //加密
        //        string fsVALIDATE_CODE = clsSECURITY.Encrypt(bobiiLoginData.fsACCOUNT_ID + fsTOKEN + bobiiLoginData.fsUSER_ID + fdEXPIRE_TIME.ToString(), loginReq.fsDEVICE);
        //        //cookie加密
        //        string fsVALUE = clsSECURITY.CookieEncrypt(bobiiLoginData.fsACCOUNT_ID + "-" + fsTOKEN + "-" + bobiiLoginData.fsUSER_ID + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE, loginReq.fsDEVICE);

        //        //寫LOG
        //        new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
        //        {
        //            fsFROM = "3",
        //            fsIP = HttpContext.Current.Request.UserHostAddress,
        //            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
        //            fsUSER_ID = loginReq.fsUSER_ID,
        //            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
        //        }); // 錯誤紀錄方式沒更新成 MyException

        //        //clsBOBII_RESP clsBOBII_RESP = new clsBOBII_RESP();
        //        //clsBOBII_RESP.Data = clsSECURITY.BOBII.AesEncrypt(fsVALUE);
        //        return Bobii_BLL.EncryptResp(fsVALUE);
        //        //return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsVALUE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //    }
        //    catch (MyException ex)
        //    {
        //        throw ex;
        //        //return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //    }
        //}

        /// <summary>
        /// C01.新增訂單
        /// </summary>
        /// <param name="req">新增訂單</param>
        /// <response code="EA02">會員帳號無效。</response>
        /// <returns></returns>
        [Route("InsertOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public BobiiResp PostInsertOrder(BobiiReq req)
        {
            try
            {
                //return new BobiiResp(ResponseCodeEnum.BobiiServerSideError, Bobii_BLL.AesEncrypt(Newtonsoft.Json.JsonConvert.SerializeObject(new InsertOrderReq { fsUSER_ID="bobiitestuser002", fsPURCHASE_ID= "CHANNELBOBII_FULL", fsPACKAGE_ID= "CHBO02" })));
                Bobii_BLL.CheckHeader(Request);
                //Bobii_BLL.CheckIpInWhiteList(Request); // 完成前先不鎖IP
                InsertOrderReq insertOrderReq = Bobii_BLL.DeserializeReq<InsertOrderReq>(req.value);

                //檢查傳入參數
                Bobii_BLL.CheckParam(insertOrderReq);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    var orderInfo = _Bobii_BLL.InsertOrder(insertOrderReq);

                    _TransactionScope.Complete();

                    return Bobii_BLL.EncryptResp(orderInfo);
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// C02.取消訂單
        /// </summary>
        /// <param name="req">取消訂單</param>
        /// <response code="EA02">會員帳號無效。</response>
        /// <returns></returns>
        [Route("CancelOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public BobiiResp PostCancelOrder(BobiiReq req)
        {
            try
            {
                //return new BobiiResp(ResponseCodeEnum.BobiiServerSideError, Bobii_BLL.AesEncrypt(Newtonsoft.Json.JsonConvert.SerializeObject(new InsertOrderReq { fsUSER_ID="bobiitestuser002", fsPURCHASE_ID= "CHANNELBOBII_FULL", fsPACKAGE_ID= "CHBO02" })));
                Bobii_BLL.CheckHeader(Request);
                //Bobii_BLL.CheckIpInWhiteList(Request); // 完成前先不鎖IP
                CancelOrderReq cancelOrderReq = Bobii_BLL.DeserializeReq<CancelOrderReq>(req.value);

                //檢查傳入參數
                Bobii_BLL.CheckParam(cancelOrderReq);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    Bobii_BLL.CancelOrder(cancelOrderReq);

                    _TransactionScope.Complete();

                    return new BobiiResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// C04.訂單查詢
        /// </summary>
        /// <param name="req">訂單查詢</param>
        /// <returns></returns>
        [Route("GetOrderInfo")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public BobiiResp PostGetOrderInfo(BobiiReq req)
        {
            try
            {
                //return new BobiiResp(ResponseCodeEnum.BobiiServerSideError, Bobii_BLL.AesEncrypt(Newtonsoft.Json.JsonConvert.SerializeObject(new UserIdQueryReq { fsUSER_ID= "bobiitestuser002" })));
                Bobii_BLL.CheckHeader(Request);
                //Bobii_BLL.CheckIpInWhiteList(Request); // 完成前先不鎖IP
                UserIdQueryReq userIdQueryReq = Bobii_BLL.DeserializeReq<UserIdQueryReq>(req.value);

                //檢查傳入參數
                Bobii_BLL.CheckParam(userIdQueryReq);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    var lstOrderInfo = Bobii_BLL.lstGetOrderInfo(userIdQueryReq);

                    _TransactionScope.Complete();

                    return Bobii_BLL.EncryptResp(lstOrderInfo);
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// D02.取得頻道URL
        /// </summary>
        /// <param name="clsBOBII_GET_CHANNEL_URL_REQ">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        //[Route("GetChannelUrl")]
        //[Header(headersConfig = "From,string")]
        //[HttpPost]
        //public BobiiResp PostChannelUrl(BobiiReq bobiiReq)
        //{
        //    try
        //    {
        //        //return new clsBOBII_RESP(ResponseCodeEnum.BobiiServerSideError, clsSECURITY.AesEncrypt_BOBII(Newtonsoft.Json.JsonConvert.SerializeObject(new clsBOBII_GET_CHANNEL_URL_ARGS() { fnCHANNEL_ID=3, fsASSET_ID="4gtv-4gtv002", fsTOKEN= "1uUTWusE3caWzlQ/2Ha7D5DTfc2wa6zFqeqrAWRnEKQy15a78zKn4Ibft7WUkI2mJgPwkxrrEMerEvtjhGaljQZo4gwKQT5HPzkpXdkCfccGd5ZHIL+Cfp1+zWjymjBCyRTyVD9DgiYZImSSYOHz37794uniFRodjQ20baQA9mi5M9GAc5NjcRhoCPWuHzRJZqS5d1+YwS4hx0VV17Fqc6bYHOBL0QF2phYK6BnxlH/bRRoj7i/+E7X44Gjhjiw0IQI2rg0uMWlW0hg3y6wx25a0jactKGFAnmAug2w3wqu+dCysVPIfae2aDlit9Bmig0wt4yyALvD2yMUkzCAYfw==", fsDEVICE="test0" })));
        //        Bobii_BLL.CheckHeader(Request);
        //        GetChannelUrlReq getChannelUrlReq = Bobii_BLL.DeserializeReq<GetChannelUrlReq>(bobiiReq.value);

        //        string fsDEVICE_TYPE = "tv";
        //        string fsIP = clsTOOL.GetUserIP(Request);

        //        clsUSER_DATA clsUSER_DATA = null;
        //        //判斷是否已登入
        //        if (clsSECURITY.IsLogin(getChannelUrlReq.fsTOKEN, getChannelUrlReq.fsDEVICE))
        //        {
        //            clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(getChannelUrlReq.fsTOKEN, getChannelUrlReq.fsDEVICE);
        //        }
        //        //寫LOG
        //        new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
        //        {
        //            fsFROM = "3",
        //            fsNAME = "CHANNEL",
        //            fsASSET_ID = getChannelUrlReq.fsASSET_ID,
        //            fsIP = fsIP,
        //            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
        //            fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
        //            fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
        //        });
        //        ////測試環境才用
        //        //if (int.Parse(fcIS_PRODUCTION) == 0)
        //        //{
        //        //    if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
        //        //        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
        //        //}
        //        //else
        //        //{
        //        //    if (fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
        //        //        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
        //        //}

        //        //取得頻道資訊
        //        clsCHANNEL clsCHANNEL = new repCHANNEL().fnGET_CHANNEL(getChannelUrlReq.fsASSET_ID);
        //        GetChannelUrlResp getChannelUrlResp = new GetChannelUrlResp();

        //        if (clsCHANNEL == null || String.IsNullOrEmpty(clsCHANNEL.fs4GTV_ID))
        //        {
        //            throw new MyException(ResponseCodeEnum.BobiiChannelNotExist);
        //        }

        //        getChannelUrlResp.fsCHANNEL_NAME = clsCHANNEL.fsNAME;

        //        //查看是否為會員
        //        if (clsUSER_DATA != null)
        //        {
        //            ////取得黑名單
        //            //List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPUSER.txt", System.Text.Encoding.UTF8).ToList();

        //            //if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
        //            //{
        //            //    if (lstSTOP_USERs.FirstOrDefault(f => f == clsUSER_DATA.fsUSER) != null)
        //            //    {
        //            //        //無權限可以播
        //            //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //            //    }
        //            //}
        //            //取得會員頻道權限
        //            clsGET_MEMBER_CHANNEL_AUTH clsMEMBER_CHANNEL_AUTH = new repCHANNEL().fnGET_MEMBER_CHANNEL_AUTH(clsUSER_DATA.fsACCOUNT_ID, getChannelUrlReq.fsASSET_ID);
        //            getChannelUrlResp.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, fsDEVICE_TYPE, getChannelUrlReq.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE,-1));
        //            getChannelUrlResp.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, fsDEVICE_TYPE, "live", getChannelUrlReq.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE,-1));

        //            //是否有觀看權限
        //            bool fbAUTH = clsMEMBER_CHANNEL_AUTH.fcAUTH;
        //            //是否為付費會員
        //            bool fbIS_PAY_MEMBER = clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER;

        //            //有觀看權限跟付費會員
        //            if (!(fbAUTH && fbIS_PAY_MEMBER))
        //            {
        //                //無權限可以播
        //                throw new MyException(ResponseCodeEnum.BobiiNoAuthToWatch);
        //                //return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //            }
        //        }
        //        else
        //        {
        //            //無權限可以播
        //            throw new MyException(ResponseCodeEnum.BobiiNoAuthToWatch);
        //            //return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //        }

        //        //string fsVALUE = Newtonsoft.Json.JsonConvert.SerializeObject(clsBOBII_GET_CHANNEL_URL);

        //        //clsBOBII_RESP clsBOBII_RESP = new clsBOBII_RESP();
        //        //clsBOBII_RESP.Data = clsSECURITY.BOBII.AesEncrypt(fsVALUE);
        //        return Bobii_BLL.EncryptResp(getChannelUrlResp);
        //    }
        //    catch (MyException ex)
        //    {
        //        throw ex;
        //    }
        //}

        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}