﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;
using System.Net.Http.Headers;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Web.Configuration;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.BLL.Model.Req.AppAccount;
using static FourgTV_OTT2.Common.Setting.EnumSetting;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.Model.Model;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("AppAccount")]
    public class AppAccountController : ApiController
    {
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];

        AppAccount_BLL _AppAccount_BLL = new AppAccount_BLL();

        /// <summary>
        /// 帳號登入
        /// </summary>
        /// <param name="clsAPP_LOGIN_ARGS">登入資訊</param>
        /// <returns></returns>
        [Route("SignIn")]
        public HttpResponseMessage PostSignin(clsAPP_LOGIN_ARGS clsAPP_LOGIN_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷密碼是否要重設，如果要就去litv驗證
                    bool fbRESET = false;

                    if (!string.IsNullOrEmpty(clsAPP_LOGIN_ARGS.fsPASSWORD))
                    {
                        fbRESET = new repACCOUNT().fnGET_MEMBER_PASSWORD_NEED_RESET(clsAPP_LOGIN_ARGS.fsUSER);
                        if (fbRESET)
                        {
                            var clsLITV_LOGIN = new repACCOUNT().fnLI_LOGIN(clsAPP_LOGIN_ARGS.fsUSER, clsAPP_LOGIN_ARGS.fsPASSWORD);
                            if (clsLITV_LOGIN.result == null)
                            {
                                //登入失敗
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "登入失敗，請確認帳密是否正確!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                    }

                    clsLOGIN clsLOGIN = new repACCOUNT().fnSIGNIN(clsAPP_LOGIN_ARGS.fsUSER, clsAPP_LOGIN_ARGS.fsPASSWORD, fbRESET);
                    if (!string.IsNullOrEmpty(clsLOGIN.fsACCOUNT_ID))
                    {
                        //登入成功
                        string fsENC_KEY = clsAPP_LOGIN_ARGS.fsENC_KEY;
                        //取得Token
                        string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
                        //取得有效的TimeSpan日期
                        long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddYears(100).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                        //加密
                        string fsVALIDATE_CODE = clsSECURITY.Encrypt(clsLOGIN.fsACCOUNT_ID + fsTOKEN + clsLOGIN.fsUSER + fdEXPIRE_TIME.ToString(), fsENC_KEY);
                        //cookie加密
                        string fsVALUE = clsSECURITY.CookieEncrypt(clsLOGIN.fsACCOUNT_ID + "-" + fsTOKEN + "-" + clsLOGIN.fsUSER + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE, fsENC_KEY);

                        //寫LOG
                        new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
                        {
                            fsFROM = "2",
                            fsIP = HttpContext.Current.Request.UserHostAddress,
                            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                            fsUSER_ID = clsAPP_LOGIN_ARGS.fsUSER,
                            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                        });
                        //記錄用
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.AccountID, clsLOGIN.fsACCOUNT_ID);
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.UserID, clsLOGIN.fsUSER);
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "登入成功");
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsVALUE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        //記錄用
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.UserID, clsAPP_LOGIN_ARGS.fsUSER);
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "登入失敗");
                        //登入失敗
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "登入失敗，請確認帳密!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    //記錄用
                    LogHelper.RequestVariables.SetVariable(LogRequestVariableType.UserID, clsAPP_LOGIN_ARGS.fsUSER);
                    LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "header-error");
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得帳號狀態
        /// </summary>
        /// <param name="clsAPP_ACCOUNT_STATUS_ARGS">帳號狀態資訊</param>
        /// <returns></returns>
        [Route("GetAccountStatus")]
        public HttpResponseMessage PostGetAccountStatus(clsAPP_ACCOUNT_STATUS_ARGS clsAPP_ACCOUNT_STATUS_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    var clsACCOUNT_STATUS = new repACCOUNT().fnGET_ACCOUNT_STATUS(clsAPP_ACCOUNT_STATUS_ARGS.fsUSER);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsACCOUNT_STATUS }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 使用非四季帳號登入
        /// </summary>
        /// <param name="clsAPP_ACCOUNT_IS_LINK_ARGS">帳號連結資訊</param>
        /// <returns></returns>
        [Route("GetAccountIsLink")]
        public HttpResponseMessage PostGetAccountIsLink(clsAPP_ACCOUNT_IS_LINK_ARGS clsAPP_ACCOUNT_IS_LINK_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    string fsRESULT = new repACCOUNT().fnGET_ACCOUNT_IS_LINK(clsAPP_ACCOUNT_IS_LINK_ARGS.fsLOGIN_TYPE, clsAPP_ACCOUNT_IS_LINK_ARGS.fsLINK_ID);
                    if (fsRESULT == "Y")
                    {
                        //若已連結則直接登入
                        clsLOGIN clsLOGIN = new repACCOUNT().fnSIGNIN(clsAPP_ACCOUNT_IS_LINK_ARGS.fsLINK_ID, string.Empty, false);
                        if (!string.IsNullOrEmpty(clsLOGIN.fsACCOUNT_ID))
                        {

                            //記錄用
                            LogHelper.RequestVariables.SetVariable(LogRequestVariableType.AccountID, clsLOGIN.fsACCOUNT_ID);
                            LogHelper.RequestVariables.SetVariable(LogRequestVariableType.UserID, clsLOGIN.fsUSER);
                            //登入成功
                            string fsENC_KEY = clsAPP_ACCOUNT_IS_LINK_ARGS.fsENC_KEY;
                            //取得Token
                            string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
                            //取得有效的TimeSpan日期
                            long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddYears(100).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                            //加密
                            string fsVALIDATE_CODE = clsSECURITY.Encrypt(clsLOGIN.fsACCOUNT_ID + fsTOKEN + clsLOGIN.fsUSER + fdEXPIRE_TIME.ToString(), fsENC_KEY);
                            //cookie加密
                            string fsVALUE = clsSECURITY.CookieEncrypt(clsLOGIN.fsACCOUNT_ID + "-" + fsTOKEN + "-" + clsLOGIN.fsUSER + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE, fsENC_KEY);

                            //寫LOG
                            new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
                            {
                                fsFROM = "2",
                                fsIP = HttpContext.Current.Request.UserHostAddress,
                                fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                fsUSER_ID = clsLOGIN.fsUSER,
                                fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                            });

                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsVALUE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = string.Empty }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = string.Empty }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }

                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 新增非四季帳號連結
        /// </summary>
        /// <param name="clsAPP_INSERT_ACCOUNT_LINK_ARGS">帳號連結資訊</param>
        /// <returns></returns>
        [Route("AccountLink")]
        public HttpResponseMessage PostAccountLink(clsAPP_INSERT_ACCOUNT_LINK_ARGS clsAPP_INSERT_ACCOUNT_LINK_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_INSERT_ACCOUNT_LINK_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_INSERT_ACCOUNT_LINK_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_INSERT_ACCOUNT_LINK_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_INSERT_ACCOUNT_LINK_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                        //判斷是否已經綁過
                        string fsRESULT = new repACCOUNT().fnGET_ACCOUNT_IS_LINK(clsAPP_INSERT_ACCOUNT_LINK_ARGS.fsLOGIN_TYPE, clsAPP_INSERT_ACCOUNT_LINK_ARGS.fsLINK_ID);

                        if (fsRESULT == "Y")
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "此外部帳號已經驗證過!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            fsRESULT = new repACCOUNT().fnINSERT_ACCOUNT_LINK(clsAPP_INSERT_ACCOUNT_LINK_ARGS.fsLOGIN_TYPE, clsUSER_DATA.fsACCOUNT_ID, clsAPP_INSERT_ACCOUNT_LINK_ARGS.fsLINK_ID);
                            if (string.IsNullOrEmpty(fsRESULT))
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }

        }

        /// <summary>
        /// 取得帳號是否可以試用狀態
        /// </summary>
        /// <param name="clsAPP_IDENTITY_VALIDATE_ARUS">帳號資訊</param>
        /// <returns></returns>
        [Route("GetAccountPromoEligible")]
        public HttpResponseMessage PostGetPromoEligible(clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                        var clsACCOUNT_STATUS = new repACCOUNT().fnGET_ACCOUNT_STATUS(clsUSER_DATA.fsUSER);

                        //判斷帳號是否已驗證
                        if (clsACCOUNT_STATUS.Validated)
                        {
                            string fsRESULT = new repACCOUNT().fnGET_PROMO_ELIGIBLE(clsUSER_DATA.fsACCOUNT_ID);

                            if (!string.IsNullOrEmpty(fsRESULT))
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = (fsRESULT == "Y" ? "true" : "false") }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "取得資料錯誤!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "帳號尚未驗證!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 啟用帳號試用
        /// </summary>
        /// <param name="clsAPP_IDENTITY_VALIDATE_ARUS">帳號資訊</param>
        /// <returns></returns>
        [Route("AccountPromo")]
        public HttpResponseMessage PostPromo(clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS)
        {
            try
            {

                string fsDEVICE = Request.Headers.Contains("fsDEVICE") ? Request.Headers.GetValues("fsDEVICE").First() : string.Empty;
                string fsVERSION = Request.Headers.Contains("fsVERSION") ? Request.Headers.GetValues("fsVERSION").First() : string.Empty;
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
                        string fsRESULT = new repACCOUNT().fnACCOUNT_PROMO(clsUSER_DATA.fsACCOUNT_ID);

                        if (fsRESULT.IndexOf("ERROR") > -1)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            //寫LOG
                            new repLOG().fnINSERT_MEMBER_PROMO_LOG(new clsLOG_MEMBER_PROMO_ARGS()
                            {
                                fsACCOUNT_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsACCOUNT_ID),
                                fsENC_KEY = (clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY == null ? string.Empty : clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY),
                                fsDEVICE = (fsDEVICE == null ? string.Empty : fsDEVICE),
                                fsVERSION = (fsVERSION == null ? string.Empty :fsVERSION),
                                fsIP = HttpContext.Current.Request.UserHostAddress,
                                fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                            });

                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 是否已登入
        /// </summary>
        /// <param name="clsAPP_IDENTITY_VALIDATE_ARUS">帳號資訊</param>
        /// <returns></returns>
        [Route("AccountIsLogin")]
        public HttpResponseMessage PostAccountIsLogin(clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    if (clsSECURITY.IsLogin(clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "Y" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "N" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <param name="clsAPP_EDIT_PASSWORD">修改密碼資訊</param>
        /// <returns></returns>
        [Route("EditPassword")]
        public HttpResponseMessage PostEditPassword(clsAPP_EDIT_PASSWORD clsAPP_EDIT_PASSWORD)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_EDIT_PASSWORD.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_EDIT_PASSWORD.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        if (clsAPP_EDIT_PASSWORD.fsPASSWORD_NEW != clsAPP_EDIT_PASSWORD.fsPASSWORD_NEW1)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "密碼與確認密碼不符!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_EDIT_PASSWORD.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_EDIT_PASSWORD.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
                            string fsRESULT = new repACCOUNT().fnEDIT_PASSWORD(clsUSER_DATA.fsACCOUNT_ID, clsAPP_EDIT_PASSWORD.fsPASSWORD_OLD, clsAPP_EDIT_PASSWORD.fsPASSWORD_NEW);

                            if (string.IsNullOrEmpty(fsRESULT))
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 重設密碼
        /// </summary>
        /// <param name="clsAPP_RESET_PASSWORD">重設密碼資訊</param>
        /// <returns></returns>
        [Route("ResetPassword")]
        public HttpResponseMessage PostResetPassword(clsAPP_RESET_PASSWORD clsAPP_RESET_PASSWORD)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    if (clsAPP_RESET_PASSWORD.fsPASSWORD != clsAPP_RESET_PASSWORD.fsPASSWORD1)
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "密碼與確認密碼不符!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        string fsRESULT = new repACCOUNT().fnRESET_PASSWORD(clsAPP_RESET_PASSWORD.fsUSER, clsAPP_RESET_PASSWORD.fsPASSCODE, clsAPP_RESET_PASSWORD.fsPASSWORD);

                        if (string.IsNullOrEmpty(fsRESULT))
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }

                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得通行碼
        /// </summary>
        /// <param name="clsAPP_GET_PASSCODE_ARGS">通行碼物件資訊</param>
        /// <returns></returns>
        [Route("GetPasscode")]
        public HttpResponseMessage PostGetPassCode(clsAPP_GET_PASSCODE_ARGS clsAPP_GET_PASSCODE_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    string fsRESULT = new FourgTV_OTT2.Api2.Common.clsTOOL().fnGET_PASSCODE(clsAPP_GET_PASSCODE_ARGS.fnREGISTER_TYPE, clsAPP_GET_PASSCODE_ARGS.fsUSER, clsAPP_GET_PASSCODE_ARGS.fsVALIDATE_TYPE);
                    if (fsRESULT.IndexOf("ERROR") > -1)
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(fsRESULT))
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "取得通行碼失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 驗證通行碼與手機是否批配
        /// </summary>
        /// <param name="clsAPP_CONFIRM_PASSCODE_ARGS">通行碼物件資訊</param>
        /// <returns></returns>
        [Route("ConfirmPasscode")]
        public HttpResponseMessage PostConfirmPassCode(clsAPP_CONFIRM_PASSCODE_ARGS clsAPP_CONFIRM_PASSCODE_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    var clsPASSCODE = new repACCOUNT().fnCONFIRM_PASSCODE(clsAPP_CONFIRM_PASSCODE_ARGS.fsUSER, clsAPP_CONFIRM_PASSCODE_ARGS.fsVALIDATE_TYPE, clsAPP_CONFIRM_PASSCODE_ARGS.fsPASSCODE);

                    if (clsPASSCODE != null)
                    {
                        if (clsPASSCODE.fbRESULT)
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        else
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsPASSCODE.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "驗證通行碼失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 註冊帳號
        /// </summary>
        /// <param name="clsAPP_REGISTER_ARGS">註冊資訊</param>
        /// <returns></returns>
        [Route("Register")]
        public HttpResponseMessage PostRegister(clsAPP_REGISTER_ARGS clsAPP_REGISTER_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //檢查欄位
                    if (!ModelState.IsValid)
                    {
                        foreach (var item in ModelState.Keys)
                            if (ModelState[item].Errors.Count > 0)
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ModelState[item].Errors[0].ErrorMessage }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        Regex rgx = null;

                        if (clsAPP_REGISTER_ARGS.fnREGISTER_TYPE == 2)
                            rgx = new Regex(@"^[0-9]{10}$");
                        else if (clsAPP_REGISTER_ARGS.fnREGISTER_TYPE == 1)
                            rgx = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                        Match match = rgx.Match(clsAPP_REGISTER_ARGS.fsUSER);

                        if (match.Success)
                        {
                            //註冊
                            var clsREGISTER = new repACCOUNT().fnREGISTER(clsAPP_REGISTER_ARGS.fnREGISTER_TYPE, clsAPP_REGISTER_ARGS.fsLOGIN_TYPE, clsAPP_REGISTER_ARGS.fsUSER, clsAPP_REGISTER_ARGS.fsLINK_ID, clsAPP_REGISTER_ARGS.fnBIRTH_YEAR, clsAPP_REGISTER_ARGS.fsSEX, clsAPP_REGISTER_ARGS.fsPASSWORD);

                            if (clsREGISTER != null && clsREGISTER.fbRESULT)
                            {

                                string fsDEVICE = Request.Headers.Contains("fsDEVICE") ? Request.Headers.GetValues("fsDEVICE").First() : string.Empty;
                                string fsVERSION = Request.Headers.Contains("fsVERSION") ? Request.Headers.GetValues("fsVERSION").First() : string.Empty;
                                string fsENC_KEY = Request.Headers.Contains("fsENC_KEY") ? Request.Headers.GetValues("fsENC_KEY").First() : string.Empty;

                                //寫LOG
                                new repLOG().fnINSERT_MEMBER_REGISTER_LOG(new clsLOG_MEMBER_REGISTER_ARGS()
                                {
                                    fsUSER_ID = clsAPP_REGISTER_ARGS.fsUSER,
                                    fsENC_KEY = fsENC_KEY,
                                    fsDEVICE = fsDEVICE,
                                    fsVERSION = fsVERSION,
                                    fsIP = HttpContext.Current.Request.UserHostAddress,
                                    fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                    fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                });
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsREGISTER.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "帳號格式錯誤!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "註冊失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得會員資訊
        /// </summary>
        /// <param name="clsAPP_GET_ACCOUNT_INFO_ARGS">會員資訊</param>
        /// <returns></returns>
        [Route("GetAccountInfo")]
        public HttpResponseMessage PostGetAccountInfo(clsAPP_GET_ACCOUNT_INFO_ARGS clsAPP_GET_ACCOUNT_INFO_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_GET_ACCOUNT_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_ACCOUNT_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_GET_ACCOUNT_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_ACCOUNT_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                        clsMEMBER_INFO clsMEMBER_INFO = new repACCOUNT().fnGET_ACCOUNT_INFO(clsUSER_DATA.fsACCOUNT_ID);

                        string fsPICTURE = clsMEMBER_INFO.fsPICTURE;
                        //大頭貼如果不是URL，組成4gTV_URL
                        if ((!fsPICTURE.Contains("http://") && !fsPICTURE.Contains("https://")) && !string.IsNullOrEmpty(fsPICTURE))
                        {
                            clsMEMBER_INFO.fsPICTURE = new repCONFIG().fnGET_CFG("IMAGE_URL")[0].fsVALUE + @"MemberPicture/" + clsMEMBER_INFO.fsACCOUNT_ID + @"/" + fsPICTURE;
                        }

                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsMEMBER_INFO }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 設定會員資訊
        /// </summary>
        /// <param name="clsAPP_SET_ACCOUNT_INFO_ARGS">會員資訊</param>
        /// <returns></returns>
        [Route("SetAccountInfo")]
        public HttpResponseMessage PostSetAccountInfo(clsAPP_SET_ACCOUNT_INFO_ARGS clsAPP_SET_ACCOUNT_INFO_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_SET_ACCOUNT_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_SET_ACCOUNT_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        //檢查欄位
                        if (!ModelState.IsValid)
                        {
                            foreach (var item in ModelState.Keys)
                                if (ModelState[item].Errors.Count > 0)
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ModelState[item].Errors[0].ErrorMessage }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_SET_ACCOUNT_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_SET_ACCOUNT_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                            clsSET_ACCOUNT_INFO clsSET_ACCOUNT_INFO = new repACCOUNT().fnSET_ACCOUNT_INFO(new clsMEMBER_INFO()
                            {
                                fsACCOUNT_ID = clsUSER_DATA.fsACCOUNT_ID,
                                fsSEX = clsAPP_SET_ACCOUNT_INFO_ARGS.fsSEX,
                                fnBIRTHYEAR = clsAPP_SET_ACCOUNT_INFO_ARGS.fnBIRTHYEAR,
                                fsREALNAME = clsAPP_SET_ACCOUNT_INFO_ARGS.fsREALNAME,
                                fsADDRESS = clsAPP_SET_ACCOUNT_INFO_ARGS.fsADDRESS,
                                fsPHONE = clsAPP_SET_ACCOUNT_INFO_ARGS.fsPHONE,
                                fsEMAIL = clsAPP_SET_ACCOUNT_INFO_ARGS.fsEMAIL
                            });

                            if (clsSET_ACCOUNT_INFO != null)
                            {
                                if (clsSET_ACCOUNT_INFO.fbRESULT)
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                                else
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsSET_ACCOUNT_INFO.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "修改失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }

                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "修改失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 設定會員大頭貼
        /// </summary>
        /// <param name="clsAPP_SET_ACCOUNT_PICTURE">會員資訊</param>
        /// <returns></returns>
        [Route("SetAccountPicture")]
        public HttpResponseMessage PostSetAccountPicture(clsAPP_SET_ACCOUNT_PICTURE clsAPP_SET_ACCOUNT_PICTURE)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_SET_ACCOUNT_PICTURE.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_SET_ACCOUNT_PICTURE.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        //檢查欄位
                        if (!ModelState.IsValid)
                        {
                            foreach (var item in ModelState.Keys)
                                if (ModelState[item].Errors.Count > 0)
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ModelState[item].Errors[0].ErrorMessage }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            UserInfo clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_SET_ACCOUNT_PICTURE.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_SET_ACCOUNT_PICTURE.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                            string fsPICTURE_BASE64 = clsAPP_SET_ACCOUNT_PICTURE.fsPICTURE_BASE64;
                            string fsPICTURE_URL = clsAPP_SET_ACCOUNT_PICTURE.fsPICTURE_URL;
                            string fsPICTUREName = string.Empty;
                            //URL jpg
                            if (!string.IsNullOrEmpty(fsPICTURE_URL))
                            {
                                //確認是URL     
                                if (Regex.IsMatch(fsPICTURE_URL, @"^http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$"))
                                {
                                    fsPICTUREName = fsPICTURE_URL;
                                }
                                else
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "非URL格式!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                            }
                            //抓上傳的Base64轉jpg
                            if (!string.IsNullOrEmpty(fsPICTURE_BASE64))
                            {

                                try
                                {
                                    //儲存檔案
                                    byte[] fbPICTURE_BASE64 = Convert.FromBase64String(fsPICTURE_BASE64);
                                    //判斷檔案大小
                                    if (fbPICTURE_BASE64.Length <= int.Parse(new repCONFIG().fnGET_CFG("MEMBER_PICTURE_UPLOAD_MAX_SIZE")[0].fsVALUE))
                                    {
                                        //目標路徑
                                        string fsTARGET_FOLDER = new repCONFIG().fnGET_CFG("IMAGE_UPLOAD")[0].fsVALUE + @"MemberPicture\" + clsUSER_DATA.fsACCOUNT_ID;
                                        //副檔名
                                        string fsFilesExt = ".jpg";
                                        //檔名
                                        fsPICTUREName = DateTime.Now.ToString("yyyyMMddhhmmss") + fsFilesExt;
                                        //目標檔案
                                        string fsTARGET_FILE = string.Format(@"{0}\{1}", fsTARGET_FOLDER, fsPICTUREName);
                                        //回傳檔按路徑
                                        fsPICTURE_URL = new repCONFIG().fnGET_CFG("IMAGE_URL")[0].fsVALUE + @"MemberPicture/" + clsUSER_DATA.fsACCOUNT_ID + @"/" + fsPICTUREName;

                                        //資料夾不存在，則建立新的
                                        if (!Directory.Exists(fsTARGET_FOLDER))
                                        {
                                            Directory.CreateDirectory(fsTARGET_FOLDER);
                                        }
                                        //原始的Stream
                                        MemoryStream MemoryStream = new MemoryStream(fbPICTURE_BASE64);
                                        //建立原始圖
                                        Bitmap Bitmap = new Bitmap(MemoryStream);
                                        //目前長寬
                                        int fnIMG_WIDTH = Bitmap.Width;
                                        int fnIMG_HEIGHT = Bitmap.Height;
                                        //縮小後的長寬
                                        int fnAFTER_IMG_WIDTH = fnIMG_WIDTH;
                                        int fnAFTER_IMG_HEIGHT = fnIMG_HEIGHT;
                                        //最大圖片大小
                                        int fnPICTURE_SIZE_MAX = int.Parse(new repCONFIG().fnGET_CFG("MEMBER_PICTURE_MAX_SIZE")[0].fsVALUE);
                                        do
                                        {
                                            //替換用的Stream
                                            using (MemoryStream MemoryStreamTemp = new MemoryStream())
                                            {
                                                using (var bitmap = new Bitmap(fnAFTER_IMG_WIDTH, fnAFTER_IMG_HEIGHT))
                                                {
                                                    using (var Graph = Graphics.FromImage(bitmap))
                                                    {
                                                        if (MemoryStream.Length > fnPICTURE_SIZE_MAX)
                                                        {
                                                            //對圖做壓縮
                                                            Graph.CompositingQuality = CompositingQuality.HighQuality;
                                                            Graph.SmoothingMode = SmoothingMode.HighQuality;
                                                            Graph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                                            Graph.PixelOffsetMode = PixelOffsetMode.HighQuality;
                                                            Graph.DrawImage(Bitmap, new Rectangle(0, 0, fnAFTER_IMG_WIDTH, fnAFTER_IMG_HEIGHT), 0, 0, fnIMG_WIDTH, fnIMG_HEIGHT, GraphicsUnit.Pixel);
                                                            bitmap.Save(MemoryStreamTemp, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                            if (MemoryStreamTemp.Length <= fnPICTURE_SIZE_MAX)
                                                            {
                                                                bitmap.Save(fsTARGET_FILE, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                            }
                                                            else
                                                            {
                                                                //縮小長寬90%
                                                                fnAFTER_IMG_WIDTH = Convert.ToInt32(fnAFTER_IMG_WIDTH * 0.9);
                                                                fnAFTER_IMG_HEIGHT = Convert.ToInt32(fnAFTER_IMG_HEIGHT * 0.9);
                                                            }
                                                            //釋放與交換縮小後的stream
                                                            MemoryStream.Dispose();
                                                            MemoryStream = new MemoryStream();
                                                            MemoryStreamTemp.WriteTo(MemoryStream);
                                                        }
                                                        else       //一開始就符合檔案大小，直接產圖
                                                        {
                                                            Graph.DrawImage(Bitmap, new Rectangle(0, 0, fnAFTER_IMG_WIDTH, fnAFTER_IMG_HEIGHT), 0, 0, fnIMG_WIDTH, fnIMG_HEIGHT, GraphicsUnit.Pixel);
                                                            bitmap.Save(fsTARGET_FILE, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                        }
                                                    }
                                                }
                                            }
                                        } while (MemoryStream.Length > fnPICTURE_SIZE_MAX);
                                        Bitmap.Dispose();
                                        MemoryStream.Dispose();
                                    }
                                    else
                                    {
                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "超過容量上限!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    }
                                }
                                catch (Exception e)
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = e.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                            }
                            if (!string.IsNullOrEmpty(fsPICTUREName))
                            {
                                string fsSET_ACCOUNT_PICTURE = new repACCOUNT().fsSET_ACCOUNT_PICTURE(new clsMEMBER_INFO()
                                {
                                    fsACCOUNT_ID = clsUSER_DATA.fsACCOUNT_ID,
                                    fsPICTURE = fsPICTUREName
                                });

                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsSET_ACCOUNT_PICTURE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "修改失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }

                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "修改失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }



        /// <summary>
        /// 掃QRCode登入
        /// </summary>
        /// <returns></returns>
        [Route("QrCodeLogin")]
        [HttpPost]
        [Header(headersConfig = "fsENC_KEY,string|fsVERSION,string|fsDEVICE,string")]
        public StdResp QrCodeLogin(QrCodeLogin_req req)
        {
            StdResp stdResp = new StdResp();

            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.fsToken == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.SessionID == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }

            _AppAccount_BLL.QrCodeLogin(req);

            return stdResp;

        }
    }
}
