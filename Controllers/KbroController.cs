﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Http;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Req.Kbro;
using System.Transactions;
using FourgTV_OTT2.Api2.SwaggerHeader;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("KBTW")]
    public class KbroController : ApiController
    {
        Kbro_BLL _Kbro_BLL = new Kbro_BLL();

        [Route("InsertOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostInsertOrder(InsertOrderReq req)
        {
            try
            {
                Kbro_BLL.CheckHeader(Request);

                Kbro_BLL.CheckIpInWhiteList(Request);

                Kbro_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    _Kbro_BLL.InsertOrder(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        [Route("CancelOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostCancelOrder(CancelOrderReq req)
        {
            try
            {
                Kbro_BLL.CheckHeader(Request);

                Kbro_BLL.CheckIpInWhiteList(Request);

                Kbro_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    Kbro_BLL.CancelOrder(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }
    }
}