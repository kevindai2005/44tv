﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;
using System.Net.Http.Headers;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Web.Configuration;
using ZXing;
using ZXing.QrCode;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.BLL.Model.Req.Account;
using FourgTV_OTT2.BLL.API;
using static FourgTV_OTT2.Common.Setting.EnumSetting;
using System.Transactions;
using FourgTV_OTT2.BLL.Model.Res.Account;
using FourgTV_OTT2.BLL.Model.Model;
using FourgTV_OTT2.Api2.SwaggerHeader;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Account")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class AccountController : ApiController
    {
        Account_BLL _Account_BLL = new Account_BLL();

        private static readonly string fsENC_KEY = WebConfigurationManager.AppSettings["fsENC_KEY"];
        //生成隨機生成器
        private Random random = new Random();
        /// <summary>
        /// 生成隨機淺顏色
        /// </summary>
        /// <returns>randomColor</returns>
        public Color GetRandomLightColor()
        {


            int nRed, nGreen, nBlue;    //越大顏色越淺
            int low = 80;           //色彩的下限
            int high = 200;          //色彩的上限      
            nRed = random.Next(high) % (high - low) + low;
            nGreen = random.Next(high) % (high - low) + low;
            nBlue = random.Next(high) % (high - low) + low;
            Color color = Color.FromArgb(nRed, nGreen, nBlue);
            return color;
        }

        /// <summary>
        /// 取得登入驗證碼圖形
        /// </summary>
        /// <returns></returns>
        [Route("GetLoginValidateImageCode")]
        [HttpGet]
        public HttpResponseMessage GetLoginValidateImageCode()
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                //4位數字
                string fsVALUE = string.Format("{0:0000}", (new Random()).Next(10000));

                while (!string.IsNullOrEmpty(new repACCOUNT().fnINSERT_LOGIN_VALIDATE_CODE(fsVALUE)))
                {
                    fsVALUE = string.Format("{0:0000}", (new Random()).Next(10000));
                }

                if (true)
                {
                    System.Drawing.Bitmap image = new System.Drawing.Bitmap(System.Convert.ToInt32(Math.Ceiling((fsVALUE.Length * 25.0))), 45);
                    System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(image);

                    try
                    {
                        //生成隨機生成器
                        Random random = new Random();

                        //清空圖片背景色
                        g.Clear(System.Drawing.Color.White);
                        for (int i = 0; i <= 24; i++)
                        {

                            // ‘畫圖片的背景噪音線
                            int x1 = random.Next(image.Width);
                            int x2 = random.Next(image.Width);
                            int y1 = random.Next(image.Height);
                            int y2 = random.Next(image.Height);

                            g.DrawLine(new System.Drawing.Pen(System.Drawing.Color.Silver), x1, y1, x2, y2);
                        }

                        for (int i = 0; i < fsVALUE.Length; i++)
                        {
                            Brush brush = new System.Drawing.SolidBrush(GetRandomLightColor());

                            System.Drawing.Font font = new System.Drawing.Font("Arial", random.Next(15, 20), (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic));

                            //System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(new System.Drawing.Rectangle(0, 0, image.Width, image.Height), System.Drawing.Color.Blue, System.Drawing.Color.DarkRed, 1.2F, true);

                            //隨機轉動的度數
                            float angle = random.Next(-10, 10);

                            //轉動畫布
                            g.RotateTransform(angle);
                            g.DrawString(fsVALUE.ToCharArray()[i].ToString(), font, brush, 5 + (i * 20), 10);
                            //轉回去，為下一個字符做準備
                            g.RotateTransform(-angle);
                            //釋放資源
                            brush.Dispose();
                        }

                        //g.DrawString(code, font, brush, 2, 2);

                        for (int i = 0; i <= 99; i++)
                        {

                            // ‘畫圖片的前景噪音點
                            int x = random.Next(image.Width);
                            int y = random.Next(image.Height);

                            image.SetPixel(x, y, System.Drawing.Color.FromArgb(random.Next()));
                        }

                        // ‘畫圖片的邊框線
                        g.DrawRectangle(new System.Drawing.Pen(System.Drawing.Color.Silver), 0, 0, image.Width - 1, image.Height - 1);

                        System.IO.MemoryStream ms = new System.IO.MemoryStream();
                        image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                        result.Content = new ByteArrayContent(ms.ToArray());
                        result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/Gif");

                        return result;
                    }
                    catch (Exception ex1)
                    {
                        throw;
                    }
                    finally
                    {
                        g.Dispose();
                        image.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        /// <summary>
        /// 取得QRCode
        /// </summary>
        /// <returns></returns>
        [Route("GetQRCode")]
        [HttpGet]
        public HttpResponseMessage GetQRCode()
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            string fsDEVICE = Request.Headers.Contains("fsDEVICE") ? Request.Headers.GetValues("fsDEVICE").First() : string.Empty;
            string fsVERSION = Request.Headers.Contains("fsVERSION") ? Request.Headers.GetValues("fsVERSION").First() : string.Empty;
            string fsENC_KEY = Request.Headers.Contains("fsENC_KEY") ? Request.Headers.GetValues("fsENC_KEY").First() : string.Empty;


            try
            {
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    var writer = new BarcodeWriter  //dll裡面可以看到屬性
                    {
                        Format = BarcodeFormat.QR_CODE,
                        Options = new QrCodeEncodingOptions //設定大小
                        {
                            Height = 300,
                            Width = 300,
                            CharacterSet = "UTF-8",  //編碼格式 UTF-8  中文才不會出現亂
                                                     //錯誤修正內容
                            ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.M
                        }
                    };

                    string sessionID = Guid.NewGuid().ToString().Substring(1, 28).ToUpper();

                    string qrcodePath = WebConfigurationManager.AppSettings["QRcodePath"];


                    var Image = writer.Write(qrcodePath + "?sid=" + sessionID); //轉QRcode的文字   


                    Image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                    result.Content = new ByteArrayContent(ms.ToArray());
                    result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/jpeg");
                    result.Headers.Add("session-id", sessionID);
                    return result;
                }
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        /// <summary>
        /// 取得登入驗證碼
        /// </summary>
        /// <returns></returns>
        [Route("GetLoginValidateCode")]
        public HttpResponseMessage GetLoginValidateCode()
        {
            try
            {
                string fsVALUE = Guid.NewGuid().ToString().ToLower().Replace("-", "").Replace("1", "").Replace("l", "").Replace("o", "").Replace("0", "").ToUpper().Substring(0, 4);

                while (!string.IsNullOrEmpty(new repACCOUNT().fnINSERT_LOGIN_VALIDATE_CODE(fsVALUE)))
                {
                    fsVALUE = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Substring(0, 4);
                }

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsVALUE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 帳號登入
        /// </summary>
        /// <param name="clsLOGIN_ARGS">登入資訊</param>
        /// <returns></returns>
        [Route("SignIn")]
        public HttpResponseMessage PostSignin(clsLOGIN_ARGS clsLOGIN_ARGS)
        {
            try
            {
                //判斷密碼是否要重設，如果要就去litv驗證
                bool fbRESET = false;

                if (!string.IsNullOrEmpty(clsLOGIN_ARGS.fsPASSWORD))
                {
                    fbRESET = new repACCOUNT().fnGET_MEMBER_PASSWORD_NEED_RESET(clsLOGIN_ARGS.fsUSER);
                    if (fbRESET)
                    {
                        var clsLITV_LOGIN = new repACCOUNT().fnLI_LOGIN(clsLOGIN_ARGS.fsUSER, clsLOGIN_ARGS.fsPASSWORD);
                        if (clsLITV_LOGIN.result == null)
                        {
                            //登入失敗
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "登入失敗，請確認帳密是否正確!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                }

                clsLOGIN clsLOGIN = new repACCOUNT().fnSIGNIN(clsLOGIN_ARGS.fsUSER, clsLOGIN_ARGS.fsPASSWORD, fbRESET);

                if (!string.IsNullOrEmpty(clsLOGIN.fsACCOUNT_ID))
                {
                    //判斷驗證碼
                    string fsVALIDATE_CODE_RESULT = new repACCOUNT().fnGET_LOGIN_VALIDATE_CODE(clsLOGIN_ARGS.fsVALIDATE_CODE);

                    if (string.IsNullOrEmpty(fsVALIDATE_CODE_RESULT))
                    {

                        //取得Token
                        string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
                        //取得有效的TimeSpan日期
                        long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddDays(7).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                        //加密
                        string fsVALIDATE_CODE = clsSECURITY.Encrypt(clsLOGIN.fsACCOUNT_ID + fsTOKEN + clsLOGIN.fsUSER + fdEXPIRE_TIME.ToString(), fsENC_KEY);
                        //cookie加密
                        string fsVALUE = clsSECURITY.CookieEncrypt(clsLOGIN.fsACCOUNT_ID + "-" + fsTOKEN + "-" + clsLOGIN.fsUSER + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE);

                        //寫LOG
                        new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
                        {
                            fsFROM = "1",
                            fsIP = HttpContext.Current.Request.UserHostAddress,
                            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                            fsUSER_ID = clsLOGIN_ARGS.fsUSER,
                            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                        });
                        //記錄用
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.AccountID, clsLOGIN.fsACCOUNT_ID);
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.UserID, clsLOGIN_ARGS.fsUSER);
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "登入成功");
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsVALUE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        //記錄用
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.UserID, clsLOGIN_ARGS.fsUSER);
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "驗證碼錯誤或已過期!");
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsVALIDATE_CODE_RESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }

                }
                else
                {
                    //記錄用
                    LogHelper.RequestVariables.SetVariable(LogRequestVariableType.UserID, clsLOGIN_ARGS.fsUSER);
                    LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "登入失敗");
                    //登入失敗
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "登入失敗，請確認帳密是否正確!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得帳號狀態
        /// </summary>
        /// <param name="clsACCOUNT_STATUS_ARGS">帳號狀態資訊</param>
        /// <returns></returns>
        [Route("GetAccountStatus")]
        public HttpResponseMessage PostGetAccountStatus(clsACCOUNT_STATUS_ARGS clsACCOUNT_STATUS_ARGS)
        {
            try
            {
                var clsACCOUNT_STATUS = new repACCOUNT().fnGET_ACCOUNT_STATUS(clsACCOUNT_STATUS_ARGS.fsUSER);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsACCOUNT_STATUS }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 使用非四季帳號登入
        /// </summary>
        /// <param name="clsACCOUNT_IS_LINK_ARGS">帳號連結資訊</param>
        /// <returns></returns>
        [Route("GetAccountIsLink")]
        public HttpResponseMessage PostGetAccountIsLink(clsACCOUNT_IS_LINK_ARGS clsACCOUNT_IS_LINK_ARGS)
        {
            try
            {
                string fsRESULT = new repACCOUNT().fnGET_ACCOUNT_IS_LINK(clsACCOUNT_IS_LINK_ARGS.fsLOGIN_TYPE, clsACCOUNT_IS_LINK_ARGS.fsLINK_ID);
                if (fsRESULT == "Y")
                {
                    //若已連結則直接登入
                    clsLOGIN clsLOGIN = new repACCOUNT().fnSIGNIN(clsACCOUNT_IS_LINK_ARGS.fsLINK_ID, string.Empty, false);
                    if (!string.IsNullOrEmpty(clsLOGIN.fsACCOUNT_ID))
                    {
                        //記錄用
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.AccountID, clsLOGIN.fsACCOUNT_ID);
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.UserID, clsLOGIN.fsUSER);
                        //取得Token
                        string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
                        //取得有效的TimeSpan日期
                        long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddDays(7).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                        //加密
                        string fsVALIDATE_CODE = clsSECURITY.Encrypt(clsLOGIN.fsACCOUNT_ID + fsTOKEN + clsLOGIN.fsUSER + fdEXPIRE_TIME.ToString(), fsENC_KEY);
                        //cookie加密
                        string fsVALUE = clsSECURITY.CookieEncrypt(clsLOGIN.fsACCOUNT_ID + "-" + fsTOKEN + "-" + clsLOGIN.fsUSER + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE);

                        //寫LOG
                        new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
                        {
                            fsFROM = "1",
                            fsIP = HttpContext.Current.Request.UserHostAddress,
                            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                            fsUSER_ID = clsLOGIN.fsUSER,
                            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                        });

                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsVALUE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = string.Empty }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = string.Empty }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 新增非四季帳號連結
        /// </summary>
        /// <param name="clsINSERT_ACCOUNT_LINK_ARGS">帳號連結資訊</param>
        /// <returns></returns>
        [Route("AccountLink")]
        public HttpResponseMessage PostAccountLink(clsINSERT_ACCOUNT_LINK_ARGS clsINSERT_ACCOUNT_LINK_ARGS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsINSERT_ACCOUNT_LINK_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsINSERT_ACCOUNT_LINK_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    //判斷是否已經綁過
                    string fsRESULT = new repACCOUNT().fnGET_ACCOUNT_IS_LINK(clsINSERT_ACCOUNT_LINK_ARGS.fsLOGIN_TYPE, clsINSERT_ACCOUNT_LINK_ARGS.fsLINK_ID);

                    if (fsRESULT == "Y")
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "此外部帳號已經驗證過!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        fsRESULT = new repACCOUNT().fnINSERT_ACCOUNT_LINK(clsINSERT_ACCOUNT_LINK_ARGS.fsLOGIN_TYPE, clsUSER_DATA.fsACCOUNT_ID, clsINSERT_ACCOUNT_LINK_ARGS.fsLINK_ID);
                        if (string.IsNullOrEmpty(fsRESULT))
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }

        }

        /// <summary>
        /// 取得帳號是否可以試用狀態
        /// </summary>
        /// <param name="clsIDENTITY_VALIDATE_ARUS">帳號資訊</param>
        /// <returns></returns>
        [Route("GetAccountPromoEligible")]
        public HttpResponseMessage PostGetPromoEligible(clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                    string fsRESULT = new repACCOUNT().fnGET_PROMO_ELIGIBLE(clsUSER_DATA.fsACCOUNT_ID);

                    if (!string.IsNullOrEmpty(fsRESULT))
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsRESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "取得資料錯誤!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 啟用帳號試用
        /// </summary>
        /// <param name="clsIDENTITY_VALIDATE_ARUS">帳號資訊</param>
        /// <returns></returns>
        [Route("AccountPromo")]
        public HttpResponseMessage PostPromo(clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                    string fsRESULT = new repACCOUNT().fnACCOUNT_PROMO(clsUSER_DATA.fsACCOUNT_ID);
                    if (fsRESULT.IndexOf("ERROR") > -1)
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        //寫LOG
                        new repLOG().fnINSERT_MEMBER_PROMO_LOG(new clsLOG_MEMBER_PROMO_ARGS()
                        {
                            fsACCOUNT_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsACCOUNT_ID),
                            fsENC_KEY = "",
                            fsDEVICE = "web",
                            fsVERSION = FourgTV_OTT2.Api2.Common.clsTOOL.GetOSNameByUserAgent(HttpContext.Current.Request.UserAgent),
                            fsIP = HttpContext.Current.Request.UserHostAddress,
                            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                        });
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 是否已登入
        /// </summary>
        /// <param name="clsIDENTITY_VALIDATE_ARUS">帳號資訊</param>
        /// <returns></returns>
        [Route("AccountIsLogin")]
        public HttpResponseMessage PostAccountIsLogin(clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS)
        {
            try
            {
                if (clsSECURITY.IsLogin(clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "Y" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "N" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <param name="clsEDIT_PASSWORD">修改密碼資訊</param>
        /// <returns></returns>
        [Route("EditPassword")]
        public HttpResponseMessage PostEditPassword(clsEDIT_PASSWORD clsEDIT_PASSWORD)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsEDIT_PASSWORD.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    if (clsEDIT_PASSWORD.fsPASSWORD_NEW != clsEDIT_PASSWORD.fsPASSWORD_NEW1)
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "密碼與確認密碼不符!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsEDIT_PASSWORD.clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                        string fsRESULT = new repACCOUNT().fnEDIT_PASSWORD(clsUSER_DATA.fsACCOUNT_ID, clsEDIT_PASSWORD.fsPASSWORD_OLD, clsEDIT_PASSWORD.fsPASSWORD_NEW);

                        if (string.IsNullOrEmpty(fsRESULT))
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }

                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 重設密碼
        /// </summary>
        /// <param name="clsRESET_PASSWORD">重設密碼資訊</param>
        /// <returns></returns>
        [Route("ResetPassword")]
        public HttpResponseMessage PostResetPassword(clsRESET_PASSWORD clsRESET_PASSWORD)
        {
            try
            {
                if (clsRESET_PASSWORD.fsPASSWORD != clsRESET_PASSWORD.fsPASSWORD1)
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "密碼與確認密碼不符!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    string fsRESULT = new repACCOUNT().fnRESET_PASSWORD(clsRESET_PASSWORD.fsUSER, clsRESET_PASSWORD.fsPASSCODE, clsRESET_PASSWORD.fsPASSWORD);

                    if (string.IsNullOrEmpty(fsRESULT))
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得通行碼
        /// </summary>
        /// <param name="clsGET_PASSCODE_ARGS">通行碼物件資訊</param>
        /// <returns></returns>
        [Route("GetPasscode")]
        public HttpResponseMessage PostGetPassCode(clsGET_PASSCODE_ARGS clsGET_PASSCODE_ARGS)
        {
            try
            {
                string fsRESULT = new FourgTV_OTT2.Api2.Common.clsTOOL().fnGET_PASSCODE(clsGET_PASSCODE_ARGS.fnREGISTER_TYPE, clsGET_PASSCODE_ARGS.fsUSER, clsGET_PASSCODE_ARGS.fsVALIDATE_TYPE);
                if (fsRESULT.IndexOf("ERROR") > -1)
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    if (string.IsNullOrEmpty(fsRESULT))
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "取得通行碼失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 驗證通行碼與手機是否批配
        /// </summary>
        /// <param name="clsCONFIRM_PASSCODE_ARGS">通行碼物件資訊</param>
        /// <returns></returns>
        [Route("ConfirmPasscode")]
        public HttpResponseMessage PostConfirmPassCode(clsCONFIRM_PASSCODE_ARGS clsCONFIRM_PASSCODE_ARGS)
        {
            try
            {
                var clsPASSCODE = new repACCOUNT().fnCONFIRM_PASSCODE(clsCONFIRM_PASSCODE_ARGS.fsUSER, clsCONFIRM_PASSCODE_ARGS.fsVALIDATE_TYPE, clsCONFIRM_PASSCODE_ARGS.fsPASSCODE);

                if (clsPASSCODE != null)
                {
                    if (clsPASSCODE.fbRESULT)
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    else
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsPASSCODE.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "驗證通行碼失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 註冊帳號
        /// </summary>
        /// <param name="clsREGISTER_ARGS">註冊資訊</param>
        /// <returns></returns>
        [Route("Register")]
        public HttpResponseMessage PostRegister(clsREGISTER_ARGS clsREGISTER_ARGS)
        {

       
            try
            {
                //檢查欄位
                if (!ModelState.IsValid)
                {
                    foreach (var item in ModelState.Keys)
                        if (ModelState[item].Errors.Count > 0)
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ModelState[item].Errors[0].ErrorMessage }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    Regex rgx = null;

                    if (clsREGISTER_ARGS.fnREGISTER_TYPE == 2)
                        rgx = new Regex(@"^[0-9]{10}$");
                    else if (clsREGISTER_ARGS.fnREGISTER_TYPE == 1)
                        rgx = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

                    Match match = rgx.Match(clsREGISTER_ARGS.fsUSER);

                    if (match.Success)
                    {
                        //註冊
                        var clsREGISTER = new repACCOUNT().fnREGISTER(clsREGISTER_ARGS.fnREGISTER_TYPE, clsREGISTER_ARGS.fsLOGIN_TYPE, clsREGISTER_ARGS.fsUSER, clsREGISTER_ARGS.fsLINK_ID, clsREGISTER_ARGS.fnBIRTH_YEAR, clsREGISTER_ARGS.fsSEX, clsREGISTER_ARGS.fsPASSWORD);

                        if (clsREGISTER != null && clsREGISTER.fbRESULT)
                        {
                            //寫LOG
                            new repLOG().fnINSERT_MEMBER_REGISTER_LOG(new clsLOG_MEMBER_REGISTER_ARGS()
                            {
                                fsUSER_ID = clsREGISTER_ARGS.fsUSER,
                                fsENC_KEY = "",
                                fsDEVICE = "web",
                                fsVERSION = FourgTV_OTT2.Api2.Common.clsTOOL.GetOSNameByUserAgent(HttpContext.Current.Request.UserAgent),
                                fsIP = HttpContext.Current.Request.UserHostAddress,
                                fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                            });
                            //東森送優惠碼
                            _Account_BLL.ETMallCoupon(clsREGISTER_ARGS.fsUSER);

                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsREGISTER.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = "帳號格式錯誤!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "註冊失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得會員資訊
        /// </summary>
        /// <param name="clsGET_ACCOUNT_INFO_ARGS">會員資訊</param>
        /// <returns></returns>
        [Route("GetAccountInfo")]
        public HttpResponseMessage PostGetAccountInfo(clsGET_ACCOUNT_INFO_ARGS clsGET_ACCOUNT_INFO_ARGS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsGET_ACCOUNT_INFO_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_ACCOUNT_INFO_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    clsMEMBER_INFO clsMEMBER_INFO = new repACCOUNT().fnGET_ACCOUNT_INFO(clsUSER_DATA.fsACCOUNT_ID);

                    string fsPICTURE = clsMEMBER_INFO.fsPICTURE;
                    //大頭貼如果不是URL，組成4gTV_URL
                    if ((!fsPICTURE.Contains("http://") && !fsPICTURE.Contains("https://")) && !string.IsNullOrEmpty(fsPICTURE))
                    {
                        clsMEMBER_INFO.fsPICTURE = new repCONFIG().fnGET_CFG("IMAGE_URL")[0].fsVALUE + @"MemberPicture/" + clsMEMBER_INFO.fsACCOUNT_ID + @"/" + fsPICTURE;
                    }
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsMEMBER_INFO }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 設定會員資訊
        /// </summary>
        /// <param name="clsSET_ACCOUNT_INFO_ARGS">會員資訊</param>
        /// <returns></returns>
        [Route("SetAccountInfo")]
        public HttpResponseMessage PostSetAccountInfo(clsSET_ACCOUNT_INFO_ARGS clsSET_ACCOUNT_INFO_ARGS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsSET_ACCOUNT_INFO_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    //檢查欄位
                    if (!ModelState.IsValid)
                    {
                        foreach (var item in ModelState.Keys)
                            if (ModelState[item].Errors.Count > 0)
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { ErrMessage = ModelState[item].Errors[0].ErrorMessage }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsSET_ACCOUNT_INFO_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                        clsSET_ACCOUNT_INFO clsSET_ACCOUNT_INFO = new repACCOUNT().fnSET_ACCOUNT_INFO(new clsMEMBER_INFO()
                        {
                            fsACCOUNT_ID = clsUSER_DATA.fsACCOUNT_ID,
                            fsSEX = clsSET_ACCOUNT_INFO_ARGS.fsSEX,
                            fnBIRTHYEAR = clsSET_ACCOUNT_INFO_ARGS.fnBIRTHYEAR,
                            fsREALNAME = clsSET_ACCOUNT_INFO_ARGS.fsREALNAME,
                            fsADDRESS = clsSET_ACCOUNT_INFO_ARGS.fsADDRESS,
                            fsPHONE = clsSET_ACCOUNT_INFO_ARGS.fsPHONE,
                            fsEMAIL = clsSET_ACCOUNT_INFO_ARGS.fsEMAIL
                        });

                        if (clsSET_ACCOUNT_INFO != null)
                        {
                            if (clsSET_ACCOUNT_INFO.fbRESULT)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsSET_ACCOUNT_INFO.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "修改失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "修改失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 設定會員大頭貼
        /// </summary>
        /// <param name="clsSET_ACCOUNT_PICTURE">會員資訊</param>
        /// <returns></returns>
        [Route("SetAccountPicture")]
        public HttpResponseMessage PostSetAccountPicture(clsSET_ACCOUNT_PICTURE clsSET_ACCOUNT_PICTURE)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsSET_ACCOUNT_PICTURE.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    //檢查欄位
                    if (!ModelState.IsValid)
                    {
                        foreach (var item in ModelState.Keys)
                            if (ModelState[item].Errors.Count > 0)
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { ErrMessage = ModelState[item].Errors[0].ErrorMessage }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        UserInfo clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsSET_ACCOUNT_PICTURE.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                        string fsPICTURE_BASE64 = clsSET_ACCOUNT_PICTURE.fsPICTURE_BASE64;
                        string fsPICTURE_URL = clsSET_ACCOUNT_PICTURE.fsPICTURE_URL;
                        string fsPICTUREName = string.Empty;
                        //URL jpg
                        if (!string.IsNullOrEmpty(fsPICTURE_URL))
                        {
                            //確認是URL     
                            if (Regex.IsMatch(fsPICTURE_URL, @"^http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$"))
                            {
                                fsPICTUREName = fsPICTURE_URL;
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "非URL格式!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        //抓上傳的Base64轉jpg
                        if (!string.IsNullOrEmpty(fsPICTURE_BASE64))
                        {

                            try
                            {
                                //儲存檔案
                                byte[] fbPICTURE_BASE64 = Convert.FromBase64String(fsPICTURE_BASE64);
                                //判斷檔案大小
                                if (fbPICTURE_BASE64.Length <= int.Parse(new repCONFIG().fnGET_CFG("MEMBER_PICTURE_UPLOAD_MAX_SIZE")[0].fsVALUE))
                                {
                                    //目標路徑
                                    string fsTARGET_FOLDER = new repCONFIG().fnGET_CFG("IMAGE_UPLOAD")[0].fsVALUE + @"MemberPicture\" + clsUSER_DATA.fsACCOUNT_ID;
                                    //副檔名
                                    string fsFilesExt = ".jpg";
                                    //檔名
                                    fsPICTUREName = DateTime.Now.ToString("yyyyMMddhhmmss") + fsFilesExt;
                                    //目標檔案
                                    string fsTARGET_FILE = string.Format(@"{0}\{1}", fsTARGET_FOLDER, fsPICTUREName);
                                    //回傳檔按路徑
                                    fsPICTURE_URL = new repCONFIG().fnGET_CFG("IMAGE_URL")[0].fsVALUE + @"MemberPicture/" + clsUSER_DATA.fsACCOUNT_ID + @"/" + fsPICTUREName;

                                    //資料夾不存在，則建立新的
                                    if (!Directory.Exists(fsTARGET_FOLDER))
                                    {
                                        Directory.CreateDirectory(fsTARGET_FOLDER);
                                    }
                                    //原始的Stream
                                    MemoryStream MemoryStream = new MemoryStream(fbPICTURE_BASE64);
                                    //建立原始圖
                                    Bitmap Bitmap = new Bitmap(MemoryStream);
                                    //目前長寬
                                    int fnIMG_WIDTH = Bitmap.Width;
                                    int fnIMG_HEIGHT = Bitmap.Height;
                                    //縮小後的長寬
                                    int fnAFTER_IMG_WIDTH = fnIMG_WIDTH;
                                    int fnAFTER_IMG_HEIGHT = fnIMG_HEIGHT;
                                    //最大圖片大小
                                    int fnPICTURE_SIZE_MAX = int.Parse(new repCONFIG().fnGET_CFG("MEMBER_PICTURE_MAX_SIZE")[0].fsVALUE);
                                    do
                                    {
                                        //替換用的Stream
                                        using (MemoryStream MemoryStreamTemp = new MemoryStream())
                                        {
                                            using (var bitmap = new Bitmap(fnAFTER_IMG_WIDTH, fnAFTER_IMG_HEIGHT))
                                            {
                                                using (var Graph = Graphics.FromImage(bitmap))
                                                {
                                                    if (MemoryStream.Length > fnPICTURE_SIZE_MAX)
                                                    {
                                                        //對圖做壓縮
                                                        Graph.CompositingQuality = CompositingQuality.HighQuality;
                                                        Graph.SmoothingMode = SmoothingMode.HighQuality;
                                                        Graph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                                        Graph.PixelOffsetMode = PixelOffsetMode.HighQuality;
                                                        Graph.DrawImage(Bitmap, new Rectangle(0, 0, fnAFTER_IMG_WIDTH, fnAFTER_IMG_HEIGHT), 0, 0, fnIMG_WIDTH, fnIMG_HEIGHT, GraphicsUnit.Pixel);
                                                        bitmap.Save(MemoryStreamTemp, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                        if (MemoryStreamTemp.Length <= fnPICTURE_SIZE_MAX)
                                                        {
                                                            bitmap.Save(fsTARGET_FILE, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                        }
                                                        else
                                                        {
                                                            //縮小長寬90%
                                                            fnAFTER_IMG_WIDTH = Convert.ToInt32(fnAFTER_IMG_WIDTH * 0.9);
                                                            fnAFTER_IMG_HEIGHT = Convert.ToInt32(fnAFTER_IMG_HEIGHT * 0.9);
                                                        }
                                                        //釋放與交換縮小後的stream
                                                        MemoryStream.Dispose();
                                                        MemoryStream = new MemoryStream();
                                                        MemoryStreamTemp.WriteTo(MemoryStream);
                                                    }
                                                    else       //一開始就符合檔案大小，直接產圖
                                                    {
                                                        Graph.DrawImage(Bitmap, new Rectangle(0, 0, fnAFTER_IMG_WIDTH, fnAFTER_IMG_HEIGHT), 0, 0, fnIMG_WIDTH, fnIMG_HEIGHT, GraphicsUnit.Pixel);
                                                        bitmap.Save(fsTARGET_FILE, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                    }
                                                }
                                            }
                                        }
                                    } while (MemoryStream.Length > fnPICTURE_SIZE_MAX);
                                    Bitmap.Dispose();
                                    MemoryStream.Dispose();
                                }
                                else
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "超過容量上限!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = e.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        if (!string.IsNullOrEmpty(fsPICTUREName))
                        {
                            string fsSET_ACCOUNT_PICTURE = new repACCOUNT().fsSET_ACCOUNT_PICTURE(new clsMEMBER_INFO()
                            {
                                fsACCOUNT_ID = clsUSER_DATA.fsACCOUNT_ID,
                                fsPICTURE = fsPICTUREName
                            });

                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsSET_ACCOUNT_PICTURE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "修改失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "修改失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// 活動註冊
        /// </summary>
        /// <returns></returns>
        [Route("ShareholderRegister")]
        [HttpPost]
        public StdResp<ShareholderRegister_res> ShareholderRegister(ShareholderRegister_req req)
        {
            try
            {
                //檢查參數
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                //檢查參數
                if (req.fsUSER == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                //檢查參數
                if (req.fsPASSWORD == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                //檢查參數
                if (req.fsVALIDATE_CODE == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp<ShareholderRegister_res> stdResp = new StdResp<ShareholderRegister_res>();
                    //註冊
                    var token = _Account_BLL.ShareholderRegister(req);

                    stdResp.Data = token;
                    _TransactionScope.Complete();
                    return stdResp;
                }


            }
            catch (MyException e)
            {
                throw e;
            }


        }
        /// <summary>
        /// 是否有此帳號
        /// </summary>
        /// <returns></returns>
        [Route("CheckUserID")]
        [HttpPost]
        public StdResp<bool> CheckUserID(CheckUserID_req req)
        {
            try
            {
                //檢查參數
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                //檢查參數
                if (req.fsUserID == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                StdResp<bool> stdResp = new StdResp<bool>();
                var check = false;
                check = _Account_BLL.CheckUserID(req);
                stdResp.Data = check;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }


        }

        /// <summary>
        /// 啟用優惠碼
        /// </summary>
        /// <returns></returns>
        [Route("UseCouponCode")]
        [HttpPost]
        public StdResp<UseCouponCode_Res> UseCouponCode(UseCouponCode_Req req)
        {
            using (TransactionScope _TransactionScope = new TransactionScope())
            {
                StdResp<UseCouponCode_Res> stdResp = new StdResp<UseCouponCode_Res>();

                stdResp.Data = _Account_BLL.UseCouponCode(req);

                _TransactionScope.Complete();
                return stdResp;
            }

        }
    }
}
