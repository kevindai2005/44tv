﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.Common;
using System.Web.Configuration;
using System.Transactions;
using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.Model.Req.Heran;
using FourgTV_OTT2.BLL.Model.Res.Heran;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Heran")]
    public class HeranController : ApiController
    {
        Heran_BLL _Heran_BLL = new Heran_BLL();

        [Route("SignIn")]
        [Header(headersConfig = "From,string|Device,string")]
        [HttpPost]
        public StdResp<SignInResp> PostSignIn(SignInReq req)
        {
            try
            {
                //Heran_BLL.CheckHeader(Request);
                //Heran_BLL.CheckIpInWhiteList(Request);
                Heran_BLL.CheckHeaderClient(Request);
                Heran_BLL.CheckParam(req);
                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp<SignInResp> stdResp = new StdResp<SignInResp>();
                    stdResp.Data = Heran_BLL.SignIn(req);
                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }
        [Route("GetChannels")]
        [Header(headersConfig = "From,string|Device,string")]
        [HttpPost]
        public StdResp<List<GetChannelsResp>> PostGetChannels(GetChannelsReq req)
        {
            try
            {
                //Heran_BLL.CheckHeader(Request);
                //Heran_BLL.CheckIpInWhiteList(Request);
                Heran_BLL.CheckHeaderClient(Request);
                Heran_BLL.CheckParam(req);
                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp<List<GetChannelsResp>> stdResp = new StdResp<List<GetChannelsResp>>();
                    stdResp.Data = Heran_BLL.GetChannels(req);
                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }
        [Route("GetChannelUrl")]
        [Header(headersConfig = "From,string|Device,string")]
        [HttpPost]
        public StdResp<string> PostGetChannelUrl(GetChannelUrlReq req)
        {
            try
            {
                //Heran_BLL.CheckHeader(Request);
                //Heran_BLL.CheckIpInWhiteList(Request);
                Heran_BLL.CheckHeaderClient(Request);
                Heran_BLL.CheckParam(req);
                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    var stdResp = new StdResp<string>();
                    stdResp.Data = Heran_BLL.HeranAesEncrypt(Heran_BLL.GetChannelUrl(req));
                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        [Route("InsertOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostInsertOrder(InsertOrderReq req)
        {
            try
            {
                Heran_BLL.CheckHeader(Request);

                Heran_BLL.CheckIpInWhiteList(Request);

                Heran_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    _Heran_BLL.InsertOrder(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        [Route("CancelOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostCancelOrder(CancelOrderReq req)
        {
            try
            {
                Heran_BLL.CheckHeader(Request);

                Heran_BLL.CheckIpInWhiteList(Request);

                Heran_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    Heran_BLL.CancelOrder(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        [Route("ResetData")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostResetMacData(ResetMacDataReq req)
        {
            try
            {
                //LOG
                //LogHelper.RequestVariables.SetVariable(LogRequestVariableType.MachineID, req.fsMAC_ADDRESS.Clean());
                //LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "CheckInfo");

                Heran_BLL.CheckHeader(Request);

                Heran_BLL.CheckIpInWhiteList(Request);

                Heran_BLL.CheckParam(req); // 有ErrorCode需調整

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    Heran_BLL.ResetData(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// A01.mac權限異動
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Route("UpdateData")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostUpdateMacData(UpdateMacDataReq req)
        {
            try
            {
                //LOG
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.MachineID, req.fsMAC_ADDRESS.Clean());
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "CheckInfo");

                Heran_BLL.CheckHeader(Request);

                Heran_BLL.CheckIpInWhiteList(Request);

                Heran_BLL.CheckParam(req); // 有ErrorCode需調整

                using (TransactionScope _TransactionScope = new TransactionScope())
                {

                    Heran_BLL.UpdateData(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        /*
        /// <summary>
        /// A02.mac退換貨重置
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Route("ResetData")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostResetMacData(ResetMacDataReq req)
        {
            try
            {
                //LOG
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.MachineID, req.fsMAC_ADDRESS.Clean());
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "CheckInfo");

                Heran_BLL.CheckHeader(Request);

                Heran_BLL.CheckIpInWhiteList(Request);

                Heran_BLL.CheckParam(req); // 有ErrorCode需調整

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    Heran_BLL.ResetData(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }
        */

        /// <summary>
        /// B01.請求生成序號
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Route("CreateCoupon")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp<string> CreateCoupon(CreateCoupon_Req req)
        {
            try
            {
                //LOG

                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "CreateCoupon");

                Heran_BLL.CheckHeader(Request);

                Heran_BLL.CheckIpInWhiteList(Request);


                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.HeranPurchasePackageError);
                }
                if (string.IsNullOrEmpty(req.fsPACKAGE_ID) )
                {
                    throw new MyException(ResponseCodeEnum.HeranPurchasePackageError);
                }

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp<string> stdResp = new StdResp<string>();

                    stdResp.Data= _Heran_BLL.CreateCoupon(req);

                    _TransactionScope.Complete();

                    return stdResp;
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// C01.訂單查詢
        /// </summary>
        /// <param name="req">訂單查詢</param>
        /// <returns></returns>
        [Route("GetOrderInfo")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp<List<GetOrderInfoResp>> PostGetOrderInfo(GetOrderInfoReq req)
        {
            try
            {
                //LOG
                //LogHelper.RequestVariables.SetVariable(LogRequestVariableType.MachineID, req.fsMAC_ADDRESS.Clean());
                //LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "CheckInfo");

                Heran_BLL.CheckHeader(Request);

                Heran_BLL.CheckIpInWhiteList(Request);

                Heran_BLL.CheckParam(req); // 有ErrorCode需調整

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    var stdResp = new StdResp<List<GetOrderInfoResp>>();
                    stdResp.Data = Heran_BLL.GetOrderInfo(req);

                    _TransactionScope.Complete();

                    return stdResp;
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }
        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}