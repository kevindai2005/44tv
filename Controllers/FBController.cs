﻿using FourgTV_OTT2.BLL.Model.Req.Article;
using FourgTV_OTT2.BLL.Model.Req.FB;
using FourgTV_OTT2.BLL.Model.Res.FB;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("FB")]
    public class FBController : ApiController
    {
        /// <summary>
        /// FB
        /// </summary>
        /// <returns></returns>
        [Route("DataDeletionCallback")]
        [HttpPost]
        public DataDeletionCallback_Req DataDeletionCallback(DataDeletionCallback_Res res)
        {
            DataDeletionCallback_Req req = new DataDeletionCallback_Req();
           

            try
            {

                var signed_request = res.signed_request.Split('.');

                string encoded_sig = signed_request[0];

                string payload = signed_request[1];

                string secret = "appsecret";

                var sig = Encoding.Default.GetString(Convert.FromBase64String(encoded_sig.Replace("-", "+").Replace("_", "/")));


                Data data = JsonConvert.DeserializeObject<Data>(Encoding.Default.GetString(Convert.FromBase64String(payload.Replace("-", "+").Replace("_", "/"))));


                //檢查

                var aaa = hash_hmac(secret, payload);

                if (aaa.Equals(sig))
                {


                    req.url = @"http://api2.4gtv.tv/FB/DataDeletionCallback" + @"?id=" + data.user_id;

                    req.confirmation_code = data.user_id;

                    return req;


                }



                req.url = @"http://api2.4gtv.tv/FB/DataDeletionCallback";

                req.confirmation_code = data.user_id;

            }
            catch (Exception e)
            {
                req.url = @"http://api2.4gtv.tv/FB/DataDeletionCallback";

                req.confirmation_code = e.Message;

            }
            return req;

        }

        private string hash_hmac(string key, string message)
        {

            var keyByte = Encoding.Default.GetBytes(key);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                hmacsha256.ComputeHash(Encoding.Default.GetBytes(message));

                return ByteToString(hmacsha256.Hash);
            }


        }

        private string ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString("X2"); /* hex format */
            return sbinary;
        }


        private class Data
        {
            /// <summary>
            /// algorithm
            /// </summary>
            public string algorithm { get; set; }
            /// <summary>
            /// expires
            /// </summary>
            public int? expires { get; set; }
            /// <summary>
            /// issued_at
            /// </summary>
            public int? issued_at { get; set; }
            /// <summary>
            /// user_id
            /// </summary>
            public string user_id { get; set; }
        }

    }
}
