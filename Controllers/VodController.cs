﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;

using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;

using WebApi.OutputCache.V2;
using System.Web.Http.Description;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using FourgTV_OTT2.DAL.DAL;
using System.Web.Configuration;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Model;
using FourgTV_OTT2.BLL.Model.Req.Vod;
using FourgTV_OTT2.BLL.Model.Res.Vod;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.SwaggerHeader;
using System.Transactions;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Vod")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://embed.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class VodController : ApiController
    {
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];
        Vod_BLL _Vod_BLL = new Vod_BLL();
        clsM3U8 _clsM3U8 = new clsM3U8();
        public string ENC_KEY
        {
            get
            {
                var obj = LogHelper.RequestVariables[LogRequestVariableType.ENC_KEY];
                return obj == null ? "" : obj.ToString();
            }
            set
            {
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.ENC_KEY, value);
            }
        }
        public string VALUE
        {
            get
            {
                var obj = LogHelper.RequestVariables[LogRequestVariableType.VALUE];
                return obj == null ? "" : obj.ToString();
            }
            set
            {
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.VALUE, value);
            }
        }

        /// <summary>
        /// 紀錄VOD觀看次數
        /// </summary>
        /// <returns></returns>
        [Route("AddVodPageView")]
        public HttpResponseMessage PostVodPageView(clsINSERT_VOD_PAGEVIEW_ARGS clsINSERT_VOD_PAGEVIEW_ARGS)
        {
            try
            {
                string fsRESULT = new repVOD().fnINSERT_VOD_PAGEVIEW(clsINSERT_VOD_PAGEVIEW_ARGS.fsVOD_NO, clsINSERT_VOD_PAGEVIEW_ARGS.fnSEQ);
                if (string.IsNullOrEmpty(fsRESULT))
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                else
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD URL
        /// </summary>
        /// <param name="clsGET_VOD_URL_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetVodUrl")]
        public HttpResponseMessage PostVodUrl(clsGET_VOD_URL_ARGS clsGET_VOD_URL_ARGS)
        {
            //此URL關閉，寫死盜版連結
            clsGET_VOD_URL clsGET_VOD_URL = new clsGET_VOD_URL();
            clsGET_VOD_URL.flstURLs = new List<string>();
            clsGET_VOD_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area7/index.m3u8");
            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_VOD_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

            //try
            //{
            //    string fsIP = HttpContext.Current.Request.UserHostAddress;

            //    clsUSER_DATA clsUSER_DATA = null;

            //    //判斷是否已登入
            //    if (clsSECURITY.IsLogin(clsGET_VOD_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
            //    {
            //        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_VOD_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);
            //    }

            //    //寫LOG
            //    new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
            //    {
            //        fsFROM = "1",
            //        fsNAME = "VOD",
            //        fsASSET_ID = clsGET_VOD_URL_ARGS.fsASSET_ID,
            //        fsIP = HttpContext.Current.Request.UserHostAddress,
            //        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
            //        fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
            //        fsUSER_AGENT = HttpContext.Current.Request.UserAgent
            //    });

            //    clsGET_VOD_URL clsGET_VOD_URL = new clsGET_VOD_URL();
            //    clsGET_VOD_URL.flstURLs = new List<string>();
            //    clsGET_VOD_URL.lstTIME_CODE = new List<int>();

            //    //測試環境才用
            //    if (int.Parse(fcIS_PRODUCTION) == 0)
            //    {
            //        if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
            //            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
            //    }
            //    else
            //    {
            //        if (fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
            //            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
            //    }

            //    //判斷是LiTV上架或FTV上架
            //    var clsVOD_FROM = new repVOD().fnGET_VOD_FROM(clsGET_VOD_URL_ARGS.fsASSET_ID);

            //    clsGET_VOD_URL.fsHEAD_FRAME = clsVOD_FROM.fsHEAD_FRAME;

            //    //判斷是否為台灣的IP或允許海外播放
            //    switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsVOD_FROM.fcOVERSEAS, clsVOD_FROM.lstEXCEPT_COUNTRY))
            //    {
            //        case "IsAllowArea"://允許播放
            //            break;
            //        case "OVERSEAS"://不允許海外
            //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //        case "EXCEPT_COUNTRY"://海外限制不能播放的國家
            //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //        default:
            //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //    }

            //    if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "scene")
            //    {
            //        //直擊
            //        //判斷是否已登入
            //        if (clsUSER_DATA != null)
            //        {
            //            //取得會員VOD權限
            //            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }
            //        else
            //        {
            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }
            //    }
            //    else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "education")
            //    {

            //        //文化教育
            //        //判斷是否已登入
            //        if (clsUSER_DATA != null)
            //        {
            //            //取得會員VOD權限
            //            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }
            //        else
            //        {
            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }

            //    }
            //    else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "movie")
            //    {

            //        //電影
            //        //判斷是否已登入
            //        if (clsUSER_DATA != null)
            //        {
            //            //取得會員VOD權限
            //            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }
            //        else
            //        {
            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }

            //    }
            //    else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "celebrity")
            //    {

            //        //網路紅什麼
            //        //判斷是否已登入
            //        if (clsUSER_DATA != null)
            //        {
            //            //取得會員VOD權限
            //            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }
            //        else
            //        {
            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }

            //    }
            //    else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "anime")
            //    {

            //        //動漫
            //        //判斷是否已登入
            //        if (clsUSER_DATA != null)
            //        {
            //            //取得會員VOD權限
            //            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }
            //        else
            //        {
            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }

            //    }
            //    else
            //    {

            //        //戲劇/綜藝
            //        //判斷是否已登入
            //        if (clsUSER_DATA != null)
            //        {
            //            //取得會員VOD權限
            //            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);
            //            //戲劇綜藝由LITV上傳的沒有中華m3u8
            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            if (clsVOD_FROM.fsFROM == "FTV")
            //            {
            //                clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            }
            //            else
            //            {
            //                clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsGET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP));
            //            }
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }
            //        else
            //        {
            //            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
            //            if (clsVOD_FROM.fsFROM == "FTV")
            //            {
            //                clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
            //            }
            //            else
            //            {
            //                clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsGET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP));
            //            }
            //            clsGET_VOD_URL.fbPLAY_AD = true;
            //        }
            //    }

            //    clsGET_VOD_URL.lstTIME_CODE = new repVOD().fnGET_VOD_TIME_CODE(clsGET_VOD_URL_ARGS.fsASSET_ID);

            //    //此URL關閉，寫死盜版連結
            //    clsGET_VOD_URL.flstURLs.Clear();
            //    clsGET_VOD_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area7/index.m3u8");
            //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_VOD_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //}
            //catch (Exception ex)
            //{
            //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //}
        }

        /// <summary>
        /// 取得VOD URL V2
        /// </summary>
        /// <param name="clsGET_VOD_URL_ARGS2">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetVodUrl2")]
        [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://embed.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,http://ott-manage.4gtv.tv,http://s-ott-manage.4gtv.tv,https://ott-manage.4gtv.tv,https://s-ott-manage.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
        public HttpResponseMessage PostVodUrl2(clsGET_VOD_URL_ARGS2 clsGET_VOD_URL_ARGS2)
        {
            try
            {
                //解密
                string toDes = clsSECURITY.AesDecrypt(clsGET_VOD_URL_ARGS2.value);
                //觀看時間
                int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("WEB_VOD_FREE_SEC")[0].fsVALUE);

                clsGET_VOD_URL_ARGS clsGET_VOD_URL_ARGS = Newtonsoft.Json.JsonConvert.DeserializeObject<clsGET_VOD_URL_ARGS>(toDes);


                //轉成動態看是不是WebView傳來的，如果有則會有clsIDENTITY_VALIDATE_ARUS.fsENC_KEY屬性
                dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(toDes);

                string fsIP = HttpContext.Current.Request.UserHostAddress;

                UserInfo clsUSER_DATA = null;

                //判斷是否為手機
                if (obj.clsIDENTITY_VALIDATE_ARUS.fsENC_KEY != null)
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsGET_VOD_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE, Convert.ToString(obj.clsIDENTITY_VALIDATE_ARUS.fsENC_KEY)))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_VOD_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE, Convert.ToString(obj.clsIDENTITY_VALIDATE_ARUS.fsENC_KEY));
                        //確認登入無觀看時間
                        fnALLOWTIME = -1;
                    }
                }
                else
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsGET_VOD_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_VOD_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                        //確認登入無觀看時間
                        fnALLOWTIME = -1;
                    }
                }



                //寫LOG
                new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
                {
                    fsFROM = "1",
                    fsNAME = "VOD",
                    fsASSET_ID = clsGET_VOD_URL_ARGS.fsASSET_ID,
                    fsIP = HttpContext.Current.Request.UserHostAddress,
                    fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                    fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
                    fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
                });

                clsGET_VOD_URL clsGET_VOD_URL = new clsGET_VOD_URL();

                clsGET_VOD_URL.flstURLs = new List<string>();
                clsGET_VOD_URL.lstTIME_CODE = new List<int>();
                clsGET_VOD_URL.fnALLOWTIME = fnALLOWTIME;
                //測試環境才用
                if (int.Parse(fcIS_PRODUCTION) == 0)
                {
                    if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }
                else
                {
                    if (fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }


                string fsMEDIA_TYPE = clsGET_VOD_URL_ARGS.fsMEDIA_TYPE;
                string fsDEVICE_TYPE = clsGET_VOD_URL_ARGS.fsDEVICE_TYPE;
                string fsASSET_ID = clsGET_VOD_URL_ARGS.fsASSET_ID;
                string ArticleID = obj.fsASSET_ID;
                if (fsMEDIA_TYPE == "Article" && string.IsNullOrEmpty(ArticleID) == false)
                {
                    fnALLOWTIME = -1;

                    clsGET_VOD_URL clsGET_Article_VOD_URL = new clsGET_VOD_URL();

                    clsGET_Article_VOD_URL.flstURLs = new List<string>();
                    clsGET_Article_VOD_URL.lstTIME_CODE = new List<int>();
                    clsGET_Article_VOD_URL.fnALLOWTIME = fnALLOWTIME;
                    clsGET_Article_VOD_URL.fbPLAY_AD = bool.Parse(WebConfigurationManager.AppSettings["Article_AD"]);

                    var fsCDN_URL = _Vod_BLL.fnGET_ArticleID_ARC_VIDEOInfo(ArticleID);


                    if (fsCDN_URL != null)
                    {
                        clsGET_Article_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(true, fsDEVICE_TYPE, "vod", fsCDN_URL.fsCDNFILE_ID, fsCDN_URL.fnFOLDER_YEAR.Value, fnALLOWTIME));
                        clsGET_Article_VOD_URL.flstURLs.Add(clsM3U8.GetTypeCVodUrl(true, fsDEVICE_TYPE, "vod", fsCDN_URL.fsCDNFILE_ID, fsCDN_URL.fnFOLDER_YEAR.Value, fsIP, fnALLOWTIME));
                    }

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(clsGET_Article_VOD_URL)) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }

                //判斷fsASSET_ID 是否有效  clsGET_VOD_URL_ARGS.fsASSET_ID

                if (!_Vod_BLL.CheckfsASSET_ID(clsGET_VOD_URL_ARGS.fsASSET_ID))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }

                //判斷是LiTV上架或FTV上架
                var clsVOD_FROM = new repVOD().fnGET_VOD_FROM(clsGET_VOD_URL_ARGS.fsASSET_ID);

                clsGET_VOD_URL.fsHEAD_FRAME = clsVOD_FROM.fsHEAD_FRAME;

                //判斷是否為台灣的IP或允許海外播放
                switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsVOD_FROM.fcOVERSEAS, clsVOD_FROM.lstEXCEPT_COUNTRY))
                {
                    case "IsAllowArea"://允許播放
                        break;
                    case "OVERSEAS"://不允許海外
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "EXCEPT_COUNTRY"://海外限制不能播放的國家
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "scene")
                {
                    //直擊
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                }
                else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "education")
                {
                    //文化教育
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                }
                else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "movie")
                {
                    //電影
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }

                }
                else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "celebrity")
                {

                    //網路紅什麼
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }

                }
                else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "anime")
                {

                    //動漫
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }

                }
                else
                {

                    //戲劇/綜藝
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);
                        //戲劇綜藝由LITV上傳的沒有中華m3u8
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        if (clsVOD_FROM.fsFROM == "FTV")
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        }
                        else
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsGET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP, fnALLOWTIME));
                        }
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                        if (clsVOD_FROM.fsFROM == "FTV")
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                        }
                        else
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsGET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP, fnALLOWTIME));
                        }
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                }
                clsGET_VOD_URL.lstTIME_CODE = new repVOD().fnGET_VOD_TIME_CODE(clsGET_VOD_URL_ARGS.fsASSET_ID);

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(clsGET_VOD_URL)) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD URL V3
        /// </summary>
        /// <param name="req">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域,05:請期待)</returns>
        [Route("GetVodUrl3")]
        [Header(headersConfig = "fsVALUE,string|fsENC_KEY,string")]

        [HttpPost]
        [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://embed.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,http://ott-manage.4gtv.tv,http://s-ott-manage.4gtv.tv,https://ott-manage.4gtv.tv,https://s-ott-manage.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
        public StdResp<string> GetVodUrl3(GetVodUrl_Req_Encrypt req)
        {

            StdResp<string> stdResp = new StdResp<string>();

            try
            {
                //解密
                string toDes = clsSECURITY.AesDecrypt(req.value);
                //觀看時間
                int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("WEB_VOD_FREE_SEC")[0].fsVALUE);

                GetVodUrl_Req GetVodUrl_Req = Newtonsoft.Json.JsonConvert.DeserializeObject<GetVodUrl_Req>(toDes);


                //轉成動態看是不是WebView傳來的，如果有則會有clsIDENTITY_VALIDATE_ARUS.fsENC_KEY屬性
                dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(toDes);

                string fsIP = HttpContext.Current.Request.UserHostAddress;

                UserInfo clsUSER_DATA = null;

                //判斷是否為手機
                if (!string.IsNullOrEmpty(this.ENC_KEY))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(this.VALUE, this.ENC_KEY))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(this.VALUE, Convert.ToString(this.ENC_KEY));

                    }
                }
                else
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(this.VALUE))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(this.VALUE);

                    }
                }

                var GetVodUrl = _Vod_BLL.GetVodUrl(GetVodUrl_Req, clsUSER_DATA);

                stdResp.Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(GetVodUrl));

                return stdResp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 取得VOD URL
        /// </summary>
        /// <param name="clsGET_VOD_URL_ARGS2">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetEmbedVodUrl")]
        [HttpPost]
        [ResponseType(typeof(GetEmbedVodUrl_Res))]
        [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://embed.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,http://ott-manage.4gtv.tv,http://s-ott-manage.4gtv.tv,https://ott-manage.4gtv.tv,https://s-ott-manage.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
        public StdResp<List<GetEmbedVodUrl_Res>> GetEmbedVodUrl(List<GetEmbedVodUrl_Req> req)
        {
            StdResp<List<GetEmbedVodUrl_Res>> stdResp = new StdResp<List<GetEmbedVodUrl_Res>>();

            stdResp.Data = _Vod_BLL.GetEmbedVodUrl(req);

            return stdResp;
        }

        ///// <summary>
        ///// 取得VOD URL
        ///// </summary>
        ///// <param name="clsGET_VOD_URL_ARGS2">取得頻道URL資訊</param>
        ///// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        //[Route("GetEmbedVodUrl")]
        //[HttpPost]
        //[EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://embed.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,http://ott-manage.4gtv.tv,http://s-ott-manage.4gtv.tv,https://ott-manage.4gtv.tv,https://s-ott-manage.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
        //public HttpResponseMessage GetEmbedVodUrl(List<GetEmbedVodUrl_Req> req)
        //{
        //    try
        //    {

        //        List<clsGET_VOD_URL> clsGET_VOD_URLs = new List<clsGET_VOD_URL>();

        //        foreach (var _req in req)
        //        {

        //            //觀看時間
        //            int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("WEB_VOD_FREE_SEC")[0].fsVALUE);

        //            string fsIP = HttpContext.Current.Request.UserHostAddress;

        //            UserInfo clsUSER_DATA = null;

        //            //查詢fsASSET_ID

        //            string fsVOD_NO = _req.fsVOD_NO;
        //            int fnSEQ = _req.fnSEQ.Value;

        //            var GetVodUrlInfo = _Vod_BLL.GetVodUrlInfo(fsVOD_NO, fnSEQ);
        //            if (GetVodUrlInfo == null)
        //            {
        //                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

        //            }
        //            string fsASSET_ID = GetVodUrlInfo.fsASSET_ID;
        //            string fsFROM = GetVodUrlInfo.fsFROM;
        //            string fcCASE_CHT = GetVodUrlInfo.fcCASE_CHT;
        //            string fsWRITTEN_BY = GetVodUrlInfo.fsWRITTEN_BY;
        //            string fsDEVICE_TYPE = "pc";
        //            bool fcOVERSEAS = GetVodUrlInfo.fcOVERSEAS;
        //            string fsHEAD_FRAME = GetVodUrlInfo.fsHEAD_FRAME;
        //            int fnYEAR = GetVodUrlInfo.fnYEAR;
        //            List<string> lstEXCEPT_COUNTRY = GetVodUrlInfo.lstEXCEPT_COUNTRY;

        //            //寫LOG
        //            new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
        //            {
        //                fsFROM = "99",
        //                fsNAME = "VOD",
        //                fsASSET_ID = fsASSET_ID,
        //                fsIP = HttpContext.Current.Request.UserHostAddress,
        //                fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
        //                fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
        //                fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
        //            });

        //            clsGET_VOD_URL clsGET_VOD_URL = new clsGET_VOD_URL();

        //            clsGET_VOD_URL.flstURLs = new List<string>();
        //            clsGET_VOD_URL.lstTIME_CODE = new List<int>();
        //            clsGET_VOD_URL.fnALLOWTIME = fnALLOWTIME;
        //            //測試環境才用
        //            if (int.Parse(fcIS_PRODUCTION) == 0)
        //            {
        //                if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
        //                    fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
        //            }
        //            else
        //            {
        //                if (fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
        //                    fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
        //            }


        //            //string fsMEDIA_TYPE = clsGET_VOD_URL_ARGS.fsMEDIA_TYPE;

        //            //if (fsMEDIA_TYPE == "Article" && string.IsNullOrEmpty(ArticleID) == false)
        //            //{
        //            //    fnALLOWTIME = -1;

        //            //    clsGET_VOD_URL clsGET_Article_VOD_URL = new clsGET_VOD_URL();

        //            //    clsGET_Article_VOD_URL.flstURLs = new List<string>();
        //            //    clsGET_Article_VOD_URL.lstTIME_CODE = new List<int>();
        //            //    clsGET_Article_VOD_URL.fnALLOWTIME = fnALLOWTIME;
        //            //    clsGET_Article_VOD_URL.fbPLAY_AD = bool.Parse(WebConfigurationManager.AppSettings["Article_AD"]);

        //            //    var fsCDN_URL = _Vod_BLL.fnGET_ArticleID_ARC_VIDEOInfo(ArticleID);


        //            //    if (fsCDN_URL != null)
        //            //    {
        //            //        clsGET_Article_VOD_URL.flstURLs.Add(new FourgTV_OTT2.Common.clsM3U8().GetUrlFromNewMozai(true, fsDEVICE_TYPE, "vod", fsCDN_URL.fsCDNFILE_ID, fsCDN_URL.fnFOLDER_YEAR.Value, fnALLOWTIME));
        //            //        clsGET_Article_VOD_URL.flstURLs.Add(new FourgTV_OTT2.Common.clsM3U8().GetTypeCVodUrl(true, fsDEVICE_TYPE, "vod", fsCDN_URL.fsCDNFILE_ID, fsCDN_URL.fnFOLDER_YEAR.Value, fsIP, fnALLOWTIME));
        //            //    }

        //            //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(clsGET_Article_VOD_URL)) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

        //            //}

        //            //判斷fsASSET_ID 是否有效  clsGET_VOD_URL_ARGS.fsASSET_ID

        //            if (!_Vod_BLL.CheckfsASSET_ID(fsASSET_ID))
        //            {
        //                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

        //            }

        //            //判斷是LiTV上架或FTV上架
        //            //var clsVOD_FROM = new repVOD().fnGET_VOD_FROM(fsASSET_ID);

        //            clsGET_VOD_URL.fsHEAD_FRAME = fsHEAD_FRAME;

        //            //判斷是否為台灣的IP或允許海外播放
        //            switch (clsSECURITY.GetIpIsAllowArea(fsIP, fcOVERSEAS, lstEXCEPT_COUNTRY))
        //            {
        //                case "IsAllowArea"://允許播放
        //                    break;
        //                case "OVERSEAS"://不允許海外
        //                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //                case "EXCEPT_COUNTRY"://海外限制不能播放的國家
        //                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //                default:
        //                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //            }

        //            if (clsUSER_DATA != null)
        //            {
        //                //取得會員VOD權限
        //                var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, fsVOD_NO);
        //                //戲劇綜藝由LITV上傳的沒有中華m3u8
        //                clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, fsDEVICE_TYPE, "vod", fsASSET_ID, fnYEAR, fnALLOWTIME));
        //                if (fsFROM == "FTV")
        //                {
        //                    clsGET_VOD_URL.flstURLs.Add(fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, fsDEVICE_TYPE, fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, fsDEVICE_TYPE, "vod", fsASSET_ID, fnYEAR, fsIP, fnALLOWTIME));
        //                }
        //                else
        //                {
        //                    clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(fcCASE_CHT, clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, fsDEVICE_TYPE, fsWRITTEN_BY, fsASSET_ID.Replace("_1500K", ""), fsIP, fnALLOWTIME));
        //                }
        //                clsGET_VOD_URL.fbPLAY_AD = true;
        //            }
        //            else
        //            {
        //                clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, fsDEVICE_TYPE, "vod", fsASSET_ID, fnYEAR, fnALLOWTIME));
        //                if (fsFROM == "FTV")
        //                {
        //                    clsGET_VOD_URL.flstURLs.Add(fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, fsDEVICE_TYPE, fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, fsDEVICE_TYPE, "vod", fsASSET_ID, fnYEAR, fsIP, fnALLOWTIME));
        //                }
        //                else
        //                {
        //                    clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(fcCASE_CHT, false, fsDEVICE_TYPE, fsWRITTEN_BY, fsASSET_ID.Replace("_1500K", ""), fsIP, fnALLOWTIME));
        //                }
        //                clsGET_VOD_URL.fbPLAY_AD = true;
        //            }

        //            clsGET_VOD_URL.lstTIME_CODE = new repVOD().fnGET_VOD_TIME_CODE(fsASSET_ID);
        //            clsGET_VOD_URLs.Add(clsGET_VOD_URL);
        //        }




        //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_VOD_URLs }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //    }
        //}
        /// <summary>
        /// 取得VOD戲劇類型
        /// </summary>
        ///  <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodDramaType/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodDramaType(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_DRAMA_TYPE(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD戲劇年份
        /// </summary>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodDramaYear/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodDramaYear(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_DRAMA_YEAR(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD戲劇首頁資訊
        /// </summary>
        /// <param name="fcFIXED">固定年份(Y:固定年份、N:年份以前)</param>
        /// <param name="fnYEAR">年份(0:不分年代)</param>
        /// <param name="fsTYPE">VOD類型代碼(00:不分類)</param>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodDramaIndex/{fcFIXED}/{fnYEAR:int}/{fsTYPE}/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodDramaIndex(string fcFIXED, int fnYEAR, string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_DRAMA_INDEX(fcFIXED, fnYEAR, fsTYPE, fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD戲劇首頁資訊V3
        /// </summary>
        /// <param name="fsTYPE">VOD類型代碼(00:不分類)</param>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodDramaIndex3/{fsTYPE}/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodDramaIndex3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_DRAMA_INDEX3(fsTYPE, fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD戲劇首頁資訊-手機用
        /// </summary>
        /// <returns></returns>
        [Route("GetMobileVodDramaIndex")]
        [CacheOutput(ServerTimeSpan = 21600, AnonymousOnly = true)]
        public HttpResponseMessage GetMobileVodDramaIndex()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_DRAMA_INDEX_MOBILE() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD戲劇集數資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetVodDramaEpisode/{fsVOD_NO}")]
        public HttpResponseMessage GetVodDramaEpisode(string fsVOD_NO)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_DRAMA_EPISODE(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD戲劇集數明細
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">VOD序號</param>
        /// <returns></returns>
        [Route("GetVodDramaEpisodeDetail/{fsVOD_NO}/{fnSEQ:int}")]
        [ResponseType(typeof(clsVOD_EPISODE_DETAIL))]
        public HttpResponseMessage GetVodDramaEpisodeDetail(string fsVOD_NO, int fnSEQ)
        {
            try
            {
                var data = new repVOD().fnGET_VOD_DRAMA_EPISODE_DETAIL(fsVOD_NO, fnSEQ);
                if (!string.IsNullOrEmpty(data.fsURL_ID) && data.fsVIDEO_FROM.Equals("1"))
                {
                    data.fsM3U8_360P = _Vod_BLL.fnGET_M3U8_360P(data.fsURL_ID);
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = data }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD戲劇集數明細(By Content Id)
        /// </summary>
        /// <param name="fsCONTENT_ID_LI">Content Id</param>
        /// <returns></returns>
        [Route("GetVodDramaEpisodeDetailByContentId/{fsCONTENT_ID_LI}")]
        public HttpResponseMessage GetVodDramaEpisodeDetailByContentId(string fsCONTENT_ID_LI)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_DRAMA_EPISODE_DETAIL_BY_CONTENT_ID(fsCONTENT_ID_LI) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD戲劇推薦影片
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodDramaRelated/{fsVOD_NO}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodDramaRelated(string fsVOD_NO, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_DRAMA_RELATED(fsVOD_NO, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD綜藝類型
        /// </summary>
        ///  <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodShowType/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodShowType(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SHOW_TYPE(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD綜藝首頁資訊
        /// </summary>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodShowIndex/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodShowIndex(string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SHOW_INDEX(fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD綜藝首頁資訊V3
        /// </summary>
        /// <param name="fsTYPE">VOD類型代碼(00:不分類)</param>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodShowIndex3/{fsTYPE}/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodShowIndex3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SHOW_INDEX3(fsTYPE, fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD綜藝首頁資訊-手機用
        /// </summary>
        /// <returns></returns>
        [Route("GetMobileVodShowIndex")]
        [CacheOutput(ServerTimeSpan = 21600, AnonymousOnly = true)]
        public HttpResponseMessage GetMobileVodShowIndex()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SHOW_INDEX_MOBILE() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD綜藝集數資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetVodShowEpisode/{fsVOD_NO}")]
        public HttpResponseMessage GetVodShowEpisode(string fsVOD_NO)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SHOW_EPISODE(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD綜藝集數明細
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">VOD序號</param>
        /// <returns></returns>
        [Route("GetVodShowEpisodeDetail/{fsVOD_NO}/{fnSEQ:int}")]
        [ResponseType(typeof(clsVOD_EPISODE_DETAIL))]
        public HttpResponseMessage GetVodShowEpisodeDetail(string fsVOD_NO, int fnSEQ)
        {
            try
            {
                var data = new repVOD().fnGET_VOD_SHOW_EPISODE_DETAIL(fsVOD_NO, fnSEQ);
                if (!string.IsNullOrEmpty(data.fsURL_ID) && data.fsVIDEO_FROM.Equals("1"))
                {
                    data.fsM3U8_360P = _Vod_BLL.fnGET_M3U8_360P(data.fsURL_ID);
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = data }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD綜藝集數明細(By Content Id)
        /// </summary>
        /// <param name="fsCONTENT_ID_LI">Content Id</param>
        /// <returns></returns>
        [Route("GetVodShowEpisodeDetailByContentId/{fsCONTENT_ID_LI}")]
        public HttpResponseMessage GetVodShowEpisodeDetailByContentId(string fsCONTENT_ID_LI)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SHOW_EPISODE_DETAIL_BY_CONTENT_ID(fsCONTENT_ID_LI) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD綜藝推薦影片
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodShowRelated/{fsVOD_NO}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodShowRelated(string fsVOD_NO, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SHOW_RELATED(fsVOD_NO, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得直擊分類
        /// </summary>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodSceneCategory/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodSceneCategory(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SCENE_CATEGORY(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD直擊首頁資訊
        /// </summary>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodSceneIndex/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        public HttpResponseMessage GetVodSceneIndex(int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SCENE_INDEX(fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD直擊系列首頁資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodSceneSeriesIndex/{fsVOD_NO}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        public HttpResponseMessage GetVodSceneSeriesIndex(string fsVOD_NO, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SCENE_INDEX_SERIES(fsVOD_NO, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD直擊集數資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetVodSceneEpisode/{fsVOD_NO}")]
        public HttpResponseMessage GetVodceneEpisode(string fsVOD_NO)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SCENE_EPISODE(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD直擊集數段落資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">序號</param>
        /// <returns></returns>
        [Route("GetVodSceneEpisodePart/{fsVOD_NO}/{fnSEQ:int}")]
        public HttpResponseMessage GetVodSceneEpisodePart(string fsVOD_NO, int fnSEQ)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SCENE_EPISODE_PART(fsVOD_NO, fnSEQ) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD直擊集數段落明細
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">VOD序號</param>
        /// <param name="fsPART">段落</param>
        /// <returns></returns>
        [Route("GetVodSceneEpisodeDetail/{fsVOD_NO}/{fnSEQ:int}/{fsPART:alpha=A}")]
        public HttpResponseMessage GetVodSceneEpisodeDetail(string fsVOD_NO, int fnSEQ, string fsPART)
        {

            try
            {
                clsVOD_SCENE_EPISODE_DETAIL info = new clsVOD_SCENE_EPISODE_DETAIL();
                string part = fsPART;

                info = new repVOD().fnGET_VOD_SCENE_EPISODE_DETAIL(fsVOD_NO, fnSEQ, part);

                if (!string.IsNullOrEmpty(info.fsURL_ID))
                {
                    info.fsM3U8_360P = _Vod_BLL.fnGET_M3U8_360P(info.fsURL_ID);
                }
                if (info == null || string.IsNullOrEmpty(info.fsVOD_NO))
                {
                    try
                    {
                        var partlist = new repVOD().fnGET_VOD_SCENE_EPISODE_PART(fsVOD_NO, fnSEQ);

                        if (partlist != null)
                        {
                            part = partlist.lstPART[0].fsPART;
                        }
                        info = new repVOD().fnGET_VOD_SCENE_EPISODE_DETAIL(fsVOD_NO, fnSEQ, part);

                    }
                    catch (Exception e)
                    {
                    }

                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = info }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD直擊推薦影片
        /// </summary>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodSceneRelated/{fcREGION:alpha=L}")]
        public HttpResponseMessage GetVodSceneRelated(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SCENE_RELATED(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD文化教育類型
        /// </summary>
        ///  <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodEducationType/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodEducationType(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_EDUCATION_TYPE(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD文化教育首頁資訊V3
        /// </summary>
        /// <param name="fsTYPE">VOD類型代碼(00:不分類)</param>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodEducationIndex3/{fsTYPE}/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodEducationIndex3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_EDUCATION_INDEX3(fsTYPE, fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD文化教育首頁資訊-手機用
        /// </summary>
        /// <returns></returns>
        [Route("GetMobileVodEducationIndex")]
        [CacheOutput(ServerTimeSpan = 21600, AnonymousOnly = true)]
        public HttpResponseMessage GetMobileVodEducationIndex()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_EDUCATION_INDEX_MOBILE() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD文化教育集數資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetVodEducationEpisode/{fsVOD_NO}")]
        public HttpResponseMessage GetVodEducationEpisode(string fsVOD_NO)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_EDUCATION_EPISODE(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD文化教育集數明細
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">VOD序號</param>
        /// <returns></returns>
        [Route("GetVodEducationEpisodeDetail/{fsVOD_NO}/{fnSEQ:int}")]
        [ResponseType(typeof(clsVOD_EPISODE_DETAIL))]
        public HttpResponseMessage GetVodEducationEpisodeDetail(string fsVOD_NO, int fnSEQ)
        {
            try
            {
                var data = new repVOD().fnGET_VOD_EDUCATION_EPISODE_DETAIL(fsVOD_NO, fnSEQ);
                if (!string.IsNullOrEmpty(data.fsURL_ID) && data.fsVIDEO_FROM.Equals("1"))
                {
                    data.fsM3U8_360P = _Vod_BLL.fnGET_M3U8_360P(data.fsURL_ID);
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = data }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD文化教育推薦影片
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodEducationRelated/{fsVOD_NO}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodEducationRelated(string fsVOD_NO, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_EDUCATION_RELATED(fsVOD_NO, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得類型中所有VOD
        /// </summary>
        /// <param name="fsTYPE">VOD類型(01:戲劇、02:綜藝、03:直擊)</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodByType/{fsTYPE}/{fcREGION:alpha=L}")]
        public HttpResponseMessage GetVodByType(string fsTYPE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_BY_TYPE(fsTYPE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得所有VOD(BOX用)
        /// </summary>
        /// <param name="fsTYPE">VOD類型(01:戲劇、02:綜藝)</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetBoxVod/{fsTYPE}")]
        public HttpResponseMessage GetBoxVod(string fsTYPE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_ALL_BOX(fsTYPE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD網紅類型
        /// </summary>
        ///  <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodCelebrityType/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodCelebrityType(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_CELEBRITY_TYPE(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD網紅首頁資訊V3
        /// </summary>
        /// <param name="fsTYPE">VOD類型代碼(00:不分類)</param>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodCelebrityIndex3/{fsTYPE}/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodCelebrityIndex3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_CELEBRITY_INDEX3(fsTYPE, fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD網紅首頁資訊-手機用
        /// </summary>
        /// <returns></returns> 
        [Route("GetMobileVodCelebrityIndex")]
        [CacheOutput(ServerTimeSpan = 21600, AnonymousOnly = true)]
        public HttpResponseMessage GetMobileVodCelebrityIndex()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_CELEBRITY_INDEX_MOBILE() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD網紅集數資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetVodCelebrityEpisode/{fsVOD_NO}")]
        public HttpResponseMessage GetVodCelebrityEpisode(string fsVOD_NO)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_CELEBRITY_EPISODE(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD網紅集數明細
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">VOD序號</param>
        /// <returns></returns>
        [Route("GetVodCelebrityEpisodeDetail/{fsVOD_NO}/{fnSEQ:int}")]
        [ResponseType(typeof(clsVOD_EPISODE_DETAIL))]
        public HttpResponseMessage GetVodCelebrityEpisodeDetail(string fsVOD_NO, int fnSEQ)
        {
            try
            {
                var data = new repVOD().fnGET_VOD_CELEBRITY_EPISODE_DETAIL(fsVOD_NO, fnSEQ);
                if (!string.IsNullOrEmpty(data.fsURL_ID) && data.fsVIDEO_FROM.Equals("1"))
                {
                    data.fsM3U8_360P = _Vod_BLL.fnGET_M3U8_360P(data.fsURL_ID);
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = data }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD網推薦影片
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodCelebrityRelated/{fsVOD_NO}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodCelebrityRelated(string fsVOD_NO, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_CELEBRITY_RELATED(fsVOD_NO, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得網紅首頁推薦
        /// </summary>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetCelebrityPromo/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetCelebrityPromo(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_CELEBRITY_PROMO(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        ///取得網紅個人首頁
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetCelebrityHome/{fsVOD_NO}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetCelebrityHome(string fsVOD_NO)
        {
            try
            {
                var fnGET_CELEBRITY_HOME = new repVOD().fnGET_CELEBRITY_HOME(fsVOD_NO);

                if (fnGET_CELEBRITY_HOME == null)
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_CELEBRITY_HOME(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }



            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得網紅子集類別
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetVodCelebrityHomeType/{fsVOD_NO}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodCelebrityHomeType(string fsVOD_NO)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_CELEBRITY_HOME_TYPE(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得網紅個人首頁資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fsTYPE">VOD類型代碼(00:不分類)</param>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <returns></returns>
        [Route("GetVodCelebrityHomeIndex/{fsVOD_NO}/{fsTYPE}/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodCelebrityHomeIndex(string fsVOD_NO, string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_CELEBRITY_HOME_INDEX(fsVOD_NO, fsTYPE, fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得網紅個人推薦(子檔 類別)
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">VOD序號</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param> 
        /// <returns></returns>
        [Route("GetVodCelebrityHomeDetailPromo/{fsVOD_NO}/{fnSEQ:int}/{fcREGION:alpha=L}")]
        [ResponseType(typeof(clsVOD_CELEBRITY_HOME_DETAIL_PROMO))]
        public HttpResponseMessage GetVodCelebrityHomeDetailPromo(string fsVOD_NO, int fnSEQ, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_CELEBRITY_HOME_DETAIL_PROMO(fsVOD_NO, fnSEQ, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD電影類型
        /// </summary>
        ///  <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodMovieType/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodMovieType(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_MOVIE_TYPE(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD電影首頁資訊V3
        /// </summary>
        /// <param name="fsTYPE">VOD類型代碼(00:不分類)</param>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodMovieIndex3/{fsTYPE}/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodMovieIndex3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_MOVIE_INDEX3(fsTYPE, fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD電影首頁資訊-手機用
        /// </summary>
        /// <returns></returns> 
        [Route("GetMobileVodMovieIndex")]
        [CacheOutput(ServerTimeSpan = 21600, AnonymousOnly = true)]
        public HttpResponseMessage GetMobileVodMovieIndex()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_MOVIE_INDEX_MOBILE() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD電影集數資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetVodMovieEpisode/{fsVOD_NO}")]
        public HttpResponseMessage GetVodMovieEpisode(string fsVOD_NO)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_MOVIE_EPISODE(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD電影集數明細
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">VOD序號</param>
        /// <returns></returns>
        [Route("GetVodMovieEpisodeDetail/{fsVOD_NO}/{fnSEQ:int}")]
        [ResponseType(typeof(clsVOD_EPISODE_DETAIL))]
        public HttpResponseMessage GetVodMovieEpisodeDetail(string fsVOD_NO, int fnSEQ)
        {
            try
            {
                var data = new repVOD().fnGET_VOD_MOVIE_EPISODE_DETAIL(fsVOD_NO, fnSEQ);
                if (!string.IsNullOrEmpty(data.fsURL_ID) && data.fsVIDEO_FROM.Equals("1"))
                {
                    data.fsM3U8_360P = _Vod_BLL.fnGET_M3U8_360P(data.fsURL_ID);
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = data }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD電影推薦影片
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodMovieRelated/{fsVOD_NO}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodMovieRelated(string fsVOD_NO, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_MOVIE_RELATED(fsVOD_NO, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// 取得VOD動漫類型
        /// </summary>
        ///  <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodAnimeType/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodAnimeType(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_ANIME_TYPE(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD動漫首頁資訊V3
        /// </summary>
        /// <param name="fsTYPE">VOD類型代碼(00:不分類)</param>
        /// <param name="fcSEARCH_TYPE">顯示類型(hot:熱門、new:最新)</param>
        /// <param name="fnINDEX">開始筆數</param>
        /// <param name="fnPAGE_SIZE">取幾筆</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodAnimeIndex3/{fsTYPE}/{fcSEARCH_TYPE}/{fnINDEX:int}/{fnPAGE_SIZE:int}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodAnimeIndex3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_ANIME_INDEX3(fsTYPE, fcSEARCH_TYPE, fnINDEX, fnPAGE_SIZE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD動漫首頁資訊-手機用
        /// </summary>
        /// <returns></returns> 
        [Route("GetMobileVodAnimeIndex")]
        [CacheOutput(ServerTimeSpan = 21600, AnonymousOnly = true)]
        public HttpResponseMessage GetMobileVodAnimeIndex()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_ANIME_INDEX_MOBILE() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD動漫集數資訊
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <returns></returns>
        [Route("GetVodAnimeEpisode/{fsVOD_NO}")]
        public HttpResponseMessage GetVodAnimeEpisode(string fsVOD_NO)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_ANIME_EPISODE(fsVOD_NO) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD動漫集數明細
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fnSEQ">VOD序號</param>
        /// <returns></returns>
        [Route("GetVodAnimeEpisodeDetail/{fsVOD_NO}/{fnSEQ:int}")]
        [ResponseType(typeof(clsVOD_EPISODE_DETAIL))]
        public HttpResponseMessage GetVodAnimeEpisodeDetail(string fsVOD_NO, int fnSEQ)
        {
            try
            {
                var data = new repVOD().fnGET_VOD_ANIME_EPISODE_DETAIL(fsVOD_NO, fnSEQ);
                if (!string.IsNullOrEmpty(data.fsURL_ID) && data.fsVIDEO_FROM.Equals("1"))
                {
                    data.fsM3U8_360P = _Vod_BLL.fnGET_M3U8_360P(data.fsURL_ID);
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = data }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD動漫推薦影片
        /// </summary>
        /// <param name="fsVOD_NO">VOD編號</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodAnimeRelated/{fsVOD_NO}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodAnimeRelated(string fsVOD_NO, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_ANIME_RELATED(fsVOD_NO, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// VOD清單
        /// </summary>
        /// <param name="Type">VOD類別</param>
        /// <param name="SubType">次類別00表示全部</param>
        /// <param name="Index">頁數</param>
        /// <param name="Size">數量</param>
        /// <returns></returns>
        [Route("GetVodIndex/{Type}/{SubType}/{Index:int}/{Size:int}")]
        public StdResp<List<GetVodIndex_Res>> GetVodIndex(string Type,string SubType,int? Index,int? Size)
        {
            StdResp<List<GetVodIndex_Res>> stdResp = new StdResp<List<GetVodIndex_Res>>();


            if (Index == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (Size == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
          
            if (Index.Value < 0)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (Size.Value < 0)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }

            stdResp.Data = _Vod_BLL.GetVodIndex(Type, SubType, Index.Value, Size.Value);

            return stdResp;
        }
        /// <summary>
        /// VOD主檔Type
        /// </summary>
        /// <param name="Type">VOD類別</param>
        /// <returns></returns>
        [Route("GetVodType/{Type}/{Region:alpha=L}")]
        public StdResp<List<GetVodType_Res>> GetVodType(string Type, string Region)
        {
            StdResp<List<GetVodType_Res>> stdResp = new StdResp<List<GetVodType_Res>>();


            stdResp.Data = _Vod_BLL.GetVodType(Type, Region);
            return stdResp;
        }
        /// <summary>
        /// VOD主檔info
        /// </summary>
        /// <param name="VODID">VODID</param>
        /// <returns></returns>
        [Route("GetVodEpisode/{VODID}")]
        public StdResp<GetVodEpisode_Res> GetVodEpisode(string VODID)
        {
            StdResp<GetVodEpisode_Res> stdResp = new StdResp<GetVodEpisode_Res>();
            stdResp.Data = _Vod_BLL.GetVodEpisode(VODID);
            return stdResp;
        }
        /// <summary>
        /// VOD 所有影片資訊
        /// </summary>
        /// <param name="VODID">VODID</param>
        /// <returns></returns>
        [Route("GetVodEpisodeList/{VODID}")]
        public StdResp<GetVodEpisodeList_Res> GetVodEpisodeList(string VODID)
        {
            StdResp<GetVodEpisodeList_Res> stdResp = new StdResp<GetVodEpisodeList_Res>();
            stdResp.Data = _Vod_BLL.GetVodEpisodeList(VODID);
            return stdResp;
        }
        /// <summary>
        /// VOD子檔info
        /// </summary>
        /// <param name="VODID">VODID</param>
        /// <param name="Seq">Seq</param>
        /// <returns></returns>
        [Route("GetVodDetail/{VODID}/{Seq}")]
        public StdResp<GetVodDetail_Res> GetVodDetail(string VODID, string Seq)
        {
            StdResp<GetVodDetail_Res> stdResp = new StdResp<GetVodDetail_Res>();
            stdResp.Data = _Vod_BLL.GetVodDetail(VODID, Seq);
            return stdResp;
        }
        /// <summary>
        /// VOD子檔info
        /// </summary>
        /// <param name="VODID">VODID</param>
        /// <param name="Region">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetVodRelated/{VODID}/{Region:alpha=L}")]
        public StdResp<List<GetVodRelated_Res>> GetVodRelated(string VODID,string Region)
        {
            StdResp<List<GetVodRelated_Res>> stdResp = new StdResp<List<GetVodRelated_Res>>();
            stdResp.Data = _Vod_BLL.GetVodRelated(VODID, Region);
            return stdResp;
        }

        /// <summary>
        /// RSS
        /// </summary>
        /// <returns></returns>
        [Route("WorkExceptionReJOB")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp<int> WorkExceptionReJOB()
        {
            try
            {
                if (Request.Headers == null)
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.Contains("apikey"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.GetValues("apikey").First().Equals("job"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }


                StdResp<int> stdResp = new StdResp<int>();
                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    stdResp.Data = _Vod_BLL.WorkExceptionRe();

                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException e)
            {
                throw e;
            }
        }

    }
}
