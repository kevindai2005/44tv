﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;
//using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;
using System.Net.Http.Headers;
using System.Collections.Specialized;
using System.Web.Configuration;
using FourgTV_OTT2.BLL.Model.Model;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("HciVod")]
    public class HciVodController : ApiController
    {
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];
        /// <summary>
        /// 取得VOD URL
        /// </summary>
        /// <param name="clsGET_VOD_URL_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetVodUrl")]
        public HttpResponseMessage PostVodUrl(clsGET_VOD_URL_ARGS clsGET_VOD_URL_ARGS)
        {
            try
            {
                string fsIP = HttpContext.Current.Request.UserHostAddress;

                UserInfo clsUSER_DATA = null;

                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsGET_VOD_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_VOD_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                }

                //寫LOG
                new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
                {
                    fsFROM = "3",
                    fsNAME = "VOD",
                    fsASSET_ID = clsGET_VOD_URL_ARGS.fsASSET_ID,
                    fsIP = HttpContext.Current.Request.UserHostAddress,
                    fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                    fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
                    fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
                });

                clsGET_VOD_URL clsGET_VOD_URL = new clsGET_VOD_URL();
                clsGET_VOD_URL.flstURLs = new List<string>();
                clsGET_VOD_URL.lstTIME_CODE = new List<int>();

                //測試環境才用
                if (int.Parse(fcIS_PRODUCTION) == 0)
                {
                    if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }
                else
                {
                    if (fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }

                //判斷是LiTV上架或FTV上架
                var clsVOD_FROM = new repVOD().fnGET_VOD_FROM(clsGET_VOD_URL_ARGS.fsASSET_ID);

                clsGET_VOD_URL.fsHEAD_FRAME = clsVOD_FROM.fsHEAD_FRAME;

                //判斷是否為台灣的IP或允許海外播放
                switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsVOD_FROM.fcOVERSEAS, clsVOD_FROM.lstEXCEPT_COUNTRY))
                {
                    case "IsAllowArea"://允許播放
                        break;
                    case "OVERSEAS"://不允許海外
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "EXCEPT_COUNTRY"://海外限制不能播放的國家
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "scene")
                {
                    //直擊
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                }
                else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "education")
                {

                    //文化教育
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }

                }
                else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "movie")
                {

                    //電影
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }

                }
                else if (clsGET_VOD_URL_ARGS.fsMEDIA_TYPE == "celebrity")
                {

                    //網路紅什麼
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }

                }
                else
                {

                    //戲劇/綜藝
                    //判斷是否已登入
                    if (clsUSER_DATA != null)
                    {
                        //取得會員VOD權限
                        var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);
                        //戲劇綜藝由LITV上傳的沒有中華m3u8
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        if (clsVOD_FROM.fsFROM == "FTV")
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        }
                        else
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsGET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP));
                        }
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR));
                        if (clsVOD_FROM.fsFROM == "FTV")
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsGET_VOD_URL_ARGS.fsASSET_ID, fsIP) : clsM3U8.GetTypeCVodUrl(false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsGET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP));
                        }
                        else
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, false, clsGET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsGET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP));
                        }
                        clsGET_VOD_URL.fbPLAY_AD = true;
                    }
                }

                clsGET_VOD_URL.lstTIME_CODE = new repVOD().fnGET_VOD_TIME_CODE(clsGET_VOD_URL_ARGS.fsASSET_ID);

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_VOD_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
    }
}
