﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AutoMapper;
//using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.FilterAttribute;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.Model.Model;
using Newtonsoft.Json;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Log")]
    public class LogController : ApiController
    {
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];
        /// <summary>
        /// 新增LOG
        /// </summary>
        /// <returns></returns>
        [Route("AddLog")]
        public HttpResponseMessage PostAddLog(clsLOG_ARGS clsLOG_ARGS)
        {
            try
            {
                if (clsLOG_ARGS == null || clsLOG_ARGS.fsTYPE == null || clsLOG_ARGS.fsNAME == null || clsLOG_ARGS.fsCONTENT == null || clsLOG_ARGS.fsCREATED_BY == null)
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false}, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                clsLOG_ARGS.fsIP = HttpContext.Current.Request.UserHostAddress;
                string fsRESULT = new repLOG().fnINSERT_LOG(clsLOG_ARGS);
                if (string.IsNullOrEmpty(fsRESULT))
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                else
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        ///// <summary>
        ///// 新增串流BUFFER LOG
        ///// </summary>
        ///// <returns></returns>
        //[Route("AddStreamBufferLog")]
        //[ResponseType(typeof(clsLOG_BUFFER_TIME_REQ))]
        //[ValidationActionFilter]
        //public HttpResponseMessage PostAddStreamBufferLog(clsLOG_BUFFER_TIME_REQ req)
        //{
        //    try
        //    {
        //        var res = clsTOOL.ConvertModelToModel<clsLOG_BUFFER_TIME_REQ, clsLOG_BUFFER_TIME_CONDITION>(req);
        //        res.fsIP = HttpContext.Current.Request.UserHostAddress;
        //        res.fdCreated_Date = DateTime.Now;

        //        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        //        res.fdBufferStart = dtDateTime.AddSeconds(req.fnBufferStart).ToLocalTime();

        //        new repLOG().fnINSERT_BUFFER_TIME_LOG(res);
        //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
        //    }
        //}
        /// <summary>
        /// 新增串流BUFFER LOG
        /// </summary>
        /// <returns></returns>
        [Route("AddStreamBufferLog")]
        //[ResponseType(typeof(clsLOG_BUFFER_TIME_REQ))]
        // [ValidationActionFilter]
        [HttpGet]
        //[AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AddStreamBufferLog(
            string fsEvent,
            string fsAsset_Name,
            string fsAsset_ID,
             int? fnBitrate_Type,
             int? fnNetwork_Type,
             int? fnBufferTime,
             string fsOS,
              string fsOS_Version,
              string fsDevice_Vendor,
               string fsDevice_Type,
               string fsBrowser,
               string fsBrowser_Version,
                string fsDesc,
                long fnBufferStart

            )
        {
            try
            {



                //string fsAsset_Name = HttpContext.Current.Request.Params["fsAsset_Name"];

                //string fsEvent = string.Empty;
                //string fsAsset_Name = string.Empty;
                //string fsAsset_ID = string.Empty;
                //int fnBitrate_Type = 0;
                //int fnNetwork_Type = 0;
                //int fnBufferTime = 0;
                //string fsOS = string.Empty;
                //string fsOS_Version = string.Empty;
                //string fsDevice_Vendor = string.Empty;
                //string fsDevice_Type = string.Empty;
                //string fsBrowser = string.Empty;
                //string fsBrowser_Version = string.Empty;
                //string fsDesc = string.Empty;
                //int fnBufferStart = 0;

                clsLOG_BUFFER_TIME_REQ req = new clsLOG_BUFFER_TIME_REQ();
                if (HttpContext.Current.Request.RequestType == "GET")
                {

                   
                    req.fsEvent = fsEvent;
                    req.fsAsset_Name = fsAsset_Name;
                    req.fsAsset_ID = fsAsset_ID;
                    req.fnBitrate_Type = fnBitrate_Type;
                    req.fnNetwork_Type = fnNetwork_Type;
                    req.fnBufferTime = fnBufferTime;
                    req.fsOS = fsOS;
                    req.fsOS_Version = fsOS_Version;
                    req.fsDevice_Vendor = fsDevice_Vendor;
                    req.fsDevice_Type = fsDevice_Type;
                    req.fsBrowser = fsBrowser;
                    req.fsBrowser_Version = fsBrowser_Version;
                    req.fsDesc = fsDesc;
                    req.fnBufferStart = fnBufferStart;

               
                    //fsEvent = HttpContext.Current.Request.QueryString["fsEvent"];
                    //fsAsset_Name = HttpContext.Current.Request.QueryString["fsAsset_Name"];
                    //fsAsset_ID = HttpContext.Current.Request.QueryString["fsAsset_ID"];
                    //fnBitrate_Type = HttpContext.Current.Request.QueryString["fnBitrate_Type"] == null ? 0 : int.Parse(HttpContext.Current.Request.QueryString["fnBitrate_Type"]);
                    //fnNetwork_Type = HttpContext.Current.Request.QueryString["fnNetwork_Type"] == null ? 0 : int.Parse(HttpContext.Current.Request.QueryString["fnNetwork_Type"]);
                    //fnBufferTime = HttpContext.Current.Request.QueryString["fnBufferTime"] == null ? 0 : int.Parse(HttpContext.Current.Request.QueryString["fnBufferTime"]);
                    //fsOS = HttpContext.Current.Request.QueryString["fsOS"];
                    //fsOS_Version = HttpContext.Current.Request.QueryString["fsOS_Version"];
                    //fsDevice_Vendor = HttpContext.Current.Request.QueryString["fsDevice_Vendor"];
                    //fsDevice_Type = HttpContext.Current.Request.QueryString["fsDevice_Type"];
                    //fsBrowser = HttpContext.Current.Request.QueryString["fsBrowser"];
                    //fsBrowser_Version = HttpContext.Current.Request.QueryString["fsBrowser_Version"];
                    //fsDesc = HttpContext.Current.Request.QueryString["fsDesc"];
                    //fnBufferStart = HttpContext.Current.Request.QueryString["fnBufferStart"] == null ? 0 : int.Parse(HttpContext.Current.Request.QueryString["fnBufferStart"]);

                }


              


                var res = clsTOOL.ConvertModelToModel<clsLOG_BUFFER_TIME_REQ, clsLOG_BUFFER_TIME_CONDITION>(req);
                res.fsIP = HttpContext.Current.Request.UserHostAddress;
                res.fdCreated_Date = DateTime.Now;

                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                res.fdBufferStart = dtDateTime.AddSeconds(req.fnBufferStart).ToLocalTime();

                new repLOG().fnINSERT_BUFFER_TIME_LOG(res);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 新增串流BUFFER LOG
        /// </summary>
        /// <returns></returns>
        [Route("AddStreamBufferLog")]
        //[ResponseType(typeof(clsLOG_BUFFER_TIME_REQ))]
        // [ValidationActionFilter]
        [HttpPost]
        //[AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AddStreamBufferLog([FromBody]clsLOG_BUFFER_TIME_REQ req
          
            )
        {
            try
            {



                //string fsAsset_Name = HttpContext.Current.Request.Params["fsAsset_Name"];

                //string fsEvent = string.Empty;
                //string fsAsset_Name = string.Empty;
                //string fsAsset_ID = string.Empty;
                //int fnBitrate_Type = 0;
                //int fnNetwork_Type = 0;
                //int fnBufferTime = 0;
                //string fsOS = string.Empty;
                //string fsOS_Version = string.Empty;
                //string fsDevice_Vendor = string.Empty;
                //string fsDevice_Type = string.Empty;
                //string fsBrowser = string.Empty;
                //string fsBrowser_Version = string.Empty;
                //string fsDesc = string.Empty;
                //int fnBufferStart = 0;



                if (req == null)
                {

                    Request.Content.ReadAsStreamAsync().Result.Seek(0, System.IO.SeekOrigin.Begin);
                    string content = Request.Content.ReadAsStringAsync().Result;

                    //轉成物件
                    req = JsonConvert.DeserializeObject<clsLOG_BUFFER_TIME_REQ>(content);
                }



                var res = clsTOOL.ConvertModelToModel<clsLOG_BUFFER_TIME_REQ, clsLOG_BUFFER_TIME_CONDITION>(req);
                res.fsIP = HttpContext.Current.Request.UserHostAddress;
                res.fdCreated_Date = DateTime.Now;

                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                res.fdBufferStart = dtDateTime.AddSeconds(req.fnBufferStart).ToLocalTime();

                new repLOG().fnINSERT_BUFFER_TIME_LOG(res);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }



        /// <summary>
        /// 新增連續觀看11分鐘LOG
        /// </summary>
        /// <returns></returns>
        [Header(headersConfig = "4GTV_AUTH,string")]
        [Route("AddActivity11MinuteLog")]
        [ResponseType(typeof(clsLOG_ACTIVITY_11MINUTE_REQ))]
        [ValidationActionFilter]
        public HttpResponseMessage PostAddActivity11MinuteLog(clsLOG_ACTIVITY_11MINUTE_REQ req)
        {
            try
            {
                UserInfo clsUSER_DATA = new UserInfo();

                if (req == null)
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                if (req.clsAPP_IDENTITY_VALIDATE_ARUS != null)
                {
                    if (clsSECURITY.IsLogin(req.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, req.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                        {
                            clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(req.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, req.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
                        }
                    }

                }
                if (req.clsIDENTITY_VALIDATE_ARUS != null)
                {
                    if (clsSECURITY.IsLogin(req.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(req.clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                    }
                }

                if (clsUSER_DATA.fsACCOUNT_ID == null || clsUSER_DATA.fsUSER == null)
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                DateTime dateTime = DateTime.Now;
                DateTime fdCreated_Date_Start = dateTime.Date;
                DateTime fdCreated_Date_End = dateTime.Date.AddDays(1);


                var res = new clsLOG_ACTIVITY_11MINUTE_CONDITION();
                res.fsACCOUNT_ID = clsUSER_DATA.fsACCOUNT_ID;
                res.fsIP = HttpContext.Current.Request.UserHostAddress;
                res.fdCreated_Date = dateTime;
                res.fdCreated_Date_Start = fdCreated_Date_Start;
                res.fdCreated_Date_End = fdCreated_Date_End;

                if (new repLOG().fnGET_ACTIVITY_11MINUTE_LOG_TODAY_INFO(res) == null)
                {
                    new repLOG().fnINSERT_ACTIVITY_11MINUTE_LOG(res);
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
    }
}
