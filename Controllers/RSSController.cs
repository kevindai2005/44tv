﻿using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.DAL.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Syndication;
using System.Text;
using System.Transactions;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("RSS")]
    public class RSSController : ApiController
    {
        RSS_BLL _RSS_BLL = new RSS_BLL();
        /// <summary>
        /// FTV
        /// </summary>
        /// <returns></returns>
        [Route("RSS")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp<List<string>> RSS()
        {
            try
            {
                string aaa = string.Empty;
                List<string> sss = new List<string>();
                List<string> nnn = new List<string>();
                StdResp<List<string>> stdResp = new StdResp<List<string>>();

                //  _RSS_BLL.RSStoPool();

                //C:\Users\Angus_Hsieh\Desktop\txt

                //  StreamReader str = new StreamReader(@"C:\Users\Angus_Hsieh\Desktop\txt\VideoList.txt");

                var str = System.IO.File.ReadAllLines(@"C:\Users\Angus_Hsieh\Desktop\txt\vvvv.txt", Encoding.Default);

                string fsLOG_PATH = @"C:\Users\Angus_Hsieh\Desktop\txt\re.txt";

                tbmARC_VIDEO_DAL _tbmARC_VIDEO_DAL = new tbmARC_VIDEO_DAL();



                foreach (string line in str)
                {
                    var vod = line.Split('\t');

                    // line.Trim();

                    //var fnGET_tbmARC_VIDEOInfoByfsCDNFILE_ID = _tbmARC_VIDEO_DAL.fnGET_tbmARC_VIDEOInfoByfsCDNFILE_ID(line);
                    //if (fnGET_tbmARC_VIDEOInfoByfsCDNFILE_ID!=null)
                    //{
                    //    if (!fnGET_tbmARC_VIDEOInfoByfsCDNFILE_ID.fcSTATUS.Equals("Y"))
                    //    {
                    //        fnGET_tbmARC_VIDEOInfoByfsCDNFILE_ID = null;
                    //    }
                    //}
                    //var fnGET_tbmARC_VIDEOInfoByfsASSET_ID_LI = _tbmARC_VIDEO_DAL.fnGET_tbmARC_VIDEOInfoByfsASSET_ID_LI(line);
                    //if (fnGET_tbmARC_VIDEOInfoByfsASSET_ID_LI != null)
                    //{
                    //    if (!fnGET_tbmARC_VIDEOInfoByfsASSET_ID_LI.fcSTATUS.Equals("Y"))
                    //    {
                    //        fnGET_tbmARC_VIDEOInfoByfsASSET_ID_LI = null;
                    //    }
                    //}

                    //if (fnGET_tbmARC_VIDEOInfoByfsCDNFILE_ID == null && fnGET_tbmARC_VIDEOInfoByfsASSET_ID_LI == null)
                    //{z
                    //    nnn.Add(line);
                    //}
                    //else
                    //{

                    //    sss.Add(line);
                    //}





                    //   aaa += "'" + line.Trim().Replace("4gtv-", "") + "'" + ",";

                    clsM3U8 _clsM3U8 = new clsM3U8();

                    string CDNID = vod[5];

                    if (CDNID.IndexOf("_1500K") > 0)
                    {
                        CDNID = vod[5].Replace("_1500K", "");
                    }
                    else if (CDNID.IndexOf("_800K") > 0)
                    {
                        CDNID = vod[5].Replace("_800K", "");

                        var s = CDNID.Split('-');

                        CDNID = s[0] + "-" + s[1];
                    }
                    else
                    {
                        CDNID = vod[5];

                    }

                    var m3u8 = clsM3U8.GetLiTVVodUrl(vod[3], false, "pc", vod[4], CDNID, "61.216.147.98", -1);

                    try
                    {
                        using (WebClient wclient = new WebClient())
                        {
                            string RSSData = wclient.DownloadString(m3u8);

                            System.IO.File.AppendAllText(fsLOG_PATH, vod[0] + "\t" + vod[1] + "\t" + vod[2] + "\t" + vod[3] + "\t" + vod[4] + "\t" + CDNID + "\t" + "OK" + "\r\n");

                        }

                    }
                    catch (Exception e)
                    {
                        System.IO.File.AppendAllText(fsLOG_PATH, vod[0] + "\t" + vod[1] + "\t" + vod[2] + "\t" + vod[3] + "\t" + vod[4] + "\t" + CDNID + "\t" + e.Message + "\r\n");

                    }
                }

                stdResp.Data = sss;

                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// RSS
        /// </summary>
        /// <returns></returns>
        [Route("RSSTransfer4GTVJOB")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp<int> RSSTransfer4GTVJOB()
        {
            try
            {
                if (Request.Headers == null)
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.Contains("apikey"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.GetValues("apikey").First().Equals("job"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }


                StdResp<int> stdResp = new StdResp<int>();
                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    stdResp.Data = _RSS_BLL.RSS();

                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException e)
            {
                throw e;
            }
        }

    }
}
