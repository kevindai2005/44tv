﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Http;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Req.OVO;
using System.Transactions;
using FourgTV_OTT2.Api2.SwaggerHeader;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("OVO")]
    public class OVOController : ApiController
    {

        OVO_BLL _OVO_BLL = new OVO_BLL();



        [Route("LinkMember")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostLinkMember(LinkMemberReq req)
        {
            try
            {
                OVO_BLL.CheckHeader(Request);

                OVO_BLL.CheckIpInWhiteList(Request);

                OVO_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    OVO_BLL.LinkMember(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        [Route("InsertOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostInsertOrder(InsertOrderReq req)
        {
            try
            {
                OVO_BLL.CheckHeader(Request);

                OVO_BLL.CheckIpInWhiteList(Request);

                OVO_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    _OVO_BLL.InsertOrder(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        [Route("CancelOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp PostCancelOrder(CancelOrderReq req)
        {
            try
            {
                OVO_BLL.CheckHeader(Request);

                OVO_BLL.CheckIpInWhiteList(Request);

                OVO_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    OVO_BLL.CancelOrder(req);

                    _TransactionScope.Complete();

                    return new StdResp();
                }
            }
            catch (MyException ex)
            {
                throw ex;
            }
        }
    }
}