﻿using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Req.WSAPC;
using FourgTV_OTT2.BLL.Model.Res.WSAPC;
using FourgTV_OTT2.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Http;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("WSAPC")]
    public class WSAPCController : ApiController
    {


        WSAPC_BLL _WSAPC_BLL = new WSAPC_BLL();

        /// <summary>
        /// B01.新增會員
        /// </summary>
        /// <param name="req">新增會員</param>
        /// <response code="EA02">會員帳號已存在並且已綁定響迴家帳號。</response>
        /// <returns></returns>
        [Route("InsertMember")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp<string> InsertMember(InsertWSAPCMember_Req req)
        {

            try
            {
                //CheckHeader
                WSAPC_BLL.CheckHeader(Request);

                WSAPC_BLL.CheckIpInWhiteList(Request);

                //CheckParam
                WSAPC_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp<string> stdResp = new StdResp<string>();
                    stdResp.Data = _WSAPC_BLL.InsertMember(req);

                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// B03.修改會員密碼
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Route("SetAccountPassword")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp CancelOrder(SetWSAPCAccountPassword_Req req)
        {

            try
            {
                //CheckHeader
                WSAPC_BLL.CheckHeader(Request);

                WSAPC_BLL.CheckIpInWhiteList(Request);

                WSAPC_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp stdResp = new StdResp();
                    _WSAPC_BLL.SetAccountPassword(req);

                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// C01.新增訂單
        /// </summary>
        /// <param name="req">新增訂單</param>
        /// <returns></returns>
        [Route("InsertOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp InsertOrder(InsertWSAPCOrder_Req req)
        {

            try
            {
                WSAPC_BLL.CheckHeader(Request);

                WSAPC_BLL.CheckIpInWhiteList(Request);

                WSAPC_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp stdResp = new StdResp();
                     _WSAPC_BLL.InsertOrder(req);

                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException e)
            {
                throw e;
            }

     
        }


        /// <summary>
        /// C02.終止訂單
        /// </summary>
        /// <param name="req">新增訂單</param>
        /// <returns></returns>
        [Route("CancelOrder")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp CancelOrder(CancelWSAPCOrder_Req req)
        {

            try
            {
                //CheckHeader
                WSAPC_BLL.CheckHeader(Request);

                WSAPC_BLL.CheckIpInWhiteList(Request);

                WSAPC_BLL.CheckParam(req);

                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp stdResp = new StdResp();
                    _WSAPC_BLL.CancelOrder(req);

                    _TransactionScope.Complete();
                    return stdResp;
                }
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// C04.訂單查詢
        /// </summary>
        /// <param name="req">訂單查詢</param>
        /// <returns></returns>
        [Route("GetOrderInfo")]
        [Header(headersConfig = "From,string")]
        [HttpPost]
        public StdResp<List<GetWSAPCOrderInfo_Res>> GetOrderInfo(GetWSAPCOrderInfo_Req req)
        {
            try
            {
                WSAPC_BLL.CheckHeader(Request);

                WSAPC_BLL.CheckIpInWhiteList(Request);

                WSAPC_BLL.CheckParam(req);

                StdResp<List<GetWSAPCOrderInfo_Res>> stdResp = new StdResp<List<GetWSAPCOrderInfo_Res>>();  
                stdResp.Data = _WSAPC_BLL.GetWSAPCOrderInfo(req.fsUSER_ID);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
    }
}
