﻿using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using FourgTV_OTT2.BLL.Model.Res.Article;
using FourgTV_OTT2.BLL.Model.Req.Article;
using FourgTV_OTT2.API.BLL;
using System.Transactions;
using static FourgTV_OTT2.Common.Setting.EnumSetting;
using WebApi.OutputCache.V2;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using System.Net.Http.Formatting;
using System.Xml;
using System.Text;
using System.ServiceModel.Syndication;
using System.Xml.Linq;
using System.Globalization;
using FourgTV_OTT2.BLL.Model.Req.FTV;
using FourgTV_OTT2.BLL.Module;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Article")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ArticleController : ApiController
    {
        Article_BLL _Article_BLL = new Article_BLL();
        ArticleModule _ArticleModule = new ArticleModule();
        public string ClientIP
        {
            get
            {
                var obj = LogHelper.RequestVariables[LogRequestVariableType.ClientIP];
                return obj == null ? "" : obj.ToString();
            }
            set
            {
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.ClientIP, value);
            }
        }

        /// <summary>
        /// FTV
        /// </summary>
        /// <returns></returns>
        [Route("FTVNewsTransfer4GTVJOB")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp<int> FTVNewsTransfer4GTVJOB(FTVNewsTransfer4GTVJOB_Req req)
        {
            try
            {
                if (Request.Headers == null)
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.Contains("apikey"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.GetValues("apikey").First().Equals("job"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.LastTime == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(req.LastTime.Value).ToLocalTime();

                StdResp<int> stdResp = new StdResp<int>();

                List<string> key = new List<string> { "美食", "娛樂", "生活", "健康", "氣象", "財經", "體育" };

                stdResp.Data = _Article_BLL.FTVNews(key, dateTime).Count();

                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// ArticlePoolTransfer4GTVJOB
        /// </summary>
        /// <returns></returns>
        [Route("ArticlePoolTransfer4GTVJOB")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp<int> ArticlePoolTransfer4GTVJOB(ArticlePoolTransfer4GTVJOB_Req req)
        {
            try
            {
                if (Request.Headers == null)
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.Contains("apikey"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.GetValues("apikey").First().Equals("job"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.LastTime == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(req.LastTime.Value).ToLocalTime();

                StdResp<int> stdResp = new StdResp<int>();
                stdResp.Data = _Article_BLL.ArticlePoolTransfer4GTV(dateTime);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// ArticlePoolExTransfer4GTVJOB
        /// </summary>
        /// <returns></returns>
        [Route("ArticlePoolExTransfer4GTVJOB")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp ArticlePoolExTransfer4GTVJOB()
        {
            try
            {
                if (Request.Headers == null)
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.Contains("apikey"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.GetValues("apikey").First().Equals("job"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }


                StdResp stdResp = new StdResp();
                _Article_BLL.ArticlePoolExTransfer4GTV();
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// ArticleTransferToDAM
        /// </summary>
        /// <returns></returns>
        [Route("ArticleTransferToDAM")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp<int> ArticleTransferToDAM(ArticlePoolTransfer4GTVJOB_Req req)
        {
            try
            {
                if (Request.Headers == null)
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.Contains("apikey"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.GetValues("apikey").First().Equals("job"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.LastTime == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                DateTime dateTime = new DateTime(1970, 01, 01, 0, 0, 0).AddSeconds(req.LastTime.Value).ToLocalTime();

                StdResp<int> stdResp = new StdResp<int>();
                stdResp.Data = _Article_BLL.ArticleTransferDAM(dateTime);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// ArticleTransferDAMJOB
        /// </summary>
        /// <returns></returns>
        [Route("ArticleTransferDAMJOB")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp<int> ArticleTransferDAMJOB(ArticleTransferDAMJOB_Req req)
        {
            try
            {
                if (Request.Headers == null)
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.Contains("apikey"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (!Request.Headers.GetValues("apikey").First().Equals("job"))
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.LastTime == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                DateTime dateTime = new DateTime(1970, 01, 01, 0, 0, 0).AddSeconds(req.LastTime.Value).ToLocalTime().AddMinutes(-30);

                StdResp<int> stdResp = new StdResp<int>();
                stdResp.Data = _Article_BLL.ArticleUpdateDAM(dateTime);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 更新總瀏覽
        /// </summary>
        /// <returns></returns>
        [Route("UpdateArticlePageviewTotalJOB")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp UpdateArticlePageviewTotalJOB()
        {
            if (Request.Headers == null)
            {
                throw new MyException(ResponseCodeEnum.HeaderError);
            }
            if (!Request.Headers.Contains("apikey"))
            {
                throw new MyException(ResponseCodeEnum.HeaderError);
            }
            if (!Request.Headers.GetValues("apikey").First().Equals("job"))
            {
                throw new MyException(ResponseCodeEnum.HeaderError);
            }
            StdResp stdResp = new StdResp();
            _Article_BLL.UpdateArticlePageviewTotal();

            return stdResp;


        }

        /// <summary>
        /// 取得文章資訊
        /// </summary>
        /// <param name="fsArticleID">文章ID</param>
        /// <returns></returns>
        [Route("GetArticleDetail")]
        [HttpGet]
        //[CacheOutput(ServerTimeSpan = 10, AnonymousOnly = true)]
        public StdResp<GetArticleDetail_Res> GetArticleDetail(string fsArticleID)
        {
            using (TransactionScope _TransactionScope = new TransactionScope())
            {
                StdResp<GetArticleDetail_Res> stdResp = new StdResp<GetArticleDetail_Res>();
                stdResp.Data = _Article_BLL.GetArticleDetail(fsArticleID);
                _TransactionScope.Complete();
                return stdResp;
            }

        }


        /// <summary>
        /// 取得文章資訊(搜尋用)
        /// </summary>
        /// <param name="fsArticleID">文章ID</param>
        /// <returns></returns>
        [Route("GetArticleDetailForSearch")]
        [HttpGet]
        //[CacheOutput(ServerTimeSpan = 10, AnonymousOnly = true)]
        public StdResp<GetArticleDetailForSearch_Res> GetArticleDetailForSearch(string fsArticleID)
        {
            using (TransactionScope _TransactionScope = new TransactionScope())
            {
                StdResp<GetArticleDetailForSearch_Res> stdResp = new StdResp<GetArticleDetailForSearch_Res>();
                stdResp.Data = _Article_BLL.GetArticleDetailForSearch(fsArticleID);
                _TransactionScope.Complete();
                return stdResp;
            }
        }

        /// <summary>                                                                                                                                                                                                                                                                      
        /// 取得文章資訊(搜尋用)
        /// </summary>
        /// <param name="fsArticleID">文章ID</param>
        /// <returns></returns>
        [Route("GetArticleDetailForSearchList")]
        [HttpPost]
        //[CacheOutput(ServerTimeSpan = 10, AnonymousOnly = true)]
        public StdResp<List<GetArticleDetailForSearch_Res>> GetArticleDetailForSearchList(GetArticleDetailForSearch_Req req)
        {
            using (TransactionScope _TransactionScope = new TransactionScope())
            {
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.ArticleIDNotFound);
                }

                StdResp<List<GetArticleDetailForSearch_Res>> stdResp = new StdResp<List<GetArticleDetailForSearch_Res>>();
                List<GetArticleDetailForSearch_Res> list = new List<GetArticleDetailForSearch_Res>();

                list = _Article_BLL.GetArticleDetailForSearch(req);

                stdResp.Data = list;
                _TransactionScope.Complete();
                return stdResp;
            }
        }


        /// <summary>
        /// 取得文章延伸閱讀
        /// </summary>
        /// <param name="fsArticleID">文章ID</param>
        /// <returns></returns>
        [Route("GetArticleFurtherReading")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 3600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleFurtherReading_Res>> GetArticleFurtherReading(string fsArticleID)
        {
            using (TransactionScope _TransactionScope = new TransactionScope())
            {
                StdResp<List<GetArticleFurtherReading_Res>> stdResp = new StdResp<List<GetArticleFurtherReading_Res>>();
                List<GetArticleFurtherReading_Res> getArticleFurtherReading_Res = new List<GetArticleFurtherReading_Res>();

                stdResp.Data = _Article_BLL.GetArticleFurtherReading(fsArticleID);
                _TransactionScope.Complete();
                return stdResp;
            }

        }

        /// <summary>
        /// 取得文章推薦首頁
        /// </summary>
        /// <returns></returns>
        [Route("GetArticlePromo")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 60, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticlePromo_Res>> GetArticlePromo()
        {
            try
            {
                StdResp<List<GetArticlePromo_Res>> stdResp = new StdResp<List<GetArticlePromo_Res>>();
                List<GetArticlePromo_Res> getArticlePromo_Res = new List<GetArticlePromo_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticlePromo();
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 取得文章推薦首頁
        /// </summary>
        /// <returns></returns>
        [Route("GetArticlePromo2")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticlePromo_Res>> GetArticlePromo2()
        {
            try
            {
                StdResp<List<GetArticlePromo_Res>> stdResp = new StdResp<List<GetArticlePromo_Res>>();
                List<GetArticlePromo_Res> getArticlePromo_Res = new List<GetArticlePromo_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticlePromo();
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 取得文章推薦右側
        /// </summary>
        /// <returns></returns>
        [Route("GetArticlePromoRight")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticlePromoRight_Res>> GetArticlePromoRight()
        {
            try
            {
                StdResp<List<GetArticlePromoRight_Res>> stdResp = new StdResp<List<GetArticlePromoRight_Res>>();
                List<GetArticlePromoRight_Res> getArticlePromo_Res = new List<GetArticlePromoRight_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticlePromoRight();
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 取得文章內文推薦右側
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleDetailRight")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleDetailRight_Res>> GetArticleDetailRight(string fsArticleID)
        {
            try
            {
                StdResp<List<GetArticleDetailRight_Res>> stdResp = new StdResp<List<GetArticleDetailRight_Res>>();
                List<GetArticleDetailRight_Res> getArticlePromo_Res = new List<GetArticleDetailRight_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticleDetailRight(fsArticleID);
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 取得文章內文推薦右側
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleDetailRight2/{ArticleID}")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleDetailRight2_Res>> GetArticleDetailRight2(string ArticleID)
        {
            try
            {
                StdResp<List<GetArticleDetailRight2_Res>> stdResp = new StdResp<List<GetArticleDetailRight2_Res>>();
                List<GetArticleDetailRight2_Res> getArticlePromo_Res = new List<GetArticleDetailRight2_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticleDetailRight2(ArticleID);
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 取得文章推薦相關下面
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleDetailDown")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleDetailDown_Res>> GetArticleDetailDown(string fsArticleID)
        {
            try
            {
                StdResp<List<GetArticleDetailDown_Res>> stdResp = new StdResp<List<GetArticleDetailDown_Res>>();
                List<GetArticleDetailDown_Res> getArticlePromo_Res = new List<GetArticleDetailDown_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticleDetailDown(fsArticleID);
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 取得娛樂首頁右邊
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleHomeRight")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleHomeRight_Res>> GetArticleHomeRight()
        {
            try
            {
                StdResp<List<GetArticleHomeRight_Res>> stdResp = new StdResp<List<GetArticleHomeRight_Res>>();
                List<GetArticleHomeRight_Res> getArticlePromo_Res = new List<GetArticleHomeRight_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticleHomeRight();
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 取得娛樂類別右邊
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleCategoryRight")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleCategoryRight_Res>> GetArticleCategoryRight()
        {
            try
            {
                StdResp<List<GetArticleCategoryRight_Res>> stdResp = new StdResp<List<GetArticleCategoryRight_Res>>();
                List<GetArticleCategoryRight_Res> getArticlePromo_Res = new List<GetArticleCategoryRight_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticleCategoryRight();
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 取得娛樂專題右邊
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleProjectRight")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleProjectRight_Res>> GetArticleProjectRight()
        {
            try
            {
                StdResp<List<GetArticleProjectRight_Res>> stdResp = new StdResp<List<GetArticleProjectRight_Res>>();
                List<GetArticleProjectRight_Res> getArticlePromo_Res = new List<GetArticleProjectRight_Res>();
                getArticlePromo_Res = _Article_BLL.GetArticleProjectRight();
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 類別
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleCategory")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleCategory_Res>> GetArticleCategory()
        {
            StdResp<List<GetArticleCategory_Res>> stdResp = new StdResp<List<GetArticleCategory_Res>>();
            List<GetArticleCategory_Res> getArticleCategory_Res = new List<GetArticleCategory_Res>();
            getArticleCategory_Res = _Article_BLL.GetArticleCategory();
            stdResp.Data = getArticleCategory_Res;
            return stdResp;

        }


        /// <summary>
        /// 類別搜尋
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleCategorySearch")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<GetArticleCategorySearch_Res> GetArticleCategorySearch(int? CategoryID, int? Index, int? PageSize)
        {
            try
            {
                GetArticleCategorySearch_Req req = new GetArticleCategorySearch_Req()
                {
                    CategoryID = CategoryID,
                    Index = Index,
                    PageSize = PageSize
                };

                StdResp<GetArticleCategorySearch_Res> stdResp = new StdResp<GetArticleCategorySearch_Res>();

                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.CategoryID == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.Index == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.PageSize == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.CategoryID.Value <= 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.Index.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.PageSize.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                stdResp.Data = _Article_BLL.GetArticleCategorySearch(req);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 快訊
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleNews")]
        [HttpGet]
        public StdResp<List<GetGetArticleNews_Res>> GetArticleNews()
        {
            try
            {
                StdResp<List<GetGetArticleNews_Res>> stdResp = new StdResp<List<GetGetArticleNews_Res>>();
                stdResp.Data = _Article_BLL.GetArticleNews(10);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 首頁上方
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleHomeBanner")]
        [HttpGet]
        public StdResp<List<GetArticleHomeBanner_Res>> GetArticleHomeBanner()
        {
            try
            {
                StdResp<List<GetArticleHomeBanner_Res>> stdResp = new StdResp<List<GetArticleHomeBanner_Res>>();
                stdResp.Data = _Article_BLL.GetArticleHomeBanner(15);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 專題
        /// </summary>
        /// <returns></returns>
#if PRD
        //  [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        [Route("GetArticleProject")]
        [HttpGet]
        public StdResp<List<GetArticleProject_Res>> GetArticleProject()
        {
            try
            {
                StdResp<List<GetArticleProject_Res>> stdResp = new StdResp<List<GetArticleProject_Res>>();
                stdResp.Data = _Article_BLL.GetArticleProject();
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 專題清單
        /// </summary>
        /// <returns></returns>
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        [Route("GetArticleProjectList")]
        [HttpGet]
        public StdResp<List<GetArticleProjectList_Res>> GetArticleProjectList()
        {
            try
            {
                StdResp<List<GetArticleProjectList_Res>> stdResp = new StdResp<List<GetArticleProjectList_Res>>();
                stdResp.Data = _Article_BLL.GetArticleProjectList();
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 文章搜尋
        /// </summary>
        /// <returns></returns>
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        [Route("GetArticleSearch")]
        [HttpGet]
        public StdResp<GetArticleSearch_Res> GetArticleSearch(int? Index, int? PageSize)
        {

            GetArticleSearch_Req req = new GetArticleSearch_Req()
            {
                Index = Index,
                PageSize = PageSize
            };

            try
            {
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.Index == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.PageSize == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.Index.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.PageSize.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                StdResp<GetArticleSearch_Res> stdResp = new StdResp<GetArticleSearch_Res>();


                stdResp.Data = _Article_BLL.GetArticleSearch(req);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 專題搜尋
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleProjectSearch")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<GetArticleProjectSearch_Res> GetArticleProjectSearch(int? ProjectID, int? Index, int? PageSize)
        {
            try
            {
                GetArticleProjectSearch_Req req = new GetArticleProjectSearch_Req()
                {
                    ProjectID = ProjectID,
                    Index = Index,
                    PageSize = PageSize
                };

                StdResp<GetArticleProjectSearch_Res> stdResp = new StdResp<GetArticleProjectSearch_Res>();

                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.ProjectID == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.Index == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.PageSize == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.ProjectID.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.Index.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.PageSize.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                stdResp.Data = _Article_BLL.GetArticleProjectSearch(req);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /////// <summary>
        /////// 文章搜尋
        /////// </summary>
        /////// <returns></returns>
        //[Route("SMSTest")]
        //[HttpPost]
        //public StdResp SMSTest(PHONEANDEMAIL req)
        //{
        //    string EmailBody = ReadFileInfo("email.html");
        //    string EmailSubject = @"疫情升溫 在家看四季線上最安心，每月只要$99，觀賞最好看的戲劇綜藝及首播電影";
        //    string SMSBody = @"四季線上影視 防疫安心看，豪華頻道每月不用100元，超過100個精彩頻道+高清VOD隨你看https://4gtv.pse.is/PFENT";

        //    StdResp stdResp = new StdResp();

        //    List<string> fsPHONE = req.fsPHONE.Replace("\n", "").Replace(" ", "").Split(';').ToList();

        //    List<string> fsEMAIL = req.fsEMAIL.Replace("\n", "").Replace(" ", "").Split(';').ToList();

        //    if (fsEMAIL != null && fsEMAIL.Count() > 0)
        //    {
        //        Task.Run(() =>

        //    Parallel.ForEach(fsEMAIL, (_fsEMAIL) =>
        //    {
        //        new FourgTV_OTT2.Api2.Common.clsTOOL().fnSEND_EMAIL2(_fsEMAIL, EmailSubject, EmailBody);
        //    })

        //    );

        //    }
        //    if (fsPHONE != null && fsPHONE.Count() > 0)
        //    {
        //        Task.Run(() =>

        //    Parallel.ForEach(fsPHONE, (_fsPHONE) =>
        //    {
        //        new FourgTV_OTT2.Api2.Common.clsTOOL().fnSEND_SMS(_fsPHONE, SMSBody);
        //    })
        //    );
        //    }
        //    return stdResp;
        //}

        ////讀取文件內容
        //private static string ReadFileInfo(string FileName)
        //{
        //    string filepath = HttpContext.Current.Server.MapPath("~/emailHtml/" + FileName);
        //    FileStream fs = new FileStream(filepath, FileMode.Open);
        //    StreamReader sr = new StreamReader(fs);
        //    string res = sr.ReadToEnd();
        //    sr.Dispose();
        //    fs.Dispose();
        //    return res;
        //}
        //public class PHONEANDEMAIL
        //{
        //    public string fsPHONE { set; get; }
        //    public string fsEMAIL { set; get; }
        //}


        ///// <summary>
        ///// 給Yahoo
        ///// </summary>
        ///// <returns></returns>
        //[Route("GetArticleToYahoo")]
        //[HttpGet]
        //public StdResp<List<GetArticleToYahoo_Res>> GetArticleToYahoo()
        //{
        //    try
        //    {
        //        StdResp<List<GetArticleToYahoo_Res>> stdResp = new StdResp<List<GetArticleToYahoo_Res>>();


        //        stdResp.Data = _Article_BLL.GetArticleToYahoo();
        //        return stdResp;
        //    }
        //    catch (MyException e)
        //    {
        //        throw e;
        //    }
        //}

        //// GET api/values
        //[Route("GetArticleToYahoo")]
        //public HttpResponseMessage GetArticleToYahoo()
        //{
        //    var data = new string[] { "value1", "value2" };

        //    var resp = new HttpResponseMessage(HttpStatusCode.OK);

        //    //string LoadPath = System.Web.HttpContext.Current.Server.MapPath("~/rss/"); // RSS存放路径 
        //    //if (!Directory.Exists(LoadPath))
        //    //{
        //    //    Directory.CreateDirectory(LoadPath);
        //    //}
        //    //  string domain = MyCLib.StrClass.GetDomain();

        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        XmlTextWriter xtw = new XmlTextWriter(ms, System.Text.Encoding.GetEncoding("utf-8"));

        //        xtw.Formatting = Formatting.Indented;
        //        xtw.WriteStartDocument();
        //        //xtw.WriteProcessingInstruction("xml-stylesheet", "type='text/css' href='http://www.114390.com/images/font-awesome.min.css'"); // 增加头 
        //        xtw.WriteStartElement("rss"); // 增加rss节点 
        //        xtw.WriteAttributeString("version", "2.0");

        //        xtw.WriteStartElement("channel"); // 增加channel节点 
        //        xtw.WriteElementString("title", @"<![CDATA[ 四季娛樂 ]]>"); // 频道名称 
        //        xtw.WriteElementString("link", @"https://www.4gtv.tv/entertainment"); // 频道的URL 
        //        xtw.WriteElementString("description", @"<![CDATA[ 四季娛樂 ]]>"); // 频道的描述
        //        xtw.WriteElementString("pubDate ", DateTime.Now.ToString("r")); // 频道内容最后的修改日期

        //        //// foreach (TableRow tr in ds.Tables[0].Rows) 
        //        //for (int i = 0; i < dt.Rows.Count; i++)
        //        //{
        //        //    xtw.WriteStartElement("item");
        //        //    //xtw.WriteAttributeString("id", (i + 1).ToString());
        //        //    xtw.WriteElementString("title", dt.Rows[i]["title"].ToString());
        //        //    xtw.WriteElementString("link", domain + "/info/" + dt.Rows[i]["ID"].ToString() + ".html"); // 链接地址 
        //        //    xtw.WriteElementString("description", dt.Rows[i]["description"].ToString());              // 描述 
        //        //    xtw.WriteElementString("pubDate", dt.Rows[i]["times"].ToString());                        //发布时间
        //        //    xtw.WriteElementString("guid", dt.Rows[i]["ID"].ToString());                              //唯一标识
        //        //    xtw.WriteEndElement();
        //        //}
        //        xtw.WriteEndElement();
        //        xtw.WriteEndElement();
        //        xtw.WriteEndDocument();
        //        xtw.Flush();
        //        xtw.Close();

        //        //  resp = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(ms.ToArray().ToString(), Encoding.UTF8, "application/atom+xml") };
        //        resp.Content = new ByteArrayContent(ms.ToArray());
        //        resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/rss+xml");
        //        return resp;
        //    }
        //    //return resp;
        //}


        //[Route("GetArticleToYahoo")]
        //public XElement GetArticleToYahoo()
        //{
        //    //IP
        //    string fsIP = this.ClientIP;

        //    bool checkYahooIP = false;
        //    bool check4gtvIP = false;

        //    if (fsIP == "35.170.210.144" || fsIP == "34.239.210.170" || fsIP == "35.172.11.50" || fsIP == "35.172.3.244"
        //        //|| fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1
        //        )
        //    {
        //        checkYahooIP = true;

        //    }
        //    //if (fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
        //    //{
        //    //    check4gtvIP = true;

        //    //}

        //    //if (!checkYahooIP && !check4gtvIP)
        //    //{
        //    //    throw new MyException(ResponseCodeEnum.IPError);
        //    //}

        //    XNamespace media = "http://search.yahoo.com/mrss/";

        //    XNamespace dc = "http://purl.org/dc/elements/1.1/";
        //    XNamespace dcterms = "http://purl.org/dc/terms/";
        //    XNamespace content = "http://purl.org/rss/1.0/modules/content/";

        //    XName media_content = media + "content";
        //    XName media_keywords = media + "keywords";
        //    XName media_credit = media + "credit";
        //    XName media_thumbnail = media + "thumbnail";
        //    XName dcterms_valid = dcterms + "valid";
        //    XName content_encoded = content + "encoded";

        //    XDocument rss2 = new XDocument();
        //    rss2.Add(
        //        new XElement("rss",
        //        new XAttribute("version", "2.0"),
        //        // that's how you add namespaces to an XML document
        //        new XAttribute(XNamespace.Xmlns + "content", content.NamespaceName),
        //        new XAttribute(XNamespace.Xmlns + "dc", dc.NamespaceName),
        //        new XAttribute(XNamespace.Xmlns + "media", media.NamespaceName),
        //        new XAttribute(XNamespace.Xmlns + "dcterms", dcterms.NamespaceName),
        //        new XElement("channel",
        //        new XElement("title", new XCData("四季線上")),
        //        new XElement("link", "https://www.4gtv.tv"),
        //        new XElement("description", new XCData("四季線上4gtv")),
        //         new XElement("pubDate", GetRFC822Date(DateTime.Now))
        //                //     new XElement("pubDate", DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en")))
        //                )
        //            )
        //        );
        //    XElement rss = XElement.Parse(rss2.ToString());

        //    var GetArticleToYahoo = _Article_BLL.GetArticleToYahoo(checkYahooIP);


        //    int c = 0;

        //    foreach (var info in GetArticleToYahoo)
        //    {
        //        XName dc_creator = dc + "creator";

        //        string title = info.Title;
        //        string guid = info.Guid;
        //        string description = info.Description;
        //        string FurtherReadingID1 = info.FurtherReadingID1;
        //        string FurtherReadingID2 = info.FurtherReadingID2;
        //        string FurtherReadingID3 = info.FurtherReadingID3;

        //        string FurtherReadingHEML = string.Empty;

        //        if (!string.IsNullOrEmpty(FurtherReadingID1) || !string.IsNullOrEmpty(FurtherReadingID2) || !string.IsNullOrEmpty(FurtherReadingID3))
        //        {

        //            FurtherReadingHEML = @"<br/><p class=""read-more-vendor""><span>更多四季線上報導</span><br/>";

        //            if (!string.IsNullOrEmpty(FurtherReadingID1))
        //            {
        //                var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID1);
        //                if (GetArticleDetail != null)
        //                {
        //                    FurtherReadingHEML += @"<a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID1 + @">" + GetArticleDetail.fsTitle + @"</a>";
        //                }
        //            }

        //            if (!string.IsNullOrEmpty(FurtherReadingID2))
        //            {
        //                var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID2);
        //                if (GetArticleDetail != null)
        //                {
        //                    FurtherReadingHEML += @"<br/><a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID2 + @">" + GetArticleDetail.fsTitle + @"</a>";
        //                }
        //            }
        //            if (!string.IsNullOrEmpty(FurtherReadingID3))
        //            {
        //                var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID3);
        //                if (GetArticleDetail != null)
        //                {
        //                    FurtherReadingHEML += @"<br/><a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID3 + @">" + GetArticleDetail.fsTitle + @"</a>";
        //                }
        //            }
        //            FurtherReadingHEML += @"</p>";
        //        }

        //        string creator = "四季娛樂";
        //        //string pubDate = info.PubDate.Value.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en"));
        //        string pubDate = GetRFC822Date(info.PubDate.Value);
        //        string link = info.Link;
        //        string category = info.Category;
        //        string keywords = string.Empty;
        //        string valid = string.Empty;

        //        if (info.fdStartDate != null)
        //        {
        //            valid = "start=" + GetRFC822Date(info.fdStartDate.Value);
        //            if (info.fdEndDate != null)
        //            {
        //                valid += ";end= " + GetRFC822Date(info.fdEndDate.Value);
        //            }

        //        }

        //        if (info.keywords != null && info.keywords.Count() > 0)
        //        {
        //            for (var i = 0; i < info.keywords.Count(); i++)
        //            {
        //                keywords += info.keywords[i];
        //                if (i + 1 != info.keywords.Count())
        //                {
        //                    keywords += ", ";
        //                }
        //            }
        //        }
        //        XElement rsss = rss.Element("channel");

        //        rsss.Add(
        //            new XElement("item",
        //            new XElement("title", new XCData(title)),
        //            new XElement("guid", guid, new XAttribute("isPermaLink", "false")),
        //            new XElement("description", new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "").Substring(0, 90) + "..."))),
        //            new XElement(content_encoded, new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(description + FurtherReadingHEML))),
        //            new XElement("link", link),
        //            new XElement("pubDate", pubDate),
        //            new XElement("category", category),
        //            new XElement(dc_creator, creator)
        //            ));
        //        //檢查是否有影片
        //        if (string.IsNullOrEmpty(info.VideoURL))
        //        {
        //            XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
        //            rrrr.Add(
        //                new XElement(media_content, new XAttribute("url", info.ImageURL), new XAttribute("type", "image/jpeg")),
        //                new XElement(media_thumbnail, new XAttribute("url", info.ImageURL)),
        //                new XElement(media_keywords, keywords),
        //                new XElement(dcterms_valid, valid)
        //                );
        //        }
        //        else
        //        {
        //            XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
        //            rrrr.Add(
        //                new XElement(media_content, new XAttribute("url", info.VideoURL), new XAttribute("type", "video/mp4"), new XAttribute("isDefault", "true"), new XAttribute("medium", "video")),
        //                new XElement(media_thumbnail, new XAttribute("url", info.ImageURL)),
        //                new XElement(media_keywords, keywords),
        //                new XElement(media_credit, "四季線上"),
        //                new XElement(dcterms_valid, valid)
        //                );
        //        }
        //        c++;
        //    }

        //    //改狀態
        //    if (checkYahooIP)
        //    {
        //        //  _Article_BLL.YahooGetXML();
        //    }


        //    return rss;

        //}


        /// <summary>
        /// YahooRss
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("YahooRss")]
        public HttpResponseMessage YahooRss()
        {
            //IP
            string fsIP = this.ClientIP;

            bool checkYahooIP = false;
            bool check4gtvIP = false;

            var IPS = clsTOOL.GetYahooRssIP();

            if (IPS.Contains(fsIP))
            {
                checkYahooIP = true;
            }

            XNamespace media = "http://search.yahoo.com/mrss/";

            XNamespace dc = "http://purl.org/dc/elements/1.1/";
            XNamespace dcterms = "http://purl.org/dc/terms/";
            XNamespace content = "http://purl.org/rss/1.0/modules/content/";
            XNamespace atom = "http://www.w3.org/2005/Atom";

            XName media_content = media + "content";
            XName media_keywords = media + "keywords";
            XName media_credit = media + "credit";
            XName media_thumbnail = media + "thumbnail";
            XName dcterms_valid = dcterms + "valid";
            XName content_encoded = content + "encoded";
            XName atom_link = atom + "link";
            string href = Request.RequestUri.AbsoluteUri;

            string yahooUTM = @"?utm_source=yahoo_news&utm_medium=extended&utm_campaign=4gtvent";

            XDocument rss2 = new XDocument();
            rss2.Add(
                new XElement("rss",
                new XAttribute("version", "2.0"),
                // that's how you add namespaces to an XML document
                new XAttribute(XNamespace.Xmlns + "content", content.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "dc", dc.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "media", media.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "dcterms", dcterms.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "atom", atom.NamespaceName),
                new XElement("channel",
                new XElement("title", new XCData("四季線上")),
                new XElement(atom_link, new XAttribute("href", href), new XAttribute("rel", "self"), new XAttribute("type", "application/rss+xml")),
                new XElement("link", "https://www.4gtv.tv"),
                new XElement("description", new XCData("四季線上4gtv")),
                 new XElement("pubDate", GetRFC822Date(DateTime.Now))
                        //     new XElement("pubDate", DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en")))
                        )
                    )
                );
            XElement rss = XElement.Parse(rss2.ToString());



            //取得直
            var GetArticleToYahoo = _Article_BLL.GetArticleToYahoo(false, checkYahooIP);


            int c = 0;

            foreach (var info in GetArticleToYahoo)
            {
                XName dc_creator = dc + "creator";

                string title = info.SecTitle;
                string guid = info.Guid;
                string description = info.Description;
                string FurtherReadingID1 = info.FurtherReadingID1;
                string FurtherReadingID2 = info.FurtherReadingID2;
                string FurtherReadingID3 = info.FurtherReadingID3;


                string FurtherReadingHEML1 = string.Empty;
                string FurtherReadingHEML2 = string.Empty;
                string FurtherReadingHEML3 = string.Empty;

                string FurtherReadingHEML = string.Empty;
                string allcontrent = string.Empty;



                try
                {
                    if (!string.IsNullOrEmpty(FurtherReadingID1))
                    {
                        var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID1);
                        if (GetArticleDetail != null)
                        {
                            FurtherReadingHEML1 = @"<a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID1 + yahooUTM + @">" + GetArticleDetail.fsTitle + @"</a>";
                        }
                    }
                }

                catch
                {

                }

                try
                {
                    if (!string.IsNullOrEmpty(FurtherReadingID2))
                    {
                        var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID2);
                        if (GetArticleDetail != null)
                        {
                            FurtherReadingHEML2 = @"<br/><a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID2 + yahooUTM + @">" + GetArticleDetail.fsTitle + @"</a>";
                        }
                    }
                }
                catch
                { }


                try
                {
                    if (!string.IsNullOrEmpty(FurtherReadingID3))
                    {
                        var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID3);
                        if (GetArticleDetail != null)
                        {
                            FurtherReadingHEML3 = @"<br/><a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID3 + yahooUTM + @">" + GetArticleDetail.fsTitle + @"</a>";
                        }
                    }
                }
                catch
                { }

                if (!string.IsNullOrEmpty(FurtherReadingHEML1) || !string.IsNullOrEmpty(FurtherReadingHEML2) || !string.IsNullOrEmpty(FurtherReadingHEML3))
                {

                    FurtherReadingHEML = @"<br/><p class=""read-more-vendor""><span>更多四季線上報導</span><br/>";

                    FurtherReadingHEML += FurtherReadingHEML1;
                    FurtherReadingHEML += FurtherReadingHEML2;
                    FurtherReadingHEML += FurtherReadingHEML3;

                    FurtherReadingHEML += @"</p>";
                }

                string creator = "四季娛樂";
                //string pubDate = info.PubDate.Value.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en"));
                string pubDate = GetRFC822Date(info.PubDate.Value);
                string link = info.Link;
                string category = info.Category;
                string keywords = string.Empty;
                string valid = string.Empty;

                if (info.fdStartDate != null)
                {
                    valid = "start=" + GetRFC822Date(info.fdStartDate.Value);
                    if (info.fdEndDate != null)
                    {
                        valid += ";end= " + GetRFC822Date(info.fdEndDate.Value);
                    }

                }

                if (info.keywords != null && info.keywords.Count() > 0)
                {
                    for (var i = 0; i < info.keywords.Count(); i++)
                    {
                        keywords += info.keywords[i];
                        if (i + 1 != info.keywords.Count())
                        {
                            keywords += ", ";
                        }
                    }
                }

                if (string.IsNullOrEmpty(info.VideoURL))
                {

                    string image = info.ImageURL;

                    string htmlimage = @"<img src=""" + info.ImageURL + @""">";


                    allcontrent = htmlimage + description + FurtherReadingHEML;
                }
                else
                {
                    allcontrent = description + FurtherReadingHEML;
                }


                XElement rsss = rss.Element("channel");
                string description90 = string.Empty;

                if (System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "").Length > 90)
                {

                    description90 = System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "").Substring(0, 90) + "...";
                }
                else
                {
                    description90 = System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "");
                }
                rsss.Add(
                    new XElement("item",
                    new XElement("title", new XCData(title)),
                    new XElement("guid", guid, new XAttribute("isPermaLink", "false")),
                    new XElement("description", new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(description90))),
                    new XElement(content_encoded, new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(allcontrent))),
                    new XElement("link", link),
                    new XElement("pubDate", pubDate),
                    new XElement("category", category),
                    new XElement(dc_creator, creator)
                    ));
                //檢查是否有影片
                if (string.IsNullOrEmpty(info.VideoURL))
                {
                    XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
                    rrrr.Add(
                        new XElement(media_content, new XAttribute("url", info.ImageURL), new XAttribute("type", "image/jpeg")),
                        new XElement(media_thumbnail, new XAttribute("url", info.ImageURL)),
                        new XElement(media_keywords, keywords),
                        new XElement(dcterms_valid, valid)
                        );
                }
                else
                {
                    XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
                    rrrr.Add(
                        new XElement(media_content, new XAttribute("url", info.VideoURL), new XAttribute("type", "video/mp4"), new XAttribute("isDefault", "true"), new XAttribute("medium", "video")),
                        new XElement(media_thumbnail, new XAttribute("url", info.ImageURL)),
                        new XElement(media_keywords, keywords),
                        new XElement(media_credit, "四季線上"),
                        new XElement(dcterms_valid, valid)
                        );
                }
                c++;
            }


            var settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Entitize,
                NewLineOnAttributes = true,
                Indent = true
            };

            StringBuilder sb = new StringBuilder();

            using (XmlWriter xw = XmlWriter.Create(sb, settings))
            {
                rss.Save(xw);
            }
            HttpResponseMessage response = this.Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(sb.ToString().Replace("utf-16", "utf-8").Replace("<oembed url=", "<iframe src=").Replace("</oembed>", "</iframe>"), Encoding.UTF8, "application/rss+xml");
            return response;


        }

        /// <summary>
        /// YahooRssVideo
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("YahooRssVideo")]
        public HttpResponseMessage YahooRssVideo()
        {
            //IP
            string fsIP = this.ClientIP;

            bool checkYahooIP = false;
            bool check4gtvIP = false;

            var IPS = clsTOOL.GetYahooRssIP();

            if (IPS.Contains(fsIP))
            {
                checkYahooIP = true;
            }


            XNamespace media = "http://search.yahoo.com/mrss/";

            XNamespace dc = "http://purl.org/dc/elements/1.1/";
            XNamespace dcterms = "http://purl.org/dc/terms/";
            //   XNamespace content = "http://purl.org/rss/1.0/modules/content/";
            XNamespace atom = "http://www.w3.org/2005/Atom";

            XName media_content = media + "content";
            XName media_keywords = media + "keywords";
            XName media_credit = media + "credit";
            XName media_thumbnail = media + "thumbnail";
            XName dcterms_valid = dcterms + "valid";
            // XName content_encoded = content + "encoded";
            XName atom_link = atom + "link";
            string href = Request.RequestUri.AbsoluteUri;
            string yahooUTM = @"?utm_source=yahoo_news&utm_medium=extended&utm_campaign=4gtvent";


            XDocument rss2 = new XDocument();
            rss2.Add(
                new XElement("rss",
                new XAttribute("version", "2.0"),
                // that's how you add namespaces to an XML document
                //  new XAttribute(XNamespace.Xmlns + "content", content.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "dc", dc.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "media", media.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "dcterms", dcterms.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "atom", atom.NamespaceName),
                new XElement("channel",
                new XElement("title", new XCData("四季線上")),
                new XElement(atom_link, new XAttribute("href", href), new XAttribute("rel", "self"), new XAttribute("type", "application/rss+xml")),
                new XElement("link", "https://www.4gtv.tv"),
                new XElement("description", new XCData("四季線上4gtv")),
                 new XElement("pubDate", GetRFC822Date(DateTime.Now))
                        //     new XElement("pubDate", DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en")))
                        )
                    )
                );
            XElement rss = XElement.Parse(rss2.ToString());


            //取得直
            var GetArticleToYahoo = _Article_BLL.GetArticleToYahoo(true, checkYahooIP);

            int c = 0;

            foreach (var info in GetArticleToYahoo)
            {
                XName dc_creator = dc + "creator";

                string title = info.Title;
                string guid = info.Guid;
                string description = info.Description;
                string FurtherReadingID1 = info.FurtherReadingID1;
                string FurtherReadingID2 = info.FurtherReadingID2;
                string FurtherReadingID3 = info.FurtherReadingID3;

                string FurtherReadingHEML = string.Empty;


                string FurtherReadingHEML1 = string.Empty;
                string FurtherReadingHEML2 = string.Empty;
                string FurtherReadingHEML3 = string.Empty;


                string allcontrent = string.Empty;



                try
                {
                    if (!string.IsNullOrEmpty(FurtherReadingID1))
                    {
                        var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID1);
                        if (GetArticleDetail != null)
                        {
                            FurtherReadingHEML1 = @"<a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID1 + yahooUTM + @">" + GetArticleDetail.fsTitle + @"</a>";
                        }
                    }
                }

                catch
                {

                }

                try
                {
                    if (!string.IsNullOrEmpty(FurtherReadingID2))
                    {
                        var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID2);
                        if (GetArticleDetail != null)
                        {
                            FurtherReadingHEML2 = @"<br/><a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID2 + yahooUTM + @">" + GetArticleDetail.fsTitle + @"</a>";
                        }
                    }
                }
                catch
                { }


                try
                {
                    if (!string.IsNullOrEmpty(FurtherReadingID3))
                    {
                        var GetArticleDetail = _ArticleModule.GetArticleInfoByID(FurtherReadingID3);
                        if (GetArticleDetail != null)
                        {
                            FurtherReadingHEML3 = @"<br/><a href = " + @"https://www.4gtv.tv/article/" + FurtherReadingID3 + yahooUTM + @">" + GetArticleDetail.fsTitle + @"</a>";
                        }
                    }
                }
                catch
                { }

                if (!string.IsNullOrEmpty(FurtherReadingHEML1) || !string.IsNullOrEmpty(FurtherReadingHEML2) || !string.IsNullOrEmpty(FurtherReadingHEML3))
                {

                    FurtherReadingHEML = @"<br/><p class=""read-more-vendor""><span>更多四季線上報導</span><br/>";

                    FurtherReadingHEML += FurtherReadingHEML1;
                    FurtherReadingHEML += FurtherReadingHEML2;
                    FurtherReadingHEML += FurtherReadingHEML3;

                    FurtherReadingHEML += @"</p>";
                }


                string creator = "四季娛樂";
                //string pubDate = info.PubDate.Value.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en"));
                string pubDate = GetRFC822Date(info.PubDate.Value);
                string link = info.Link;
                string category = info.Category;
                string keywords = string.Empty;
                string valid = string.Empty;

                if (info.fdStartDate != null)
                {
                    valid = "start=" + GetRFC822Date(info.fdStartDate.Value);
                    if (info.fdEndDate != null)
                    {
                        valid += ";end= " + GetRFC822Date(info.fdEndDate.Value);
                    }

                }

                if (info.keywords != null && info.keywords.Count() > 0)
                {
                    for (var i = 0; i < info.keywords.Count(); i++)
                    {
                        keywords += info.keywords[i];
                        if (i + 1 != info.keywords.Count())
                        {
                            keywords += ", ";
                        }
                    }
                }
                XElement rsss = rss.Element("channel");



                string description90 = string.Empty;

                if (System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "").Length > 90)
                {

                    description90 = System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "").Substring(0, 90) + "...";
                }
                else
                {
                    description90 = System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "");
                }


                rsss.Add(
                    new XElement("item",
                    new XElement("title", new XCData(title)),
                    new XElement("guid", guid, new XAttribute("isPermaLink", "false")),
                    new XElement("description", new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(description.Remove(0, 3) + FurtherReadingHEML))),
                    //   new XElement(content_encoded, new XCData(description + FurtherReadingHEML)),
                    new XElement("link", link),
                    new XElement("pubDate", pubDate),
                    new XElement("category", category),
                    new XElement(dc_creator, creator)
                    ));
                //檢查是否有影片
                if (string.IsNullOrEmpty(info.VideoURL))
                {
                    XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
                    rrrr.Add(
                        // new XElement(media_content, new XAttribute("url", info.ImageURL), new XAttribute("type", "image/jpeg")),
                        new XElement(media_thumbnail, new XAttribute("url", info.ImageURL)),
                        new XElement(media_keywords, keywords),
                        new XElement(media_credit, "四季線上"),
                        new XElement(dcterms_valid, valid)
                        );
                }
                else
                {
                    XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
                    rrrr.Add(
                        new XElement(media_content, new XAttribute("url", info.VideoURL), new XAttribute("type", "video/mp4"), new XAttribute("isDefault", "true"), new XAttribute("medium", "video")),
                        new XElement(media_thumbnail, new XAttribute("url", info.ImageURL)),
                        new XElement(media_keywords, keywords),
                        new XElement(media_credit, "四季線上"),
                        new XElement(dcterms_valid, valid)
                        );
                }
                c++;
            }


            var settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Entitize,
                NewLineOnAttributes = true,
                Indent = true
            };

            StringBuilder sb = new StringBuilder();

            using (XmlWriter xw = XmlWriter.Create(sb, settings))
            {
                rss.Save(xw);
            }
            HttpResponseMessage response = this.Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(sb.ToString().Replace("utf-16", "utf-8").Replace("<oembed url=", "<iframe src=").Replace("</oembed>", "</iframe>"), Encoding.UTF8, "application/rss+xml");
            return response;


        }

        /// <summary>
        /// FTVRSS
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("FTVRSS/{Category:alpha=Empty}")]
        public HttpResponseMessage FTVRSS(string Category)
        {
            //IP
            string fsIP = this.ClientIP;

            DateTime dateTime = DateTime.Now;



            //  XNamespace media = "http://search.yahoo.com/mrss/";

            XNamespace dc = "http://purl.org/dc/elements/1.1/";
            // XNamespace dcterms = "http://purl.org/dc/terms/";
            //   XNamespace content = "http://purl.org/rss/1.0/modules/content/";
            XNamespace atom = "http://www.w3.org/2005/Atom";

            //XName media_content = media + "content";
            //XName media_keywords = media + "keywords";
            //XName media_credit = media + "credit";
            //XName media_thumbnail = media + "thumbnail";
            //    XName dcterms_valid = dcterms + "valid";
            //  XName content_encoded = content + "encoded";
            XName atom_link = atom + "link";
            string href = Request.RequestUri.AbsoluteUri;

            //string yahooUTM = @"?utm_source=yahoo_news&utm_medium=extended&utm_campaign=4gtvent";

            XDocument rss2 = new XDocument();
            rss2.Add(
                new XElement("rss",
                new XAttribute("version", "2.0"),
                // that's how you add namespaces to an XML document
                //    new XAttribute(XNamespace.Xmlns + "content", content.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "dc", dc.NamespaceName),
                //     new XAttribute(XNamespace.Xmlns + "media", media.NamespaceName),
                //    new XAttribute(XNamespace.Xmlns + "dcterms", dcterms.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "atom", atom.NamespaceName),
                new XElement("channel",
                new XElement("title", new XCData("四季線上")),
                new XElement(atom_link, new XAttribute("href", href), new XAttribute("rel", "self"), new XAttribute("type", "application/rss+xml")),
                new XElement("link", "https://www.4gtv.tv"),
                new XElement("description", new XCData("四季線上4gtv")),
                 new XElement("pubDate", GetRFC822Date(dateTime))
                        //     new XElement("pubDate", DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en")))
                        )
                    )
                );
            XElement rss = XElement.Parse(rss2.ToString());
            int CategoryID = 999;

            switch (Category)
            {
                case "entertainment":
                    CategoryID = 1;
                    break;
                case "drama":
                    CategoryID = 2;
                    break;
                case "show":
                    CategoryID = 3;
                    break;
                default:
                    break;
            }


            //取得直
            var GetArticleForFTVRSS = _Article_BLL.GetArticleForFTVRSS(dateTime.AddDays(-2), dateTime, CategoryID);


            int c = 0;

            foreach (var info in GetArticleForFTVRSS)
            {
                XName dc_creator = dc + "creator";

                string title = info.SecTitle;
                string guid = info.ID;
                string description = info.Description;
                string allcontent = info.Content;

                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(allcontent);
                var svgs = doc.DocumentNode.SelectNodes("//svg");
                if (svgs != null)
                {
                    foreach (var svg in svgs)
                    {
                        svg.Remove();
                    }
                    allcontent = doc.DocumentNode.WriteContentTo();
                }

                string FurtherReadingHEML = string.Empty;
                //  string allcontrent = string.Empty;



                string creator = "四季娛樂";
                //string pubDate = info.PubDate.Value.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en"));
                DateTime pubDate = info.UpdateTime.Value;
                string link = @"https://www.4gtv.tv/article/" + info.ID;
                string category = info.Category;
                string keywords = string.Empty;
                string valid = string.Empty;

                if (info.StartDate != null)
                {
                    valid = "start=" + GetRFC822Date(info.StartDate.Value);
                    if (info.EndDate != null)
                    {
                        valid += ";end= " + GetRFC822Date(info.EndDate.Value);
                    }

                }

                if (!string.IsNullOrEmpty(info.Tags))
                {
                    keywords = info.Tags;
                }

                //if (string.IsNullOrEmpty(allcontent))
                //{

                //    string image = info.Headframe;

                //    string htmlimage = @"<img src=""" + info.Headframe + @""">";


                //    allcontent = htmlimage + description;
                //}
                //else
                //{
                //    string image = info.Headframe;

                //    string htmlimage = @"<img src=""" + info.Headframe + @""">";

                //    allcontent = htmlimage + allcontent;
                //}

                XElement rsss = rss.Element("channel");

                rsss.Add(
                    new XElement("item",
                    new XElement("title", new XCData(title)),
                    new XElement("guid", guid, new XAttribute("isPermaLink", "false")),
                    new XElement("description", new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(allcontent))),
                    //  new XElement(content_encoded, new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(allcontent))),
                    new XElement("link", link),
                    new XElement("pubDate", GetRFC822Date(pubDate)),
                    new XElement("category", category),
                    new XElement(dc_creator, creator),
                    new XElement("enclosure", new XAttribute("url", info.Headframe), new XAttribute("type", "image/jpeg"), new XAttribute("length", "1"))
                    ));
                //檢查是否有圖
                //if (!string.IsNullOrEmpty(info.Headframe))
                //{
                //    XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
                //    rrrr.Add(
                //        new XElement(media_content, new XAttribute("url", info.Headframe), new XAttribute("type", "image/jpeg")),
                //        new XElement(media_thumbnail, new XAttribute("url", info.Headframe)),
                //        new XElement(media_keywords, keywords),
                //        new XElement(dcterms_valid, valid)
                //        );
                //}

                c++;
            }


            var settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Entitize,
                NewLineOnAttributes = true,
                Indent = true
            };

            StringBuilder sb = new StringBuilder();

            using (XmlWriter xw = XmlWriter.Create(sb, settings))
            {
                rss.Save(xw);
            }
            HttpResponseMessage response = this.Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(sb.ToString().Replace("utf-16", "utf-8").Replace("<oembed url=", "<iframe src=").Replace("</oembed>", "</iframe>"), Encoding.UTF8, "application/rss+xml");
            return response;


        }


        /// <summary>
        /// FTVRSS已下架
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("FTVRSSDown/{Category:alpha=Empty}")]
        public HttpResponseMessage FTVRSSDown(string Category)
        {
            //IP
            string fsIP = this.ClientIP;

            DateTime dateTime = DateTime.Now;



            //  XNamespace media = "http://search.yahoo.com/mrss/";

            XNamespace dc = "http://purl.org/dc/elements/1.1/";
            // XNamespace dcterms = "http://purl.org/dc/terms/";
            //   XNamespace content = "http://purl.org/rss/1.0/modules/content/";
            XNamespace atom = "http://www.w3.org/2005/Atom";

            //XName media_content = media + "content";
            //XName media_keywords = media + "keywords";
            //XName media_credit = media + "credit";
            //XName media_thumbnail = media + "thumbnail";
            //    XName dcterms_valid = dcterms + "valid";
            //  XName content_encoded = content + "encoded";
            XName atom_link = atom + "link";
            string href = Request.RequestUri.AbsoluteUri;

            //string yahooUTM = @"?utm_source=yahoo_news&utm_medium=extended&utm_campaign=4gtvent";

            XDocument rss2 = new XDocument();
            rss2.Add(
                new XElement("rss",
                new XAttribute("version", "2.0"),
                // that's how you add namespaces to an XML document
                //    new XAttribute(XNamespace.Xmlns + "content", content.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "dc", dc.NamespaceName),
                //     new XAttribute(XNamespace.Xmlns + "media", media.NamespaceName),
                //    new XAttribute(XNamespace.Xmlns + "dcterms", dcterms.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "atom", atom.NamespaceName),
                new XElement("channel",
                new XElement("title", new XCData("四季線上")),
                new XElement(atom_link, new XAttribute("href", href), new XAttribute("rel", "self"), new XAttribute("type", "application/rss+xml")),
                new XElement("link", "https://www.4gtv.tv"),
                new XElement("description", new XCData("四季線上4gtv")),
                 new XElement("pubDate", GetRFC822Date(dateTime))
                        //     new XElement("pubDate", DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en")))
                        )
                    )
                );
            XElement rss = XElement.Parse(rss2.ToString());
            int CategoryID = 999;

            switch (Category)
            {
                case "entertainment":
                    CategoryID = 1;
                    break;
                case "drama":
                    CategoryID = 2;
                    break;
                case "show":
                    CategoryID = 3;
                    break;
                default:
                    break;
            }


            //取得直
            var GetArticleForFTVRSS = _Article_BLL.GetArticleForFTVRSSDown(dateTime.AddDays(-5), CategoryID);


            int c = 0;

            foreach (var info in GetArticleForFTVRSS)
            {
                XName dc_creator = dc + "creator";

                string title = info.SecTitle;
                string guid = info.ID;
                string description = info.Description;
                string allcontent = info.Content;



                string FurtherReadingHEML = string.Empty;
                //  string allcontrent = string.Empty;



                string creator = "四季娛樂";
                //string pubDate = info.PubDate.Value.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en"));
                DateTime pubDate = info.UpdateTime.Value;
                string link = @"https://www.4gtv.tv/article/" + info.ID;
                string category = info.Category;
                string keywords = string.Empty;
                string valid = string.Empty;

                if (info.StartDate != null)
                {
                    valid = "start=" + GetRFC822Date(info.StartDate.Value);
                    if (info.EndDate != null)
                    {
                        valid += ";end= " + GetRFC822Date(info.EndDate.Value);
                    }

                }

                if (!string.IsNullOrEmpty(info.Tags))
                {
                    keywords = info.Tags;
                }

                //if (string.IsNullOrEmpty(allcontent))
                //{

                //    string image = info.Headframe;

                //    string htmlimage = @"<img src=""" + info.Headframe + @""">";


                //    allcontent = htmlimage + description;
                //}
                //else
                //{
                //    string image = info.Headframe;

                //    string htmlimage = @"<img src=""" + info.Headframe + @""">";

                //    allcontent = htmlimage + allcontent;
                //}

                XElement rsss = rss.Element("channel");

                rsss.Add(
                    new XElement("item",
                    new XElement("title", new XCData(title)),
                    new XElement("guid", guid, new XAttribute("isPermaLink", "false")),
                    new XElement("description", new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(allcontent))),
                    //  new XElement(content_encoded, new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(allcontent))),
                    new XElement("link", link),
                    new XElement("pubDate", GetRFC822Date(pubDate)),
                    new XElement("category", category),
                    new XElement(dc_creator, creator)
                  //  new XElement("enclosure", new XAttribute("url", info.Headframe ?? string.Empty), new XAttribute("type", "image/jpeg"), new XAttribute("length", "1"))
                    ));
                //檢查是否有圖
                //if (!string.IsNullOrEmpty(info.Headframe))
                //{
                //    XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
                //    rrrr.Add(
                //        new XElement(media_content, new XAttribute("url", info.Headframe), new XAttribute("type", "image/jpeg")),
                //        new XElement(media_thumbnail, new XAttribute("url", info.Headframe)),
                //        new XElement(media_keywords, keywords),
                //        new XElement(dcterms_valid, valid)
                //        );
                //}

                c++;
            }


            var settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Entitize,
                NewLineOnAttributes = true,
                Indent = true
            };

            StringBuilder sb = new StringBuilder();

            using (XmlWriter xw = XmlWriter.Create(sb, settings))
            {
                rss.Save(xw);
            }
            HttpResponseMessage response = this.Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(sb.ToString().Replace("utf-16", "utf-8").Replace("<oembed url=", "<iframe src=").Replace("</oembed>", "</iframe>"), Encoding.UTF8, "application/rss+xml");
            return response;


        }

        /// <summary>
        /// FBIARss
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("FBIARss")]
        public HttpResponseMessage FBIARss()
        {

            XNamespace content = "http://purl.org/rss/1.0/modules/content/";
            XName content_encoded = content + "encoded";
            string href = Request.RequestUri.AbsoluteUri;

            XDocument rss2 = new XDocument();
            rss2.Add(
                new XElement("rss",
                new XAttribute("version", "2.0"),
                new XAttribute(XNamespace.Xmlns + "content", content.NamespaceName),
                new XElement("channel",
                new XElement("title", new XCData("四季線上")),
                new XElement("link", "https://www.4gtv.tv"),
                new XElement("description", new XCData("四季線上4gtv")),
                new XElement("language", "zh-tw"),
                new XElement("lastBuildDate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ"))

                        )
                    )
                );
            XElement rss = XElement.Parse(rss2.ToString());
            //取得直
            var GetArticleToFBIA = _Article_BLL.GetArticleToFBIA();


            int c = 0;

            foreach (var info in GetArticleToFBIA)
            {
                string title = info.Title;
                string vendor = info.Vendor;
                string artcat = info.ArtCat;
                string guid = info.Guid;
                string description = info.Description;
                string Content = info.Content;
                string link = info.Link;
                string author = "四季娛樂";
                DateTime op_published = info.StartDate.Value;
                DateTime op_modified = info.UpdateTime.Value;
                //   DateTime op_modified = DateTime.Now;
                string imageURL = info.ImageURL;
                string videoURL = info.VideoURL;
                string footer_placement = "586019318756725_586019358756721";
                string body_placement = "586019318756725_588923968466260";
                switch (vendor)
                {
                    case "0":
                        vendor = "四季線上".ToString();
                        break;
                    case "1":
                        vendor = "民視新聞".ToString();
                        break;
                    case "2":
                        vendor = "Now健康".ToString();
                        break;
                    case "3":
                        vendor = "今健康".ToString();
                        break;
                    case "4":
                        vendor = "健康醫療網".ToString();
                        break;
}

                // string content_encoded = string.Empty;

                string html = @"<!doctype html><html lang ='en' prefix='op: http://media.facebook.com/op#'>";

                string head = @"<head>";

                head += @"<meta charset='utf-8'> <link rel='canonical' href='" + link + @"'>";

                head += @"<meta property='op:markup_version' content='v1.0'>";

                head += @"<meta property = 'fb:use_automatic_ad_placement' content = 'enable=true ad_density=default'>";

                head += @"<meta property = 'fb:op-recirculation-ads' content = 'placement_id=" + footer_placement + @"'>";

                head += @"</head>";




                string body = @"<body><article>";
                //頁首
                string header = @"<header>";

                //影片
                if (!string.IsNullOrEmpty(videoURL))
                {
                    header += @"<figure><video><source src='" + videoURL + "' type='video/mp4'/></video></figure>";
                }

                //代表圖
                header += @"<figure><img src='" + imageURL + "' /></figure> ";

                //標題
                header += @"<h1>" + title + @"</h1>";
                //廣告

                header += @"<figure class='op-ad'> <iframe width='300' height='250' style='border:0; margin:0;' src='https://www.facebook.com/adnw_request?placement=" + body_placement + @"&adtype=banner300x250'></iframe> </figure>";

                //推時間
                header += @"<time class='op-published' dateTime='" + op_published.ToString("yyyy-MM-ddTHH:mm:ssZ") + @"'>" + op_published.ToString("yyyy/MM/dd HH:mm:ss") + @"</time>";
                //更新時間
                header += @"<time class='op-modified' dateTime='" + op_modified.ToString("yyyy-MM-ddTHH:mm:ssZ") + @"'>" + op_modified.ToString("yyyy/MM/dd HH:mm:ss") + @"</time>";

                header += @"</header>";

                string ArticleBody = Content.Replace(@"<figure class=""media""><oembed url=", "<figure class='op-interactive'><iframe width='560' height='315' src=").Replace("</oembed></figure>", "</iframe></figure>");

                //延伸閱讀
                string Reading = @"";
                if (info.FurtherReadingID != null && info.FurtherReadingID.Count() > 0)
                {
                    Reading += @"<h2>【延伸閱讀】</h2><ul class='op-related-articles'>";

                    foreach (var r in info.FurtherReadingID)
                    {
                        Reading += @"<li>";
                        Reading += @"<a href='https://www.4gtv.tv/article/" + r + @"'></a>";
                        Reading += @"</li>";
                    }
                    Reading += @"</ul>";
                }
                ArticleBody += Reading;
                //頁尾
                string footer = @"<footer>";
                //相關文章
                string Promo = @"";
                if (info.PromoID != null && info.PromoID.Count() > 0)
                {
                    Promo += @"<ul class='op-related-articles'>";

                    foreach (var p in info.PromoID)
                    {
                        Promo += @"<li>";
                        Promo += @"<a href='https://www.4gtv.tv/article/" + p + @"'></a>";
                        Promo += @"</li>";
                    }
                    Promo += @"</ul>";
                }

                footer += Promo;

                //出處
                footer += @"<aside>";

                footer += @"<p><a rel='facebook' href='https://www.facebook.com/4gtv.tv/'>四季線上4gtv FB粉絲團</a></p>";
                footer += @"<p><a href='https://www.4gtv.tv/'>四季線上4gtv 首頁</a></p>";

                footer += @"</aside>";
                //著作
                footer += @"<small>Copyright © 2015 4gTV 鳳梨傳媒股份有限公司</small>";
                footer += @"</footer>";

                //GA
                string GA = @"
        <figure class='op-tracker'>
          <iframe>
          <script async src = 'https://www.googletagmanager.com/gtag/js?id=UA-63498140-2' ></script>
           <script>
           window.dataLayer = window.dataLayer || [];
                        function gtag() { dataLayer.push(arguments); }
                        gtag('js', new Date());
                        gtag('config', 'UA-63498140-2', {
                            'page_title' : '" + title + @"',
            'page_path': '" + "article/" + guid + @"',
            'page_location': location.href,
            'send_page_view': false,
            'campaign': {
                                'medium': 'Social Instant Article',
                'source': 'Facebook'
            },
            'custom_map': { 'dimension1': 'Content ID',     //1_文章ID
                            'dimension3': 'Mode',           //3_裝置
                            'dimension5': 'Category',       //5_來源
                            'dimension6': 'Type',           //6_主要文章分類
                            'dimension8': 'Content Vendor', //8_內容來源
                            'dimension9': 'Creator',        //9_編輯者
                            'dimension10': 'Memo',          //10_是否是有四季影音
                            'dimension11': 'Client ID',     //11_GA_ID
                            'dimension12': 'Account ID',    //12_登入紀錄
                          }
                        });
            gtag('event', 'page_view', { 'Content ID': '" + guid + @"',
                                       'Mode': 'FB', 
                                       'Category': 'article',
                                       'Type':  '" + artcat + @"',
                                       'Content Vendor': '" + vendor + @"',
                                       'Creator': 'job',
                                       'Memo': 'hasVideo=Y', //Y/N
                                       'Client ID': '',
                                       'Account ID': false,
                                     });  
        </script>
        </iframe>
        </figure>
        ";
                //GA
                string Alexa = @"
        <figure class='op-tracker'>
          <iframe>
           <script type='text/javascript'>
                _atrk_opts = {
                            atrk_acct:'+qzju1O7kI20L7',
                    domain: '4gtv.tv',
                    dynamic: true
                };
                        (function() {
                            var as = document.createElement('script');
                    as.type = 'text/javascript';
                    as.async = true;
                    as.src = 'https://certify-js.alexametrics.com/atrk.js';
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(as, s);
                        })();
            </script>
            < noscript >< img src = 'https://certify.alexametrics.com/atrk.gif?account=+qzju1O7kI20L7' style = 'display:none' height = '1' width = '1' alt = '' /></noscript>
                   </iframe>
        </figure>
        ";
                string Comscore = @"<figure class='op-tracker'>
              <iframe>
                  <script>
                var _comscore = _comscore || [];
                _comscore.push({ c1: '2', c2: '28889464' });
                (function () {
                    var s = document.createElement('script'), el = document.getElementsByTagName('script')[0]; s.async = true;
                    s.src = (document.location.protocol == 'https:' ? 'https://sb' : 'http://b') + '.scorecardresearch.com/beacon.js';
                    el.parentNode.insertBefore(s, el);
                })();              
                  </script>
              </iframe>
        </figure>";


                body += header;
                body += ArticleBody;
                body += footer;
                body += GA;
                body += Alexa;
                body += Comscore;
                body += @"</article></body>";


                html += head;
                html += body;

                html += @"</html>";


                XElement rsss = rss.Element("channel");
                rsss.Add(
                    new XElement("item",
                    new XElement("title", new XCData(title)),
                    new XElement("guid", guid, new XAttribute("isPermaLink", "false")),
                    new XElement("description", new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(description))),
                    new XElement("link", link),
                    new XElement("pubDate", op_modified.ToString("yyyy-MM-ddTHH:mm:ssZ")),
                    new XElement("author", author),
                     new XElement(content_encoded, new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(html)))
                    ));

                c++;
            }


            var settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Entitize,
                NewLineOnAttributes = true,
                Indent = true
            };

            StringBuilder sb = new StringBuilder();

            using (XmlWriter xw = XmlWriter.Create(sb, settings))
            {
                rss.Save(xw);
            }

            HttpResponseMessage response = this.Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(sb.ToString().Replace("utf-16", "utf-8"), Encoding.UTF8, "application/rss+xml");
            return response;
        }
        private static string GetRFC822Date(DateTime date)
        {
            int offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours;
            string timeZone = "+" + offset.ToString().PadLeft(2, '0');
            if (offset < 0)
            {
                int i = offset * -1;
                timeZone = "-" + i.ToString().PadLeft(2, '0');
            }
            return date.ToString("ddd, dd MMM yyyy HH:mm:ss " + timeZone.PadRight(5, '0'), CultureInfo.CreateSpecificCulture("en"));

        }
        //[HttpGet]
        //[Route("Rss")]
        //public Rss20FeedFormatter Rss()
        //{
        //    //var feed = new SyndicationFeed("Title", "Description", new Uri("SiteUrl"), "RSSUrl", DateTime.Now);
        //    SyndicationFeed feed = new SyndicationFeed();

        //    feed.Title = new TextSyndicationContent(@"<![CDATA[ 四季娛樂 ]]>");
        //    feed.Copyright = new TextSyndicationContent
        //    ("Copyright (C) 2018. All rights reserved.");
        //    feed.Description = new TextSyndicationContent
        //    ("RSS Feed Generated .NET Syndication Classes");
        //    feed.Generator = "My RSS Feed Generator";

        //    feed.Copyright = new TextSyndicationContent($"{DateTime.Now.Year} Mitchel Sellers");
        //    feed.ElementExtensions.Add("pubDate", "", DateTime.UtcNow.ToString("r"));
        //    var items = new List<SyndicationItem>();
        //    //var postings = _blogDataService.ListBlogForRss();
        //    //foreach (var item in postings)
        //    //{
        //    //    var postUrl = Url.Action("Article", "Blog", new { id = item.UrlSlug }, HttpContext.Request.Scheme);
        //    //    var title = item.Title;
        //    //    var description = item.Preview;
        //    //    items.Add(new SyndicationItem(title, description, new Uri(postUrl), item.UrlSlug, item.PostDate));
        //    //}

        //    //feed.Items = items;
        //    var settings = new XmlWriterSettings
        //    {
        //        Encoding = Encoding.UTF8,
        //        NewLineHandling = NewLineHandling.Entitize,
        //        NewLineOnAttributes = true,
        //        Indent = true
        //    };
        //    //using (var stream = new MemoryStream())
        //    //{
        //    //    using (var xmlWriter = XmlWriter.Create(stream, settings))
        //    //    {
        //    //        var rssFormatter = new Rss20FeedFormatter(feed, false);
        //    //        rssFormatter.WriteTo(xmlWriter);
        //    //        xmlWriter.Flush();

        //    //        return rssFormatter;
        //    //    }


        //    //}



        //    XNamespace extxmlns = "http://www.yoursite.com/someurl";
        //    feed.AttributeExtensions.Add(new XmlQualifiedName("extxx", XNamespace.Xmlns.NamespaceName), extxmlns.NamespaceName);
        //    feed.ElementExtensions.Add(new XElement(extxmlns + "link", new XAttribute("rel", "self"), new XAttribute("type", "application/rss+xml")));

        //    var postUrl = @"http://localhost/FourgTV_OTT2.Api2/Article/Rss";
        //    var title = "456";
        //    var description = "789";
        //    var i = new SyndicationItem(title, description, new Uri(postUrl));

        //    XNamespace _yahooMediaNamespace = "http://search.yahoo.com/mrss/";
        //    // Result is a SyndicationItem
        //    i.ElementExtensions.Add(
        //        new SyndicationElementExtension(
        //            new XElement(
        //                _yahooMediaNamespace + "thumbnail",
        //                new XAttribute("url", "http://www.example.com/image.jpg")
        //            )
        //        )
        //    );
        //    items.Add(i);
        //    feed.Items = items;
        //    var aaa = new Rss20FeedFormatter(feed, false);
        //    //  aaa.Feed.AttributeExtensions
        //    return aaa;
        //    // return new Rss20FeedFormatter(feed, false);
        //}

        //[HttpGet]
        //[Route("Rss")]
        //public HttpResponseMessage RSS()
        //{

        //    var aa = GetArticleToYahoo();

        //    var settings = new XmlWriterSettings
        //    {
        //        Encoding = Encoding.UTF8,
        //        NewLineHandling = NewLineHandling.Entitize,
        //        NewLineOnAttributes = true,
        //        Indent = true
        //    };

        //    StringBuilder sb = new StringBuilder();

        //    using (XmlWriter xw = XmlWriter.Create(sb, settings))
        //    {
        //        aa.Save(xw);
        //    }
        //    HttpResponseMessage response = this.Request.CreateResponse(HttpStatusCode.OK);
        //    response.Content = new StringContent(sb.ToString(), Encoding.UTF8, "application/xml");
        //    return response;


        //}





        /// <summary>
        /// GoogleRss
        /// </summary>
        /// <param name="Category">類別</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GoogleRss/{Category:int=0}")]
        public HttpResponseMessage GoogleRss(int? Category)
        {


            XNamespace media = "http://search.yahoo.com/mrss/";

            XNamespace dc = "http://purl.org/dc/elements/1.1/";
            XNamespace dcterms = "http://purl.org/dc/terms/";
            XNamespace content = "http://purl.org/rss/1.0/modules/content/";
            XNamespace atom = "http://www.w3.org/2005/Atom";

            XName media_content = media + "content";
            XName media_keywords = media + "keywords";
            XName media_credit = media + "credit";
            XName media_thumbnail = media + "thumbnail";
            XName dcterms_valid = dcterms + "valid";
            XName content_encoded = content + "encoded";
            XName atom_link = atom + "link";

            string href = Request.RequestUri.AbsoluteUri;

            string googleUTM = @"?utm_source=google_news&utm_medium=extended&utm_campaign=4gtvent";

            XDocument rss2 = new XDocument();
            rss2.Add(
                new XElement("rss",
                new XAttribute("version", "2.0"),
                // that's how you add namespaces to an XML document
                new XAttribute(XNamespace.Xmlns + "content", content.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "dc", dc.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "media", media.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "dcterms", dcterms.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "atom", atom.NamespaceName),
                new XElement("channel",
                new XElement("title", new XCData("四季線上")),
                new XElement(atom_link, new XAttribute("href", href), new XAttribute("rel", "self"), new XAttribute("type", "application/rss+xml")),
                new XElement("link", "https://www.4gtv.tv"),
                new XElement("description", new XCData("四季線上4gtv")),
                 new XElement("pubDate", GetRFC822Date(DateTime.Now))
                        //     new XElement("pubDate", DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en")))
                        )
                    )
                );
            XElement rss = XElement.Parse(rss2.ToString());


            var GetArticleToGoogle = _Article_BLL.GetArticleToGoogle(Category);

            //取得直



            int c = 0;

            foreach (var info in GetArticleToGoogle)
            {
                XName dc_creator = dc + "creator";

                string title = info.Title;
                string guid = info.Guid;
                string description = info.Description;




                string FurtherReadingHEML = string.Empty;

                string allcontrent = string.Empty;


                //20211005-kevin-減少延伸閱讀讀取次數
                //var ArticleFurtherReading = _Article_BLL.GetArticleFurtherReading(guid).Take(3).ToList();


                //if (ArticleFurtherReading.Count() > 0)
                //{
                //    FurtherReadingHEML = @"<br/><p class=""read-more-vendor""><span>更多四季線上報導</span><br/>";

                //    foreach (var _ArticleFurtherReading in ArticleFurtherReading)
                //    {

                //        FurtherReadingHEML += @"<br/><a href = " + @"https://www.4gtv.tv/article/" + _ArticleFurtherReading.fsArticleID + googleUTM + @">" + _ArticleFurtherReading.fsTitle + @"</a>";

                //    }
                //    FurtherReadingHEML += @"</p>";
                //}


                string creator = "四季娛樂";
                //string pubDate = info.PubDate.Value.ToString("ddd, dd MMM yyy HH:mm:ss zzz", CultureInfo.CreateSpecificCulture("en"));
                string pubDate = GetRFC822Date(info.PubDate.Value);
                string link = info.Link;
                string category = info.Category;
                string keywords = string.Empty;
                string valid = string.Empty;

                if (info.fdStartDate != null)
                {
                    valid = "start=" + GetRFC822Date(info.fdStartDate.Value);
                    if (info.fdEndDate != null)
                    {
                        valid += ";end= " + GetRFC822Date(info.fdEndDate.Value);
                    }

                }

                if (info.keywords != null && info.keywords.Count() > 0)
                {
                    for (var i = 0; i < info.keywords.Count(); i++)
                    {
                        keywords += info.keywords[i];
                        if (i + 1 != info.keywords.Count())
                        {
                            keywords += ", ";
                        }
                    }
                }
                XElement rsss = rss.Element("channel");

                if (string.IsNullOrEmpty(info.VideoURL))
                {

                    string image = info.ImageURL;

                    string htmlimage = @"<img src=""" + info.ImageURL + @""" alt = """ + title.Replace(@"""", @"") + @""">";


                    allcontrent = htmlimage + description + FurtherReadingHEML;
                }
                else
                {
                    allcontrent = description + FurtherReadingHEML;
                }


                string description90 = string.Empty;

                if (System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "").Length > 90)
                {

                    description90 = System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "").Substring(0, 90) + "...";
                }
                else
                {
                    description90 = System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", "");
                }


                rsss.Add(
         new XElement("item",
         new XElement("title", new XCData(title)),
         new XElement("guid", guid, new XAttribute("isPermaLink", "false")),
         new XElement("description", new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(description90))),
         new XElement(content_encoded, new XCData(clsTOOL.ReplaceLowOrderASCIICharacters(allcontrent))),
         new XElement("link", link),
         new XElement("pubDate", pubDate),
         new XElement("category", category),
         new XElement(dc_creator, creator)
         ));



                //檢查是否有影片
                if (string.IsNullOrEmpty(info.VideoURL))
                {
                    XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
                    rrrr.Add(
                        // new XElement(media_content, new XAttribute("url", info.ImageURL), new XAttribute("type", "image/jpeg")),
                        new XElement(media_thumbnail, new XAttribute("url", info.ImageURL)),
                        new XElement(media_keywords, keywords),
                        new XElement(media_credit, "四季線上"),
                        new XElement(dcterms_valid, valid)
                        );
                }
                else
                {
                    XElement rrrr = rss.Element("channel").Elements("item").ElementAt(c);
                    rrrr.Add(
                        new XElement(media_content, new XAttribute("url", info.VideoURL), new XAttribute("type", "video/mp4"), new XAttribute("isDefault", "true"), new XAttribute("medium", "video")),
                        new XElement(media_thumbnail, new XAttribute("url", info.ImageURL)),
                        new XElement(media_keywords, keywords),
                        new XElement(media_credit, "四季線上"),
                        new XElement(dcterms_valid, valid)
                        );
                }
                c++;
            }


            var settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Entitize,
                NewLineOnAttributes = true,
                Indent = true
            };

            StringBuilder sb = new StringBuilder();

            using (XmlWriter xw = XmlWriter.Create(sb, settings))
            {
                rss.Save(xw);
            }
            HttpResponseMessage response = this.Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(sb.ToString().Replace("utf-16", "utf-8").Replace("<oembed url=", "<iframe src=").Replace("</oembed>", "</iframe>"), Encoding.UTF8, "application/rss+xml");
            return response;


        }



        /// <summary>
        /// 健康館首頁上方
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleHealthBanner")]
        [HttpGet]
        public StdResp<List<GetArticleHealthBanner_Res>> GetArticleHealthBanner()
        {
            try
            {
                StdResp<List<GetArticleHealthBanner_Res>> stdResp = new StdResp<List<GetArticleHealthBanner_Res>>();

                stdResp.Data = clsTOOL.ConvertListModelToListModel<GetArticleHomeBanner_Res, GetArticleHealthBanner_Res>(_Article_BLL.GetArticleHomeBanner(15));
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 取得健康文章推薦首頁
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleHealthPromo")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 60, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleHealthPromo_Res>> GetArticleHealthPromo()
        {
            try
            {
                StdResp<List<GetArticleHealthPromo_Res>> stdResp = new StdResp<List<GetArticleHealthPromo_Res>>();
                List<GetArticleHealthPromo_Res> getArticlePromo_Res = new List<GetArticleHealthPromo_Res>();
                getArticlePromo_Res = clsTOOL.ConvertListModelToListModel<GetArticlePromo_Res, GetArticleHealthPromo_Res>(_Article_BLL.GetArticlePromo());
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 健康專題
        /// </summary>
        /// <returns></returns>
#if PRD
        //  [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        [Route("GetArticleHealthProject")]
        [HttpGet]
        public StdResp<List<GetArticleHealthProject_Res>> GetArticleHealthProject()
        {
            try
            {
                StdResp<List<GetArticleHealthProject_Res>> stdResp = new StdResp<List<GetArticleHealthProject_Res>>();
                stdResp.Data = clsTOOL.ConvertListModelToListModel<GetArticleProject_Res, GetArticleHealthProject_Res>(_Article_BLL.GetArticleProject());
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 文章健康搜尋
        /// </summary>
        /// <returns></returns>
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        [Route("GetArticleHealthSearch")]
        [HttpGet]
        public StdResp<GetArticleSearch_Res> GetArticleHealthSearch(int? Index, int? PageSize)
        {

            GetArticleSearch_Req req = new GetArticleSearch_Req()
            {
                Index = Index,
                PageSize = PageSize
            };

            try
            {
                if (req == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.Index == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.PageSize == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.Index.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (req.PageSize.Value < 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }

                StdResp<GetArticleSearch_Res> stdResp = new StdResp<GetArticleSearch_Res>();


                stdResp.Data = _Article_BLL.GetArticleSearch(req);
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }
        /// <summary>
        /// 取得健康首頁右邊
        /// </summary>
        /// <returns></returns>
        [Route("GetArticleHealthRight")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetArticleHealthRight_Res>> GetArticleHealthRight()
        {
            try
            {
                StdResp<List<GetArticleHealthRight_Res>> stdResp = new StdResp<List<GetArticleHealthRight_Res>>();
                List<GetArticleHealthRight_Res> getArticlePromo_Res = new List<GetArticleHealthRight_Res>();
                getArticlePromo_Res = clsTOOL.ConvertListModelToListModel<GetArticleHomeRight_Res, GetArticleHealthRight_Res>(_Article_BLL.GetArticleHomeRight());
                stdResp.Data = getArticlePromo_Res;
                return stdResp;
            }
            catch (MyException e)
            {
                throw e;
            }
        }

    }

}

