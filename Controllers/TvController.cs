﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;

using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;

using WebApi.OutputCache.V2;
using System.Globalization;
using System.Web.Configuration;
using FourgTV_OTT2.BLL.API;
using System.Transactions;
using ZXing;
using ZXing.QrCode;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.BLL.Model.Res.TV;
using FourgTV_OTT2.BLL.Model.Req.TV;
using FourgTV_OTT2.DAL.Model.Condition;
using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.Model.Model;
using FourgTV_OTT2.BLL.Module;
using FourgTV_OTT2.BLL.Model.Req.Heran;
using FourgTV_OTT2.DAL.DAL;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Tv")]
    public class TvController : ApiController
    {

        private static readonly string fsLOG_PATH = WebConfigurationManager.AppSettings["fsLOG_PATH"];
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];

        Tv_BLL _Tv_BLL = new Tv_BLL();
        Account_BLL _Account_BLL = new Account_BLL();
        tbmCHANNEL_DAL _tbmCHANNEL_DAL = new tbmCHANNEL_DAL();
        public string ClientIP
        {
            get
            {
                var obj = LogHelper.RequestVariables[LogRequestVariableType.ClientIP];
                return obj == null ? "" : obj.ToString();
            }
            set
            {
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.ClientIP, value);
            }
        }
        /// <summary>
        /// 取得頻道URL
        /// </summary>
        /// <param name="clsTV_GET_CHANNEL_URL_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetChannelUrl")]
        [Header(headersConfig = "fsENC_KEY,string|fsVERSION,string|fsDEVICE,string")]
        public HttpResponseMessage PostChannelUrl(clsTV_GET_CHANNEL_URL_ARGS clsTV_GET_CHANNEL_URL_ARGS)
        {

            string fsDEVICE = Request.Headers.Contains("fsDEVICE") ? Request.Headers.GetValues("fsDEVICE").First() : string.Empty;
            string fsVERSION = Request.Headers.Contains("fsVERSION") ? Request.Headers.GetValues("fsVERSION").First() : string.Empty;

            int version = 0; // 版本

            string CDNID = clsTV_GET_CHANNEL_URL_ARGS.fsASSET_ID;

            try
            {

                version = int.Parse(fsVERSION.Replace(".", ""));
            }
            catch (Exception e)
            {

            }



            //擋掉黑名單IP
            try
            {
                List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPIP.txt", System.Text.Encoding.UTF8).ToList();
                lstSTOP_USERs = lstSTOP_USERs.Where(x => !string.IsNullOrEmpty(x)).ToList();

                if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
                {
                    if (lstSTOP_USERs.FirstOrDefault(f => HttpContext.Current.Request.UserHostAddress.IndexOf(f) > -1) != null)
                    {
                        clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                        clsGET_CHANNEL_URL.fsCHANNEL_NAME = "";
                        clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                        clsGET_CHANNEL_URL.flstURLs = new List<string>();
                        clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area8/index.m3u8");
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                    }
                }
            }
            catch (Exception e)
            {

            }


            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    string fsIP = HttpContext.Current.Request.UserHostAddress;
                    UserInfo clsUSER_DATA = null;
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsTV_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsTV_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsTV_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsTV_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
                    }
                    //寫LOG
                    new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
                    {
                        fsFROM = "3",
                        fsNAME = "CHANNEL",
                        fsASSET_ID = CDNID,
                        fsIP = HttpContext.Current.Request.UserHostAddress,
                        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                        fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
                        fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
                    });
                    //測試環境才用
                    if (int.Parse(fcIS_PRODUCTION) == 0)
                    {
                        if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
                            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                    }
                    else
                    {
                        if (fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                    }
                    //是否為TV專用免費頻道
                    //2021-11-04 舊的寫法參考tbzCONFIG裡的值，但頻道清單卻是看tbmCHANNEL_SET，使用起來不方便所以改掉
                    //bool fbTV_FREE_CHANNEL = !string.IsNullOrEmpty(new repCONFIG().fnGET_CFG("TV_CHANNEL_FREE_SET")[0].fsVALUE.Split(';').ToList().Find(x => x.Equals(clsTV_GET_CHANNEL_URL_ARGS.fnCHANNEL_ID.ToString())));
                    //10是tbmSET裡面tv的免費頻道fnID
                    //2021-11-05 SP會帶上pc的免費頻道所以改寫法
                    //bool fbTV_FREE_CHANNEL = new repCHANNEL().fnGET_CHANNEL_BY_SETID(10, "TV", "L").Exists(t => t.fnID == clsTV_GET_CHANNEL_URL_ARGS.fnCHANNEL_ID);
                    bool fbTV_FREE_CHANNEL = tbmCHANNEL_SET_DAL.fnGet_tbmCHANNEL_SETInfoListByfnSET_ID(10).Exists(t => t.fnCHANNEL_ID == clsTV_GET_CHANNEL_URL_ARGS.fnCHANNEL_ID);

                    CDNID = clsM3U8.changeCDNIDForOlympics(fsIP, CDNID);



                    //取得頻道資訊
                    clsCHANNEL clsCHANNEL = new repCHANNEL().fnGET_CHANNEL(CDNID);
                    clsGET_CHANNEL_TV_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_TV_URL();
                    clsGET_CHANNEL_URL.flstURLs = new List<string>();
                    clsGET_CHANNEL_URL.fsHEAD_FRAME = clsCHANNEL.fsHEAD_FRAME;
                    clsGET_CHANNEL_URL.flstBITRATE = clsCHANNEL.lstALL_BITRATE.Select<string, int>(q => int.Parse(q)).OrderBy(c => c).ToList();
                    clsGET_CHANNEL_URL.fsCHANNEL_NAME = clsCHANNEL.fsNAME;

                    //判斷是否為台灣的IP或允許海外播放
                    switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsCHANNEL.fcOVERSEAS, clsCHANNEL.lstEXCEPT_COUNTRY))
                    {
                        case "IsAllowArea"://允許播放
                            break;
                        case "OVERSEAS"://不允許海外
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EXCEPT_COUNTRY"://海外限制不能播放的國家
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }

                    //特規
                    if (fsDEVICE.Equals("GLOBAL"))
                    {

                        //找頻道清單
                        var list = _tbmCHANNEL_DAL.fnGet_tbmCHANNELListInfoByfsPARTNER("GLOBAL");

                        var fsASSET_IDs = list.Select(x => x.fs4GTV_ID).ToList();

                        if (fsASSET_IDs.Contains(clsTV_GET_CHANNEL_URL_ARGS.fsASSET_ID))
                        {
                            if (!clsTV_GET_CHANNEL_URL_ARGS.fsASSET_ID.Equals(CDNID))
                            {
                                clsTV_GET_CHANNEL_URL_ARGS.fsASSET_ID = CDNID;
                            }


                            clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                            clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                            clsGET_CHANNEL_URL.flstBITRATE = new List<int> { 360, 480, 720 };
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, CDNID, fsIP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                            clsGET_CHANNEL_URL.fbPLAY_AD = true;
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }



                    //查看是否為會員
                    if (clsUSER_DATA != null)
                    {
                        try
                        {
                            //取得黑名單
                            List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPUSER.txt", System.Text.Encoding.UTF8).ToList();
                            lstSTOP_USERs = lstSTOP_USERs.Where(x => !string.IsNullOrEmpty(x)).ToList();
                            if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
                            {
                                if (lstSTOP_USERs.FirstOrDefault(f => f.Equals(clsUSER_DATA.fsUSER)) != null)
                                {
                                    //無權限可以播
                                    // return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    clsGET_CHANNEL_URL.fsCHANNEL_NAME = "";
                                    clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                                    clsGET_CHANNEL_URL.flstURLs = new List<string>();
                                    clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area8/index.m3u8");
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                                }
                            }
                        }
                        catch (Exception e)
                        {
                            System.IO.File.AppendAllText(fsLOG_PATH + DateTime.Now.ToString("yyyyMMdd") + ".txt", "發生時間:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "，錯誤訊息:" + e + "=>" + e.Message + "\r\n");

                        }
                        //取得會員頻道權限
                        clsGET_MEMBER_CHANNEL_AUTH clsMEMBER_CHANNEL_AUTH = new repCHANNEL().fnGET_MEMBER_CHANNEL_AUTH(clsUSER_DATA.fsACCOUNT_ID, CDNID);

                        clsGET_CHANNEL_URL.fbPLAY_AD = clsMEMBER_CHANNEL_AUTH.fcAD;
                        //是否有觀看權限
                        bool fbAUTH = clsMEMBER_CHANNEL_AUTH.fcAUTH;
                        //是否為付費會員
                        bool fbIS_PAY_MEMBER = clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER;
                        //確認是否有試看期間
                        bool clsMEMBER_PROMO = new repACCOUNT().fnGET_ACCOUNT_INFO(clsUSER_DATA.fsACCOUNT_ID).fnLEFT_PROMO_DAYS > 0;

                        //是否有禾聯
                        //if (fsDEVICE.Equals("HERAN"))
                        //{

                        //    //暫時先這樣
                        //    clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                        //    clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                        //    if (version >= 126)
                        //    {
                        //        clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetMediatailorURLHERAN(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME, fsIP));
                        //        clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "heran-mozai", "high"));
                        //    }
                        //    else
                        //        clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "heran-mozai", "high"));
                        //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                        //}
                        //暫時先這樣
                        HeranModule _HeranModule = new HeranModule();
                        var HERAN_MACHINEs = _HeranModule.GetHERAN_MACHINE_Auth_BYfsACCOUNT_ID(clsUSER_DATA.fsACCOUNT_ID, clsTV_GET_CHANNEL_URL_ARGS.fnCHANNEL_ID);

                        //有觀看權限跟付費會員
                        if (fbAUTH && fbIS_PAY_MEMBER)
                        {
                            clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                            clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                        }
                        //確認是否有試看期間 無時間 有廣告
                        else if (clsMEMBER_PROMO)
                        {
                            clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                            clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                            clsGET_CHANNEL_URL.fbPLAY_AD = true;
                        }
                        else if (HERAN_MACHINEs != null)//禾聯權限
                        {
                            //暫時先這樣
                            clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                            clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                            clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER = true;
                            clsGET_CHANNEL_URL.fbPLAY_AD = HERAN_MACHINEs.fcAD;
                        }
                        else
                        {
                            //TV版 無權限 有付費 有廣告
                            if (!fbAUTH && fbIS_PAY_MEMBER)
                            {
                                clsGET_CHANNEL_URL.fbPLAY_AD = true;
                            }
                            //非免費頻道
                            if (!fbTV_FREE_CHANNEL)
                            {
                                //取得TV免費觀看資訊
                                clsGET_MEMBER_FREE_TV_WATCH clsMEMBER_FREE_TV_WATCH_INFO = new repACCOUNT().fnGET_MEMBER_FREE_TV_WATCH(clsUSER_DATA.fsACCOUNT_ID);
                                //開始免費時間轉unixtime
                                Int32 fnFREESTARTTIME = (Int32)(clsMEMBER_FREE_TV_WATCH_INFO.fdFREESTART_DATE.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                                //免費觀看的總時間(秒)
                                int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("TV_CHANNEL_FREE_SEC")[0].fsVALUE);
                                //現在的時間(unixTime)
                                Int32 fnUNIXTIMESTAMP = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                                //檢查是否有超過免費觀看時段
                                var watchingTime = fnUNIXTIMESTAMP - fnFREESTARTTIME;
                                if (fnALLOWTIME > watchingTime)
                                {
                                    clsGET_CHANNEL_URL.fnFREESTARTTIME = fnFREESTARTTIME;
                                    //值固定，前端會自己隨時計算，避免不轉台就可以一直看
                                    clsGET_CHANNEL_URL.fnALLOWTIME = fnALLOWTIME;
                                }
                                else
                                {
                                    //無權限可以播
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                            }
                            else
                            {
                                clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                                clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                            }
                        }

                        if (fsDEVICE.Equals("HERAN"))
                        {
                            if (version >= 126)
                            {
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetMediatailorURLHERAN(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME, fsIP));
                                //clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "heran-mozai", "high"));
                            }
                            else
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "heran-mozai", "high"));
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }

                        if (fsDEVICE.Equals("KBRO"))
                        {
                            if (version >= 126 && clsTOOL.IsInTheNetworkSegment(fsIP, clsTOOL.GetKBROCIDRSubnets()))
                            {
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.KBROURL(CDNID));
                            }
                            else
                            {

                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "a0aka-mozai", "high"));

                            }


                            // clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "aka-mozai", "high"));
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }

                        if (fsDEVICE.Equals("TV"))
                        {
                            if (version >= 127)
                            {
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetMediatailorURL(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME, fsIP));
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, CDNID, fsIP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                            }
                            else if (version == 126)
                            {
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetMediatailorURLHERAN(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME, fsIP));
                            }
                            else
                            {
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, CDNID, fsIP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                                clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                            }

                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                        }

                        clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, CDNID, fsIP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                        clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));


                    }
                    else
                    {

                        //判斷是否為免費且沒會員
                        if (fbTV_FREE_CHANNEL)
                        {
                            clsGET_CHANNEL_URL.fbPLAY_AD = true;
                            clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                            clsGET_CHANNEL_URL.fnALLOWTIME = -1;


                            if (fsDEVICE.Equals("HERAN"))
                            {

                                //暫時先這樣
                                clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                                clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                                if (version >= 126)
                                {
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetMediatailorURLHERAN(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME, fsIP));
                                    //clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "heran-mozai", "high"));
                                }
                                else
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "heran-mozai", "high"));
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }

                            if (fsDEVICE.Equals("KBRO"))
                            {
                                if (version >= 126 && clsTOOL.IsInTheNetworkSegment(fsIP, clsTOOL.GetKBROCIDRSubnets()))
                                {
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.KBROURL(CDNID));
                                }
                                else
                                {

                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.PMozai(CDNID, "a0aka-mozai", "high"));

                                }
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }

                            if (fsDEVICE.Equals("TV"))
                            {
                                if (version >= 127)
                                {
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetMediatailorURL(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME, fsIP));
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, CDNID, fsIP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                                }
                                else if (version == 126)
                                {
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetMediatailorURLHERAN(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME, fsIP));
                                }
                                else
                                {
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, CDNID, fsIP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                                    clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));

                                }

                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };


                            }

                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, CDNID, fsIP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(false, clsTV_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", CDNID, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));

                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }





                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得可使用服務
        /// </summary>
        /// <returns></returns>
        [Route("GetServiceInfo")]
        public HttpResponseMessage PostServiceInfo(clsTV_GET_MEMBER_SERVICE_INFO_ARGS clsTV_GET_MEMBER_SERVICE_INFO_ARGS)
        {
            try
            {
                // 判斷是否已登入
                if (clsSECURITY.IsLogin(clsTV_GET_MEMBER_SERVICE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsTV_GET_MEMBER_SERVICE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsTV_GET_MEMBER_SERVICE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsTV_GET_MEMBER_SERVICE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBSM().fnGET_MEMBER_SERVICE_INFO(clsUSER_DATA.fsACCOUNT_ID, clsTV_GET_MEMBER_SERVICE_INFO_ARGS.fsFROM) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得購買紀錄清單
        /// </summary>
        /// <returns></returns>
        [Route("GetPurchaseInfo")]
        public HttpResponseMessage PostPurchaseInfo(clsTV_GET_MEMBER_PURCHASE_INFO_ARGS clsTV_GET_MEMBER_PURCHASE_INFO_ARGS)
        {
            try
            {
                // 判斷是否已登入
                if (clsSECURITY.IsLogin(clsTV_GET_MEMBER_PURCHASE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsTV_GET_MEMBER_PURCHASE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsTV_GET_MEMBER_PURCHASE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsTV_GET_MEMBER_PURCHASE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBSM().fnGET_MEMBER_PURCHASE_INFO(clsUSER_DATA.fsACCOUNT_ID, clsTV_GET_MEMBER_PURCHASE_INFO_ARGS.fsOTPW) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }


        /// <summary>
        /// 取得套餐
        /// </summary>
        /// <param name="clsGET_CHANNEL_SET"> 取得套餐</param>
        /// <returns></returns>
        /// <response code="001">未登入</response>
        /// <response code="002">查無此裝置</response>
        /// <response code="003">無訂單</response>
        [Route("GetChannelSet")]
        [Header(headersConfig = "fsENC_KEY,string|fsVALUE,string|fsDEVICE,string")]
        [HttpPost]
        public StdResp<List<GetChannelSet_Res>> GetChannelSet2()
        {
            //是否登入

            StdResp<List<GetChannelSet_Res>> stdResp = new StdResp<List<GetChannelSet_Res>>();

            stdResp.Data = _Tv_BLL.GetChannelSet();

            return stdResp;
        }


        /// <summary>
        /// 訪客註冊登入
        /// </summary>
        /// <param name="clsGUEST_SIGN_UP"></param>
        /// <returns></returns>
        [Route("GuestSignUp")]
        public HttpResponseMessage PostGuestSignUp(clsGUEST_SIGN_UP clsGUEST_SIGN_UP)
        {
            try
            {
                if (String.IsNullOrEmpty(clsGUEST_SIGN_UP.fsPARTNER_VALUE1) || String.IsNullOrEmpty(clsGUEST_SIGN_UP.fsFROM) || String.IsNullOrEmpty(clsGUEST_SIGN_UP.fsENC_KEY))
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                string fsFROM = clsGUEST_SIGN_UP.fsFROM == null ? string.Empty : clsGUEST_SIGN_UP.fsFROM;
                string fsLINK_ID = String.Empty;

                switch (fsFROM)
                {
                    case "CNS":
                        #region - CNS -
                        CnsDeviceInfo cnsDeviceInfo;
                        try
                        {
                            cnsDeviceInfo = Tv_BLL.GetCnsDeviceInfo(clsGUEST_SIGN_UP.fsPARTNER_VALUE1);
                        }
                        catch (Exception e)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        if (clsTOOL.IsDefault(cnsDeviceInfo) || String.IsNullOrEmpty(cnsDeviceInfo.subsid))
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                        fsLINK_ID = cnsDeviceInfo.subsid;
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                var accountId = Tv_BLL.GetAccountIdByCnsLinkId(fsLINK_ID);
                                var fsVALUE = String.Empty;
                                if (String.IsNullOrEmpty(accountId))
                                {
                                    //註冊，虛擬帳號密碼，登入時用fsLINK_ID，比照Google或Facebook
                                    var user = String.Empty;
                                    while (String.IsNullOrEmpty(user))
                                    {
                                        var checkUser = Guid.NewGuid().ToString().Replace("-", "");
                                        if (!_Account_BLL.CheckUserID(new BLL.Model.Req.Account.CheckUserID_req() { fsUserID = checkUser })) user = checkUser;
                                    }
                                    var password = Guid.NewGuid().ToString().Replace("-", "");
                                    var clsREGISTER = new repACCOUNT().fnREGISTER(4, ((int)FourgTV_OTT2.Common.Setting.EnumSetting.LoginType.CNS).ToString("d2"), user, fsLINK_ID, 0, "", password);

                                    if (clsREGISTER != null && clsREGISTER.fbRESULT)
                                    {

                                        string fsDEVICE = Request.Headers.Contains("fsDEVICE") ? Request.Headers.GetValues("fsDEVICE").First() : string.Empty;
                                        string fsVERSION = Request.Headers.Contains("fsVERSION") ? Request.Headers.GetValues("fsVERSION").First() : string.Empty;
                                        string fsENC_KEY = Request.Headers.Contains("fsENC_KEY") ? Request.Headers.GetValues("fsENC_KEY").First() : string.Empty;

                                        //寫LOG
                                        new repLOG().fnINSERT_MEMBER_REGISTER_LOG(new clsLOG_MEMBER_REGISTER_ARGS()
                                        {
                                            fsUSER_ID = user,
                                            fsENC_KEY = fsENC_KEY,
                                            fsDEVICE = fsDEVICE,
                                            fsVERSION = fsVERSION,
                                            fsIP = HttpContext.Current.Request.UserHostAddress,
                                            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                        });
                                    }
                                    else
                                    {
                                        //新增失敗
                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsREGISTER.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    }

                                    accountId = Tv_BLL.GetAccountIdByCnsLinkId(fsLINK_ID);
                                }

                                // 訂閱狀態正常則新增訂單
                                if (cnsDeviceInfo.status == "001")
                                {
                                    _Tv_BLL.CreateCnsOrder(cnsDeviceInfo, accountId);
                                }

                                clsLOGIN clsLOGIN = new repACCOUNT().fnSIGNIN(fsLINK_ID, String.Empty, false);
                                if (!string.IsNullOrEmpty(clsLOGIN.fsACCOUNT_ID))
                                {
                                    //登入成功
                                    string fsENC_KEY = clsGUEST_SIGN_UP.fsENC_KEY;
                                    //取得Token
                                    string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
                                    //取得有效的TimeSpan日期
                                    long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddYears(100).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                                    //加密
                                    string fsVALIDATE_CODE = clsSECURITY.Encrypt(clsLOGIN.fsACCOUNT_ID + fsTOKEN + clsLOGIN.fsUSER + fdEXPIRE_TIME.ToString(), fsENC_KEY);
                                    //cookie加密
                                    fsVALUE = clsSECURITY.CookieEncrypt(clsLOGIN.fsACCOUNT_ID + "-" + fsTOKEN + "-" + clsLOGIN.fsUSER + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE, fsENC_KEY);

                                    //寫LOG
                                    new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
                                    {
                                        fsFROM = "3",
                                        fsIP = HttpContext.Current.Request.UserHostAddress,
                                        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                        fsUSER_ID = clsLOGIN.fsUSER,
                                        fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                    });
                                }
                                else
                                {
                                    //登入失敗
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }

                                // 取帳戶資訊，回傳register type用
                                var accountInfo = new repACCOUNT().fnGET_ACCOUNT_INFO(clsLOGIN.fsACCOUNT_ID);

                                //暫時帳號就給訂編(subsid, fsLINK_ID)，正式帳號就給使用者帳號(fsUSER_ID)
                                var userId = accountInfo.fnREGISTER_TYPE == 4 ? fsLINK_ID : accountInfo.fsUSER_ID;

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new { fsUSER_ID = userId, fsVALUE = fsVALUE, fnREGISTER_TYPE = accountInfo.fnREGISTER_TYPE } }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion
                    case "KBRO":
                        #region - KBRO -
                        KbroUserInfo kbroUserInfo;
                        try
                        {
                            kbroUserInfo = Tv_BLL.GetKbroUserInfo(clsGUEST_SIGN_UP.fsPARTNER_VALUE1);
                        }
                        catch (Exception e)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        if (clsTOOL.IsDefault(kbroUserInfo) || String.IsNullOrEmpty(kbroUserInfo.OTT_accountID))
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                        fsLINK_ID = kbroUserInfo.OTT_accountID;
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                var accountId = Tv_BLL.GetAccountIdByKbroLinkId(fsLINK_ID);
                                var fsVALUE = String.Empty;
                                if (String.IsNullOrEmpty(accountId))
                                {
                                    //註冊，虛擬帳號密碼，登入時用fsLINK_ID，比照Google或Facebook
                                    var user = String.Empty;
                                    while (String.IsNullOrEmpty(user))
                                    {
                                        var checkUser = Guid.NewGuid().ToString().Replace("-", "");
                                        if (!_Account_BLL.CheckUserID(new BLL.Model.Req.Account.CheckUserID_req() { fsUserID = checkUser })) user = checkUser;
                                    }
                                    var password = Guid.NewGuid().ToString().Replace("-", "");
                                    var clsREGISTER = new repACCOUNT().fnREGISTER(4, ((int)FourgTV_OTT2.Common.Setting.EnumSetting.LoginType.Kbro).ToString("d2"), user, fsLINK_ID, 0, "", password);

                                    if (clsREGISTER != null && clsREGISTER.fbRESULT)
                                    {

                                        string fsDEVICE = Request.Headers.Contains("fsDEVICE") ? Request.Headers.GetValues("fsDEVICE").First() : string.Empty;
                                        string fsVERSION = Request.Headers.Contains("fsVERSION") ? Request.Headers.GetValues("fsVERSION").First() : string.Empty;
                                        string fsENC_KEY = Request.Headers.Contains("fsENC_KEY") ? Request.Headers.GetValues("fsENC_KEY").First() : string.Empty;

                                        //寫LOG
                                        new repLOG().fnINSERT_MEMBER_REGISTER_LOG(new clsLOG_MEMBER_REGISTER_ARGS()
                                        {
                                            fsUSER_ID = user,
                                            fsENC_KEY = fsENC_KEY,
                                            fsDEVICE = fsDEVICE,
                                            fsVERSION = fsVERSION,
                                            fsIP = HttpContext.Current.Request.UserHostAddress,
                                            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                        });
                                    }
                                    else
                                    {
                                        //新增失敗
                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsREGISTER.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    }

                                    accountId = Tv_BLL.GetAccountIdByKbroLinkId(fsLINK_ID);
                                }

                                clsLOGIN clsLOGIN = new repACCOUNT().fnSIGNIN(fsLINK_ID, String.Empty, false);
                                if (!string.IsNullOrEmpty(clsLOGIN.fsACCOUNT_ID))
                                {
                                    //登入成功
                                    string fsENC_KEY = clsGUEST_SIGN_UP.fsENC_KEY;
                                    //取得Token
                                    string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
                                    //取得有效的TimeSpan日期
                                    long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddYears(100).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                                    //加密
                                    string fsVALIDATE_CODE = clsSECURITY.Encrypt(clsLOGIN.fsACCOUNT_ID + fsTOKEN + clsLOGIN.fsUSER + fdEXPIRE_TIME.ToString(), fsENC_KEY);
                                    //cookie加密
                                    fsVALUE = clsSECURITY.CookieEncrypt(clsLOGIN.fsACCOUNT_ID + "-" + fsTOKEN + "-" + clsLOGIN.fsUSER + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE, fsENC_KEY);

                                    //寫LOG
                                    new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
                                    {
                                        fsFROM = "3",
                                        fsIP = HttpContext.Current.Request.UserHostAddress,
                                        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                        fsUSER_ID = clsLOGIN.fsUSER,
                                        fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                    });
                                }
                                else
                                {
                                    //登入失敗
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }

                                // 取帳戶資訊，回傳register type用
                                var accountInfo = new repACCOUNT().fnGET_ACCOUNT_INFO(clsLOGIN.fsACCOUNT_ID);

                                //暫時帳號就給帳編(OTT_accountID, fsLINK_ID)，正式帳號就給使用者帳號(fsUSER_ID)
                                var userId = accountInfo.fnREGISTER_TYPE == 4 ? fsLINK_ID : accountInfo.fsUSER_ID;

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new { fsUSER_ID = userId, fsVALUE = fsVALUE, fnREGISTER_TYPE = accountInfo.fnREGISTER_TYPE } }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion
                    case "HERAN":
                        #region - HERAN -
                        fsLINK_ID = clsGUEST_SIGN_UP.fsPARTNER_VALUE1.Clean();
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                var accountId = Tv_BLL.GetAccountIdByHeranLinkId(fsLINK_ID);
                                var fsVALUE = String.Empty;
                                if (String.IsNullOrEmpty(accountId))
                                {
                                    //註冊，虛擬帳號密碼，登入時用fsLINK_ID，比照Google或Facebook
                                    var user = String.Empty;
                                    while (String.IsNullOrEmpty(user))
                                    {
                                        var checkUser = Guid.NewGuid().ToString().Replace("-", "");
                                        if (!_Account_BLL.CheckUserID(new BLL.Model.Req.Account.CheckUserID_req() { fsUserID = checkUser })) user = checkUser;
                                    }
                                    var password = Guid.NewGuid().ToString().Replace("-", "");
                                    var clsREGISTER = new repACCOUNT().fnREGISTER(4, ((int)FourgTV_OTT2.Common.Setting.EnumSetting.LoginType.Heran).ToString("d2"), user, fsLINK_ID, 0, "", password);

                                    if (clsREGISTER != null && clsREGISTER.fbRESULT)
                                    {

                                        string fsDEVICE = Request.Headers.Contains("fsDEVICE") ? Request.Headers.GetValues("fsDEVICE").First() : string.Empty;
                                        string fsVERSION = Request.Headers.Contains("fsVERSION") ? Request.Headers.GetValues("fsVERSION").First() : string.Empty;
                                        string fsENC_KEY = Request.Headers.Contains("fsENC_KEY") ? Request.Headers.GetValues("fsENC_KEY").First() : string.Empty;

                                        //寫LOG
                                        new repLOG().fnINSERT_MEMBER_REGISTER_LOG(new clsLOG_MEMBER_REGISTER_ARGS()
                                        {
                                            fsUSER_ID = user,
                                            fsENC_KEY = fsENC_KEY,
                                            fsDEVICE = fsDEVICE,
                                            fsVERSION = fsVERSION,
                                            fsIP = HttpContext.Current.Request.UserHostAddress,
                                            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                        });
                                    }
                                    else
                                    {
                                        //新增失敗
                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsREGISTER.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    }

                                    accountId = Tv_BLL.GetAccountIdByHeranLinkId(fsLINK_ID);
                                }

                                clsLOGIN clsLOGIN = new repACCOUNT().fnSIGNIN(fsLINK_ID, String.Empty, false);
                                if (!string.IsNullOrEmpty(clsLOGIN.fsACCOUNT_ID))
                                {
                                    //登入成功
                                    string fsENC_KEY = clsGUEST_SIGN_UP.fsENC_KEY;
                                    //取得Token
                                    string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
                                    //取得有效的TimeSpan日期
                                    long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddYears(100).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                                    //加密
                                    string fsVALIDATE_CODE = clsSECURITY.Encrypt(clsLOGIN.fsACCOUNT_ID + fsTOKEN + clsLOGIN.fsUSER + fdEXPIRE_TIME.ToString(), fsENC_KEY);
                                    //cookie加密
                                    fsVALUE = clsSECURITY.CookieEncrypt(clsLOGIN.fsACCOUNT_ID + "-" + fsTOKEN + "-" + clsLOGIN.fsUSER + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE, fsENC_KEY);

                                    //寫LOG
                                    new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
                                    {
                                        fsFROM = "3",
                                        fsIP = HttpContext.Current.Request.UserHostAddress,
                                        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                        fsUSER_ID = clsLOGIN.fsUSER,
                                        fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                    });
                                }
                                else
                                {
                                    //登入失敗
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }

                                // 取帳戶資訊，回傳register type用
                                var accountInfo = new repACCOUNT().fnGET_ACCOUNT_INFO(clsLOGIN.fsACCOUNT_ID);

                                //暫時帳號就給MAC(fsLINK_ID)，正式帳號就給使用者帳號(fsUSER_ID)
                                var userId = accountInfo.fnREGISTER_TYPE == 4 ? fsLINK_ID : accountInfo.fsUSER_ID;

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new { fsUSER_ID = userId, fsVALUE = fsVALUE, fnREGISTER_TYPE = accountInfo.fnREGISTER_TYPE } }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion
                    case "WSAPC":
                        #region - WSAPC -
                        fsLINK_ID = clsGUEST_SIGN_UP.fsPARTNER_VALUE1.Clean();
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                var accountId = Tv_BLL.GetAccountIdByWSAPCLinkId(fsLINK_ID);
                                var fsVALUE = String.Empty;
                                if (String.IsNullOrEmpty(accountId))
                                {
                                    //註冊，虛擬帳號密碼，登入時用fsLINK_ID，比照Google或Facebook
                                    var user = String.Empty;
                                    while (String.IsNullOrEmpty(user))
                                    {
                                        var checkUser = Guid.NewGuid().ToString().Replace("-", "");
                                        if (!_Account_BLL.CheckUserID(new BLL.Model.Req.Account.CheckUserID_req() { fsUserID = checkUser })) user = checkUser;
                                    }
                                    var password = Guid.NewGuid().ToString().Replace("-", "");
                                    var clsREGISTER = new repACCOUNT().fnREGISTER(4, ((int)FourgTV_OTT2.Common.Setting.EnumSetting.LoginType.WSAPC).ToString("d2"), user, fsLINK_ID, 0, "", password);

                                    if (clsREGISTER != null && clsREGISTER.fbRESULT)
                                    {

                                        string fsDEVICE = Request.Headers.Contains("fsDEVICE") ? Request.Headers.GetValues("fsDEVICE").First() : string.Empty;
                                        string fsVERSION = Request.Headers.Contains("fsVERSION") ? Request.Headers.GetValues("fsVERSION").First() : string.Empty;
                                        string fsENC_KEY = Request.Headers.Contains("fsENC_KEY") ? Request.Headers.GetValues("fsENC_KEY").First() : string.Empty;

                                        //寫LOG
                                        new repLOG().fnINSERT_MEMBER_REGISTER_LOG(new clsLOG_MEMBER_REGISTER_ARGS()
                                        {
                                            fsUSER_ID = user,
                                            fsENC_KEY = fsENC_KEY,
                                            fsDEVICE = fsDEVICE,
                                            fsVERSION = fsVERSION,
                                            fsIP = HttpContext.Current.Request.UserHostAddress,
                                            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                            fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                        });
                                    }
                                    else
                                    {
                                        //新增失敗
                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsREGISTER.fsMESSAGE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    }

                                    accountId = Tv_BLL.GetAccountIdByWSAPCLinkId(fsLINK_ID);
                                }

                                clsLOGIN clsLOGIN = new repACCOUNT().fnSIGNIN(fsLINK_ID, String.Empty, false);
                                if (!string.IsNullOrEmpty(clsLOGIN.fsACCOUNT_ID))
                                {
                                    //登入成功
                                    string fsENC_KEY = clsGUEST_SIGN_UP.fsENC_KEY;
                                    //取得Token
                                    string fsTOKEN = Guid.NewGuid().ToString().Replace("-", "");
                                    //取得有效的TimeSpan日期
                                    long fdEXPIRE_TIME = Convert.ToInt64(DateTime.Now.AddYears(100).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                                    //加密
                                    string fsVALIDATE_CODE = clsSECURITY.Encrypt(clsLOGIN.fsACCOUNT_ID + fsTOKEN + clsLOGIN.fsUSER + fdEXPIRE_TIME.ToString(), fsENC_KEY);
                                    //cookie加密
                                    fsVALUE = clsSECURITY.CookieEncrypt(clsLOGIN.fsACCOUNT_ID + "-" + fsTOKEN + "-" + clsLOGIN.fsUSER + "-" + fdEXPIRE_TIME.ToString() + "-" + fsVALIDATE_CODE, fsENC_KEY);

                                    //寫LOG
                                    new repLOG().fnINSERT_LOG_SIGNIN(new clsLOG_SIGNIN_ARGS()
                                    {
                                        fsFROM = "3",
                                        fsIP = HttpContext.Current.Request.UserHostAddress,
                                        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                                        fsUSER_ID = clsLOGIN.fsUSER,
                                        fsUSER_AGENT = HttpContext.Current.Request.UserAgent
                                    });
                                }
                                else
                                {
                                    //登入失敗
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }

                                // 取帳戶資訊，回傳register type用
                                var accountInfo = new repACCOUNT().fnGET_ACCOUNT_INFO(clsLOGIN.fsACCOUNT_ID);

                                //暫時帳號就給MAC(fsLINK_ID)，正式帳號就給使用者帳號(fsUSER_ID)
                                var userId = accountInfo.fnREGISTER_TYPE == 4 ? fsLINK_ID : accountInfo.fsUSER_ID;

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new { fsUSER_ID = userId, fsVALUE = fsVALUE, fnREGISTER_TYPE = accountInfo.fnREGISTER_TYPE } }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion

                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 會員與裝置綁訂
        /// </summary>
        /// <param name="clsMEMBER_MACHINE_BINDING">會員與裝置綁訂</param>
        /// <returns></returns>
        /// <response code="001">未登入</response>
        /// <response code="002">查無此裝置</response>
        /// <response code="003">綁定失敗</response>
        [Route("MemberMachineBinding")]
        public HttpResponseMessage PostMemberMachineBinding(clsMEMBER_MACHINE_BINDING clsMEMBER_MACHINE_BINDING)
        {
            try
            {
                // 判斷是否已登入
                if (!clsSECURITY.IsLogin(clsMEMBER_MACHINE_BINDING.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsMEMBER_MACHINE_BINDING.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "001" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsMEMBER_MACHINE_BINDING.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsMEMBER_MACHINE_BINDING.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                string fsFROM = clsMEMBER_MACHINE_BINDING.fsFROM == null ? string.Empty : clsMEMBER_MACHINE_BINDING.fsFROM;
                string fsMAC_ADDRESS = clsMEMBER_MACHINE_BINDING.fsMAC_ADDRESS == null ? string.Empty : clsMEMBER_MACHINE_BINDING.fsMAC_ADDRESS;

                switch (fsFROM)
                {
                    case "TBOX":
                        #region - TBOX -
                        //確認是否是綁定此帳號
                        if (clsUSER_DATA != null)
                        {
                            if (_Tv_BLL.GetMEMBER_MACHINEInfo(fsFROM, fsMAC_ADDRESS, clsUSER_DATA.fsACCOUNT_ID) != null)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "成功" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                        }
                        //判斷是否有匹配的DEVICE_MODE跟MAC
                        if (!new repTV().fnGET_TV_MACHINE_FROM(fsFROM, fsMAC_ADDRESS))
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        ////綁定帳號，SP沒有空的話，代表綁定失敗
                        //string fsTV_MEMBER_MACHINE_AND_BINDING = new repTV().fnINSERT_TV_MEMBER_MACHINE_AND_BINDING(clsUSER_DATA.fsACCOUNT_ID, fsFROM, fsMAC_ADDRESS);
                        //if (string.IsNullOrEmpty(fsTV_MEMBER_MACHINE_AND_BINDING))
                        //{
                        //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "成功" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        //}
                        //else
                        //{
                        //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        //}
                        //綁定
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {

                                //綁tbmMEMBER_MACHINE
                                _Tv_BLL.BindingMEMBER_MACHINE(fsFROM, fsMAC_ADDRESS, clsUSER_DATA.fsACCOUNT_ID);

                                //綁tbmMEMBER_ORDER
                                var info = _Tv_BLL.BindingMEMBER_ORDER(fsFROM, fsMAC_ADDRESS, clsUSER_DATA.fsACCOUNT_ID);

                                if (!info)
                                {
                                    _TransactionScope.Dispose();
                                    //機器沒有訂單
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "成功" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion
                    case "AGC":
                        #region - AGC -
                        //判斷是否有匹配的DEVICE_MODE跟MAC
                        if (!new repTV().fnGET_TV_MACHINE_FROM(fsFROM, fsMAC_ADDRESS))
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        if (_Tv_BLL.GetMEMBER_MACHINEInfo(fsFROM, fsMAC_ADDRESS, clsUSER_DATA.fsACCOUNT_ID) == null)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "成功" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    #endregion
                    case "WSAPC":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "成功" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "BANDOTT":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "成功" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "TV":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "成功" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "BOBII":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "成功" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "HERAN":
                        #region - HERAN -

                        //LOG
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.MachineID, fsMAC_ADDRESS.Clean());
                        LogHelper.RequestVariables.SetVariable(LogRequestVariableType.Activity, "Binding");

                        var machineInfo = Tv_BLL.GetHeranMachineInfo(fsMAC_ADDRESS);
                        if (clsTOOL.IsDefault(machineInfo))
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        if (!String.IsNullOrEmpty(machineInfo.fsACCOUNT_ID))
                        {
                            if (machineInfo.fsACCOUNT_ID != clsUSER_DATA.fsACCOUNT_ID)
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            else //not changed
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "pass" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        //綁定
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                Tv_BLL.SetHeranBinding(fsMAC_ADDRESS, clsUSER_DATA.fsACCOUNT_ID);

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "binding" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                            finally
                            {
                                //釋放資源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion
                    case "CNS":
                        #region - CNS -
                        CnsDeviceInfo deviceInfo;
                        try
                        {
                            deviceInfo = Tv_BLL.GetCnsDeviceInfo(fsMAC_ADDRESS);
                        }
                        catch (Exception e)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        if (clsTOOL.IsDefault(deviceInfo) || String.IsNullOrEmpty(deviceInfo.subsid))
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        var accountId = Tv_BLL.GetAccountIdByCnsLinkId(deviceInfo.subsid);
                        if (clsUSER_DATA.fsACCOUNT_ID == accountId)
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "pass" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                if (String.IsNullOrEmpty(accountId))
                                    Tv_BLL.LinkCnsAccount(deviceInfo.subsid, clsUSER_DATA.fsACCOUNT_ID);
                                else
                                {
                                    if (new repACCOUNT().fnGET_ACCOUNT_INFO(accountId).fnREGISTER_TYPE != 4)
                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    Tv_BLL.MergeCnsAccount(accountId, clsUSER_DATA.fsACCOUNT_ID);
                                }

                                // 訂閱狀態正常則新增訂單
                                if (deviceInfo.status == "001")
                                {
                                    _Tv_BLL.CreateCnsOrder(deviceInfo, clsUSER_DATA.fsACCOUNT_ID);
                                }

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "binding" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion

                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 帳號權限轉移
        /// </summary>
        /// <param name="clsACCOUNT_MERGE"></param>
        /// <returns></returns>
        [Route("AccountMerge")]
        public HttpResponseMessage PostAccountMerge(clsACCOUNT_MERGE clsACCOUNT_MERGE)
        {
            try
            {
                // 判斷是否已登入
                if (!clsSECURITY.IsLogin(clsACCOUNT_MERGE.fsVALUE_OLD, clsACCOUNT_MERGE.fsENC_KEY))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "001" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                if (!clsSECURITY.IsLogin(clsACCOUNT_MERGE.fsVALUE, clsACCOUNT_MERGE.fsENC_KEY))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "001" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                var clsUSER_DATA_OLD = new repACCOUNT().fnGET_USER_DATA(clsACCOUNT_MERGE.fsVALUE_OLD, clsACCOUNT_MERGE.fsENC_KEY);
                var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsACCOUNT_MERGE.fsVALUE, clsACCOUNT_MERGE.fsENC_KEY);

                string fsFROM = clsACCOUNT_MERGE.fsFROM == null ? string.Empty : clsACCOUNT_MERGE.fsFROM;

                if (clsUSER_DATA_OLD.fsACCOUNT_ID == clsUSER_DATA.fsACCOUNT_ID)
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "pass" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                if (new repACCOUNT().fnGET_ACCOUNT_INFO(clsUSER_DATA_OLD.fsACCOUNT_ID).fnREGISTER_TYPE != 4)
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                switch (fsFROM)
                {
                    case "CNS":
                        #region - CNS -
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                Tv_BLL.MergeCnsAccount(clsUSER_DATA_OLD.fsACCOUNT_ID, clsUSER_DATA.fsACCOUNT_ID);

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "binding" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion
                    case "KBRO":
                        #region - KBRO -
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                Tv_BLL.MergeKbroAccount(clsUSER_DATA_OLD.fsACCOUNT_ID, clsUSER_DATA.fsACCOUNT_ID);

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "binding" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion
                    case "HERAN":
                        #region - HERAN -
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                Tv_BLL.MergeHeranAccount(clsUSER_DATA_OLD.fsACCOUNT_ID, clsUSER_DATA.fsACCOUNT_ID);

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "binding" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion
                    case "WSAPC":
                        #region - WSAPC -
                        using (TransactionScope _TransactionScope = new TransactionScope())
                        {
                            try
                            {
                                Tv_BLL.MergeWSAPCAccount(clsUSER_DATA_OLD.fsACCOUNT_ID, clsUSER_DATA.fsACCOUNT_ID);

                                _TransactionScope.Complete();
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "binding" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            catch (Exception e)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            finally
                            {
                                //释放资源
                                _TransactionScope.Dispose();
                            }
                        }
                    #endregion


                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 新增訂單
        /// </summary>
        /// <param name="clsPURCHASE">會員與裝置綁訂</param>
        /// <returns></returns>
        /// <response code="001">未登入</response>
        /// <response code="002">查無此裝置</response>
        /// <response code="003">裝置與此帳號無匹配</response>
        [Route("Purchase")]
        public HttpResponseMessage PostPurchase(clsPURCHASE clsPURCHASE)
        {
            try
            {
                // 判斷是否已登入
                if (!clsSECURITY.IsLogin(clsPURCHASE.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsPURCHASE.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "001" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsPURCHASE.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsPURCHASE.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                string fsFROM = clsPURCHASE.fsFROM == null ? string.Empty : clsPURCHASE.fsFROM;
                string fsMAC_ADDRESS = clsPURCHASE.fsMAC_ADDRESS == null ? string.Empty : clsPURCHASE.fsMAC_ADDRESS;

                switch (fsFROM)
                {
                    case "TBOX":

                        //判斷是否有匹配的DEVICE_MODE跟MAC
                        if (!fsFROM.Equals("TV"))
                        {
                            if (!new repTV().fnGET_TV_MACHINE_FROM(fsFROM, fsMAC_ADDRESS))
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        //判斷是否已經匹配此裝置
                        if (!fsFROM.Equals("TV"))
                        {
                            if (int.Parse(new repTV().fnGET_TV_MEMBER_MACHINE_AND_BINDING(clsUSER_DATA.fsACCOUNT_ID, fsFROM, fsMAC_ADDRESS)) == 0)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        //是否有可使用的訂單
                        List<clsGET_MEMBER_SERVICE_INFO> listGET_MEMBER_SERVICE_INFO = new repBSM().fnGET_MEMBER_SERVICE_INFO(clsUSER_DATA.fsACCOUNT_ID, fsFROM);

                        //如果有訂單確認是否盒子一樣
                        List<clsGET_MEMBER_SERVICE_INFO> listGET_MEMBER_SERVICE_INFO_WHERE_ADDRESS = new List<clsGET_MEMBER_SERVICE_INFO>();

                        //塞選有經有重複盒子ADDRESS訂單*
                        if (listGET_MEMBER_SERVICE_INFO.Count() > 0)
                        {
                            listGET_MEMBER_SERVICE_INFO_WHERE_ADDRESS = listGET_MEMBER_SERVICE_INFO.Where(x => x.fsMACHINE_ID.Equals(fsMAC_ADDRESS)).ToList();
                        }
                        //if (listGET_MEMBER_SERVICE_INFO_WHERE_ADDRESS.Count() == 0)
                        //{
                        //    string fsRESULT = string.Empty;
                        //    int fnRETRY_COUNT = 1;
                        //    //產生訂單編號
                        //    string fsORDER_NO = string.Empty;
                        //    while (fnRETRY_COUNT <= 3 && string.IsNullOrEmpty(fsORDER_NO))
                        //    {
                        //        fsORDER_NO = new repL_NO().fnGET_L_NO("ORDERNO", "訂單編號", DateTime.Now.ToString("yyyyMMdd"), "", 5, "Purchase");
                        //        fnRETRY_COUNT += 1;
                        //    }
                        //    if (string.IsNullOrEmpty(fsORDER_NO))
                        //    {
                        //        new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                        //        {
                        //            fsTYPE = "PURCHASE",
                        //            fsNAME = "購買",
                        //            fsCONTENT = "無法取得訂單編號",
                        //            fsCREATED_BY = "Purchase"
                        //        });
                        //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "003" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        //    }
                        //    //新增訂單
                        //    fsRESULT = new repBSM().fnINSERT_ORDER(clsUSER_DATA.fsACCOUNT_ID, fsORDER_NO, Guid.NewGuid().ToString(), "CHANNELTBOX_FULL", "CHTBOX01",
                        //        "TBOX", 0, "Y", string.Empty, string.Empty, fsMAC_ADDRESS, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                        //        string.Empty, string.Empty, false, false, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "1", clsUSER_DATA.fsACCOUNT_ID);
                        //}
                        //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        if (listGET_MEMBER_SERVICE_INFO_WHERE_ADDRESS.Count() == 0)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                    case "AGC":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "WSAPC":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "BANDOTT":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "TV":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "BOBII":
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, Data = "002" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }


        /// <summary>
        /// 取得QRCode
        /// </summary>
        /// <returns></returns>
        [Route("GetQRCode")]
        [HttpPost]
        [Header(headersConfig = "fsENC_KEY,string|fsVERSION,string|fsDEVICE,string")]
        public StdResp<GetQRCode_res> GetQRCode()
        {

            StdResp<GetQRCode_res> stdResp = new StdResp<GetQRCode_res>();

            GetQRCode_res getQRCode_Res = new GetQRCode_res();
            try
            {

                stdResp.Data = _Tv_BLL.GetQRCode();
                return stdResp;

            }
            catch (MyException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 輪尋QRCode
        /// </summary>
        /// <returns></returns>
        [Route("CheakQRCodePolling")]
        [HttpPost]
        [Header(headersConfig = "fsENC_KEY,string|fsVERSION,string|fsDEVICE,string")]
        public StdResp<CheakQRCodePolling_res> CheakQRCodePolling(CheakQRCodePolling_req req)
        {
            StdResp<CheakQRCodePolling_res> stdResp = new StdResp<CheakQRCodePolling_res>();

            stdResp.Data = _Tv_BLL.CheakQRCodePolling(req);
            return stdResp;

        }
        /// <summary>
        /// 註冊帳號
        /// </summary>
        /// <param name="req">註冊資訊</param>
        /// <returns></returns>
        [Route("Register")]
        [HttpPost]
        public StdResp<string> PhoneRegister(PhoneRegister_req req)
        {

            StdResp<string> stdResp = new StdResp<string>();
            using (TransactionScope _TransactionScope = new TransactionScope())
            {

                stdResp.Data = _Account_BLL.PhoneRegister(req);

                _TransactionScope.Complete();
                return stdResp;
            }
        }
    }
}
