﻿using Dapper;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Req.Web;
using FourgTV_OTT2.BLL.Model.Res.Web;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.DAL.Model.Condition;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Script.Serialization;
using WebApi.OutputCache.V2;
using static FourgTV_OTT2.Common.Mail;
using static FourgTV_OTT2.Common.Setting.EnumSetting;
using clsM3U8 = FourgTV_OTT2.Common.clsM3U8;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Web")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class WebController : ApiController
    {

        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];
        Web_BLL _Web_BLL = new Web_BLL();
        /// <summary>
        /// 取得Banner
        /// </summary>
        /// <param name="fsPOSITION">出現位置(01:首頁、02:頻道首頁、03:戲劇首頁、04:綜藝首頁、05:直擊首頁、06:文化教育首頁、07:電影首頁、08:網路紅什麼首頁)</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetBanner/{fsPOSITION}/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetBanner(string fsPOSITION, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBANNER().fnGET_BANNER(fsPOSITION, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得首頁推薦
        /// </summary>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetPromo/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetPromo(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repPROMO().fnGET_PROMO(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得熱門關鍵字
        /// </summary>
        /// <returns></returns>
        [Route("GetHotKeyword")]
        [CacheOutput(ServerTimeSpan = 1200, AnonymousOnly = true)]
        public HttpResponseMessage GetHotKeyword()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repKEYWORD().fnGET_HOT_KEYWORD() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// IP是否在台灣
        /// </summary>
        /// <returns></returns>
        [Route("IsTaiwanArea")]
        public HttpResponseMessage GetIsTaiwan()
        {
            try
            {
                string fsIP = HttpContext.Current.Request.UserHostAddress;
                //測試環境才用
                if (int.Parse(fcIS_PRODUCTION) == 0)
                {
                    if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }
                else
                {
                    if (fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }

                //判斷是否為台灣的IP
                if (FourgTV_OTT2.Common.clsSECURITY.GetIpIsInTaiwan(fsIP))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "Y" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "N" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// 取得WEBCONFIG
        /// </summary>
        /// <param name="fsDEVICE">裝置</param>
        /// <param name="fsVERSION">版本</param>
        /// <returns></returns>
        [Route("GetWebConfig/{fsDEVICE}/{fsVERSION}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetWebConfig(string fsDEVICE, string fsVERSION)
        {
            //路徑
            string fsPATH = string.Empty;
            //預設路徑
            string fsDEFULTPATH = string.Empty;
            //JSON
            dynamic obj;
            try
            {
                var fsAPPCONFIG_JSON_URLPATH = new repCONFIG().fnGET_CFG("APPCONFIG_JSON_URL")[0].fsVALUE;
                fsDEFULTPATH = fsAPPCONFIG_JSON_URLPATH + fsDEVICE + "-0.json";
                fsPATH = fsAPPCONFIG_JSON_URLPATH + fsDEVICE + "-" + fsVERSION + ".json";
                //判斷檔案是否存在
                if (System.IO.File.Exists(fsPATH))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    obj = serializer.Deserialize(File.ReadAllText(fsPATH), typeof(object));
                }
                else
                {
                    //判斷是否有預設的檔案
                    if (System.IO.File.Exists(fsDEFULTPATH))
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        obj = serializer.Deserialize(File.ReadAllText(fsDEFULTPATH), typeof(object));
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請確認裝置或版本" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = obj }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得搜尋結果
        /// </summary>
        /// <param name="fsArticleID">文章ID</param>
        /// <returns></returns>
        [Route("GetSearchResult")]
        [HttpPost]
        [ResponseType(typeof(GetSearchResult_Req))]
        public StdResp<GetSearchResult_Res> GetSearchResult(GetSearchResult_Req req)
        {
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.Type == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.KeyWord == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.Index == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.PageSize == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.fcREGION == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.Index.Value < 0)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.PageSize.Value < 0)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            StdResp<GetSearchResult_Res> stdResp = new StdResp<GetSearchResult_Res>();
            stdResp.Data = _Web_BLL.GetSearchResult(req);
            return stdResp;

        }

        /// <summary>
        /// 推薦
        /// </summary>
        /// <returns></returns>
        [Route("GetPromoTemp/{Type:int}")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetPromoTemp_Res>> GetPromoTemp(int Type)
        {
            StdResp<List<GetPromoTemp_Res>> stdResp = new StdResp<List<GetPromoTemp_Res>>();
            stdResp.Data = _Web_BLL.GetPromoTemp(Type.ToEnum<PromoExposedTypeEnum>());
            return stdResp;
        }
        /// <summary>
        /// 推薦
        /// </summary>
        /// <returns></returns>
        [Route("GetVodPromo/{Type:int}")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif

        public StdResp<List<GetVodPromo_Res>> GetVodPromo(int Type)
        {
            StdResp<List<GetVodPromo_Res>> stdResp = new StdResp<List<GetVodPromo_Res>>();
            stdResp.Data = _Web_BLL.GetVodPromo(Type.ToEnum<PromoExposedTypeEnum>());
            return stdResp;
        }

        /// <summary>
        /// 推薦
        /// </summary>
        /// <returns></returns>
        [Route("GetBannerTemp/{Type:int}")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetBannerTemp_Res>> GetBannerTemp(int Type)
        {
            StdResp<List<GetBannerTemp_Res>> stdResp = new StdResp<List<GetBannerTemp_Res>>();
            stdResp.Data = _Web_BLL.GetBannerTemp(Type.ToEnum<BannerExposedTypeEnum>());
            return stdResp;
        }
        [Route("FileTest")]
        [HttpGet]

        public StdResp<string> FileTest()
        {
            StdResp<string> stdResp = new StdResp<string>();

            var ex = string.Empty;

            try
            {

                //轉代表圖
                using (var client = new WebClient())
                {
                    var file = client.OpenRead("https://newsimg.ftv.com.tw/manasystem/FileData/News/9793065c-a9e4-47be-bdd2-77f0ade1057e.jpg");

                    var imagePath = @"\\192.168.100.179\video_portal\FourgTV_OTT2\Staging";

                    string imageName = DateTime.Now.ToString("yyyyMMddhhssmmffff") + ".jpg";

                    string imageFile = Path.Combine(imagePath, imageName);

                    if (System.IO.File.Exists(imageFile))
                    {
                        System.IO.File.Delete(imageFile);
                    }


                    Image iSource = Image.FromStream(file);
                    ImageFormat tFormat = iSource.RawFormat;

                    Image origin = iSource;


                    int dHeight = iSource.Height;
                    int dWidth = iSource.Width;


                    Bitmap ob = new Bitmap(dWidth, dHeight);
                    Graphics g = Graphics.FromImage(ob);

                    g.Clear(Color.WhiteSmoke);
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(iSource, new Rectangle(0, 0, dWidth, dHeight), 0, 0, iSource.Width, iSource.Height, GraphicsUnit.Pixel);

                    g.Dispose();

                    ob.Save(imageFile, tFormat);
                    ex = "GOOD";

                    iSource.Dispose();
                    ob.Dispose();
                }

            }
            catch (Exception e)
            {
                ex = e.Message;
            }


            stdResp.Data = ex;
            return stdResp;
        }

        /// <summary>
        /// MailTest
        /// </summary>
        /// <returns></returns>
        [Route("MailTest")]
        [HttpGet]

        public StdResp MailTest()
        {
            StdResp stdResp = new StdResp();
            List<string> vs = new List<string>();
            //using (StreamReader sr = new StreamReader(@"\\192.168.100.179\FourgTV_Website\FourgTV_OTT2\API2\Staging\bbb\EEE.txt"))     //小寫TXT
            //{

            //    String line;
            //    //Read and display lines from the file until the end of
            //    //the file is reached.
            //    while ((line = sr.ReadLine()) != null)
            //    {
            //        vs.Add(line);
            //        // Console.WriteLine(line);
            //    }
            //}

            var day = DateTime.Now.Day;


            //   vs = vs.Skip((day - 4) * 8000).Take(8000).ToList();




            //  var aaa = vs.ToArray();



            string From = "support@4gtv.tv";
            string[] To =
                {

                "angus.hsieh@ftv.com.tw",
                 "yenru.shi@ftv.com.tw",
                //"aaaa"
               //  "angushsiehpay@gmail.com",
             //     "angus963852741@gmail.com",
               //   "kwai.hwa@ftv.com.tw"

            };

            //   string[] filePaths = { @"\\192.168.100.179\FourgTV_Website\FourgTV_OTT2\API2\Staging\bbb\1.png", @"\\192.168.100.179\FourgTV_Website\FourgTV_OTT2\API2\Staging\bbb\2.jpg", @"\\192.168.100.179\FourgTV_Website\FourgTV_OTT2\API2\Staging\bbb\3.png" };
            string[] filePaths = { };
            Mail _Mail = new Mail();

            AsyncSendMail delt = new AsyncSendMail(_Mail.SendMailAsync);                      //非同步執行
            AsyncCallback acb = new AsyncCallback(_Mail.SendMailCallBack);                    //非同步執行
            IAsyncResult isr = delt.BeginInvoke(From, To, filePaths, acb, delt);    //非同步執行
            ////                                                                              ReturnLiteral.Text = "郵件已加入工作排程，完成後會寄送一封通知信給您，謝謝";



            //   _Mail.SendMailAsync(From, To, filePaths);
            //發送信件
            //FourgTV_OTT2.Common.clsTOOL.fnSEND_EMAIL("angus.hsieh@ftv.com.tw", "test", "test");


            return stdResp;
        }

        public string ClientIP
        {
            get
            {
                var obj = LogHelper.RequestVariables[LogRequestVariableType.ClientIP];
                return obj == null ? "" : obj.ToString();
            }
            set
            {
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.ClientIP, value);
            }
        }

        /// <summary>
        /// MailTest
        /// </summary>
        /// <returns></returns>
        [Route("M3u8Test")]
        [HttpGet]

        public StdResp<List<VodUrlInfo>> M3u8Test()
        {
            StdResp<List<VodUrlInfo>> stdResp = new StdResp<List<VodUrlInfo>>();
            clsM3U8 _clsM3U8 = new clsM3U8();
            DateTime dateTime = DateTime.Now;
            string fsSQL_CONN = WebConfigurationManager.ConnectionStrings["fsSQL_CONN"].ConnectionString;

            string URLss = string.Empty;

            List<VodUrlInfo> _vodUrlInfo = new List<VodUrlInfo>();


            using (var _conn = new SqlConnection(fsSQL_CONN))
            {
                string sql = @"

                                ";

                _vodUrlInfo = _conn.Query<VodUrlInfo>(sql).ToList();


            }

            string fsIP = "61.216.147.98";


            Parallel.ForEach(_vodUrlInfo, (info, pls, index) =>
            {

                if (string.IsNullOrEmpty(info._WRITTEN_BY))
                {
                    info.fcCASE_CHT = string.IsNullOrEmpty(info.fcCASE_CHT) ? "B" : info.fcCASE_CHT;

                    info.URL = info.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, "pc", info.fsCDNFILE_ID, fsIP, -1) : clsM3U8.GetTypeCVodUrl(false, "pc", "vod", info.fsCDNFILE_ID, info.fnFOLDER_YEAR, fsIP, -1);
                }
                else
                {
                    info.URL = clsM3U8.GetLiTVVodUrl(info.fcCASE_CHT, false, "pc", info._WRITTEN_BY, info.fsASSET_ID_LI.Replace("_1500K", ""), fsIP, -1);
                }


                try
                {

                    IRestResponse response = CallAPI.GetApi(info.URL, "");

                    info.Message = response.Content;


                }

                catch (Exception e)
                {
                    info.Message = e.Message;
                }
            });



            //foreach (var info in _vodUrlInfo)
            //{

            //    if (string.IsNullOrEmpty(info._WRITTEN_BY))
            //    {
            //        info.fcCASE_CHT = string.IsNullOrEmpty(info.fcCASE_CHT) ? "B" : info.fcCASE_CHT;

            //        info.URL = info.fcCASE_CHT == "B" ? _clsM3U8.GetVodUrl(false, "pc", info.fsCDNFILE_ID, fsIP, -1) : _clsM3U8.GetTypeCVodUrl(false, "pc", "vod", info.fsCDNFILE_ID, info.fnFOLDER_YEAR, fsIP, -1);
            //    }
            //    else
            //    {
            //        info.URL = _clsM3U8.GetLiTVVodUrl(info.fcCASE_CHT, false, "pc", info._WRITTEN_BY, info.fsASSET_ID_LI.Replace("_1500K", ""), fsIP, -1);
            //    }


            //    try
            //    {

            //        IRestResponse response = CallAPI.GetApi(info.URL, "");

            //        info.Message = response.StatusCode.ToString()+ response.Content;


            //    }

            //    catch (Exception e)
            //    {
            //        info.Message = e.Message;
            //    }

            //}

            _vodUrlInfo = _vodUrlInfo.Where(x => x.Message.IndexOf("#EXTM3U") >= 0).ToList();



            Parallel.ForEach(_vodUrlInfo, (info, pls, index) =>
            {
                try
                {



                    string URL = @"http://ottapi2.media.hinet.net/api/streaming/jobs/" + info.fsPUBLISH_ID_CHT + @"/";
                    Uri myUri = new Uri(URL);
                    var Client = new RestClient(myUri);
                    // var Client = new RestClient(@"http://ottapi2.media.hinet.net/api/streaming/jobs/" + info.fsPUBLISH_ID_CHT);
                    var request = new RestRequest(Method.GET);
                    request.Timeout = 12000000;
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("content-type", "application/json");
                    request.AddHeader("Authorization", "Basic NGd0djpIMlZiNldkTA==");
                    // request.AddParameter("application/json", reqJson, ParameterType.RequestBody);
                    IRestResponse response = Client.Execute(request);
                    string fsFILE_NO = info.fsFILE_NO;


                    try
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {

                            var Content = JsonConvert.DeserializeObject<CHTCDN>(response.Content);

                            if (Content.status == 10099)//有檔案
                            {
                                //準備刪掉

                                string URL_D = @"https://ottapi2.media.hinet.net/api/streaming/jobs/" + info.fsPUBLISH_ID_CHT + @"/";


                                // string Authorization = "Basic ";


                                Uri myUri_D = new Uri(URL_D);

                                //  WebRequest myWebRequest = HttpWebRequest.Create(myUri);

                                //  HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;

                                //  myHttpWebRequest.Method = WebRequestMethods.Http.Get;


                                ////  authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                                //  myHttpWebRequest.Headers["Authorization"] = "Basic " + authInfo;
                                //  myHttpWebRequest.Headers["ContentType"] = "application/json";

                                //  using (WebResponse response = myWebRequest.GetResponse())
                                //  {
                                //      StreamReader sr = new StreamReader(response.GetResponseStream());
                                //    var  fsRESULT = sr.ReadToEnd();
                                //      sr.Close();
                                //  }

                                var Client_D = new RestClient(myUri_D);
                                var request_D = new RestRequest(Method.POST);
                                
                                request_D.Timeout = 12000000;
                                request_D.AddHeader("cache-control", "no-cache");
                                request_D.AddHeader("content-type", "application/x-www-form-urlencoded");
                                request_D.AddHeader("Authorization", "Basic NGd0djpIMlZiNldkTA==");
                                request_D.AddParameter("action", "clear");
                                //  request_D.AddParameter("application/json", JsonConvert.SerializeObject(new { action = "clear" }), ParameterType.RequestBody);
                                IRestResponse response_D = Client.Execute(request_D);




                                // 資料庫更新

                                using (var _conn = new SqlConnection(fsSQL_CONN))
                                {
                                    string sql = @"
                                  UPDATE [dbo].[tbmARC_VIDEO]
	                              SET fcSTATUS='X',fsMEMO='CDN刪除',fdUPDATED_DATE=@dateTime,fsUPDATED_BY='job'
	                              WHERE fsFILE_NO=@fsFILE_NO

                                    UPDATE [dbo].[m3u8Test]
	                              SET [DeleteMessage]='X'
	                              WHERE fsFILE_NO=@fsFILE_NO
                                ";
                                    _conn.Execute(sql, new { fsFILE_NO, dateTime });
                                }


                            }
                            else
                            {

                                // 資料庫更新
                                using (var _conn = new SqlConnection(fsSQL_CONN))
                                {
                                    string sql = @"
                                  UPDATE [dbo].[tbmARC_VIDEO]
	                              SET fcSTATUS='X',fsMEMO='CDN刪除',fdUPDATED_DATE=@dateTime,fsUPDATED_BY='job'
	                              WHERE fsFILE_NO=@fsFILE_NO


                                    UPDATE [dbo].[m3u8Test]
	                              SET [DeleteMessage]='X'
	                              WHERE fsFILE_NO=@fsFILE_NO
                                ";
                                    _conn.Execute(sql, new { fsFILE_NO, dateTime });
                                }

                            }



                        }
                        else
                        {

                            string res = response.Content;
                            // 資料庫更新
                            using (var _conn = new SqlConnection(fsSQL_CONN))
                            {
                                string sql = @"
                               
                                    UPDATE [dbo].[m3u8Test]
	                              SET [DeleteMessage]=@res
	                              WHERE fsFILE_NO=@fsFILE_NO
                                ";
                                _conn.Execute(sql, new { fsFILE_NO, res });
                            }

                        }



                    }
                    catch (Exception e)
                    {
                        string res = e.Message;
                        // 資料庫更新
                        using (var _conn = new SqlConnection(fsSQL_CONN))
                        {
                            string sql = @"
                               
                                    UPDATE [dbo].[m3u8Test]
	                              SET [DeleteMessage]=@res
	                              WHERE fsFILE_NO=@fsFILE_NO
                                ";
                            _conn.Execute(sql, new { fsFILE_NO, res });
                        }
                    }


                    info.Message = response.Content;


                }

                catch (Exception e)
                {
                    info.Message = e.Message;
                }
            });


            stdResp.Data = _vodUrlInfo;
            return stdResp;
        }



        /// <summary>
        /// MailTest
        /// </summary>
        /// <returns></returns>
        [Route("GETCDN")]
        [HttpGet]

        public StdResp<List<VodUrlInfo>> GETCDN()
        {
            StdResp<List<VodUrlInfo>> stdResp = new StdResp<List<VodUrlInfo>>();
            string fsSQL_CONN = WebConfigurationManager.ConnectionStrings["fsSQL_CONN"].ConnectionString;
            List<VodUrlInfo> _vodUrlInfo = new List<VodUrlInfo>();
            DateTime dateTime = DateTime.Now;
            using (var _conn = new SqlConnection(fsSQL_CONN))
            {
                string sql = @"
 SELECT TT.*
  FROM 
  [dbo].[tbmVOD_M] AS M WITH (NOLOCK) 
  LEFT JOIN [dbo].[tbmVOD_D] AS D WITH (NOLOCK) ON M.fsVOD_NO = D.fsVOD_NO 
  LEFT JOIN [dbo].[tbmARC_VIDEO] AS V WITH (NOLOCK) ON V.fsVOD_NO = D.fsVOD_NO AND V.fnSEQ =  D.fnSEQ 
  LEFT JOIN [dbo].[m3u8Test] AS TT WITH (NOLOCK) ON V.fsVOD_NO = TT.fsVOD_NO  AND V.fsFILE_NO = TT.fsFILE_NO AND V.fnSEQ =  TT.fnSEQ 
  WHERE V.fsFILE_NO <> '' AND M.fsVIDEO_FROM = 1
  AND 
  (
  V.fcSTATUS = 'D'--置換
    OR
  V.fcSTATUS = 'E'--處理機制
 
  )
  -- AND ( [Message] like '%Not Found%'OR [Message] ='' )
  AND TT.[fsPUBLISH_ID_CHT] <> '' AND TT.fcCASE_CHT = 'C'
     -- AND TT.fsPUBLISH_ID_CHT = '43aa72992d6d4cfab6572d01a743d12d'
  ORDER BY V.fdCREATED_DATE
                                           ";

                   _vodUrlInfo = _conn.Query<VodUrlInfo>(sql).ToList();


            }


            //try
            //{
            //    string ID = "9c91537b55b34a918ae0d0137e553690";


            //    string URL = @"https://ottapi2.media.hinet.net/api/streaming/jobs/" + ID + @"/";
            //    string authInfo = "NGd0djpIMlZiNldkTA==";

            //    // string Authorization = "Basic ";


            //    Uri myUri = new Uri(URL);

            //    //  WebRequest myWebRequest = HttpWebRequest.Create(myUri);

            //    //  HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;

            //    //  myHttpWebRequest.Method = WebRequestMethods.Http.Get;


            //    ////  authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            //    //  myHttpWebRequest.Headers["Authorization"] = "Basic " + authInfo;
            //    //  myHttpWebRequest.Headers["ContentType"] = "application/json";

            //    //  using (WebResponse response = myWebRequest.GetResponse())
            //    //  {
            //    //      StreamReader sr = new StreamReader(response.GetResponseStream());
            //    //    var  fsRESULT = sr.ReadToEnd();
            //    //      sr.Close();
            //    //  }

            //    var Client = new RestClient(myUri);
            //    var request = new RestRequest(Method.GET);
            //    request.Timeout = 12000000;
            //    request.AddHeader("cache-control", "no-cache");
            //    request.AddHeader("content-type", "application/json");
            //    request.AddHeader("Authorization", "Basic " + authInfo);
            //    // request.AddParameter("application/json", reqJson, ParameterType.RequestBody);
            //    IRestResponse response = Client.Execute(request);
            //    //   var Content = JsonConvert.DeserializeObject<CHTCDN>(response.Content);
            //    try
            //    {

            //        string fsFILE_NO = "20180718_0010001";

            //        if (response.StatusCode == HttpStatusCode.OK)
            //        {

            //            var Content = JsonConvert.DeserializeObject<CHTCDN>(response.Content);

            //            if (Content.status == 10099)//有檔案
            //            {
            //                //準備刪掉

            //                string URL_D = @"https://ottapi2.media.hinet.net/api/streaming/jobs/" + ID + @"/";


            //                // string Authorization = "Basic ";


            //                Uri myUri_D = new Uri(URL_D);

            //                //  WebRequest myWebRequest = HttpWebRequest.Create(myUri);

            //                //  HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;

            //                //  myHttpWebRequest.Method = WebRequestMethods.Http.Get;


            //                ////  authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            //                //  myHttpWebRequest.Headers["Authorization"] = "Basic " + authInfo;
            //                //  myHttpWebRequest.Headers["ContentType"] = "application/json";

            //                //  using (WebResponse response = myWebRequest.GetResponse())
            //                //  {
            //                //      StreamReader sr = new StreamReader(response.GetResponseStream());
            //                //    var  fsRESULT = sr.ReadToEnd();
            //                //      sr.Close();
            //                //  }

            //                var Client_D = new RestClient(myUri_D);
            //                var request_D = new RestRequest(Method.POST);
            //                request_D.Timeout = 12000000;
            //                request_D.AddHeader("cache-control", "no-cache");
            //                request_D.AddHeader("content-type", "application/x-www-form-urlencoded");
            //                request_D.AddHeader("Authorization", "Basic " + authInfo);
            //                request_D.AddParameter("action", "clear");
            //              //  request_D.AddParameter("application/json", JsonConvert.SerializeObject(new { action = "clear" }), ParameterType.RequestBody);
            //                IRestResponse response_D = Client.Execute(request_D);




            //                // 資料庫更新

            //                using (var _conn = new SqlConnection(fsSQL_CONN))
            //                {
            //                    string sql = @"
            //                      UPDATE [dbo].[tbmARC_VIDEO]
            //                   SET fcSTATUS='X',fsMEMO='CDN刪除',fdUPDATED_DATE=@dateTime,fsUPDATED_BY='job'
            //                   WHERE fsFILE_NO=@fsFILE_NO

            //                        UPDATE [dbo].[m3u8Test]
            //                   SET [DeleteMessage]='X'
            //                   WHERE fsFILE_NO=@fsFILE_NO
            //                    ";
            //                    _conn.Execute(sql, new { fsFILE_NO, dateTime });
            //                }


            //            }
            //            else
            //            {

            //                // 資料庫更新
            //                using (var _conn = new SqlConnection(fsSQL_CONN))
            //                {
            //                    string sql = @"
            //                      UPDATE [dbo].[tbmARC_VIDEO]
            //                   SET fcSTATUS='X',fsMEMO='CDN刪除',fdUPDATED_DATE=@dateTime,fsUPDATED_BY='job'
            //                   WHERE fsFILE_NO=@fsFILE_NO


            //                        UPDATE [dbo].[m3u8Test]
            //                   SET [DeleteMessage]='X'
            //                   WHERE fsFILE_NO=@fsFILE_NO
            //                    ";
            //                    _conn.Execute(sql, new { fsFILE_NO, dateTime });
            //                }

            //            }



            //        }
            //        else
            //        {

            //            //  info.Message = response.Content;


            //        }



            //    }
            //    catch (Exception e)
            //    {
            //        // info.Message = response.Content;
            //    }


            //}

            //catch (Exception e)
            //{
            //    //  info.Message = e.Message;
            //}

            Parallel.ForEach(_vodUrlInfo, (info, pls, index) =>
        {
            try
            {
                string URL = @"http://ottapi2.media.hinet.net/api/streaming/jobs/" + info.fsPUBLISH_ID_CHT + @"/";
                Uri myUri = new Uri(URL);
                var Client = new RestClient(myUri);
                // var Client = new RestClient(@"http://ottapi2.media.hinet.net/api/streaming/jobs/" + info.fsPUBLISH_ID_CHT);
                var request = new RestRequest(Method.GET);
                request.Timeout = 12000000;
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("Authorization", "Basic NGd0djpIMlZiNldkTA==");
                // request.AddParameter("application/json", reqJson, ParameterType.RequestBody);
                IRestResponse response = Client.Execute(request);

                string fsFILE_NO = info.fsFILE_NO;

                try
                {

                    if (response.StatusCode == HttpStatusCode.OK)
                    {

                        var Content = JsonConvert.DeserializeObject<CHTCDN>(response.Content);

                        if (Content.status == 10099)//有檔案
                        {
                            //準備刪掉

                            string URL_D = @"https://ottapi2.media.hinet.net/api/streaming/jobs/" + info.fsPUBLISH_ID_CHT + @"/";


                            // string Authorization = "Basic ";


                            Uri myUri_D = new Uri(URL_D);

                            //  WebRequest myWebRequest = HttpWebRequest.Create(myUri);

                            //  HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;

                            //  myHttpWebRequest.Method = WebRequestMethods.Http.Get;


                            ////  authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                            //  myHttpWebRequest.Headers["Authorization"] = "Basic " + authInfo;
                            //  myHttpWebRequest.Headers["ContentType"] = "application/json";

                            //  using (WebResponse response = myWebRequest.GetResponse())
                            //  {
                            //      StreamReader sr = new StreamReader(response.GetResponseStream());
                            //    var  fsRESULT = sr.ReadToEnd();
                            //      sr.Close();
                            //  }

                            var Client_D = new RestClient(myUri_D);
                            var request_D = new RestRequest(Method.POST);
                            request_D.Timeout = 12000000;
                            request_D.AddHeader("cache-control", "no-cache");
                            request_D.AddHeader("content-type", "application/x-www-form-urlencoded");
                            request_D.AddHeader("Authorization", "Basic NGd0djpIMlZiNldkTA==");
                            request_D.AddParameter("action", "clear");
                            //  request_D.AddParameter("application/json", JsonConvert.SerializeObject(new { action = "clear" }), ParameterType.RequestBody);
                            IRestResponse response_D = Client.Execute(request_D);




                            // 資料庫更新

                            using (var _conn = new SqlConnection(fsSQL_CONN))
                            {
                                string sql = @"
                                         UPDATE [dbo].[tbmARC_VIDEO]
                                      SET fcSTATUS='X',fsMEMO='CDN刪除',fdUPDATED_DATE=@dateTime,fsUPDATED_BY='job'
                                      WHERE fsFILE_NO=@fsFILE_NO

                                           
                                       ";
                                _conn.Execute(sql, new { fsFILE_NO, dateTime });
                            }


                        }
                        else
                        {

                            // 資料庫更新
                            using (var _conn = new SqlConnection(fsSQL_CONN))
                            {
                                string sql = @"
                                         UPDATE [dbo].[tbmARC_VIDEO]
                                      SET fcSTATUS='X',fsMEMO='CDN刪除',fdUPDATED_DATE=@dateTime,fsUPDATED_BY='job'
                                      WHERE fsFILE_NO=@fsFILE_NO


                                           
                                       ";
                                _conn.Execute(sql, new { fsFILE_NO, dateTime });
                            }

                        }



                    }
                    else
                    {

                        string res = response.Content;
                        // 資料庫更新
                        using (var _conn = new SqlConnection(fsSQL_CONN))
                        {
                            //string sql = @"

                            //                   UPDATE [dbo].[m3u8Test]
                            //              SET [DeleteMessage]=@res
                            //              WHERE fsFILE_NO=@fsFILE_NO
                            //               ";
                            string sql = @"";
                            _conn.Execute(sql, new { fsFILE_NO, res });
                        }

                    }



                }
                catch (Exception e)
                {
                    string res = e.Message;
                    // 資料庫更新
                    using (var _conn = new SqlConnection(fsSQL_CONN))
                    {
                        //string sql = @"

                        //                   UPDATE [dbo].[m3u8Test]
                        //              SET [DeleteMessage]=@res
                        //              WHERE fsFILE_NO=@fsFILE_NO
                        //               ";
                        string sql = @"";
                        _conn.Execute(sql, new { fsFILE_NO, res });
                    }
                }


                info.Message = response.Content;


            }

            catch (Exception e)
            {
                info.Message = e.Message;
            }
        });


            stdResp.Data = _vodUrlInfo;
            return stdResp;
        }




        public class CHTCDN
        {
            public string id { get; set; }
            public string owner { get; set; }
            public string input_dir_path { get; set; }
            public object subtitle_file_path { get; set; }
            public object subtitle_language { get; set; }
            public int status { get; set; }
            public object error_code { get; set; }
            public string message { get; set; }
            public bool is_deletesource { get; set; }
            public bool poster_thumbnail { get; set; }
            public string streaming_preset_group { get; set; }
            public string submit_time { get; set; }
            public string start_time { get; set; }
            public string finish_time { get; set; }
            public long streaming_size { get; set; }
            public string publish_type { get; set; }
            public string drm_content_id { get; set; }
            public object skd_number { get; set; }
            public object fairplay_event_id { get; set; }
            public object widevine_event_id { get; set; }
            public object verimatrix_event_id { get; set; }
            public string domain_name { get; set; }
            public string scrambling_verimatrix_resourceid { get; set; }
            public string scrambling_widevine_resourceid { get; set; }
            public string scrambling_fairplay_resourceid { get; set; }
            public string[] endpoint_set { get; set; }
            public object[] subtitleendpoint_set { get; set; }
            public string thumbnail_endpoint { get; set; }
        }



        public class VodUrlInfo
        {

            public string fsTITLE { get; set; }
            public string fsVOD_NO { get; set; }
            public string fnSEQ { get; set; }
            public string fsFILE_NO { get; set; }
            public string fnVERSION { get; set; }
            public string fsASSET_ID_LI { get; set; }
            public string fsCDNFILE_ID { get; set; }
            public string fcCASE_CHT { get; set; }
            public string fsPUBLISH_ID_CHT { get; set; }
            public string _WRITTEN_BY { get; set; }
            public string fcSTATUS { get; set; }
            public string fnDURATION { get; set; }
            public DateTime fdCREATED_DATE { get; set; }
            public int fnFOLDER_YEAR { get; set; }

            public string URL { get; set; }
            public string Message { get; set; }

            public string DeleteMessage { get; set; }


        }


        /// <summary>
        /// 推薦
        /// </summary>
        /// <returns></returns>
        [Route("IPtest")]
        [HttpGet]

        public StdResp<string> IPtest()
        {
            StdResp<string> stdResp = new StdResp<string>();
            Redis.set("Test2", "t2t2t2t");

            Dictionary<string, string> configDic = new Dictionary<string, string>();
            configDic.Add("config_IP", "localhost");
            configDic.Add("config_Port", "1521");
            configDic.Add("config_serviceName", "orcl");
            configDic.Add("config_userName", "DE_POWERPMS");
            configDic.Add("config_password", "cppepass");
            //  client.SetRangeInHash("config_info", configDic);
            //  Dictionary<string, string> testHash = client.GetAllEntriesFromHash("config_info");

            //  Redis.Hashset("ttt", configDic);
            //  Redis.set("_Country", "TW", 600);
            //  Redis.allkey();


            // var C = Redis.get( "_Country");

            //string aa = @"6/23因八點檔黃金歲月上檔，瞬間流量導致服務不堪負荷，為表歉意，自動將您的收視期限延長15天，造成您的不便請見諒，謝謝您持續支持四季線上";

            //clsTOOL _clsTOOL = new clsTOOL();

            //List<string> vs = new List<string>
            //{ };


            //foreach (var _vs in vs)
            //{
            //    _clsTOOL.fnSEND_SMS(_vs, aa);
            //}



            return stdResp;
        }
        ///// <summary>
        ///// 推薦
        ///// </summary>
        ///// <returns></returns>
        //[Route("IPtest")]
        //[HttpGet]

        //public StdResp<string> IPtest()
        //{
        //    StdResp<string> stdResp = new StdResp<string>();

        //    var addda = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("103.152.151.86");
        //    var a1 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("123.192.229.252");
        //    var a2 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("111.255.118.247");
        //    var a3 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("114.26.104.172");
        //    var a4 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("111.82.120.133");
        //    var a5 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("60.251.232.251");
        //    var a6 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("114.43.87.144");
        //    var a7 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("220.143.82.228");
        //    var a8 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("111.83.186.51");
        //    var a9 = FourgTV_OTT2.Common.clsSECURITY.GetIpCountry("180.217.33.206");

        //    var aa = FourgTV_OTT2.Common.clsSECURITY.GetIpIsAllowArea("220.135.128.245", false, null);

        //    //stdResp.Data = _Web_BLL.GetVodPromo(Type.ToEnum<PromoExposedTypeEnum>());
        //    return stdResp;
        //}

    }
}
