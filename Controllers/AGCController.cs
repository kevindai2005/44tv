﻿using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Req.AGC;
using FourgTV_OTT2.BLL.Model.Res.AGC;
using FourgTV_OTT2.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("AGC")]
    public class AGCController : ApiController
    {
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];

        AGC_BLL _AGC_BLL = new AGC_BLL();

        /// <summary>
        /// A01.啟用/新增Mac白名單
        /// </summary>
        /// <param name="clsUPSERT_AGC_MAC_BINDING">啟用/新增Mac白名單</param>
        /// <returns></returns>
        [Route("UpsertMACBinding")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostUpsertMACBinding(clsUPSERT_AGC_MAC_BINDING clsUPSERT_AGC_MAC_BINDING)
        {
            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //MAC檢查
                if (!ChackMACLength(clsUPSERT_AGC_MAC_BINDING.fsMAC_ADDRESS)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA01", Data = "Mac格式錯誤" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //密碼長度
                if (!ChackPassWordLength(clsUPSERT_AGC_MAC_BINDING.fsPASSWORD)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB03", Data = "密碼長度應為8~20" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //SP
                string fsRESULT = new repAGC().fnGET_MEMBER_MACHINE_AND_BINDING(clsUPSERT_AGC_MAC_BINDING.fsMAC_ADDRESS, clsUPSERT_AGC_MAC_BINDING.fsPASSWORD, "3");

                if (fsRESULT.IndexOf("ERROR:") != -1)
                {
                    switch (fsRESULT.Replace("ERROR:", ""))
                    {
                        case "EA02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EA03":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsRESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// A02.停用Mac服務
        /// </summary>
        /// <param name="clsDISABLE_AGC_MAC_SERVICE">停用Mac服務</param>
        /// <returns></returns>
        [Route("DisableMacService")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostDisableMacService(clsDISABLE_AGC_MAC_SERVICE clsDISABLE_AGC_MAC_SERVICE)
        {

            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //MAC檢查
                if (!ChackMACLength(clsDISABLE_AGC_MAC_SERVICE.fsMAC_ADDRESS)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA01", Data = "Mac格式錯誤" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //SP
                string fsRESULT = new repAGC().fnUPDATE_MAC_STATUS(clsDISABLE_AGC_MAC_SERVICE.fsMAC_ADDRESS, "N");


                if (fsRESULT.IndexOf("ERROR:") != -1)
                {
                    switch (fsRESULT.Replace("ERROR:", ""))
                    {
                        case "EA04":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsRESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// A03.Mac清單查詢
        /// </summary>
        /// <param name="clsGET_AGC_MAC_INFO">Mac清單查詢</param>
        /// <returns></returns>
        [Route("GetMACInfoList")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostGetMACInfoList(clsGET_AGC_MAC_INFO clsGET_AGC_MAC_INFO)
        {

            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //SP
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repAGC().fnGET_AGC_MAC_INFO(clsGET_AGC_MAC_INFO.DISABLED == false ? "Y" : "N") }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// B01.修改會員密碼
        /// </summary>
        /// <param name="clsSET_AGC_ACCOUNT_PASSWORD">修改會員密碼</param>
        /// <returns></returns>
        [Route("SetAccountPassword")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostSetAccountPassword(clsSET_AGC_ACCOUNT_PASSWORD clsSET_AGC_ACCOUNT_PASSWORD)
        {
            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //密碼長度
                if (!ChackPassWordLength(clsSET_AGC_ACCOUNT_PASSWORD.fsPASSWORD)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB03", Data = "密碼長度應為8~20" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //SP
                string fsRESULT = new repAGC().fnUPDATE_ACG_ACCOUNT_PASSWORD(clsSET_AGC_ACCOUNT_PASSWORD.fsUSER_ID, clsSET_AGC_ACCOUNT_PASSWORD.fsPASSWORD);


                if (fsRESULT.IndexOf("ERROR:") != -1)
                {
                    switch (fsRESULT.Replace("ERROR:", ""))
                    {
                        case "EB01":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// B02.會員換機
        /// </summary>
        /// <param name="clsSET_AGC_ACCOUNT_CHANGE_MAC">會員換機</param>
        /// <returns></returns>
        [Route("SetAccountChangeMAC")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostSetAccountChangeMAC(clsSET_AGC_ACCOUNT_CHANGE_MAC clsSET_AGC_ACCOUNT_CHANGE_MAC)
        {
            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //MAC檢查
                if (!ChackMACLength(clsSET_AGC_ACCOUNT_CHANGE_MAC.fsMAC_ADDRESS_NEW)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA01", Data = "Mac格式錯誤" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //SP
                string fsRESULT = new repAGC().fnUPDATE_ACCOUNT_CHANGE_MAC(clsSET_AGC_ACCOUNT_CHANGE_MAC.fsUSER_ID, clsSET_AGC_ACCOUNT_CHANGE_MAC.fsMAC_ADDRESS_OLD, clsSET_AGC_ACCOUNT_CHANGE_MAC.fsMAC_ADDRESS_NEW);


                if (fsRESULT.IndexOf("ERROR:") != -1)
                {
                    switch (fsRESULT.Replace("ERROR:", ""))
                    {
                        case "EB01":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EB02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EA02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }

        }

        /// <summary>
        /// B03.查詢綁定機器
        /// </summary>
        /// <param name="clsGET_AGC_MAC_BY_ACCOUNTID">查詢綁定機器</param>
        /// <returns></returns>
        [Route("GetMACByAccountID")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostGetMACByAccountID(clsGET_AGC_MAC_BY_ACCOUNTID clsGET_AGC_MAC_BY_ACCOUNTID)
        {
            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //檢查USERID
                string fsACCOUNTID = ChackAccountID(clsGET_AGC_MAC_BY_ACCOUNTID.fsUSER_ID);
                if (string.IsNullOrEmpty(fsACCOUNTID)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01", Data = "找不到此會員帳號。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //SP
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repAGC().fnGET_MAC_BY_ACCOUNTID(clsGET_AGC_MAC_BY_ACCOUNTID.fsUSER_ID) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// B04.取消綁定機器
        /// </summary>
        /// <param name="clsCANCEL_AGC_MAC_BINDING">取消綁定機器</param>
        /// <returns></returns>
        [Route("CancelMACBinding")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostCancelMACBinding(clsCANCEL_AGC_MAC_BINDING clsCANCEL_AGC_MAC_BINDING)
        {

            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //MAC檢查
                if (!ChackMACLength(clsCANCEL_AGC_MAC_BINDING.fsMAC_ADDRESS)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA01", Data = "Mac格式錯誤" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //SP
                string fsRESULT = new repAGC().fnUPDATE_MAC_BINDING_STATUS(clsCANCEL_AGC_MAC_BINDING.fsUSER_ID, clsCANCEL_AGC_MAC_BINDING.fsMAC_ADDRESS, "N");


                if (fsRESULT.IndexOf("ERROR:") != -1)
                {
                    switch (fsRESULT.Replace("ERROR:", ""))
                    {
                        case "EA04":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EB01":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EB02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsRESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// C01.新增訂單
        /// </summary>
        /// <param name="clsGET_AGC_INSERT_ORDER">新增訂單</param>
        /// <returns></returns>
        [Route("InsertOrder")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostInsertOrder(clsGET_AGC_INSERT_ORDER clsGET_AGC_INSERT_ORDER)
        {
            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //MAC檢查
                string fsMAC_ADDRESS = clsGET_AGC_INSERT_ORDER.fsMAC_ADDRESS;
                if (!ChackMACLength(fsMAC_ADDRESS)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA01", Data = "Mac格式錯誤" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //檢查USERID
                string fsACCOUNTID = ChackAccountID(clsGET_AGC_INSERT_ORDER.fsUSER_ID);
                if (string.IsNullOrEmpty(fsACCOUNTID)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01", Data = "找不到此會員帳號。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }

                string fsRESULT = string.Empty;
                int fnRETRY_COUNT = 1;
                //產生訂單編號
                string fsORDER_NO = string.Empty;
                while (fnRETRY_COUNT <= 3 && string.IsNullOrEmpty(fsORDER_NO))
                {
                    fsORDER_NO = new repL_NO().fnGET_L_NO("ORDERNO", "訂單編號", DateTime.Now.ToString("yyyyMMdd"), "", 5, "AGC Import");
                    fnRETRY_COUNT += 1;
                }
                if (string.IsNullOrEmpty(fsORDER_NO))
                {
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "PURCHASE",
                        fsNAME = "購買",
                        fsCONTENT = "無法取得訂單編號",
                        fsCREATED_BY = "Purchase"
                    });
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                string fsPURCHASE_ID = clsGET_AGC_INSERT_ORDER.fsPURCHASE_ID;
                switch (fsPURCHASE_ID)
                {
                    case "CHANNELAGC_FULL":
                        //新增訂單
                        fsRESULT = new repAGC().fnINSERT_ORDER(clsGET_AGC_INSERT_ORDER.fsUSER_ID, clsGET_AGC_INSERT_ORDER.fsMAC_ADDRESS, clsGET_AGC_INSERT_ORDER.fsPURCHASE_ID, "CHAGC01", fsORDER_NO);
                        break;
                    case "CHANNELAGC_STU":
                        //新增訂單
                        fsRESULT = new repAGC().fnINSERT_ORDER(clsGET_AGC_INSERT_ORDER.fsUSER_ID, clsGET_AGC_INSERT_ORDER.fsMAC_ADDRESS, clsGET_AGC_INSERT_ORDER.fsPURCHASE_ID, "CHAGC02", fsORDER_NO);
                        break;
                    case "CHANNELAGC_4GTV":
                        //新增訂單
                        fsRESULT = new repAGC().fnINSERT_ORDER(clsGET_AGC_INSERT_ORDER.fsUSER_ID, clsGET_AGC_INSERT_ORDER.fsMAC_ADDRESS, clsGET_AGC_INSERT_ORDER.fsPURCHASE_ID, "CHAGC03", fsORDER_NO);
                        break;
                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EC01", Data = "找不到匹配的套餐" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                if (fsRESULT.IndexOf("ERROR:") != -1)
                {
                    switch (fsRESULT.Replace("ERROR:", ""))
                    {
                        case "EB01":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EB02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EA03":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EC03":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EC03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// C02.終止訂單
        /// </summary>
        /// <param name="clsGET_AGC_CANCEL_ORDER">終止訂單</param>
        /// <returns></returns>
        [Route("CancelOrder")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostCancelOrder(clsGET_AGC_CANCEL_ORDER clsGET_AGC_CANCEL_ORDER)
        {
            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //MAC檢查
                string fsMAC_ADDRESS = clsGET_AGC_CANCEL_ORDER.fsMAC_ADDRESS;
                if (!ChackMACLength(fsMAC_ADDRESS)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA01", Data = "Mac格式錯誤" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //檢查USERID
                string fsACCOUNTID = ChackAccountID(clsGET_AGC_CANCEL_ORDER.fsUSER_ID);
                if (string.IsNullOrEmpty(fsACCOUNTID)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01", Data = "找不到此會員帳號。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //SP
                string fsRESULT = new repAGC().fnUPDATE_CANCEL_ORDER(clsGET_AGC_CANCEL_ORDER.fsUSER_ID, clsGET_AGC_CANCEL_ORDER.fsMAC_ADDRESS, clsGET_AGC_CANCEL_ORDER.fsPURCHASE_ID);


                string fsPURCHASE_ID = clsGET_AGC_CANCEL_ORDER.fsPURCHASE_ID;
                if (fsRESULT.IndexOf("ERROR:") != -1)
                {
                    switch (fsRESULT.Replace("ERROR:", ""))
                    {
                        case "EB01":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EB02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EA03":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EC02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EC02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// C03.套餐異動
        /// </summary>
        /// <param name="clsSET_AGC_PURCHASE">會員與裝置綁訂</param>
        /// <returns></returns>
        [Route("SetPurchase")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostSetPurchase(clsSET_AGC_PURCHASE clsSET_AGC_PURCHASE)
        {
            try
            {
                //檢查IP
                if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //HEADER
                if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //MAC檢查
                string fsMAC_ADDRESS = clsSET_AGC_PURCHASE.fsMAC_ADDRESS;
                if (!ChackMACLength(fsMAC_ADDRESS)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA01", Data = "Mac格式錯誤" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
                //檢查USERID
                string fsACCOUNTID = ChackAccountID(clsSET_AGC_PURCHASE.fsUSER_ID);
                if (string.IsNullOrEmpty(fsACCOUNTID)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01", Data = "找不到此會員帳號。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }

                string fsRESULT = string.Empty;
                int fnRETRY_COUNT = 1;
                //產生訂單編號
                string fsORDER_NO = string.Empty;
                while (fnRETRY_COUNT <= 3 && string.IsNullOrEmpty(fsORDER_NO))
                {
                    fsORDER_NO = new repL_NO().fnGET_L_NO("ORDERNO", "訂單編號", DateTime.Now.ToString("yyyyMMdd"), "", 5, "AGC Import");
                    fnRETRY_COUNT += 1;
                }
                if (string.IsNullOrEmpty(fsORDER_NO))
                {
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "PURCHASE",
                        fsNAME = "購買",
                        fsCONTENT = "無法取得訂單編號",
                        fsCREATED_BY = "Purchase"
                    });
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                string fsPURCHASE_ID = clsSET_AGC_PURCHASE.fsPURCHASE_NEW;
                switch (fsPURCHASE_ID)
                {
                    case "CHANNELAGC_FULL":
                        //新增訂單
                        fsRESULT = new repAGC().fnUPDATE_CHANGE_PURCHASE(clsSET_AGC_PURCHASE.fsUSER_ID, clsSET_AGC_PURCHASE.fsMAC_ADDRESS, clsSET_AGC_PURCHASE.fsPURCHASE_OLD, clsSET_AGC_PURCHASE.fsPURCHASE_NEW, "CHAGC01", fsORDER_NO);
                        break;
                    case "CHANNELAGC_STU":
                        //新增訂單
                        fsRESULT = new repAGC().fnUPDATE_CHANGE_PURCHASE(clsSET_AGC_PURCHASE.fsUSER_ID, clsSET_AGC_PURCHASE.fsMAC_ADDRESS, clsSET_AGC_PURCHASE.fsPURCHASE_OLD, clsSET_AGC_PURCHASE.fsPURCHASE_NEW, "CHAGC02", fsORDER_NO);
                        break;
                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EC01", Data = "找不到匹配的套餐" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                if (fsRESULT.IndexOf("ERROR:") != -1)
                {
                    switch (fsRESULT.Replace("ERROR:", ""))
                    {
                        case "EB01":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EB02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EA03":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EA03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EC02":
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EC02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsRESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
            }
            catch (Exception e)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E001", Data = "內部錯誤，請回報四季人員。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// C04.訂單查詢
        /// </summary>
        /// <param name="clsGET_AGC_ORDER_BY_USERID">訂單查詢</param>
        /// <returns></returns>
        [Route("GetOrderInfo")]
        [Header(headersConfig = "From,string")]
        public HttpResponseMessage PostGetOrderInfo(clsGET_AGC_ORDER_BY_USERID clsGET_AGC_ORDER_BY_USERID)
        {
            //檢查IP
            if (!ChackClientIp()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
            //HEADER
            if (!ChackHeader()) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "E002", Data = "拒絕存取" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }
            //檢查USERID
            string fsACCOUNTID = ChackAccountID(clsGET_AGC_ORDER_BY_USERID.fsUSER_ID);
            if (string.IsNullOrEmpty(fsACCOUNTID)) { return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EB01", Data = "找不到此會員帳號。" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) }; }

            //SP
            var fsRESULT = new repAGC().fnGET_AGC_ORDER_INFO(fsACCOUNTID, clsGET_AGC_ORDER_BY_USERID.fsMAC_ADDRESS);

            if (fsRESULT.fsORDER_NO == null)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "EC02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            else
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsRESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// D01.查詢頻道清單
        /// </summary>
        /// <param name="req">查詢頻道清單</param>
        /// <returns></returns>
        [Route("GetChannelInfo")]
        [HttpPost]
        [Header(headersConfig = "From,string")]
        public StdResp<List<GetChannelInfo_Res>> GetChannelInfo(GetChannelInfo_Req req)
        {
            StdResp<List<GetChannelInfo_Res>> stdResp = new StdResp<List<GetChannelInfo_Res>>();

            List<GetChannelInfo_Res> list = new List<GetChannelInfo_Res>();

            //req
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.AGCReqIsNull);
            }

            //檢查IP
            if (!ChackClientIp())
            {
                throw new MyException(ResponseCodeEnum.AGCIPError);
            }
            //HEADER
            if (!ChackHeader())
            {
                throw new MyException(ResponseCodeEnum.AGCHeaderError);
            }
            //檢查
            list = _AGC_BLL.GetChannelInfo(req);
            stdResp.Data = list;



            return stdResp;
        }




        private bool ChackHeader(HttpRequestMessage request = null)
        {
            if (Request.Headers == null)
            {
                return false;
            }
            if (!Request.Headers.Contains("From"))
            {
                return false;
            }
            if (Request.Headers.GetValues("From").First().Equals("AGC"))
            //if (int.Parse(Properties.Settings.Default.fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("From") &&Request.Headers.GetValues("4GTV_AUTH").First().Equals("AGC")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ChackClientIp(HttpRequestMessage request = null)
        {
            string fsIP = string.Empty;

            request = request ?? Request;

            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                fsIP = ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                RemoteEndpointMessageProperty prop = (RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name];
                fsIP = prop.Address;
            }
            else if (HttpContext.Current != null)
            {
                fsIP = HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                fsIP = string.Empty;
            }
            try
            {

                StreamWriter sw = new StreamWriter(@"C:\AGCLOG\AGCLOG.log", true, System.Text.Encoding.UTF8);
                sw.WriteLine("================================================");
                sw.WriteLine(fsIP + "~~" + DateTime.Now.ToString());
                sw.WriteLine("================================================");
                sw.Close();

            }
            catch
            {

            }
            if (int.Parse(fcIS_PRODUCTION) == 0)//測試
            {
                if (fsIP == "60.248.0.210" || fsIP == "118.163.37.43" || fsIP == "13.75.115.241" || fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (fsIP == "60.248.0.210" || fsIP == "118.163.37.43" || fsIP == "13.75.115.241" || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        private bool ChackMACLength(string fsMAC_ADDRESS)
        {
            if (fsMAC_ADDRESS.Length == 12)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string ChackAccountID(string fsUSER_ID)
        {
            return new repAGC().fnGET_CHECK_USER_ID(fsUSER_ID);
        }

        private bool ChackPassWordLength(string fsPASSWORD)
        {
            if (fsPASSWORD.Length >= 8 && fsPASSWORD.Length <= 20)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //private void log(string IP) {

        //    if (!Directory.Exists(@"C:\AGCLOG\"))
        //        Directory.CreateDirectory(@"C:\AGCLOG\");


        //    System.IO.File.AppendAllText(@"C:\AGCLOG\AGC_IP.txt","IP:"+IP+"   TIME:"+DateTime.Now+Environment.NewLine, Encoding.Default);

        //    //using (FileStream fs = new FileStream(@"C:\AGCLOG\AGC_IP.txt", FileMode.Create, FileAccess.Write, FileShare.None))
        //    //{
        //    //    using (StreamWriter srOutFile = new StreamWriter(fs, Encoding.Default))
        //    //    {
        //    //        srOutFile.WriteLine("IP:" + IP);
        //    //        //srOutFile.Write("IP:"+ IP);
        //    //            srOutFile.Flush();

        //    //        srOutFile.Close();
        //    //    }
        //    //}

        //}


    }
}
