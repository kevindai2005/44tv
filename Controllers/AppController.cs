﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;
using System.Net.Http.Headers;
using System.Collections.Specialized;
using System.IO;
using System.Web.Script.Serialization;
using WebApi.OutputCache.V2;
using Newtonsoft.Json;
using System.Web.Configuration;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Model;
using FourgTV_OTT2.Common;
using static FourgTV_OTT2.Common.Setting.EnumSetting;
using FourgTV_OTT2.BLL.Model.Req.App;
using FourgTV_OTT2.BLL.Model.Res.App;
using System.Transactions;
using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.Module;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("App")]
    public class AppController : ApiController
    {

        private static readonly string fsLOG_PATH = WebConfigurationManager.AppSettings["fsLOG_PATH"];
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];
        private static readonly string fsAPP_HEADER_ENC_KEY = WebConfigurationManager.AppSettings["fsAPP_HEADER_ENC_KEY"];

        Vod_BLL _Vod_BLL = new Vod_BLL();
        clsM3U8 _clsM3U8 = new clsM3U8();
        Channel_BLL _Channel_BLL = new Channel_BLL();
        App_BLL _App_BLL = new App_BLL();
        CrownNameModule _CrownNameModule = new CrownNameModule();
        VodModule _VodModule = new VodModule();
        public string ClientIP
        {
            get
            {
                var obj = LogHelper.RequestVariables[LogRequestVariableType.ClientIP];
                return obj == null ? "" : obj.ToString();
            }
            set
            {
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.ClientIP, value);
            }
        }

        /// <summary>
        /// 取得頻道最後更新時間
        /// </summary>
        /// <returns></returns>
        [Route("GetChannelLatestTime")]
        public HttpResponseMessage GetChannelLatestTime()
        {
            try
            {
                string fsTIME = new repCONFIG().fnGET_CFG("CHANNEL_LATEST_TIME")[0].fsVALUE;

                DateTime dtTIME = new DateTime();

                if (!string.IsNullOrEmpty(fsTIME) && DateTime.TryParse(fsTIME, out dtTIME))
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = dtTIME.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds.ToString() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "取得時間錯誤!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得某時間後新增或更新的VOD
        /// </summary>
        /// <param name="fnSECONDS">1970-01-01 00:00:00到目前秒數</param>
        /// <returns></returns>
        [Route("GetVodByUpdateDate/{fnSECONDS}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetVodByUpdateDate(long fnSECONDS)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_BY_UPDATEDATE(fnSECONDS) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得APPCONFIG
        /// </summary>
        /// <param name="clsAPP_CONFIG">帳號資訊</param>
        /// <returns></returns>
        [Route("GetAPPConfig")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage PostAPPConfig(clsAPP_CONFIG clsAPP_CONFIG)
        {
            string fsDEVICE = clsAPP_CONFIG.fsDEVICE;
            string fsVERSION = clsAPP_CONFIG.fsVERSION;

            //路徑
            string fsPATH = string.Empty;
            //預設路徑
            string fsDEFULTPATH = string.Empty;
            //JSON
            dynamic obj;
            try
            {
                var fsAPPCONFIG_JSON_URLPATH = new repCONFIG().fnGET_CFG("APPCONFIG_JSON_URL")[0].fsVALUE;
                fsDEFULTPATH = fsAPPCONFIG_JSON_URLPATH + fsDEVICE + "-0.json";
                fsPATH = fsAPPCONFIG_JSON_URLPATH + fsDEVICE + "-" + fsVERSION + ".json";
                //判斷檔案是否存在
                if (System.IO.File.Exists(fsPATH))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    obj = serializer.Deserialize(File.ReadAllText(fsPATH), typeof(object));
                }
                else
                {
                    //判斷是否有預設的檔案
                    if (System.IO.File.Exists(fsDEFULTPATH))
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        obj = serializer.Deserialize(File.ReadAllText(fsDEFULTPATH), typeof(object));
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請確認裝置或版本" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = obj }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 回傳給APP有更新的VOD與最後更新頻道時間
        /// </summary>
        /// <param name="fnSECONDS">1970-01-01 00:00:00到目前秒數</param>
        /// <returns></returns>
        [Route("GetUpdateContent/{fnSECONDS}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetUpdateContent(long fnSECONDS)
        {
            try
            {
                clsAPP_UPDATE_CONTENT clsAPP_UPDATE_CONTENT = new clsAPP_UPDATE_CONTENT();
                clsAPP_UPDATE_CONTENT.vod = new List<clsVOD_INDEX>();

                //頻道最後更新時間
                string fsTIME = new repCONFIG().fnGET_CFG("CHANNEL_LATEST_TIME")[0].fsVALUE;

                DateTime dtTIME = new DateTime();

                if (!string.IsNullOrEmpty(fsTIME) && DateTime.TryParse(fsTIME, out dtTIME))
                {
                    clsAPP_UPDATE_CONTENT.channel = dtTIME.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds.ToString();
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "取得頻道更新時間錯誤!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                //取得VOD更新項目
                clsAPP_UPDATE_CONTENT.vod = new repVOD().fnGET_VOD_BY_UPDATEDATE(fnSECONDS);

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsAPP_UPDATE_CONTENT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得頻道URL
        /// </summary>
        /// <param name="clsAPP_GET_CHANNEL_URL_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetChannelUrl")]
        public HttpResponseMessage PostChannelUrl(clsAPP_GET_CHANNEL_URL_ARGS clsAPP_GET_CHANNEL_URL_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    string fsIP = HttpContext.Current.Request.UserHostAddress;

                    UserInfo clsUSER_DATA = null;

                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }

                    //寫LOG
                    new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
                    {
                        fsFROM = "2",
                        fsNAME = "CHANNEL",
                        fsASSET_ID = clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID,
                        fsIP = HttpContext.Current.Request.UserHostAddress,
                        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                        fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
                        fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
                    });

                    //測試環境才用
                    if (int.Parse(fcIS_PRODUCTION) == 0)
                    {
                        if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
                            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                    }
                    else
                    {
                        if (fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                    }

                    //取得頻道資訊
                    clsCHANNEL clsCHANNEL = new repCHANNEL().fnGET_CHANNEL(clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID);

                    clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                    clsGET_CHANNEL_URL.flstURLs = new List<string>();
                    clsGET_CHANNEL_URL.fsHEAD_FRAME = clsCHANNEL.fsHEAD_FRAME;
                    clsGET_CHANNEL_URL.fsCHANNEL_NAME = clsCHANNEL.fsNAME;
                    clsGET_CHANNEL_URL.flstBITRATE = clsCHANNEL.lstALL_BITRATE.Select<string, int>(q => int.Parse(q)).OrderBy(c => c).ToList();

                    //判斷是否為台灣的IP或允許海外播放
                    switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsCHANNEL.fcOVERSEAS, clsCHANNEL.lstEXCEPT_COUNTRY))
                    {
                        case "IsAllowArea"://允許播放
                            break;
                        case "OVERSEAS"://不允許海外
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EXCEPT_COUNTRY"://海外限制不能播放的國家
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    if (clsUSER_DATA != null)
                    {
                        //取得黑名單
                        List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPUSER.txt", System.Text.Encoding.UTF8).ToList();

                        if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
                        {
                            if (lstSTOP_USERs.FirstOrDefault(f => f == clsUSER_DATA.fsUSER) != null)
                            {
                                //無權限可以播
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }

                        //取得會員頻道權限
                        var clsMEMBER_CHANNEL_AUTH = new repCHANNEL().fnGET_MEMBER_CHANNEL_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID);

                        if (clsMEMBER_CHANNEL_AUTH.fcAUTH)
                        {
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetNewChannelUrl(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE));
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE));
                            clsGET_CHANNEL_URL.fbPLAY_AD = clsMEMBER_CHANNEL_AUTH.fcAD;
                        }
                        else
                        {
                            //無權限可以播
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        //判斷是否為免費
                        if (clsCHANNEL.fcFREE)
                        {
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetNewChannelUrl(false, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE));
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE));
                            clsGET_CHANNEL_URL.fbPLAY_AD = true;
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD URL
        /// </summary>
        /// <param name="clsAPP_GET_VOD_URL_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetVodUrl")]
        public HttpResponseMessage PostVodUrl(clsAPP_GET_VOD_URL_ARGS clsAPP_GET_VOD_URL_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    string fsIP = HttpContext.Current.Request.UserHostAddress;
                    int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("APP_VOD_FREE_SEC")[0].fsVALUE);
                    UserInfo clsUSER_DATA = null;

                    clsGET_CHANNEL_APP_URL clsGET_CHANNEL_APP_URL = new clsGET_CHANNEL_APP_URL();
                    //觀看時間
                    clsGET_CHANNEL_APP_URL.fnALLOWTIME = fnALLOWTIME;

                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_GET_VOD_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_VOD_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_GET_VOD_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_VOD_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
                        clsGET_CHANNEL_APP_URL.fnALLOWTIME = -1;
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }

                    //寫LOG
                    new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
                    {
                        fsFROM = "2",
                        fsNAME = "VOD",
                        fsASSET_ID = clsAPP_GET_VOD_URL_ARGS.fsASSET_ID,
                        fsIP = HttpContext.Current.Request.UserHostAddress,
                        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                        fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
                        fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
                    });

                    clsGET_VOD_URL clsGET_VOD_URL = new clsGET_VOD_URL();
                    clsGET_VOD_URL.flstURLs = new List<string>();
                    clsGET_VOD_URL.lstTIME_CODE = new List<int>();

                    //測試環境才用
                    if (int.Parse(fcIS_PRODUCTION) == 0)
                    {
                        if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
                            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                    }
                    else
                    {
                        if (fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                    }

                    //判斷是LiTV上架或FTV上架
                    var clsVOD_FROM = new repVOD().fnGET_VOD_FROM(clsAPP_GET_VOD_URL_ARGS.fsASSET_ID);

                    clsGET_VOD_URL.fsHEAD_FRAME = clsVOD_FROM.fsHEAD_FRAME;
                    //判斷是否為台灣的IP或允許海外播放
                    switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsVOD_FROM.fcOVERSEAS, clsVOD_FROM.lstEXCEPT_COUNTRY))
                    {
                        case "IsAllowArea"://允許播放
                            break;
                        case "OVERSEAS"://不允許海外
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EXCEPT_COUNTRY"://海外限制不能播放的國家
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    if (clsAPP_GET_VOD_URL_ARGS.fsMEDIA_TYPE == "scene")
                    {
                        //直擊
                        //判斷是否已登入
                        if (clsUSER_DATA != null)
                        {
                            //取得會員VOD權限
                            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_URL.fbPLAY_AD = true;
                        }
                        else
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_URL.fbPLAY_AD = true;
                        }
                    }
                    else if (clsAPP_GET_VOD_URL_ARGS.fsMEDIA_TYPE == "puppet")
                    {
                        //布袋戲館
                        //判斷是否已登入
                        if (clsUSER_DATA != null)
                        {
                            //取得會員VOD權限
                            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_URL.fbPLAY_AD = true;
                        }
                        else
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                            clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_URL.fbPLAY_AD = true;
                        }
                    }
                    else
                    {
                        //戲劇/綜藝
                        //判斷是否已登入
                        if (clsUSER_DATA != null)
                        {
                            //取得會員VOD權限
                            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);
                            //戲劇綜藝由LITV上傳的沒有中華m3u8
                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                            if (clsVOD_FROM.fsFROM == "FTV")
                            {
                                clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            }
                            else
                            {
                                clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP, fnALLOWTIME));
                            }
                            clsGET_VOD_URL.fbPLAY_AD = true;
                        }
                        else
                        {
                            clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                            if (clsVOD_FROM.fsFROM == "FTV")
                            {
                                clsGET_VOD_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            }
                            else
                            {
                                clsGET_VOD_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP, fnALLOWTIME));
                            }
                            clsGET_VOD_URL.fbPLAY_AD = true;
                        }
                    }

                    clsGET_VOD_URL.lstTIME_CODE = new repVOD().fnGET_VOD_TIME_CODE(clsAPP_GET_VOD_URL_ARGS.fsASSET_ID);

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_VOD_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得頻道URL V2
        /// </summary>
        /// <param name="clsAPP_GET_CHANNEL_URL_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetChannelUrl2")]
        public HttpResponseMessage PostChannelUrl2(clsAPP_GET_CHANNEL_URL_ARGS clsAPP_GET_CHANNEL_URL_ARGS)
        {
            try
            {
                //var a = int.Parse(fcIS_PRODUCTION);
                //var b = Request.Headers;

                //var c = Request.Headers.Contains("4GTV_AUTH");

                //var d = clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First());

                //   var e = int.Parse(fcIS_PRODUCTION);


                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {


                    //    StdResp<GetChannelUrl> stdResp = new StdResp<GetChannelUrl>();

                    //擋掉某個偷連盒子
                    if (Request.Headers.Contains("x-requested-with"))
                    {
                        if (Request.Headers.GetValues("x-requested-with").First().Equals("org.amo.videolive"))
                        {
                            GetChannelUrl clsGET_CHANNEL_URL = new GetChannelUrl();
                            clsGET_CHANNEL_URL.flstURLs = new List<string>();
                            clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area8/index.m3u8");
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }

                    //擋掉黑名單IP
                    try
                    {
                        List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPIP.txt", System.Text.Encoding.UTF8).ToList();
                        lstSTOP_USERs = lstSTOP_USERs.Where(x => !string.IsNullOrEmpty(x)).ToList();
                        if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
                        {
                            if (lstSTOP_USERs.FirstOrDefault(f => this.ClientIP.IndexOf(f) > -1) != null)
                            {
                                clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                                clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                                clsGET_CHANNEL_URL.flstURLs = new List<string>();
                                clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area8/index.m3u8");
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                        }
                    }
                    catch (Exception e)
                    {

                    }


                    if (clsAPP_GET_CHANNEL_URL_ARGS == null)
                    {
                        throw new MyException(ResponseCodeEnum.RequestNull);
                    }
                    if (clsAPP_GET_CHANNEL_URL_ARGS.fnCHANNEL_ID <= 0)
                    {
                        throw new MyException(ResponseCodeEnum.RequestNull);
                    }
                    if (clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID == null)
                    {
                        throw new MyException(ResponseCodeEnum.RequestNull);
                    }
                    if (clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE == null)
                    {
                        throw new MyException(ResponseCodeEnum.RequestNull);
                    }
                    UserInfo clsUSER_DATA = null;

                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                        //取得黑名單
                        List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPUSER.txt", System.Text.Encoding.UTF8).ToList();
                        lstSTOP_USERs = lstSTOP_USERs.Where(x => !string.IsNullOrEmpty(x)).ToList();
                        if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
                        {
                            if (lstSTOP_USERs.FirstOrDefault(f => f.Equals(clsUSER_DATA.fsUSER)) != null)
                            {
                                // throw new MyException(ResponseCodeEnum.ChannelNoAuthority);
                                clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                                clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                                clsGET_CHANNEL_URL.flstURLs = new List<string>();
                                clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area8/index.m3u8");
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                            }
                        }
                    }

                    var GetChannelUrl = _Channel_BLL.GetChannelUrl(clsAPP_GET_CHANNEL_URL_ARGS.fnCHANNEL_ID, clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsUSER_DATA);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = GetChannelUrl }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    throw new MyException(ResponseCodeEnum.HeaderError);
                }
            }
            catch (MyException e)
            {

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = e.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                throw e;
            }
            //try
            //{
            //    if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
            //    {
            //        string fsIP = HttpContext.Current.Request.UserHostAddress;
            //        //觀看時間
            //        int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("APP_CHANNEL_FREE_SEC")[0].fsVALUE);
            //        UserInfo clsUSER_DATA = null;

            //        //判斷是否已登入
            //        if (clsSECURITY.IsLogin(clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
            //        {
            //            clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_CHANNEL_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
            //            //確認登入無觀看時間
            //            fnALLOWTIME = -1;
            //        }

            //        //判斷是否為免登入頻道
            //        if (!string.IsNullOrEmpty(new repCONFIG().fnGET_CFG("CHANNEL_NO_LOGIN")[0].fsVALUE.Split(';').ToList().Find(x => x.Equals(clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID))))
            //        {
            //            fnALLOWTIME = -1;
            //        }


            //        //寫LOG
            //        new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
            //        {
            //            fsFROM = "2",
            //            fsNAME = "CHANNEL",
            //            fsASSET_ID = clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID,
            //            fsIP = HttpContext.Current.Request.UserHostAddress,
            //            fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
            //            fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
            //            fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
            //        });

            //        //測試環境才用
            //        if (int.Parse(fcIS_PRODUCTION) == 0)
            //        {
            //            if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
            //                fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
            //        }
            //        else
            //        {
            //            if (fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
            //                fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
            //        }

            //        //取得頻道資訊
            //        clsCHANNEL clsCHANNEL = new repCHANNEL().fnGET_CHANNEL(clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID);

            //        clsGET_CHANNEL_APP_URL clsGET_CHANNEL_APP_URL = new clsGET_CHANNEL_APP_URL();
            //        clsGET_CHANNEL_APP_URL.flstURLs = new List<string>();
            //        clsGET_CHANNEL_APP_URL.fsHEAD_FRAME = clsCHANNEL.fsHEAD_FRAME;
            //        clsGET_CHANNEL_APP_URL.fsCHANNEL_NAME = clsCHANNEL.fsNAME;
            //        clsGET_CHANNEL_APP_URL.flstBITRATE = clsCHANNEL.lstALL_BITRATE.Select<string, int>(q => int.Parse(q)).OrderBy(c => c).ToList();
            //        clsGET_CHANNEL_APP_URL.fnALLOWTIME = fnALLOWTIME;
            //        //判斷是否為台灣的IP或允許海外播放
            //        switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsCHANNEL.fcOVERSEAS, clsCHANNEL.lstEXCEPT_COUNTRY))
            //        {
            //            case "IsAllowArea"://允許播放
            //                break;
            //            case "OVERSEAS"://不允許海外
            //                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //            case "EXCEPT_COUNTRY"://海外限制不能播放的國家
            //                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //            default:
            //                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //        }
            //        if (clsUSER_DATA != null)
            //        {
            //            //取得黑名單
            //            List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPUSER.txt", System.Text.Encoding.UTF8).ToList();

            //            if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
            //            {
            //                if (lstSTOP_USERs.FirstOrDefault(f => f == clsUSER_DATA.fsUSER) != null)
            //                {
            //                    //無權限可以播
            //                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //                }
            //            }

            //            //取得會員頻道權限
            //            var clsMEMBER_CHANNEL_AUTH = new repCHANNEL().fnGET_MEMBER_CHANNEL_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID);

            //            if (clsMEMBER_CHANNEL_AUTH.fcAUTH)
            //            {
            //                clsGET_CHANNEL_APP_URL.flstURLs.Add(clsM3U8.GetNewChannelUrl(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE, fnALLOWTIME, clsCHANNEL.fsCDN_ROUTE));
            //                clsGET_CHANNEL_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE, fnALLOWTIME, clsCHANNEL.fsCDN_ROUTE));
            //                clsGET_CHANNEL_APP_URL.fbPLAY_AD = clsMEMBER_CHANNEL_AUTH.fcAD;
            //            }
            //            else
            //            {
            //                //無權限可以播
            //                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //            }
            //        }
            //        else
            //        {
            //            //判斷是否為免費
            //            if (clsCHANNEL.fcFREE)
            //            {
            //                clsGET_CHANNEL_APP_URL.flstURLs.Add(clsM3U8.GetNewChannelUrl(false, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE, fnALLOWTIME, clsCHANNEL.fsCDN_ROUTE));
            //                clsGET_CHANNEL_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", clsAPP_GET_CHANNEL_URL_ARGS.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE, fnALLOWTIME, clsCHANNEL.fsCDN_ROUTE));
            //                clsGET_CHANNEL_APP_URL.fbPLAY_AD = true;
            //            }
            //            else
            //            {
            //                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //            }
            //        }

            //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_APP_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

            //    }
            //    else
            //    {
            //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //}
        }

        /// <summary>
        /// 取得VOD URL V2
        /// </summary>
        /// <param name="clsAPP_GET_VOD_URL_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetVodUrl2")]
        public HttpResponseMessage PostVodUrl2(clsAPP_GET_VOD_URL_ARGS clsAPP_GET_VOD_URL_ARGS)
        {

            CrownNameInfo CrownName = new CrownNameInfo()
            {
                ID = 0,
                Title = string.Empty,
                ImageURL = string.Empty,
                URL = string.Empty,
                Position = string.Empty,
                Template = string.Empty,
                Vendor = string.Empty

            };

            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    string fsIP = HttpContext.Current.Request.UserHostAddress;

                    UserInfo clsUSER_DATA = null;
                    //觀看時間
                    int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("APP_VOD_FREE_SEC")[0].fsVALUE);

                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_GET_VOD_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_VOD_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_GET_VOD_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_VOD_URL_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
                        //確認登入無觀看時間
                        fnALLOWTIME = -1;

                    }

                    //寫LOG
                    new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
                    {
                        fsFROM = "2",
                        fsNAME = "VOD",
                        fsASSET_ID = clsAPP_GET_VOD_URL_ARGS.fsASSET_ID,
                        fsIP = HttpContext.Current.Request.UserHostAddress,
                        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                        fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
                        fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
                    });
                    clsGET_VOD_APP_URL clsGET_VOD_APP_URL = new clsGET_VOD_APP_URL();
                    clsGET_VOD_APP_URL.flstURLs = new List<string>();
                    clsGET_VOD_APP_URL.lstTIME_CODE = new List<int>();
                    clsGET_VOD_APP_URL.fnALLOWTIME = fnALLOWTIME;

                 

                    //取得
                    var ARC_VIDEOInfo = _VodModule.fnGET_ARC_VIDEOInfoByfsASSET_ID(clsAPP_GET_VOD_URL_ARGS.fsASSET_ID);
                    //冠名
                    CrownName = _CrownNameModule.GetCrownNameInfo(ARC_VIDEOInfo.fsVOD_NO);

                    clsGET_VOD_APP_URL.CrownName = CrownName;

                    //測試環境才用
                    if (int.Parse(fcIS_PRODUCTION) == 0)
                    {
                        if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
                            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                    }
                    else
                    {
                        if (fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                    }

                    //判斷fsASSET_ID 是否有效  clsGET_VOD_URL_ARGS.fsASSET_ID

                    if (!_Vod_BLL.CheckfsASSET_ID(clsAPP_GET_VOD_URL_ARGS.fsASSET_ID))
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

                    }


                    //判斷是LiTV上架或FTV上架
                    var clsVOD_FROM = new repVOD().fnGET_VOD_FROM(clsAPP_GET_VOD_URL_ARGS.fsASSET_ID);

                    clsGET_VOD_APP_URL.fsHEAD_FRAME = clsVOD_FROM.fsHEAD_FRAME;

                    //判斷是否為台灣的IP或允許海外播放
                    switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsVOD_FROM.fcOVERSEAS, clsVOD_FROM.lstEXCEPT_COUNTRY))
                    {
                        case "IsAllowArea"://允許播放
                            break;
                        case "OVERSEAS"://不允許海外
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        case "EXCEPT_COUNTRY"://海外限制不能播放的國家
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        default:
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    if (clsAPP_GET_VOD_URL_ARGS.fsMEDIA_TYPE == "scene")
                    {
                        //直擊
                        //判斷是否已登入
                        if (clsUSER_DATA != null)
                        {
                            //取得會員VOD權限
                            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);
                            clsGET_VOD_APP_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                            clsGET_VOD_APP_URL.fbPLAY_AD = true;
                        }
                        else
                        {
                            clsGET_VOD_APP_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));
                            clsGET_VOD_APP_URL.fbPLAY_AD = true;
                        }
                    }
                    else if (clsAPP_GET_VOD_URL_ARGS.fsMEDIA_TYPE == "puppet")
                    {
                        //布袋戲館
                        //判斷是否已登入
                        if (clsUSER_DATA != null)
                        {
                            //取得會員VOD權限
                            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                            clsGET_VOD_APP_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));

                            clsGET_VOD_APP_URL.fbPLAY_AD = true;
                        }
                        else
                        {
                            clsGET_VOD_APP_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));

                            clsGET_VOD_APP_URL.fbPLAY_AD = true;
                        }
                    }
                    else if (clsAPP_GET_VOD_URL_ARGS.fsMEDIA_TYPE == "shop")
                    {
                        //購物
                        //判斷是否已登入
                        if (clsUSER_DATA != null)
                        {
                            //取得會員VOD權限
                            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);

                            clsGET_VOD_APP_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));

                            clsGET_VOD_APP_URL.fbPLAY_AD = false;
                        }
                        else
                        {
                            clsGET_VOD_APP_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));

                            clsGET_VOD_APP_URL.fbPLAY_AD = false;
                        }
                    }
                    else
                    {
                        //戲劇/綜藝
                        //判斷是否已登入
                        if (clsUSER_DATA != null)
                        {
                            //取得會員VOD權限
                            var clsMEMBER_VOD_AUTH = new repVOD().fnGET_MEMBER_VOD_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsVOD_FROM.fsVOD_NO);
                            if (clsVOD_FROM.fsFROM == "FTV")
                            {
                                clsGET_VOD_APP_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(clsMEMBER_VOD_AUTH.fcAUTH, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            }
                            else
                            {
                                clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP, fnALLOWTIME));
                            }
                            //戲劇綜藝由LITV上傳的沒有中華m3u8
                            clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_VOD_AUTH.fcIS_PAY_MEMBER, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));

                            clsGET_VOD_APP_URL.fbPLAY_AD = true;
                        }
                        else
                        {
                            if (clsVOD_FROM.fsFROM == "FTV")
                            {
                                clsGET_VOD_APP_URL.flstURLs.Add(clsVOD_FROM.fcCASE_CHT == "B" ? clsM3U8.GetVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, fsIP, fnALLOWTIME) : clsM3U8.GetTypeCVodUrl(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fsIP, fnALLOWTIME));
                            }
                            else
                            {
                                clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetLiTVVodUrl(clsVOD_FROM.fcCASE_CHT, false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, clsVOD_FROM.fsWRITTEN_BY, clsAPP_GET_VOD_URL_ARGS.fsASSET_ID.Replace("_1500K", ""), fsIP, fnALLOWTIME));
                            }
                            clsGET_VOD_APP_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsAPP_GET_VOD_URL_ARGS.fsDEVICE_TYPE, "vod", clsAPP_GET_VOD_URL_ARGS.fsASSET_ID, clsVOD_FROM.fnYEAR, fnALLOWTIME));

                            clsGET_VOD_APP_URL.fbPLAY_AD = true;
                        }
                    }

                    clsGET_VOD_APP_URL.lstTIME_CODE = new repVOD().fnGET_VOD_TIME_CODE(clsAPP_GET_VOD_URL_ARGS.fsASSET_ID);

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_VOD_APP_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得VOD首頁資訊-手機用
        /// </summary>
        /// <returns></returns>
        [Route("GetMobileVodIndex")]
        [CacheOutput(ServerTimeSpan = 21600, AnonymousOnly = true)]
        public HttpResponseMessage GetMobileVodIndex()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_INDEX_MOBILE() }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// 取得個人變數
        /// </summary>
        /// <param name="clsAPP_GET_CLIENTVAR_ARGS">查詢參數</param>
        /// <returns></returns>
        [Route("GetClientVar")]
        public HttpResponseMessage PostGetClientVar(clsAPP_GET_CLIENTVAR_ARGS clsAPP_GET_CLIENTVAR_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_GET_CLIENTVAR_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_CLIENTVAR_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_GET_CLIENTVAR_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_CLIENTVAR_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repCLIENTVAR().fnGET_CLIENT_VAR(clsUSER_DATA.fsACCOUNT_ID, (string.IsNullOrEmpty(clsAPP_GET_CLIENTVAR_ARGS.fsVOD_NO) ? string.Empty : clsAPP_GET_CLIENTVAR_ARGS.fsVOD_NO), string.IsNullOrEmpty(clsAPP_GET_CLIENTVAR_ARGS.fcREGION) ? "L" : clsAPP_GET_CLIENTVAR_ARGS.fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 設定個人變數
        /// </summary>
        /// <param name="clsAPP_SET_CLIENTVAR_ARGS">帳號資訊</param>
        /// <returns></returns>
        [Route("SetClientVar")]
        public HttpResponseMessage PostSetClientVar(clsAPP_SET_CLIENTVAR_ARGS clsAPP_SET_CLIENTVAR_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_SET_CLIENTVAR_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_SET_CLIENTVAR_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_SET_CLIENTVAR_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_SET_CLIENTVAR_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                        var clsSET_CLIENTVAR = new repCLIENTVAR().fnSET_CLIENT_VAR(clsUSER_DATA.fsACCOUNT_ID, clsAPP_SET_CLIENTVAR_ARGS.fsVOD_NO, clsAPP_SET_CLIENTVAR_ARGS.fnSEQ, clsAPP_SET_CLIENTVAR_ARGS.fsPART, clsAPP_SET_CLIENTVAR_ARGS.fnTIME);

                        if (clsSET_CLIENTVAR != null)
                        {
                            if (clsSET_CLIENTVAR.fbRESULT)
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "新增失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "新增失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 啟用優惠碼
        /// </summary>
        /// <param name="clsAPP_REGISTER_COUPON_ARGS">啟用優惠碼資訊</param>
        /// <returns></returns>
        [Route("RegisterCoupon")]
        [Header(headersConfig = "fsVALUE,string|fsENC_KEY,string")]
        public HttpResponseMessage PostRegisterCoupon(clsAPP_REGISTER_COUPON_ARGS clsAPP_REGISTER_COUPON_ARGS)
        {

            UseCouponCode_Req req = new UseCouponCode_Req()
            {
                CouponCode = clsAPP_REGISTER_COUPON_ARGS.fsCOUPON_NO,
                ValidateCode = clsAPP_REGISTER_COUPON_ARGS.fsVALIDATE_CODE

            };
            LogHelper.RequestVariables.SetVariable(LogRequestVariableType.VALUE, clsAPP_REGISTER_COUPON_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE);
            LogHelper.RequestVariables.SetVariable(LogRequestVariableType.ENC_KEY, clsAPP_REGISTER_COUPON_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

            try
            {
                using (TransactionScope _TransactionScope = new TransactionScope())
                {
                    StdResp<UseCouponCode_Res> stdResp = new StdResp<UseCouponCode_Res>();

                    stdResp.Data = _App_BLL.UseCouponCode(req);

                    _TransactionScope.Complete();
                    // return stdResp;
                }

                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }

            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_REGISTER_COUPON_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_REGISTER_COUPON_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        //判斷驗證碼
                        string fsVALIDATE_CODE_RESULT = new repACCOUNT().fnGET_LOGIN_VALIDATE_CODE(clsAPP_REGISTER_COUPON_ARGS.fsVALIDATE_CODE);

                        if (string.IsNullOrEmpty(fsVALIDATE_CODE_RESULT))
                        {
                            //檢查優惠碼狀態
                            string fsRESULT = string.Empty;

                            fsRESULT = new repBSM().fnGET_COUPON_STATUS(clsAPP_REGISTER_COUPON_ARGS.fsCOUPON_NO);

                            if (string.IsNullOrEmpty(fsRESULT))
                            {
                                var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_REGISTER_COUPON_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_REGISTER_COUPON_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                                fsRESULT = new repBSM().fnREGISTER_COUPON(clsUSER_DATA.fsACCOUNT_ID, clsAPP_REGISTER_COUPON_ARGS.fsCOUPON_NO);

                                if (string.IsNullOrEmpty(fsRESULT))
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                                else
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsVALIDATE_CODE_RESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得可使用服務
        /// </summary>
        /// <param name="clsAPP_IDENTITY_VALIDATE_ARUS">帳號資訊</param>
        /// <returns></returns>
        [Route("GetServiceInfo")]
        public HttpResponseMessage PostServiceeInfo(clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    // 判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBSM().fnGET_MEMBER_SERVICE_INFO(clsUSER_DATA.fsACCOUNT_ID) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得購買紀錄清單
        /// </summary>
        /// <param name="clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS">購買資訊與帳號資訊</param>
        /// <returns></returns>
        [Route("GetPurchaseInfo")]
        public HttpResponseMessage PostPurchaseInfo(clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    // 判斷是否已登入
                    if (clsSECURITY.IsLogin(clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsVALUE, clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS.clsAPP_IDENTITY_VALIDATE_ARUS.fsENC_KEY);

                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBSM().fnGET_MEMBER_PURCHASE_INFO(clsUSER_DATA.fsACCOUNT_ID, clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS.fsOTPW) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得Header驗證碼
        /// </summary>
        /// <returns></returns>
        [Route("GetHeaderTestCode")]
        public HttpResponseMessage GetHeaderTestCode()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = Convert.ToBase64String(new System.Security.Cryptography.SHA512CryptoServiceProvider().ComputeHash(System.Text.Encoding.Default.GetBytes(DateTime.UtcNow.ToString("yyyyMMdd") + fsAPP_HEADER_ENC_KEY))) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 新增回饋
        /// </summary>
        /// <returns></returns>
        [Route("AddFeedBack")]
        public HttpResponseMessage PostAddFeedBack(clsINSERT_FEEDBACK_ARGS clsINSERT_FEEDBACK_ARGS)
        {
            try
            {
                if (int.Parse(fcIS_PRODUCTION) == 0 || (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First())))
                {
                    string fsRESULT = new repFEEDBACK().fnINSERT_FEEDBACK(clsINSERT_FEEDBACK_ARGS);
                    if (string.IsNullOrEmpty(fsRESULT))
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    else
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "header-error" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// Header驗證
        /// </summary>
        /// <returns></returns>
        [Route("TestEnc")]
        public HttpResponseMessage PostTestEnc()
        {
            try
            {
                HttpResponseMessage aaa = new HttpResponseMessage();


                if (Request.Headers != null && Request.Headers.Contains("4GTV_AUTH") && clsSECURITY.AppEncryptValidate(Request.Headers.GetValues("4GTV_AUTH").First()))
                {
                    //return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content = new ObjectContent<string>(clsSECURITY.AesEncrypt_APP(JsonConvert.SerializeObject(new clsRETURN() { Success = true, Data = new repVOD().fnGET_VOD_SHOW_INDEX_MOBILE() })), GlobalConfiguration.Configuration.Formatters.JsonFormatter)
                    };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "驗證錯誤!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 回傳400
        /// </summary>
        /// <returns></returns>
        [Route("Return400")]
        public HttpResponseMessage GetReturn400()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.BadRequest, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "回傳400!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 回傳403
        /// </summary>
        /// <returns></returns>
        [Route("Return403")]
        public HttpResponseMessage GetReturn403()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.Forbidden, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "回傳403!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 回傳500
        /// </summary>
        /// <returns></returns>
        [Route("Return500")]
        public HttpResponseMessage GetReturn500()
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "回傳500!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// 啟用優惠碼
        /// </summary>
        /// <returns></returns>
        [Route("UseCouponCode")]
        [HttpPost]
        [Header(headersConfig = "fsVALUE,string|fsENC_KEY,string")]
        public StdResp<UseCouponCode_Res> UseCouponCode(UseCouponCode_Req req)
        {
            using (TransactionScope _TransactionScope = new TransactionScope())
            {
                StdResp<UseCouponCode_Res> stdResp = new StdResp<UseCouponCode_Res>();

                stdResp.Data = _App_BLL.UseCouponCode(req);

                _TransactionScope.Complete();
                return stdResp;
            }

        }
    }
}
