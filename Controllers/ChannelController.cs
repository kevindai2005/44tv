﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;

using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;

using WebApi.OutputCache.V2;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Configuration;
using FourgTV_OTT2.Common;
using static FourgTV_OTT2.Common.Setting.EnumSetting;
using FourgTV_OTT2.BLL.Model.Model;
using FourgTV_OTT2.BLL.API;
using System.Security.Cryptography;
using System.Threading;
using System.Text.RegularExpressions;
using RestSharp;
using FourgTV_OTT2.Common.AkamaiToken;

namespace FourgTV_OTT2.Api2.Controllers
{

    [RoutePrefix("Channel")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://embed.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ChannelController : ApiController
    {
        private static readonly string fsLOG_PATH = WebConfigurationManager.AppSettings["fsLOG_PATH"];
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];
        public string ClientIP
        {
            get
            {
                var obj = LogHelper.RequestVariables[LogRequestVariableType.ClientIP];
                return obj == null ? "" : obj.ToString();
            }
            set
            {
                LogHelper.RequestVariables.SetVariable(LogRequestVariableType.ClientIP, value);
            }
        }

        clsM3U8 _clsM3U8 = new clsM3U8();
        Channel_BLL _Channel_BLL = new Channel_BLL();


        /// <summary>
        /// 紀錄Channel觀看次數
        /// </summary>
        /// <param name="clsINSERT_CHANNEL_PAGEVIEW_ARGS">觀看紀錄資訊</param>
        /// <returns></returns>
        [Route("AddChannelPageView")]
        public HttpResponseMessage PostChannelPageView(clsINSERT_CHANNEL_PAGEVIEW_ARGS clsINSERT_CHANNEL_PAGEVIEW_ARGS)
        {
            try
            {
                string fsRESULT = new repCHANNEL().fnINSERT_CHANNEL_PAGEVIEW(clsINSERT_CHANNEL_PAGEVIEW_ARGS.fs4GTV_ID);
                if (string.IsNullOrEmpty(fsRESULT))
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                else
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得頻道URL
        /// </summary>
        /// <param name="clsGET_CHANNEL_URL_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetChannelUrl")]
        public HttpResponseMessage PostChannelUrl(clsGET_CHANNEL_URL_ARGS clsGET_CHANNEL_URL_ARGS)
        {
            clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
            clsGET_CHANNEL_URL.flstURLs = new List<string>();
            clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area7/index.m3u8");
            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };

            //try
            //{
            //    string fsIP = HttpContext.Current.Request.UserHostAddress;
            //    clsUSER_DATA clsUSER_DATA = null;

            //    //判斷是否已登入
            //    if (clsSECURITY.IsLogin(clsGET_CHANNEL_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
            //    {
            //        clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_CHANNEL_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

            //        //取得黑名單
            //        List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(Properties.Settings.Default.fsLOG_PATH + "STOPUSER.txt", System.Text.Encoding.UTF8).ToList();

            //        if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
            //        {
            //            if (lstSTOP_USERs.FirstOrDefault(f => f == clsUSER_DATA.fsUSER) != null)
            //            {
            //                //無權限可以播
            //                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //            }
            //        }
            //    }

            //    //寫LOG
            //    new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
            //    {
            //        fsFROM = "1",
            //        fsNAME = "CHANNEL",
            //        fsASSET_ID = clsGET_CHANNEL_URL_ARGS.fsASSET_ID,
            //        fsIP = HttpContext.Current.Request.UserHostAddress,
            //        fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
            //        fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
            //        fsUSER_AGENT = HttpContext.Current.Request.UserAgent
            //    });

            //    //測試環境才用
            //    if (int.Parse(fcIS_PRODUCTION) == 0)
            //    {
            //        if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
            //            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
            //    }
            //    else
            //    {
            //        if (fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
            //            fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
            //    }

            //    //取得頻道資訊
            //    clsCHANNEL clsCHANNEL = new repCHANNEL().fnGET_CHANNEL(clsGET_CHANNEL_URL_ARGS.fsASSET_ID);

            //    clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
            //    clsGET_CHANNEL_URL.flstURLs = new List<string>();
            //    clsGET_CHANNEL_URL.fsHEAD_FRAME = clsCHANNEL.fsHEAD_FRAME;
            //    clsGET_CHANNEL_URL.fsCHANNEL_NAME = clsCHANNEL.fsNAME;
            //    clsGET_CHANNEL_URL.flstBITRATE = clsCHANNEL.lstALL_BITRATE.Select<string, int>(q => int.Parse(q)).OrderBy(c => c).ToList();

            //    //判斷是否為台灣的IP或允許海外播放
            //    switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsCHANNEL.fcOVERSEAS, clsCHANNEL.lstEXCEPT_COUNTRY))
            //    {
            //        case "IsAllowArea"://允許播放
            //            break;
            //        case "OVERSEAS"://不允許海外
            //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //        case "EXCEPT_COUNTRY"://海外限制不能播放的國家
            //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //        default:
            //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //    }

            //    if (clsUSER_DATA != null)
            //    {
            //        //取得會員頻道權限
            //        var clsMEMBER_CHANNEL_AUTH = new repCHANNEL().fnGET_MEMBER_CHANNEL_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsGET_CHANNEL_URL_ARGS.fsASSET_ID);

            //        if (clsMEMBER_CHANNEL_AUTH.fcAUTH)
            //        {
            //            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetNewChannelUrl(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsGET_CHANNEL_URL_ARGS.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE));
            //            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", clsGET_CHANNEL_URL_ARGS.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE));
            //            clsGET_CHANNEL_URL.fbPLAY_AD = clsMEMBER_CHANNEL_AUTH.fcAD;
            //        }
            //        else
            //        {
            //            //無權限可以播
            //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //        }
            //    }
            //    else
            //    {
            //        //判斷是否為免費
            //        if (clsCHANNEL.fcFREE)
            //        {
            //            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetNewChannelUrl(false, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsGET_CHANNEL_URL_ARGS.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE));
            //            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", clsGET_CHANNEL_URL_ARGS.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE));
            //            clsGET_CHANNEL_URL.fbPLAY_AD = true;
            //        }
            //        else
            //        {
            //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //        }
            //    }
            //    //此URL關閉，寫死盜版連結
            //    clsGET_CHANNEL_URL.flstURLs.Clear();
            //    clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area7/index.m3u8");
            //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //}
            //catch (Exception ex)
            //{
            //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            //}
        }

        /// <summary>
        /// 取得頻道URL V2
        /// </summary>
        /// <param name="clsGET_CHANNEL_URL_ARGS2">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetChannelUrl2")]
        public HttpResponseMessage PostChannelUrl2(clsGET_CHANNEL_URL_ARGS2 clsGET_CHANNEL_URL_ARGS2)
        {

            try
            {
                clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();

                if (Request.Headers.Contains("x-requested-with"))
                {
                    if (Request.Headers.GetValues("x-requested-with").First().Equals("org.amo.videolive"))
                    {

                        clsGET_CHANNEL_URL.flstURLs = new List<string>();
                        clsGET_CHANNEL_URL.flstURLs.Add("+");
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(clsGET_CHANNEL_URL)) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

                clsGET_CHANNEL_URL_ARGS clsGET_CHANNEL_URL_ARGS = new clsGET_CHANNEL_URL_ARGS();
                //觀看時間
                int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("WEB_CHANNEL_FREE_SEC")[0].fsVALUE);
                //解密
                string toDes = clsSECURITY.AesDecrypt(clsGET_CHANNEL_URL_ARGS2.value);
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(toDes)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(clsGET_CHANNEL_URL_ARGS));
                    clsGET_CHANNEL_URL_ARGS = (clsGET_CHANNEL_URL_ARGS)deseralizer.ReadObject(ms);// //反序列化ReadObject
                }

                string fsIP = HttpContext.Current.Request.UserHostAddress;
                UserInfo clsUSER_DATA = null;

                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsGET_CHANNEL_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_CHANNEL_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                    //確認登入無觀看時間
                    fnALLOWTIME = -1;
                    //取得黑名單
                    List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPUSER.txt", System.Text.Encoding.UTF8).ToList();

                    if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
                    {
                        if (lstSTOP_USERs.FirstOrDefault(f => f == clsUSER_DATA.fsUSER) != null)
                        {
                            //無權限可以播
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                }
                //判斷是否為免登入頻道
                if (!string.IsNullOrEmpty(new repCONFIG().fnGET_CFG("CHANNEL_NO_LOGIN")[0].fsVALUE.Split(';').ToList().Find(x => x.Equals(clsGET_CHANNEL_URL_ARGS.fsASSET_ID))))
                {
                    fnALLOWTIME = -1;
                }

                //寫LOG
                new repLOG().fnINSERT_LOG_URL(new clsLOG_URL_ARGS()
                {
                    fsFROM = "1",
                    fsNAME = "CHANNEL",
                    fsASSET_ID = clsGET_CHANNEL_URL_ARGS.fsASSET_ID,
                    fsIP = HttpContext.Current.Request.UserHostAddress,
                    fsCOUNTRY = clsSECURITY.GetIpCountry(HttpContext.Current.Request.UserHostAddress),
                    fsUSER_ID = (clsUSER_DATA == null ? string.Empty : clsUSER_DATA.fsUSER),
                    fsUSER_AGENT = HttpContext.Current.Request.UserAgent ?? ""
                });

                //測試環境才用
                if (int.Parse(fcIS_PRODUCTION) == 0)
                {
                    if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }
                else
                {
                    if (fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("172.22") > -1 || fsIP.IndexOf("172.21") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }

                //取得頻道資訊
                clsCHANNEL clsCHANNEL = new repCHANNEL().fnGET_CHANNEL(clsGET_CHANNEL_URL_ARGS.fsASSET_ID);
                clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                clsGET_CHANNEL_URL.flstURLs = new List<string>();
                clsGET_CHANNEL_URL.fsHEAD_FRAME = clsCHANNEL.fsHEAD_FRAME;
                clsGET_CHANNEL_URL.fsCHANNEL_NAME = clsCHANNEL.fsNAME;
                clsGET_CHANNEL_URL.flstBITRATE = clsCHANNEL.lstALL_BITRATE.Select<string, int>(q => int.Parse(q)).OrderBy(c => c).ToList();
                clsGET_CHANNEL_URL.fnALLOWTIME = fnALLOWTIME;
                //判斷是否為台灣的IP或允許海外播放
                switch (clsSECURITY.GetIpIsAllowArea(fsIP, clsCHANNEL.fcOVERSEAS, clsCHANNEL.lstEXCEPT_COUNTRY))
                {
                    case "IsAllowArea"://允許播放
                        break;
                    case "OVERSEAS"://不允許海外
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    case "EXCEPT_COUNTRY"://海外限制不能播放的國家
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "04" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    default:
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

                if (clsUSER_DATA != null)
                {

                    //取得會員頻道權限
                    var clsMEMBER_CHANNEL_AUTH = new repCHANNEL().fnGET_MEMBER_CHANNEL_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsGET_CHANNEL_URL_ARGS.fsASSET_ID);

                    if (clsMEMBER_CHANNEL_AUTH.fcAUTH)
                    {
                        clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetNewChannelUrl(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsGET_CHANNEL_URL_ARGS.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE, fnALLOWTIME, clsCHANNEL.fsCDN_ROUTE));
                        clsGET_CHANNEL_URL.flstURLs.Add(_clsM3U8.GetUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", clsGET_CHANNEL_URL_ARGS.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE, fnALLOWTIME, clsCHANNEL.fsCDN_ROUTE));
                        clsGET_CHANNEL_URL.fbPLAY_AD = clsMEMBER_CHANNEL_AUTH.fcAD;
                    }
                    else
                    {
                        //無權限可以播
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    //判斷是否為免費
                    if (clsCHANNEL.fcFREE)
                    {
                        clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetNewChannelUrl(false, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsGET_CHANNEL_URL_ARGS.fsASSET_ID, fsIP, clsCHANNEL.fsFREE_PROFILE, fnALLOWTIME, clsCHANNEL.fsCDN_ROUTE));
                        clsGET_CHANNEL_URL.flstURLs.Add(_clsM3U8.GetUrlFromNewMozai(false, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, "live", clsGET_CHANNEL_URL_ARGS.fsASSET_ID, 0, clsCHANNEL.fsFREE_PROFILE, fnALLOWTIME, clsCHANNEL.fsCDN_ROUTE));
                        clsGET_CHANNEL_URL.fbPLAY_AD = true;
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                //加密
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(clsGET_CHANNEL_URL)) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// 取得頻道URL V2
        /// </summary>
        /// <param name="clsGET_CHANNEL_URL_ARGS2">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非台灣播放區域;03:無權限;04:此IP被排除播放區域)</returns>
        [Route("GetChannelUrl3")]
        public StdResp<string> PostChannelUrl3(clsGET_CHANNEL_URL_ARGS2 clsGET_CHANNEL_URL_ARGS2)
        {
            try
            {
                string IP = this.ClientIP;

                StdResp<string> stdResp = new StdResp<string>();

                //擋掉某個偷連盒子
                if (Request.Headers.Contains("x-requested-with"))
                {
                    if (Request.Headers.GetValues("x-requested-with").First().Equals("org.amo.videolive"))
                    {
                        clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                        clsGET_CHANNEL_URL.flstURLs = new List<string>();
                        clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area8/index.m3u8");
                        stdResp.Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(clsGET_CHANNEL_URL));
                        return stdResp;
                    }
                }
                //擋掉黑名單IP
                try
                {
                    List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPIP.txt", System.Text.Encoding.UTF8).ToList();
                    lstSTOP_USERs = lstSTOP_USERs.Where(x => !string.IsNullOrEmpty(x)).ToList();
                    if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
                    {
                        if (lstSTOP_USERs.FirstOrDefault(f => IP.IndexOf(f) > -1) != null)
                        {
                            clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                            clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                            clsGET_CHANNEL_URL.flstURLs = new List<string>();
                            clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area8/index.m3u8");
                            stdResp.Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(clsGET_CHANNEL_URL));
                            return stdResp;
                        }
                    }
                }
                catch (Exception e)
                {

                }


                //解密
                clsGET_CHANNEL_URL_ARGS clsGET_CHANNEL_URL_ARGS = new clsGET_CHANNEL_URL_ARGS();
                //觀看時間
                int fnALLOWTIME = int.Parse(new repCONFIG().fnGET_CFG("WEB_CHANNEL_FREE_SEC")[0].fsVALUE);
                //解密
                string toDes = clsSECURITY.AesDecrypt(clsGET_CHANNEL_URL_ARGS2.value);
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(toDes)))
                {
                    DataContractJsonSerializer deseralizer = new DataContractJsonSerializer(typeof(clsGET_CHANNEL_URL_ARGS));
                    clsGET_CHANNEL_URL_ARGS = (clsGET_CHANNEL_URL_ARGS)deseralizer.ReadObject(ms);// //反序列化ReadObject
                }

                if (clsGET_CHANNEL_URL_ARGS == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (clsGET_CHANNEL_URL_ARGS.fnCHANNEL_ID <= 0)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (clsGET_CHANNEL_URL_ARGS.fsASSET_ID == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                if (clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE == null)
                {
                    throw new MyException(ResponseCodeEnum.RequestNull);
                }
                UserInfo clsUSER_DATA = null;
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsGET_CHANNEL_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_CHANNEL_URL_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                    //確認登入無觀看時間
                    fnALLOWTIME = -1;
                    //取得黑名單
                    try
                    {

                        List<string> lstSTOP_USERs = System.IO.File.ReadAllLines(fsLOG_PATH + "STOPUSER.txt", System.Text.Encoding.UTF8).ToList();
                        lstSTOP_USERs = lstSTOP_USERs.Where(x => !string.IsNullOrEmpty(x)).ToList();
                        if (lstSTOP_USERs != null && lstSTOP_USERs.Count > 0)
                        {
                            if (lstSTOP_USERs.FirstOrDefault(f => f.Equals(clsUSER_DATA.fsUSER)) != null)
                            {
                                //throw new MyException(ResponseCodeEnum.ChannelNoAuthority);
                                clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                                clsGET_CHANNEL_URL.fnALLOWTIME = -1;
                                clsGET_CHANNEL_URL.flstURLs = new List<string>();
                                clsGET_CHANNEL_URL.flstURLs.Add("https://mozai.4gtv.tv/noacl/service_area8/index.m3u8");
                                stdResp.Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(clsGET_CHANNEL_URL));
                                return stdResp;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        System.IO.File.AppendAllText(fsLOG_PATH + DateTime.Now.ToString("yyyyMMdd") + ".txt", "發生時間:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "，錯誤訊息:" + e + "=>" + e.Message + "\r\n");

                    }

                }
                var GetChannelUrl = _Channel_BLL.GetChannelUrl(clsGET_CHANNEL_URL_ARGS.fnCHANNEL_ID, clsGET_CHANNEL_URL_ARGS.fsASSET_ID, clsGET_CHANNEL_URL_ARGS.fsDEVICE_TYPE, clsUSER_DATA);
                stdResp.Data = clsSECURITY.AesEncrypt(JsonConvert.SerializeObject(GetChannelUrl));
                return stdResp;
            }
            catch (MyException e)
            {
                // System.IO.File.AppendAllText(fsLOG_PATH + DateTime.Now.ToString("yyyyMMdd") + ".txt", "發生時間:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "，錯誤訊息:" + e + "=>" + e.Message + "\r\n");

                throw e;
            }
        }

        /// <summary>
        /// 取得頻道套餐
        /// </summary>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <param name="fsSHOW_DEVICE">裝置</param>
        /// <returns></returns>
        [Route("GetChannelSet/{fsSHOW_DEVICE:alpha=PC}/{fcREGION:alpha=L}")]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public HttpResponseMessage GetChannelSet(string fsSHOW_DEVICE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repCHANNEL().fnGET_CHANNEL_SET(fsSHOW_DEVICE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得套餐所有頻道
        /// </summary>
        /// <param name="fnSET_ID">套猜編號</param>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <param name="fsSHOW_DEVICE">裝置</param>
        /// <returns></returns>
        [Route("GetChannelBySetId/{fnSET_ID:int}/{fsSHOW_DEVICE:alpha=PC}/{fcREGION:alpha=L}")]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public HttpResponseMessage GetChannelBySetId(int fnSET_ID, string fsSHOW_DEVICE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repCHANNEL().fnGET_CHANNEL_BY_SETID(fnSET_ID, fsSHOW_DEVICE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得頻道
        /// </summary>
        /// <param name="fnID">編號</param>
        /// <returns></returns>
        [Route("GetChannel/{fnID:int}")]
        [CacheOutput(ServerTimeSpan = 120, AnonymousOnly = true)]
        public HttpResponseMessage GetChannel(int fnID)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repCHANNEL().fnGET_CHANNEL(fnID) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得頻道By AssetId
        /// </summary>
        /// <param name="fsASSET_ID">LiTV AssetId = 4gtv-ID</param>
        /// <returns></returns>
        [Route("GetChannelByAssetId/{fsASSET_ID}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetChannel(string fsASSET_ID)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repCHANNEL().fnGET_CHANNEL(fsASSET_ID) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得所有頻道
        /// </summary>
        ///  <param name="fsSHOW_DEVICE">裝置</param>
        ///  <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetAllChannel/{fsSHOW_DEVICE:alpha=PC}/{fcREGION:alpha=L}")]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public HttpResponseMessage GetAllChannel(string fsSHOW_DEVICE, string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repCHANNEL().fnGET_ALL_CHANNEL(fsSHOW_DEVICE, fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得頻道套餐相關介紹
        /// </summary>
        ///  <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <returns></returns>
        [Route("GetChannelSetIntroduction/{fcREGION:alpha=L}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        public HttpResponseMessage GetChannelSetIntroduction(string fcREGION)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repCHANNEL().fnGET_CHANNEL_SET_INTRODUCTION(fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }


        ///// <summary>
        ///// GetAWSURL
        ///// </summary>
        /////  <param name="fsCDNFILE_ID">位置(L:台灣、G:國外)</param>
        ///// <returns></returns>
        //[Route("AWSURL")]
        //[HttpGet]
        //public StdResp<string> AWSURL(string url)
        //{

        //    StdResp<string> stdResp = new StdResp<string>();

        //    AWS _AWS = new AWS();

        //    string urlString = url;
        //    string durationUnits = "minutes";
        //    string durationNumber = "1";
        //    string pathToPolicyStmnt = @"C:\Users\Angus_Hsieh\Desktop\AWS\AWS_PrivateCF_Distributions\AWS_PrivateCF_Distributions\CannedPolicy.txt";
        //    string pathToPrivateKey = @"C:\Users\Angus_Hsieh\Desktop\AWS\pk-APKAIKLSQH3XHSGOFQSA.xml";
        //    string privateKeyId = "APKAIKLSQH3XHSGOFQSA";
        //    string AWSurl = _AWS.CreateCannedPrivateURL(urlString, durationUnits, durationNumber, pathToPolicyStmnt, pathToPrivateKey, privateKeyId);

        //    try
        //    {
        //        //string aa =  string.IsNullOrEmpty(fsCDNFILE_ID) ? "" : fsCDNFILE_ID;


        //        //stdResp.Data = _clsM3U8.GetUrlFromAWS(true, "pc", "live", aa, 0, -1);

        //        stdResp.Data = AWSurl;
        //        return stdResp;
        //    }
        //    catch (Exception e)
        //    {
        //        stdResp.Data = e.Message + e.StackTrace + e.HelpLink;
        //        return stdResp;
        //    }


        //}






        /// <summary>
        /// mozaiURL
        /// </summary>

        /// <returns></returns>
        [Route("tsURL")]
        [HttpGet]
        public StdResp<string> tsURL()
        {

            StdResp<string> stdResp = new StdResp<string>();

            try
            {
                string cdn = "4gtv-4gtv002";

                string IP = this.ClientIP;

                ////  cdn = clsM3U8.changeCDNIDForOlympics(IP, cdn);

                ////取得頻道資訊
                //clsCHANNEL clsCHANNEL = new repCHANNEL().fnGET_CHANNEL(cdn);
                //clsGET_CHANNEL_TV_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_TV_URL();
                //clsGET_CHANNEL_URL.flstURLs = new List<string>();
                //clsGET_CHANNEL_URL.fsHEAD_FRAME = clsCHANNEL.fsHEAD_FRAME;
                //clsGET_CHANNEL_URL.flstBITRATE = clsCHANNEL.lstALL_BITRATE.Select<string, int>(q => int.Parse(q)).OrderBy(c => c).ToList();
                //clsGET_CHANNEL_URL.fsCHANNEL_NAME = clsCHANNEL.fsNAME;
                //clsGET_CHANNEL_URL.fnFREESTARTTIME = -1;
                //clsGET_CHANNEL_URL.fnALLOWTIME = -1;

                //clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetMediatailorURL(false, "tv", "live", cdn, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME, IP));
                //clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvNewChannelUrl(false, "tv", cdn, IP, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                //clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTvUrlFromNewMozai(false, "tv", "live", cdn, 0, clsCHANNEL.fsFREE_PROFILE, clsCHANNEL.fsCDN_ROUTE, clsGET_CHANNEL_URL.fnALLOWTIME));
                //clsGET_CHANNEL_URL.flstURLs.Add(IP);

                //stdResp.Data = clsGET_CHANNEL_URL;
                //return stdResp;

               // stdResp.Data = clsM3U8.PMozai("litv-ftv07", "aws-mozai", "high");

                string fsIP = "61.216.147.98";

                //string ts = _clsM3U8.GetNewChannelUrl(false, "tv", cdn, fsIP, "high", -1, "A");
                string ts = clsM3U8.PMozai(cdn, "aka-mozai", "high");

                //    //     string ts = procM3u8(cdn, "pc", 3, "A");



                //    var tsURL = new Uri(ts);

                //    var PathAndQuery = tsURL.PathAndQuery;



                List<string> urlsToTest = new List<string>();
                urlsToTest.Add(ts);
                ////    urlsToTest.Add("https://akatest.akamaized.net" + PathAndQuery);


                Uri tokenUrl;

              //  var basicToken = new UnitTest().GetRealyBigToken();

                AkamaiTokenConfig conf = new AkamaiTokenConfig();
                //   conf.TokenAlgorithm = Algorithm.HMACSHA256;
                // conf.IP = "127.0.0.1";
                //   conf.StartTime = 1294788122;
                conf.Window = 30;
                conf.Acl = "/live/pool/" + cdn + "/4gtv-live-high/*";
                conf.Key = "c571ec4124638b8cf1d20ed828962f73";
                //   conf.SessionID = "11729319801";
                //conf.Payload = "THIS_WILL_BE_INCLUDED_IN_TOKEN";
                //  conf.Salt = "My User Agent goes here";
                var basicToken = AkamaiTokenGenerator.GenerateToken(conf);
                //   https://kevintest.akamaized.net/live/pool/4gtv-4gtv002/4gtv-live-high/4gtv-4gtv002-avc1_400000=5-mp4a_138000=2-begin=2061737610666667-dur=40000000-seq=51543441.ts?hdnea=exp=1626330619~acl=/*~hmac=ee770eb3029788104fa61032e0201269e07e43cbe3d1d7b53fc78aff68553b46
                string aaa = string.Empty;
                foreach (var urlToTest in urlsToTest)
                {
                    // tokenUrl = new Uri(urlToTest + "?hdnea=" + basicToken, UriKind.Absolute);
                    tokenUrl = new Uri(urlToTest + "&hdnts=" + basicToken, UriKind.Absolute);
                    // Console.WriteLine("{1}GETting {0}", tokenUrl, Environment.NewLine);
                    //  GetUrl(tokenUrl);
                    stdResp.Data = tokenUrl.AbsoluteUri;
                }

              //  stdResp.Data = IP;
                return stdResp;
            }
            catch (Exception e)
            {

                return stdResp;
            }


        }

        public string procM3u8(string channelName, string deviceType, int streamNo, string route)
        {
            string deviceProfile = deviceType;

            if (deviceType == "embed") deviceProfile = "pc";

            string errString = string.Empty;
            Int32 unixTimestamp = (Int32)(DateTime.Now.AddHours(6).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            MD5 md5 = MD5.Create();//建立一個MD5 

            byte[] source = Encoding.Default.GetBytes(@"/live/pool/" + channelName + "/4gtv-live-high/" + unixTimestamp.ToString() + GetUserIP_API() + "xYC6TBgZ2vV9oKtPk7Ei");//將字串轉為Byte[]
            byte[] crypto = md5.ComputeHash(source);//進行MD5加密

            string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串

            //2019/04/30 目前url字串deviceType目前都是PC
            string url = string.Empty;

            switch (route)
            {
                case "A":
                    url = @"https://4gtvfree" + deviceType + "-cds.cdn.hinet.net/live/pool/" + channelName + "/4gtv-live-high" + "/index.m3u8?token=" + result.Replace('+', '-').Replace('/', '_').Replace("=", "") + @"&expires=" + unixTimestamp.ToString();
                    break;
                case "B":
                    url = @"https://4gtvfree-cds.cdn.hinet.net/live/pool/" + channelName + @"/4gtv-live-high/index.m3u8?token=" + result.Replace('+', '-').Replace('/', '_').Replace("=", "") + @"&expires=" + unixTimestamp.ToString();
                    break;
                default:
                    break;
            }
            source = Encoding.Default.GetBytes(@"/live/pool/" + channelName + "/4gtv-live-high/" + unixTimestamp.ToString() + "xYC6TBgZ2vV9oKtPk7Ei");

            crypto = md5.ComputeHash(source);//進行MD5加密
            result = Convert.ToBase64String(crypto);

            url += "&expires1=" + unixTimestamp.ToString() + "&token1=" + result.Replace('+', '-').Replace('/', '_').Replace("=", "");


            //string url = @"https://4gtvfree-cds.cdn.hinet.net/live/pool/" + channelName + @"/litv-" + deviceProfile + "/index.m3u8?token=" + result.Replace('+', '-').Replace('/', '_').Replace("=", "") + @"&expires=" + unixTimestamp.ToString();
            string stream1Url = string.Empty;

            // string m3u8Path = Properties.Settings.Default.M3u8Path + @"\" + deviceType + @"\" + channelName;
            string errMsg = string.Empty;
            string linebuf = string.Empty;
            //MyWebRequest webObj = new MyWebRequest(url);
            //linebuf = webObj.GetResponse(out errMsg);

            string ss = string.Empty;
            string sss = string.Empty;
            IRestResponse response = CallAPI.GetApi(url, "");


            foreach (string s in Regex.Split(response.Content, "\n"))
            {
                if (s.Length > 0)
                {
                    if (s.IndexOf(".m3u8") > 0)
                    {
                        switch (route)
                        {
                            case "A":
                                ss = @"https://4gtvfree" + deviceType + "-cds.cdn.hinet.net/live/pool/" + channelName + "/4gtv-live-high" + "/" + s;
                                break;
                            case "B":
                                ss = @"https://4gtvfree-cds.cdn.hinet.net/live/pool/" + channelName + @"/4gtv-live-high/" + s;
                                break;
                            default:
                                break;
                        }



                        break;

                    }

                }
            }
            if (!string.IsNullOrEmpty(ss))
            {


                IRestResponse response2 = CallAPI.GetApi(ss, "");


                foreach (string s2 in Regex.Split(response2.Content, "\n"))
                {




                    if (s2.IndexOf(".ts") > 0)
                    {
                        switch (route)
                        {
                            case "A":
                                sss = @"https://4gtvfree" + deviceType + "-cds.cdn.hinet.net/live/pool/" + channelName + "/4gtv-live-high" + "/" + s2;
                                break;
                            case "B":
                                sss = @"https://4gtvfree-cds.cdn.hinet.net/live/pool/" + channelName + @"/4gtv-live-high/" + s2;
                                break;
                            default:
                                break;
                        }



                        break;

                    }

                }


            }

            return sss;
        }


        public static string GetUserIP_API()
        {
            HttpWebRequest request = HttpWebRequest.Create("http://api.ipify.org/?format=string") as HttpWebRequest;
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.UserAgent = "Mozilla/5.0";
            string _IP = String.Empty;
            WebResponse response = request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                _IP = reader.ReadToEnd();
            }
            return _IP;
        }
    }
}
