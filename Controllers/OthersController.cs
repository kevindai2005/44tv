﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;

using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;
//using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Common;
using WebApi.OutputCache.V2;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Others")]
    public class OthersController : ApiController
    {
        private static readonly string fcIS_PRODUCTION = WebConfigurationManager.AppSettings["fcIS_PRODUCTION"];
        /// <summary>
        /// 取得頻道URL(給OVO用的)
        /// </summary>
        /// <param name="clsGET_CHANNEL_URL_OVO_ARGS">取得頻道URL資訊</param>
        /// <returns>error message(01:未登入;02:此IP非允許播放區域;03:無權限)</returns>
        [Route("GetChannelUrlByOvo")]
        public HttpResponseMessage PostChannelUrlByOvo(clsGET_CHANNEL_URL_OVO_ARGS clsGET_CHANNEL_URL_OVO_ARGS)
        {
            try
            {
                string fsIP = HttpContext.Current.Request.UserHostAddress;

                clsCHANNEL clsCHANNEL = new repCHANNEL().fnGET_CHANNEL(clsGET_CHANNEL_URL_OVO_ARGS.fsASSET_ID);
                bool fcFREE = clsCHANNEL.fcFREE;

                //測試環境才用
                if (int.Parse(fcIS_PRODUCTION) == 0)
                {
                    if (fsIP == "localhost" || fsIP == "127.0.0.1" || fsIP == "::1" || fsIP.IndexOf("172.20.9") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }
                else
                {
                    if (fsIP.IndexOf("172.20") > -1 || fsIP.IndexOf("192.168") > -1)
                        fsIP = new repCONFIG().fnGET_CFG("DEFAULT_IP")[0].fsVALUE;
                }

                clsGET_CHANNEL_URL clsGET_CHANNEL_URL = new clsGET_CHANNEL_URL();
                clsGET_CHANNEL_URL.flstURLs = new List<string>();
                clsGET_CHANNEL_URL.fsHEAD_FRAME = clsCHANNEL.fsHEAD_FRAME;
                clsGET_CHANNEL_URL.fsCHANNEL_NAME = clsCHANNEL.fsNAME;

                //判斷是否為台灣的IP
                if (clsSECURITY.GetIpIsInTaiwan(fsIP))
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsGET_CHANNEL_URL_OVO_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                    {
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_CHANNEL_URL_OVO_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                        //取得會員頻道權限
                        var clsMEMBER_CHANNEL_AUTH = new repCHANNEL().fnGET_MEMBER_CHANNEL_AUTH(clsUSER_DATA.fsACCOUNT_ID, clsGET_CHANNEL_URL_OVO_ARGS.fsASSET_ID);

                        if (clsMEMBER_CHANNEL_AUTH.fcAUTH)
                        {
                            //clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTypeCVodUrl(clsCHANNEL.fcFREE, !clsMEMBER_CHANNEL_AUTH.fcAD, clsGET_CHANNEL_URL_OVO_ARGS.fsDEVICE_TYPE, "live", clsGET_CHANNEL_URL_OVO_ARGS.fsASSET_ID, 0, fsIP));
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(clsMEMBER_CHANNEL_AUTH.fcAUTH, clsGET_CHANNEL_URL_OVO_ARGS.fsDEVICE_TYPE, "live", clsGET_CHANNEL_URL_OVO_ARGS.fsASSET_ID, 0));
                            clsGET_CHANNEL_URL.fbPLAY_AD = clsMEMBER_CHANNEL_AUTH.fcAD;
                        }
                        else
                        {
                            //無權限可以播
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "03" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        //判斷是否為免費
                        if (clsCHANNEL.fcFREE)
                        {
                            //clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetTypeCVodUrl(clsCHANNEL.fcFREE, false, clsGET_CHANNEL_URL_OVO_ARGS.fsDEVICE_TYPE, "live", clsGET_CHANNEL_URL_OVO_ARGS.fsASSET_ID, 0, fsIP));
                            clsGET_CHANNEL_URL.flstURLs.Add(clsM3U8.GetUrlFromNewMozai(false, clsGET_CHANNEL_URL_OVO_ARGS.fsDEVICE_TYPE, "live", clsGET_CHANNEL_URL_OVO_ARGS.fsASSET_ID, 0));
                            clsGET_CHANNEL_URL.fbPLAY_AD = true;
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "01" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsGET_CHANNEL_URL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "02" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
    }
}
