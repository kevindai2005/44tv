﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;
//using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;
using System.Net.Http.Headers;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("ClientVar")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ClientVarController : ApiController
    {
        /// <summary>
        /// 取得個人變數
        /// </summary>
        /// <param name="clsGET_CLIENTVAR_ARGS">查詢參數</param>
        /// <returns></returns>
        [Route("GetClientVar")]
        public HttpResponseMessage PostGetClientVar(clsGET_CLIENTVAR_ARGS clsGET_CLIENTVAR_ARGS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsGET_CLIENTVAR_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_CLIENTVAR_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repCLIENTVAR().fnGET_CLIENT_VAR(clsUSER_DATA.fsACCOUNT_ID, (string.IsNullOrEmpty(clsGET_CLIENTVAR_ARGS.fsVOD_NO) ? string.Empty : clsGET_CLIENTVAR_ARGS.fsVOD_NO),string.IsNullOrEmpty(clsGET_CLIENTVAR_ARGS.fcREGION)? "L": clsGET_CLIENTVAR_ARGS.fcREGION) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 設定個人變數
        /// </summary>
        /// <param name="clsSET_CLIENTVAR_ARGS">帳號資訊</param>
        /// <returns></returns>
        [Route("SetClientVar")]
        public HttpResponseMessage PostSetClientVar(clsSET_CLIENTVAR_ARGS clsSET_CLIENTVAR_ARGS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsSET_CLIENTVAR_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsSET_CLIENTVAR_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    var clsSET_CLIENTVAR = new repCLIENTVAR().fnSET_CLIENT_VAR(clsUSER_DATA.fsACCOUNT_ID, clsSET_CLIENTVAR_ARGS.fsVOD_NO, clsSET_CLIENTVAR_ARGS.fnSEQ, clsSET_CLIENTVAR_ARGS.fsPART, clsSET_CLIENTVAR_ARGS.fnTIME);

                    if (clsSET_CLIENTVAR != null)
                    {
                        if (clsSET_CLIENTVAR.fbRESULT)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "新增失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "新增失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
    }
}
