﻿using FourgTV_OTT2.API.BLL;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Req.News;
using FourgTV_OTT2.BLL.Model.Res.News;
using FourgTV_OTT2.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("News")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class NewsController : ApiController
    {
        News_BLL _News_BLL = new News_BLL();
        /// <summary>
        /// 首頁清單
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsMenu")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetNewsMenu_Res>> GetNewsMenu()
        {
            StdResp<List<GetNewsMenu_Res>> stdResp = new StdResp<List<GetNewsMenu_Res>>();
            List<GetNewsMenu_Res> GetNewsMenu_Res = new List<GetNewsMenu_Res>();
            GetNewsMenu_Res = _News_BLL.GetNewsMenu();
            stdResp.Data = GetNewsMenu_Res;
            return stdResp;
        }

        /// <summary>
        /// 取得首頁上方
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsUp")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 180, AnonymousOnly = true)]
#endif
        public StdResp<List<GetNewsUp_Res>> GetNewsUp()
        {
            StdResp<List<GetNewsUp_Res>> stdResp = new StdResp<List<GetNewsUp_Res>>();
            List<GetNewsUp_Res> GetNewsUp_Res = new List<GetNewsUp_Res>();
            GetNewsUp_Res = _News_BLL.GetNewsUp();
            stdResp.Data = GetNewsUp_Res;
            return stdResp;
        }
        /// <summary>
        ///  取得首頁下方
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsDown")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetNewsDown_Res>> GetNewsDown()
        {
            StdResp<List<GetNewsDown_Res>> stdResp = new StdResp<List<GetNewsDown_Res>>();
            List<GetNewsDown_Res> GetNewsDown_Res = new List<GetNewsDown_Res>();
            GetNewsDown_Res = _News_BLL.GetNewsDown();
            stdResp.Data = GetNewsDown_Res;
            return stdResp;
        }

        /// <summary>
        /// 取得首頁左方
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsLeft")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetNewsLeft_Res>> GetNewsLeft()
        {
            StdResp<List<GetNewsLeft_Res>> stdResp = new StdResp<List<GetNewsLeft_Res>>();
            List<GetNewsLeft_Res> GetNewsLeft_Res = new List<GetNewsLeft_Res>();
            GetNewsLeft_Res = _News_BLL.GetNewsLeft();
            stdResp.Data = GetNewsLeft_Res;
            return stdResp;
        }
        /// <summary>
        /// 取得首頁左方
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsRight")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetNewsRight_Res>> GetNewsRight()
        {
            StdResp<List<GetNewsRight_Res>> stdResp = new StdResp<List<GetNewsRight_Res>>();
            List<GetNewsRight_Res> GetNewsRight_Res = new List<GetNewsRight_Res>();
            GetNewsRight_Res = _News_BLL.GetNewsRight();
            stdResp.Data = GetNewsRight_Res;
            return stdResp;
        }

        /// <summary>
        /// 取得APP首頁
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsAPP")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<GetNewsAPP_Res> GetNewsAPP()
        {
            StdResp<GetNewsAPP_Res> stdResp = new StdResp<GetNewsAPP_Res>();
            stdResp.Data = _News_BLL.GetNewsAPP();
            return stdResp;
        }
        /// <summary>
        /// 取得APP首頁
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsAPP2")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<GetNewsAPP_Res> GetNewsAPP2()
        {
            StdResp<GetNewsAPP_Res> stdResp = new StdResp<GetNewsAPP_Res>();
            stdResp.Data = _News_BLL.GetNewsAPP2();
            return stdResp;
        }
        /// <summary>
        /// 取得文章列表
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsInfoList/{Type:int}/{ID:int}/{Index:int}/{PageSize:int}")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<GetNewsInfoList_Res> GetNewsInfoList(int? Type ,int? ID, int? Index, int? PageSize)
        {
            if (Type == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (ID == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (Index == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (PageSize == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            GetNewsInfoList_Req req = new GetNewsInfoList_Req()
            {
                Type = Type,
                ID = ID,
                Index = Index,
                PageSize = PageSize
            };
            StdResp<GetNewsInfoList_Res> stdResp = new StdResp<GetNewsInfoList_Res>();
            stdResp.Data = _News_BLL.GetNewsInfoList(req);
            return stdResp;
        }

        /// <summary>
        /// 取得APP首頁
        /// </summary>
        /// <returns></returns>
        [Route("GetNewsCategoryChild/{Id:int}")]
        [HttpGet]
#if PRD
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
#endif
        public StdResp<List<GetNewsCategoryChild_Res>> GetNewsCategoryChild(int Id)
        {
            StdResp<List<GetNewsCategoryChild_Res>> stdResp = new StdResp<List<GetNewsCategoryChild_Res>>();
            stdResp.Data = _News_BLL.GetNewsCategoryChild(Id);
            return stdResp;
        }
    }
}
