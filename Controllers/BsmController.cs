﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;
//using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;
using WebApi.OutputCache.V2;
using Swashbuckle.Swagger.Annotations;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Bsm")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class BsmController : ApiController
    {
        /// <summary>
        /// 取得可使用服務
        /// </summary>
        /// <returns></returns>
        [Route("GetServiceInfo")]
        public HttpResponseMessage PostServiceeInfo(clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS)
        {
            try
            {
                // 判斷是否已登入
                if (clsSECURITY.IsLogin(clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBSM().fnGET_MEMBER_SERVICE_INFO(clsUSER_DATA.fsACCOUNT_ID) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得購買紀錄清單
        /// </summary>
        /// <returns></returns>
        [Route("GetPurchaseInfo")]
        public HttpResponseMessage PostPurchaseInfo(clsGET_MEMBER_PURCHASE_INFO_ARGS clsGET_MEMBER_PURCHASE_INFO_ARGS)
        {
            try
            {
                // 判斷是否已登入
                if (clsSECURITY.IsLogin(clsGET_MEMBER_PURCHASE_INFO_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsGET_MEMBER_PURCHASE_INFO_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBSM().fnGET_MEMBER_PURCHASE_INFO(clsUSER_DATA.fsACCOUNT_ID, clsGET_MEMBER_PURCHASE_INFO_ARGS.fsOTPW) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 購買首頁資料
        /// </summary>
        /// <param name="fcREGION">位置(L:台灣、G:國外)</param>
        /// <param name="fsSHOW_DEVICE">顯示裝置</param>
        /// <returns></returns>
        [Route("GetGroupInfo/{fcREGION:alpha=L}/{fsSHOW_DEVICE:alpha=PC}")]
        [CacheOutput(ServerTimeSpan = 600, AnonymousOnly = true)]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage GetGroupInfo(string fcREGION, string fsSHOW_DEVICE)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBSM().fnGET_SET_LIST(fcREGION, fsSHOW_DEVICE) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 列出套餐詳細資訊
        /// </summary>
        /// <param name="fsGROUP_ID">套餐ID</param>
        /// <returns></returns>
        [Route("GetPackageInfo/{fsGROUP_ID}")]
        public HttpResponseMessage GetPackageInfo(string fsGROUP_ID)
        {
            try
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = new repBSM().fnGET_PACKAGE_CHT(fsGROUP_ID, string.Empty) }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得購買要用的SessionId
        /// </summary>
        /// <returns></returns>
        [Route("GetPurchaseSessionId")]
        public HttpResponseMessage PostPurchaseSessionId(clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    string fsSESSION_ID = Guid.NewGuid().ToString().Substring(0, 28);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsSESSION_ID }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 取得中華支付要用的check sum
        /// </summary>
        /// <param name="clsCHT_CHECHSUM"></param>
        /// <returns></returns>
        [Route("GetChtCheckSum")]
        public HttpResponseMessage PostChtCheckSum(clsCHT_CHECHSUM clsCHT_CHECHSUM)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsCHT_CHECHSUM.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    string fsCHECKSUM = new repBSM().fnGET_CHT_CHECHSUM(clsCHT_CHECHSUM.fsPRODUCT_ID, clsCHT_CHECHSUM.fsCURL, clsCHT_CHECHSUM.fsEURL, clsCHT_CHECHSUM.fnFEE, clsCHT_CHECHSUM.fsOTHERS);
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = fsCHECKSUM }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 查詢會員購買條件是否符合
        /// </summary>
        /// <returns></returns>
        [Route("GetMemberPurchaseCondition")]
        public HttpResponseMessage PostGetMemberPurchaseCondition(clsMEMBER_PURCHASE_CONDITION_ARGS clsMEMBER_PURCHASE_CONDITION_ARGS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsMEMBER_PURCHASE_CONDITION_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    //取得使用者
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsMEMBER_PURCHASE_CONDITION_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    //判斷帳號是否已驗證通過
                    if (new repACCOUNT().fnGET_ACCOUNT_STATUS(clsUSER_DATA.fsUSER).Validated)
                    {
                        //取得會員是否尚有在有效期的訂單
                        string fsRESULT = new repBSM().fnGET_MEMBER_ORDER_INDATE(clsUSER_DATA.fsACCOUNT_ID, clsMEMBER_PURCHASE_CONDITION_ARGS.fsPACKAGE_ID);

                        if (string.IsNullOrEmpty(fsRESULT))
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "帳號尚未驗證通過!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
        /// <summary>
        /// 購買綠界 0708-kevin
        /// </summary>
        /// <returns></returns>
        [Route("PurchaseE")]
        public HttpResponseMessage PostPurchaseE(clsPURCHASE_ARGS clsPURCHASE_ARGS)
        {
            try
            {
                

                //if (int.Parse(clsPURCHASE_ARGS.OrderInfo.TradeAmt) != clsSET_PACKAGE_CHT.price)
                //{
                //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "價格與系統不符!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                //}
                //else
                //{

                string fsORDER_NO = string.Empty;
                string fsRESULT = string.Empty;
                    //取得使用者
                    //var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsPURCHASE_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA("MElto7IiM5eUbaUNrO/2U+kB4/Epk5F+6wHTZQU202evaCZzVoaIOrxak3fuO8hYswO7BMx1TVNI25AnqS2/7Yg0hC3+XgM5ufxLJpRP6LZ9+CkLs2pBWcFnA+jvlCW907CiychcoFLz7e2Ha9sEopaQFmMxLc29dIIOmOI0B7q9YvWj7ipfLbBYrgHmsP1qW9hxXhq2CzO2PJmjFgJeqq52RId+p6FXixZR6LEq8l2ySr5trxsk9xv+rpw/xWYlf+2vs7A8UwLOttMEOD7WPtLMuN2uoEq5BO53+9C1FmW8kPPJKC/FJMwoHwKAd3JJfZ6DPZKaP0Ndujy/2ra/qA==");
                    int fnRETRY_COUNT = 1;

                    if(clsPURCHASE_ARGS.OrderInfo != null)
                    {
                        
                        fsORDER_NO = clsPURCHASE_ARGS.OrderInfo.MerchantTradeNo;
                        clsPURCHASE_ARGS.fsPURCHASE_ID = clsPURCHASE_ARGS.OrderInfo.TradeNo;
                    }
                    else
                    {
                       //取得套餐價格
                       clsSET_PACKAGE_CHT clsSET_PACKAGE_CHT = new repBSM().fnGET_PACKAGE_CHT(clsPURCHASE_ARGS.fsPURCHASE_ID, clsPURCHASE_ARGS.fsPACKAGE_ID)[0];
                       if (int.Parse(clsPURCHASE_ARGS.OrderInfo.TradeAmt) != clsSET_PACKAGE_CHT.price)
                       {
                           return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "價格與系統不符!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                       }

                    //產生訂單編號
                    
                    if (string.IsNullOrEmpty(fsORDER_NO))
                    {
                        new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                        {
                            fsTYPE = "PURCHASE",
                            fsNAME = "購買",
                            fsCONTENT = "無法取得訂單編號",
                            fsCREATED_BY = "Purchase"
                        });

                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "無法取得訂單編號!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

                    while (fnRETRY_COUNT <= 3 && string.IsNullOrEmpty(fsORDER_NO))
                    {
                        fsORDER_NO = new repL_NO().fnGET_L_NO("ORDERNO", "訂單編號", DateTime.Now.ToString("yyyyMMdd"), "", 5, "Purchase");
                        fnRETRY_COUNT += 1;
                    }


                    if (clsPURCHASE_ARGS.OrderInfo == null)
                    {
                        //新增中華訂單
                        fsRESULT = new repBSM().fnINSERT_ORDER(clsUSER_DATA.fsACCOUNT_ID, fsORDER_NO, clsPURCHASE_ARGS.fsSESSION_ID, clsPURCHASE_ARGS.fsPURCHASE_ID, clsPURCHASE_ARGS.fsPACKAGE_ID,
                        clsPURCHASE_ARGS.fsPAY_FROM, clsPURCHASE_ARGS.fnPAY_TYPE_ID, clsPURCHASE_ARGS.fsCHT_PRODUCT_ID, clsPURCHASE_ARGS.fsPROMO_CODE, clsPURCHASE_ARGS.fsCHT_RESULT, clsPURCHASE_ARGS.fsCHT_UID, clsPURCHASE_ARGS.fsCHT_OTPW,
                        clsPURCHASE_ARGS.fsCHT_FEE, clsPURCHASE_ARGS.fsCHT_AUTHORITY, clsPURCHASE_ARGS.fsCHT_OTHERS, clsPURCHASE_ARGS.fsCHT_CPORDERNO, clsPURCHASE_ARGS.fsEXPIRATIONDATE,
                        clsPURCHASE_ARGS.fsBANKCODE, clsPURCHASE_ARGS.fsORDERID, false, clsPURCHASE_ARGS.fsINVOICE_GIFT, string.Empty, string.Empty, string.Empty, string.Empty,
                        string.Empty, string.Empty, "1", clsUSER_DATA.fsACCOUNT_ID, clsPURCHASE_ARGS.fsINV_STATUS);
                    }
                    else
                    {
                        //新增綠界訂單
                        fsRESULT = new repBSM().fnINSERT_ORDER(clsUSER_DATA.fsACCOUNT_ID, fsORDER_NO, clsPURCHASE_ARGS.fsvalue, clsPURCHASE_ARGS.fsPURCHASE_ID, clsPURCHASE_ARGS.OrderInfo.ItemName,
                        "ECPAY", clsPURCHASE_ARGS.fnPAY_TYPE_ID, clsPURCHASE_ARGS.OrderInfo.ItemName, clsPURCHASE_ARGS.fsPROMO_CODE, clsPURCHASE_ARGS.fsCHT_RESULT, clsPURCHASE_ARGS.fsCHT_UID, clsPURCHASE_ARGS.fsCHT_OTPW,
                        clsPURCHASE_ARGS.OrderInfo.TradeAmt, clsPURCHASE_ARGS.fsCHT_AUTHORITY, clsPURCHASE_ARGS.fsCHT_OTHERS, clsPURCHASE_ARGS.OrderInfo.TradeNo, clsPURCHASE_ARGS.fsEXPIRATIONDATE,
                        clsPURCHASE_ARGS.fsBANKCODE, clsPURCHASE_ARGS.OrderInfo.MerchantTradeNo, false, clsPURCHASE_ARGS.fsINVOICE_GIFT, string.Empty, string.Empty, string.Empty, string.Empty,
                        string.Empty, string.Empty, "1", clsUSER_DATA.fsACCOUNT_ID, clsPURCHASE_ARGS.fsINV_STATUS);
                    }
                    

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                //}
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }

        }

        /// <summary>
        /// 購買
        /// </summary>
        /// <returns></returns>
        [Route("Purchase")]
        public HttpResponseMessage PostPurchase(clsPURCHASE_ARGS clsPURCHASE_ARGS)
        {
            try
            {
                //取得套餐價格
                clsSET_PACKAGE_CHT clsSET_PACKAGE_CHT = new repBSM().fnGET_PACKAGE_CHT(clsPURCHASE_ARGS.fsPURCHASE_ID, clsPURCHASE_ARGS.fsPACKAGE_ID)[0];

                if (int.Parse(clsPURCHASE_ARGS.fsCHT_FEE) != clsSET_PACKAGE_CHT.price)
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "價格與系統不符!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    //判斷是否已登入
                    if (clsSECURITY.IsLogin(clsPURCHASE_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                    {
                        //取得使用者
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsPURCHASE_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                        //判斷是否有授權碼
                        if (!string.IsNullOrEmpty(clsPURCHASE_ARGS.fsCHT_OTPW))
                        {
                            string fsRESULT = string.Empty;
                            int fnRETRY_COUNT = 1;

                            //產生訂單編號
                            string fsORDER_NO = string.Empty;

                            while (fnRETRY_COUNT <= 3 && string.IsNullOrEmpty(fsORDER_NO))
                            {
                                fsORDER_NO = new repL_NO().fnGET_L_NO("ORDERNO", "訂單編號", DateTime.Now.ToString("yyyyMMdd"), "", 5, "Purchase");
                                fnRETRY_COUNT += 1;
                            }

                            if (string.IsNullOrEmpty(fsORDER_NO))
                            {
                                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                                {
                                    fsTYPE = "PURCHASE",
                                    fsNAME = "購買",
                                    fsCONTENT = "無法取得訂單編號",
                                    fsCREATED_BY = "Purchase"
                                });

                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "無法取得訂單編號!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }


                            //新增訂單
                            fsRESULT = new repBSM().fnINSERT_ORDER(clsUSER_DATA.fsACCOUNT_ID, fsORDER_NO, clsPURCHASE_ARGS.fsSESSION_ID, clsPURCHASE_ARGS.fsPURCHASE_ID, clsPURCHASE_ARGS.fsPACKAGE_ID,
                                clsPURCHASE_ARGS.fsPAY_FROM, clsPURCHASE_ARGS.fnPAY_TYPE_ID, clsPURCHASE_ARGS.fsCHT_PRODUCT_ID, clsPURCHASE_ARGS.fsPROMO_CODE, clsPURCHASE_ARGS.fsCHT_RESULT, clsPURCHASE_ARGS.fsCHT_UID, clsPURCHASE_ARGS.fsCHT_OTPW,
                                clsPURCHASE_ARGS.fsCHT_FEE, clsPURCHASE_ARGS.fsCHT_AUTHORITY, clsPURCHASE_ARGS.fsCHT_OTHERS, clsPURCHASE_ARGS.fsCHT_CPORDERNO, clsPURCHASE_ARGS.fsEXPIRATIONDATE,
                                clsPURCHASE_ARGS.fsBANKCODE, clsPURCHASE_ARGS.fsORDERID, false, clsPURCHASE_ARGS.fsINVOICE_GIFT, string.Empty, string.Empty, string.Empty, string.Empty,
                                string.Empty, string.Empty, "1", clsUSER_DATA.fsACCOUNT_ID, clsPURCHASE_ARGS.invoicenumber);

                            if (fsRESULT.IndexOf("ERROR") == -1)
                            {
                                //回傳訂單結果
                                #region 信用卡、ATM
                                if (fsRESULT == "2A")
                                {
                                    //進行2A授權介面
                                    clsCHT_2A clsCHT_2A = new repBSM().fnGET_CHT_2A(new clsCHT_2A_ARGS() { fsCHT_OTPW = clsPURCHASE_ARGS.fsCHT_OTPW, fsCHT_FEE = clsPURCHASE_ARGS.fsCHT_FEE, fsCHT_AUTHORITY = clsPURCHASE_ARGS.fsCHT_AUTHORITY, fsCHT_PRODUCTID = clsPURCHASE_ARGS.fsCHT_PRODUCT_ID, fsCHT_APID = clsPURCHASE_ARGS.fsCHT_AUTHORITY });

                                    if (clsCHT_2A.fsVALIDATE_A2 == "Success")
                                    {
                                        //更新2A授權
                                        fsRESULT = new repBSM().fnUPDATE_CHT_2A(fsORDER_NO, clsCHT_2A.fsVALIDATE_A2, clsCHT_2A.fsMESSAGE_A2, clsCHT_2A.fsPAY_BY_A2, clsCHT_2A.fsSTATUS_A2, clsCHT_2A.fsORDER_NO_A2, clsCHT_2A.fsAPPROVE_NO_A2, clsCHT_2A.fsRESULT_A2);
                                        if (fsRESULT.IndexOf("ERROR") == -1)
                                        {
                                            if (fsRESULT == "3A")
                                            {
                                                //進行3A計費介面
                                                clsCHT_3A clsCHT_3A = new repBSM().fnGET_CHT_3A(new clsCHT_3A_ARGS()
                                                {
                                                    fsCHT_OTPW = clsPURCHASE_ARGS.fsCHT_OTPW,
                                                    fsCHT_FEE = clsPURCHASE_ARGS.fsCHT_FEE,
                                                    fsCHT_APID = clsPURCHASE_ARGS.fsCHT_AUTHORITY,
                                                    fsCHT_STIME = DateTime.UtcNow.AddHours(8).ToString("yyyyMMddHHmmss"),
                                                    fsCHT_SREMARK = clsPURCHASE_ARGS.fsCHT_PRODUCT_ID + "^" + DateTime.UtcNow.AddHours(8).ToString("yyyyMMddHHmmss") + "^" + clsUSER_DATA.fsACCOUNT_ID + "^" + clsPURCHASE_ARGS.fsCHT_FEE + "^" + HttpContext.Current.Request.UserHostAddress,
                                                    fsCHT_AUTHORITY = clsPURCHASE_ARGS.fsCHT_AUTHORITY,
                                                    fsCHT_PRODUCTID = clsPURCHASE_ARGS.fsCHT_PRODUCT_ID
                                                });

                                                if (clsCHT_3A.fsVALIDATE_A3 == "Success")
                                                {
                                                    //更新3A授權
                                                    fsRESULT = new repBSM().fnUPDATE_CHT_3A(fsORDER_NO, clsCHT_3A.fsVALIDATE_A3, clsCHT_3A.fsMESSAGE_A3, clsCHT_3A.fsRESULT_A3);
                                                    if (fsRESULT.IndexOf("ERROR") == -1)
                                                    {

                                                        //進行攤分
                                                        clsCHT_MILTIBILL clsCHT_MILTIBILL = new repBSM().fnGET_CHT_MULTIBILL(new clsCHT_MILTIBILL_ARGS()
                                                        {
                                                            fsCHT_OTPW = clsPURCHASE_ARGS.fsCHT_OTPW,
                                                            fsCHT_AUTHORITY = clsPURCHASE_ARGS.fsCHT_AUTHORITY,
                                                            fsCHT_MULTIPLE_BILL_PARAM = "1^" + DateTime.UtcNow.AddHours(8).ToString("yyyyMMdd") + DateTime.UtcNow.AddHours(8).ToString("yyyyMMdd") + clsPURCHASE_ARGS.fsCHT_FEE.PadLeft(6, '0')
                                                        });

                                                        if (clsCHT_MILTIBILL.fsVALIDATE_BILL == "Success")
                                                        {
                                                            //更新攤分
                                                            fsRESULT = new repBSM().fnUPDATE_CHT_MULTIBILL(clsPURCHASE_ARGS.fsCHT_OTPW, clsCHT_MILTIBILL.fsMULTIPLE_BILL, string.Empty);
                                                            if (fsRESULT.IndexOf("ERROR") == -1)
                                                            {
                                                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                                            }
                                                            else
                                                            {
                                                                //是否回上一動
                                                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "訂購失敗，請聯絡客服人員!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //是否回上一動
                                                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "訂購失敗，請聯絡客服人員!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //是否回上一動
                                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "訂購失敗，請聯絡客服人員!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                                    }
                                                }
                                                else
                                                {
                                                    //刪除失敗訂單
                                                    new repBSM().fnDELETE_FAIL_ORDER(fsORDER_NO, clsUSER_DATA.fsACCOUNT_ID);
                                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "訂購失敗，請重新訂購!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                                }
                                            }
                                            else
                                            {
                                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                            }
                                        }
                                        else
                                        {
                                            //刪除失敗訂單
                                            new repBSM().fnDELETE_FAIL_ORDER(fsORDER_NO, clsUSER_DATA.fsACCOUNT_ID);
                                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                        }
                                    }
                                    else
                                    {
                                        //刪除失敗訂單
                                        new repBSM().fnDELETE_FAIL_ORDER(fsORDER_NO, clsUSER_DATA.fsACCOUNT_ID);
                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    }
                                }
                                #endregion

                                #region 月付
                                else if (fsRESULT == "SUBSCRIBE")
                                {
                                    //進行訂閱

                                    //取得動作開始日期
                                    string fsACTIONDATE = string.Empty;//空 為馬上開始動作
                                    //取得check sum
                                    string fsCHECK_SUM = clsSECURITY.GetChtSubscribeCheckSum(clsPURCHASE_ARGS.fsCHT_AUTHORITY, clsUSER_DATA.fsACCOUNT_ID, clsPURCHASE_ARGS.fsCHT_PRODUCT_ID, clsPURCHASE_ARGS.fsCHT_OTPW,
                                            fsACTIONDATE, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);

                                    clsCHT_SUBSCRIBE_ARGS clsCHT_SUBSCRIBE_ARGS = new clsCHT_SUBSCRIBE_ARGS();
                                    clsCHT_SUBSCRIBE_ARGS.fsALIAS = clsUSER_DATA.fsACCOUNT_ID;
                                    clsCHT_SUBSCRIBE_ARGS.fsPRODUCT_ID = clsPURCHASE_ARGS.fsCHT_PRODUCT_ID;
                                    clsCHT_SUBSCRIBE_ARGS.fsMIXCODE = clsPURCHASE_ARGS.fsCHT_OTPW;
                                    clsCHT_SUBSCRIBE_ARGS.fsACTIONDATE = fsACTIONDATE;
                                    clsCHT_SUBSCRIBE_ARGS.fsTOTALMONTH = string.Empty;
                                    clsCHT_SUBSCRIBE_ARGS.fsSETTINGCHARGE = string.Empty;
                                    clsCHT_SUBSCRIBE_ARGS.fsINSTALLCHARGE = string.Empty;
                                    clsCHT_SUBSCRIBE_ARGS.fsFIRSTCHARGE = string.Empty;
                                    clsCHT_SUBSCRIBE_ARGS.fsAMOUNT = string.Empty;
                                    clsCHT_SUBSCRIBE_ARGS.fsRENT = string.Empty;
                                    clsCHT_SUBSCRIBE_ARGS.fsCHECKSUM = fsCHECK_SUM;
                                    clsCHT_SUBSCRIBE_ARGS.fsAPID = clsPURCHASE_ARGS.fsCHT_AUTHORITY;
                                    clsCHT_SUBSCRIBE_ARGS.fsDIFFMSG = string.Empty;
                                    clsCHT_SUBSCRIBE_ARGS.fsRESERVED = string.Empty;
                                    clsCHT_SUBSCRIBE_ARGS.fsCHT_AUTHORITY = clsPURCHASE_ARGS.fsCHT_AUTHORITY;

                                    //進行訂閱
                                    clsCHT_SUBSCRIBE clsCHT_SUBSCRIBE = new repBSM().fnGET_CHT_SUBSCRIBE(clsCHT_SUBSCRIBE_ARGS);

                                    if (clsCHT_SUBSCRIBE.fsVALIDATE_SUBSCRIBE == "Success")
                                    {
                                        //更新訂閱
                                        fsRESULT = new repBSM().fnUPDATE_CHT_SUBSCRIBE(fsORDER_NO, clsCHT_SUBSCRIBE.fsVALIDATE_SUBSCRIBE, clsCHT_SUBSCRIBE.fsSUBSCRIBENO_SUBSCRIBE, clsCHT_SUBSCRIBE.fsRESULT_SUBSCRIBE);
                                        if (fsRESULT.IndexOf("ERROR") == -1)
                                        {
                                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                        }
                                        else
                                        {
                                            //是否回上一動
                                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                        }
                                    }
                                    else
                                    {
                                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsCHT_SUBSCRIBE.fsMESSAGE_SUBSCRIBE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                    }
                                }
                                #endregion
                                else
                                {
                                    //刪除失敗訂單
                                    new repBSM().fnDELETE_FAIL_ORDER(fsORDER_NO, clsUSER_DATA.fsACCOUNT_ID);

                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "無此付款方式!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "此交易無授權碼!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        [Route("InsertOrderDevice")]
        public HttpResponseMessage PostInsertOrderDevice(clsORDER_DEVICE_ARGS clsORDER_DEVICE_ARGS)
        {
            try
            {
                // 判斷是否已登入
                if (clsSECURITY.IsLogin(clsORDER_DEVICE_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsORDER_DEVICE_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    var fsRESULT = new repBSM().fnINSERT_ORDER_DEVICE(clsUSER_DATA.fsACCOUNT_ID, clsORDER_DEVICE_ARGS.fsOrderNo, clsORDER_DEVICE_ARGS.fsFrom,
                        clsORDER_DEVICE_ARGS.fsDeviceVendor, clsORDER_DEVICE_ARGS.fsDevice, FourgTV_OTT2.Common.clsTOOL.GetUserIP(), 
                        clsORDER_DEVICE_ARGS.fsOS, clsORDER_DEVICE_ARGS.fsOSVersion, clsORDER_DEVICE_ARGS.fsBrowser, 
                        clsORDER_DEVICE_ARGS.fsBrowserVersion);

                    if (!String.IsNullOrEmpty(fsRESULT))
                        throw new Exception(fsRESULT);

                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// ATM已繳費通知(給中華用)
        /// </summary>
        /// <returns></returns>
        [Route("ChtAtmPaid")]
        public IHttpActionResult PostChtAtmPaid()
        {
            try
            {
                clsCHT_ATM_ISPAID_ARGS clsCHT_ATM_ISPAID_ARGS = new clsCHT_ATM_ISPAID_ARGS();
                clsCHT_ATM_ISPAID_ARGS.fsORDER_NO = (string.IsNullOrEmpty(HttpContext.Current.Request.Params["order-no"]) ? string.Empty : HttpContext.Current.Request.Params["order-no"]); ;
                clsCHT_ATM_ISPAID_ARGS.fsOTPW = HttpContext.Current.Request.Params["aa-otpw"];
                clsCHT_ATM_ISPAID_ARGS.fsATM_TXNDATETIME = HttpContext.Current.Request.Params["txndatetime"];
                clsCHT_ATM_ISPAID_ARGS.fsATM_RESPONSE_CODE = "00";
                clsCHT_ATM_ISPAID_ARGS.fsATM_RESULT = string.Empty;

                //更新ATM已繳費
                string fsRESULT = new repBSM().fnUPDATE_CHT_ATM_ISPAID(clsCHT_ATM_ISPAID_ARGS);

                if (fsRESULT.IndexOf("ERROR") == -1)
                {
                    //取得支付資訊
                    clsCHT_PAY_INFO clsCHT_PAY_INFO = new repBSM().fnGET_CHT_PAY_INFO(string.Empty, clsCHT_ATM_ISPAID_ARGS.fsOTPW);

                    //進行3A計費介面
                    clsCHT_3A clsCHT_3A = new repBSM().fnGET_CHT_3A(new clsCHT_3A_ARGS()
                    {
                        fsCHT_OTPW = clsCHT_ATM_ISPAID_ARGS.fsOTPW,
                        fsCHT_FEE = clsCHT_PAY_INFO.fsFEE,
                        fsCHT_APID = clsCHT_PAY_INFO.fsAUTHORITY,
                        fsCHT_STIME = DateTime.UtcNow.AddHours(8).ToString("yyyyMMddHHmmss"),
                        fsCHT_SREMARK = clsCHT_PAY_INFO.fsCHT_PRODUCT_ID + "^" + DateTime.UtcNow.AddHours(8).ToString("yyyyMMddHHmmss") + "^" + clsCHT_PAY_INFO.fsACCOUNT_ID + "^" + clsCHT_PAY_INFO.fsFEE + "^FROM_ATM_IS_PAID",
                        fsCHT_AUTHORITY = clsCHT_PAY_INFO.fsAUTHORITY,
                        fsCHT_PRODUCTID = clsCHT_PAY_INFO.fsCHT_PRODUCT_ID
                    });

                    if (clsCHT_3A.fsVALIDATE_A3 == "Success")
                    {
                        //更新3A授權
                        fsRESULT = new repBSM().fnUPDATE_CHT_3A(clsCHT_PAY_INFO.fsORDER_NO, clsCHT_3A.fsVALIDATE_A3, clsCHT_3A.fsMESSAGE_A3, clsCHT_3A.fsRESULT_A3);
                        if (fsRESULT.IndexOf("ERROR") == -1)
                        {
                            //進行攤分
                            clsCHT_MILTIBILL clsCHT_MILTIBILL = new repBSM().fnGET_CHT_MULTIBILL(new clsCHT_MILTIBILL_ARGS()
                            {
                                fsCHT_OTPW = clsCHT_ATM_ISPAID_ARGS.fsOTPW,
                                fsCHT_AUTHORITY = "Atm",
                                fsCHT_MULTIPLE_BILL_PARAM = "1^" + DateTime.UtcNow.AddHours(8).ToString("yyyyMMdd") + DateTime.UtcNow.AddHours(8).ToString("yyyyMMdd") + clsCHT_PAY_INFO.fsFEE.PadLeft(6, '0')
                            });

                            if (clsCHT_MILTIBILL.fsVALIDATE_BILL == "Success")
                            {
                                //更新攤分
                                fsRESULT = new repBSM().fnUPDATE_CHT_MULTIBILL(clsCHT_ATM_ISPAID_ARGS.fsOTPW, clsCHT_MILTIBILL.fsMULTIPLE_BILL, string.Empty);
                                if (fsRESULT.IndexOf("ERROR") == -1)
                                {
                                    return Ok("Success");
                                }
                                else
                                {
                                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                                    {
                                        fsTYPE = "ERROR",
                                        fsNAME = "MULTIBILL",
                                        fsCONTENT = fsRESULT.Split(':')[1] + "【fsOTPW:" + clsCHT_ATM_ISPAID_ARGS.fsOTPW + "】",
                                        fsCREATED_BY = "ChtAtmPaid"
                                    });
                                    return Ok();
                                }
                            }
                            else
                            {
                                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                                {
                                    fsTYPE = "ERROR",
                                    fsNAME = "MULTIBILL",
                                    fsCONTENT = "攤分失敗【fsOTPW:" + clsCHT_ATM_ISPAID_ARGS.fsOTPW + "】",
                                    fsCREATED_BY = "ChtAtmPaid"
                                });
                                return Ok();
                            }
                        }
                        else
                        {
                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "ERROR",
                                fsNAME = "3A",
                                fsCONTENT = "更新3A授權失敗-" + fsRESULT.Split(':')[1] + "【fsOTPW:" + clsCHT_ATM_ISPAID_ARGS.fsOTPW + "】",
                                fsCREATED_BY = "ChtAtmPaid"
                            });
                            return Ok();
                        }
                        
                    }
                    else
                    {
                        new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                        {
                            fsTYPE = "ERROR",
                            fsNAME = "3A",
                            fsCONTENT = clsCHT_3A.fsMESSAGE_A3 + "【fsOTPW:" + clsCHT_ATM_ISPAID_ARGS.fsOTPW + "】",
                            fsCREATED_BY = "ChtAtmPaid"
                        });
                        return Ok();
                    }
                }
                else
                {
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "ERROR",
                        fsNAME = "ATM IS PAID",
                        fsCONTENT = fsRESULT.Split(':')[1] + "【fsOTPW:" + clsCHT_ATM_ISPAID_ARGS.fsOTPW + "】",
                        fsCREATED_BY = "ChtAtmPaid"
                    });
                    return Ok();
                }
            }
            catch (Exception)
            {
                return Ok();
            }
        }

        /// <summary>
        /// 查詢租用狀態
        /// </summary>
        /// <param name="clsCHT_SUBSCRIBE_QUERY_ARGS">查詢租用參數</param>
        /// <returns></returns>
        [Route("ChtSubscribeStatus")]
        public HttpResponseMessage PostChtSubscribeStatus(clsCHT_SUBSCRIBE_QUERY_ARGS clsCHT_SUBSCRIBE_QUERY_ARGS)
        {
            try
            {
                clsCHT_SUBSCRIBE_QUERY clsCHT_SUBSCRIBE_QUERY = new repBSM().fnGET_CHT_SUBSCRIBE_QUERY(clsCHT_SUBSCRIBE_QUERY_ARGS);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsCHT_SUBSCRIBE_QUERY.fsVALIDATE_SUBSCRIBE_QUERY }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 退租
        /// </summary>
        /// <param name="clsUNSUBSCRIBE_ARGS">退租參數</param>
        /// <returns></returns>
        [Route("ChtUnSubscribe")]
        public HttpResponseMessage PostChtUnSubscribe(clsUNSUBSCRIBE_ARGS clsUNSUBSCRIBE_ARGS)
        {
            try
            {
                string fsRESULT = string.Empty;

                //取得訂單資訊
                clsMONTH_PAY_INFO clsMONTH_PAY_INFO = new repBSM().fnGET_MONTH_PAY_INFO(clsUNSUBSCRIBE_ARGS.fsORDER_NO);


                //取得動作開始日期，空值為馬上退租
                string fsACTIONDATE = string.Empty;

                //進行退租
                clsCHT_UNSUBSCRIBE_ARGS clsCHT_UNSUBSCRIBE_ARGS = new clsCHT_UNSUBSCRIBE_ARGS();
                clsCHT_UNSUBSCRIBE_ARGS.fsALIAS = clsMONTH_PAY_INFO.fsACCOUNT_ID;
                clsCHT_UNSUBSCRIBE_ARGS.fsSUBSCRIBENO = clsMONTH_PAY_INFO.fsMONTH_SUBSCRIBENO;
                clsCHT_UNSUBSCRIBE_ARGS.fsPRODUCT_ID = clsMONTH_PAY_INFO.fsCHT_PRODUCT_ID;
                clsCHT_UNSUBSCRIBE_ARGS.fsACTIONDATE = fsACTIONDATE;
                clsCHT_UNSUBSCRIBE_ARGS.fsAMOUNT = string.Empty;
                clsCHT_UNSUBSCRIBE_ARGS.fsCHECKSUM = clsSECURITY.GetChtUnSubscribeCheckSum(clsMONTH_PAY_INFO.fsACCOUNT_ID, clsMONTH_PAY_INFO.fsMONTH_SUBSCRIBENO, clsMONTH_PAY_INFO.fsCHT_PRODUCT_ID, fsACTIONDATE, string.Empty);
                clsCHT_UNSUBSCRIBE_ARGS.fsAPID = clsMONTH_PAY_INFO.fsAUTHORITY;
                clsCHT_UNSUBSCRIBE_ARGS.fsRESERVED = string.Empty;
                clsCHT_UNSUBSCRIBE_ARGS.fsCHT_AUTHORITY = clsMONTH_PAY_INFO.fsAUTHORITY;
                clsCHT_UNSUBSCRIBE clsCHT_UNSUBSCRIBE = new repBSM().fnGET_CHT_UNSUBSCRIBE(clsCHT_UNSUBSCRIBE_ARGS);

                if (clsCHT_UNSUBSCRIBE.fsVALIDATE_UNSUBSCRIBE == "Success")
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    ////查詢月付介面，確認是否已退租
                    //clsCHT_SUBSCRIBE_QUERY_ARGS clsCHT_SUBSCRIBE_QUERY_ARGS = new clsCHT_SUBSCRIBE_QUERY_ARGS();
                    //clsCHT_SUBSCRIBE_QUERY_ARGS.fsALIAS = clsMONTH_PAY_INFO.fsACCOUNT_ID;
                    //clsCHT_SUBSCRIBE_QUERY_ARGS.fsSUBSCRIBENO = clsMONTH_PAY_INFO.fsMONTH_SUBSCRIBENO;
                    //clsCHT_SUBSCRIBE_QUERY_ARGS.fsPRODUCT_ID = clsMONTH_PAY_INFO.fsCHT_PRODUCT_ID;
                    //clsCHT_SUBSCRIBE_QUERY_ARGS.fsCHECKSUM = clsSECURITY.GetChtQueryMonthCheckSum(clsMONTH_PAY_INFO.fsACCOUNT_ID, clsMONTH_PAY_INFO.fsMONTH_SUBSCRIBENO, clsMONTH_PAY_INFO.fsCHT_PRODUCT_ID);
                    //clsCHT_SUBSCRIBE_QUERY_ARGS.fsAPID = clsMONTH_PAY_INFO.fsAUTHORITY;
                    //clsCHT_SUBSCRIBE_QUERY_ARGS.fsRESERVED = string.Empty;
                    //clsCHT_SUBSCRIBE_QUERY clsCHT_SUBSCRIBE_QUERY = new repBSM().fnGET_CHT_SUBSCRIBE_QUERY(clsCHT_SUBSCRIBE_QUERY_ARGS);

                    //if (clsCHT_SUBSCRIBE_QUERY.fsVALIDATE_SUBSCRIBE_QUERY != "Error")
                    //{
                    //    if (clsCHT_SUBSCRIBE_QUERY.fsVALIDATE_SUBSCRIBE_QUERY == "s241")
                    //    {
                    //        //確定已退租
                    //        //更新退租

                    //        fsRESULT = new repBSM().fnUPDATE_CHT_UNSUBSCRIBE(clsUNSUBSCRIBE_ARGS.fsORDER_NO, clsCHT_SUBSCRIBE_QUERY.fsVALIDATE_SUBSCRIBE_QUERY,
                    //            clsMONTH_PAY_INFO.fsMONTH_SUBSCRIBENO, fsACTIONDATE, clsUNSUBSCRIBE_ARGS.fsUPDATED_BY);

                    //        if (fsRESULT.IndexOf("ERROR") == -1)
                    //        {
                    //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    //        }
                    //        else
                    //        {
                    //            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    //        }
                    //    }
                    //    else
                    //    {
                    //        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    //    }
                    //}
                    //else
                    //{
                    //    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsCHT_SUBSCRIBE_QUERY.fsMESSAGE_SUBSCRIBE_QUERY }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    //}
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsCHT_UNSUBSCRIBE.fsMESSAGE_UNSUBSCRIBE }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 補送攤分
        /// </summary>
        /// <param name="clsCHT_REMULTIBILL_ARGS">補送攤分參數</param>
        /// <returns></returns>
        [Route("ChtReMultiBill")]
        public HttpResponseMessage PostChtReMultiBill(clsCHT_REMULTIBILL_ARGS clsCHT_REMULTIBILL_ARGS)
        {
            try
            {
                string fsRESULT = string.Empty;

                //取得訂單資訊
                clsORDER clsORDER = new repORDER().fnGET_ORDER_INFO(clsCHT_REMULTIBILL_ARGS.fsORDER_NO);

                if (!string.IsNullOrEmpty(clsORDER.fsORDER_NO))
                {
                    //進行攤分
                    clsCHT_MILTIBILL clsCHT_MILTIBILL = new repBSM().fnGET_CHT_MULTIBILL(new clsCHT_MILTIBILL_ARGS()
                    {
                        fsCHT_OTPW = clsORDER.fsOTPW,
                        fsCHT_AUTHORITY = clsORDER.fsAUTHORITY,
                        fsCHT_MULTIPLE_BILL_PARAM = "1^" + DateTime.UtcNow.AddHours(8).ToString("yyyyMMdd") + DateTime.UtcNow.AddHours(8).ToString("yyyyMMdd") + clsORDER.fsFEE.PadLeft(6, '0')
                    });

                    if (clsCHT_MILTIBILL.fsVALIDATE_BILL == "Success")
                    {
                        //更新攤分
                        fsRESULT = new repBSM().fnUPDATE_CHT_MULTIBILL(clsORDER.fsOTPW, clsCHT_MILTIBILL.fsMULTIPLE_BILL, clsCHT_REMULTIBILL_ARGS.fsUPDATED_BY);
                        if (fsRESULT.IndexOf("ERROR") == -1)
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                        else
                        {
                            //是否回上一動
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        //是否回上一動
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsCHT_MILTIBILL.fsMULTIPLE_BILL }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "查無此訂單!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 啟用優惠碼
        /// </summary>
        /// <param name="clsREGISTER_COUPON_ARGS"></param>
        /// <returns></returns>
        [Route("RegisterCoupon")]
        public HttpResponseMessage PostRegisterCoupon(clsREGISTER_COUPON_ARGS clsREGISTER_COUPON_ARGS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsREGISTER_COUPON_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    //判斷驗證碼
                    string fsVALIDATE_CODE_RESULT = new repACCOUNT().fnGET_LOGIN_VALIDATE_CODE(clsREGISTER_COUPON_ARGS.fsVALIDATE_CODE);

                    if (string.IsNullOrEmpty(fsVALIDATE_CODE_RESULT))
                    {
                        //取得使用者
                        var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsREGISTER_COUPON_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                        //判斷帳號是否已驗證通過
                        if (new repACCOUNT().fnGET_ACCOUNT_STATUS(clsUSER_DATA.fsUSER).Validated)
                        {
                            //檢查優惠碼狀態
                            string fsRESULT = string.Empty;

                            fsRESULT = new repBSM().fnGET_COUPON_STATUS(clsREGISTER_COUPON_ARGS.fsCOUPON_NO);

                            if (string.IsNullOrEmpty(fsRESULT))
                            {
                                fsRESULT = new repBSM().fnREGISTER_COUPON(clsUSER_DATA.fsACCOUNT_ID, clsREGISTER_COUPON_ARGS.fsCOUPON_NO);

                                if (string.IsNullOrEmpty(fsRESULT))
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = "success" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                                else
                                {
                                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                                }
                            }
                            else
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "帳號尚未驗證通過!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }

                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsVALIDATE_CODE_RESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }
    }
}
