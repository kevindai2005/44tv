﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Security;
//using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Repositorys;
using System.Web;
using WebApi.OutputCache.V2;
using Swashbuckle.Swagger.Annotations;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("Ecpay")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class EcpayController : ApiController
    {
        /// <summary>
        /// 購買
        /// </summary>
        /// <returns></returns>
        [Route("Purchase")]
        public HttpResponseMessage PostPurchase(clsECPAY_PURCHASE_ARGS clsECPAY_PURCHASE_ARGS)
        {
            try
            {
                //取得套餐價格
                clsSET_PACKAGE_CHT clsSET_PACKAGE_CHT = new repBSM().fnGET_PACKAGE_CHT(clsECPAY_PURCHASE_ARGS.fsPURCHASE_ID, clsECPAY_PURCHASE_ARGS.fsPACKAGE_ID)[0];

                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsECPAY_PURCHASE_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    //取得使用者
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsECPAY_PURCHASE_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    string fsRESULT = string.Empty;
                    int fnRETRY_COUNT = 1;

                    //產生訂單編號
                    string fsORDER_NO = string.Empty;

                    while (fnRETRY_COUNT <= 3 && string.IsNullOrEmpty(fsORDER_NO))
                    {
                        fsORDER_NO = new repL_NO().fnGET_L_NO("ORDERNO", "訂單編號", DateTime.Now.ToString("yyyyMMdd"), "", 5, "Purchase");
                        fnRETRY_COUNT += 1;
                    }

                    if (string.IsNullOrEmpty(fsORDER_NO))
                    {
                        new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                        {
                            fsTYPE = "PURCHASE",
                            fsNAME = "購買",
                            fsCONTENT = "無法取得訂單編號",
                            fsCREATED_BY = "Purchase"
                        });

                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "無法取得訂單編號!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }


                    //新增訂單
                    fsRESULT = new repBSM().fnINSERT_ORDER(clsUSER_DATA.fsACCOUNT_ID, fsORDER_NO, clsECPAY_PURCHASE_ARGS.fsSESSION_ID, clsECPAY_PURCHASE_ARGS.fsPURCHASE_ID, clsECPAY_PURCHASE_ARGS.fsPACKAGE_ID,
                        clsECPAY_PURCHASE_ARGS.fsPAY_FROM, clsECPAY_PURCHASE_ARGS.fnPAY_TYPE_ID, clsECPAY_PURCHASE_ARGS.fsCHT_PRODUCT_ID, string.Empty, string.Empty, string.Empty, string.Empty,
                        clsSET_PACKAGE_CHT.price.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, false, clsECPAY_PURCHASE_ARGS.fsINV_NAME, string.Empty, 
                        clsECPAY_PURCHASE_ARGS.fsINV_ADDRESS2, clsECPAY_PURCHASE_ARGS.fsINV_TEL, string.Empty, string.Empty, "1", clsUSER_DATA.fsACCOUNT_ID, clsECPAY_PURCHASE_ARGS.fsINV_STATUS);

                    if (fsRESULT.IndexOf("ERROR") == -1)
                    {
                        //回傳訂單結果
                        if (!string.IsNullOrEmpty(fsRESULT))
                        {
                            //產生ECPAY付款訂單(訂單編號+亂數，主要是讓綠界那裡可以重複取得付款編號)
                            clsECPAY_TRADE_RESULT clsECPAY_TRADE_RESULT = new repECPAY().fnCREATE_ECPAY_TRADE(fsORDER_NO + DateTime.UtcNow.ToString("ssfff"), clsSET_PACKAGE_CHT.price, clsSET_PACKAGE_CHT.catalog_description, clsECPAY_PURCHASE_ARGS.fsPAY_TYPE_NAME.ToUpper());

                            if(clsECPAY_TRADE_RESULT.RtnCode == "1")
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsECPAY_TRADE_RESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                //刪除失敗訂單
                                new repBSM().fnDELETE_FAIL_ORDER(fsORDER_NO, clsUSER_DATA.fsACCOUNT_ID);

                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsECPAY_TRADE_RESULT.RtnMsg}, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        else
                        {
                            //刪除失敗訂單
                            new repBSM().fnDELETE_FAIL_ORDER(fsORDER_NO, clsUSER_DATA.fsACCOUNT_ID);

                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "訂購失敗!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = fsRESULT.Split(':')[1] }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// 信用卡付款
        /// </summary>
        /// <returns></returns>
        [Route("PayForCredit")]
        public HttpResponseMessage PostPayForCredit(clsECPAY_FOR_CREDIT_ARGS clsECPAY_FOR_CREDIT_ARGS)
        {
            try
            {
                //判斷是否已登入
                if (clsSECURITY.IsLogin(clsECPAY_FOR_CREDIT_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE))
                {
                    //取得使用者
                    var clsUSER_DATA = new repACCOUNT().fnGET_USER_DATA(clsECPAY_FOR_CREDIT_ARGS.clsIDENTITY_VALIDATE_ARUS.fsVALUE);

                    //取得訂單資訊
                    clsORDER clsORDER = new repORDER().fnGET_ORDER_INFO(clsECPAY_FOR_CREDIT_ARGS.fsORDER_NO);

                    if (clsORDER.fsPAY_TYPE_NAME.ToUpper() == "CREDIT")
                    {
                        //取得套餐價格
                        clsSET_PACKAGE_CHT clsSET_PACKAGE_CHT = new repBSM().fnGET_PACKAGE_CHT(clsORDER.fsPURCHASE_ID, clsORDER.fsPACKAGE_ID)[0];

                        if (clsSET_PACKAGE_CHT != null)
                        {
                            //產生ECPAY付款訂單(訂單編號+亂數，主要是讓綠界那裡可以重複取得付款編號)
                            clsECPAY_TRADE_RESULT clsECPAY_TRADE_RESULT = new repECPAY().fnCREATE_ECPAY_TRADE(clsECPAY_FOR_CREDIT_ARGS.fsORDER_NO + DateTime.UtcNow.ToString("ssfff"), clsSET_PACKAGE_CHT.price, clsSET_PACKAGE_CHT.catalog_description, clsORDER.fsPAY_TYPE_NAME.ToUpper());

                            if (clsECPAY_TRADE_RESULT.RtnCode == "1")
                            {
                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = true, Data = clsECPAY_TRADE_RESULT }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                            else
                            {
                                //刪除失敗訂單
                                new repBSM().fnDELETE_FAIL_ORDER(clsECPAY_FOR_CREDIT_ARGS.fsORDER_NO, clsUSER_DATA.fsACCOUNT_ID);

                                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = clsECPAY_TRADE_RESULT.RtnMsg }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                            }
                        }
                        else
                        {
                            return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "查無訂購資訊!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                        }
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "訂單非選擇使用信用卡付款!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = "請先登入系統!" }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
                }

            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new ObjectContent<clsRETURN>(new clsRETURN() { Success = false, ErrMessage = ex.Message }, GlobalConfiguration.Configuration.Formatters.JsonFormatter) };
            }
        }

        /// <summary>
        /// ATM取號結果通知(綠界後端用)
        /// </summary>
        /// <returns></returns>
        [Route("GetAtmResult")]
        public HttpResponseMessage PostGetAtmResult(clsECPAY_ATM_RESULT clsECPAY_ATM_RESULT)
        {
            try
            {
                //判斷是否已登入
                if (clsECPAY_ATM_RESULT.RtnCode == 2)
                {
                    string fsRESULT = new repECPAY().fnUPDATE_ECPAY_ATM(clsECPAY_ATM_RESULT);

                    if (fsRESULT.IndexOf("ERROR") == -1)
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("1|OK") };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("0|" + fsRESULT.Split(':')[1]) };
                    }
                } 
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("0|" + clsECPAY_ATM_RESULT.RtnMsg) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("0|" + ex.Message) };
            }
        }

        /// <summary>
        /// 付款結果通知(綠界後端用)
        /// </summary>
        /// <returns></returns>
        [Route("GetEcpayResult")]
        public HttpResponseMessage PostGetEcpayResult(clsECPAY_PAYMENT_RESULT clsECPAY_PAYMENT_RESULT)
        {
            try
            {
                //判斷是否已登入
                if (clsECPAY_PAYMENT_RESULT.RtnCode == 1)
                {
                    string fsRESULT = new repECPAY().fnUPDATE_ECPAY_PAYMENT(clsECPAY_PAYMENT_RESULT);

                    if (fsRESULT.IndexOf("ERROR") == -1)
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("1|OK") };
                    }
                    else
                    {
                        return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("0|" + fsRESULT.Split(':')[1]) };
                    }
                }
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("0|" + clsECPAY_PAYMENT_RESULT.RtnMsg) };
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("0|" + ex.Message) };
            }
        }


    }
}
