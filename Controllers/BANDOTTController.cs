﻿using FourgTV_OTT2.Api2.SwaggerHeader;
using FourgTV_OTT2.BLL.API;
using FourgTV_OTT2.BLL.Model.Req.BANDOTT;
using FourgTV_OTT2.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using System.Web.Http.Cors;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.Controllers
{
    [RoutePrefix("BANDOTT")]
    [EnableCors(origins: "https://www.4gtv.tv,https://m.4gtv.tv,https://s-web.4gtv.tv,https://m-s-web.4gtv.tv,http://ss-web2.4gtv.tv,http://m-ss-web2.4gtv.tv,https://www2.4gtv.tv,https://m2.4gtv.tv", headers: "*", methods: "*", SupportsCredentials = true)]
    public class BANDOTTController : ApiController
    {
        BANDOTT_BLL _BANDOTT_BLL = new BANDOTT_BLL();

        /// <summary>
        /// 轉移帳號
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [Route("TransferBANDOTTMemberAndOrderJOB")]
        [HttpPost]
        [Header(headersConfig = "apikey,string")]
        public StdResp<int> TransferBANDOTTMemberAndOrderJOB(TransferBANDOTTMemberAndOrderJOB_Req req)
        {
            if (Request.Headers == null)
            {
                throw new MyException(ResponseCodeEnum.HeaderError);
            }
            if (!Request.Headers.Contains("apikey"))
            {
                throw new MyException(ResponseCodeEnum.HeaderError);
            }
            if (!Request.Headers.GetValues("apikey").First().Equals("job"))
            {
                throw new MyException(ResponseCodeEnum.HeaderError);
            }
            if (req == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }
            if (req.LastTime == null)
            {
                throw new MyException(ResponseCodeEnum.RequestNull);
            }

            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(req.LastTime.Value).ToLocalTime();

            StdResp<int> stdResp = new StdResp<int>();
            stdResp.Data = _BANDOTT_BLL.TransferBANDOTTMemberAndOrder(dateTime);
            return stdResp;


        }
    }
}
