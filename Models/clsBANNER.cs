﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{

    //Banner查詢參數
    public class clsBANNER_ARGS
    {
        /// <summary>
        /// Banner位置
        /// </summary>
        public string fsPOSITION { get; set; }
    }

    //Banner
    public class clsBANNER
    {
        /// <summary>
        /// 圖檔1路徑
        /// </summary>
        public string fsIMAGE1 { get; set; }
        /// <summary>
        /// 圖檔2路徑
        /// </summary>
        public string fsIMAGE2 { get; set; }
        /// <summary>
        /// 是否開新頁
        /// </summary>
        public bool fbIS_TARGET { get; set; }
        /// <summary>
        /// 新頁連結
        /// </summary>
        public string fsURL { get; set; }
        /// <summary>
        /// 嵌入式連結
        /// </summary>
        public string fsEMBED { get; set; }
    }
}