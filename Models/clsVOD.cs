﻿using FourgTV_OTT2.BLL.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static FourgTV_OTT2.Api2.Models.clsVOD_EPISODE;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsVOD
    {
        public string fsVOD_NO { get; set; }
        public int fnSEQ { get; set; }
        public string fsTITLE { get; set; }
        public string fsUNIT_NAME { get; set; }
        public int fnEPISODE { get; set; }
    }

    //新增VOD PageView參數
    public class clsINSERT_VOD_PAGEVIEW_ARGS
    {
        public string fsVOD_NO { get; set; }
        public int fnSEQ { get; set; }
    }

    //取得LiTV VOD URL參數
    public class clsGET_VOD_URL_ARGS
    {
        /// <summary>
        /// 媒體類型(drama/show/scene/education/movie/celebrity)
        /// </summary>
        public string fsMEDIA_TYPE { get; set; }
        /// <summary>
        /// LiTV AssetId
        /// </summary>
        public string fsASSET_ID { get; set; }
        /// <summary>
        /// 裝置類型(pc/mobile)
        /// </summary>
        public string fsDEVICE_TYPE { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //取得VOD URL參數
    public class clsGET_VOD_URL_ARGS2
    {
        /// <summary>
        /// AES_value
        /// </summary>
        public string value { get; set; }

    }

    //取得LiTV VOD URL參數(手機用)
    public class clsAPP_GET_VOD_URL_ARGS
    {
        /// <summary>
        /// 媒體類型(drama/show/scene/education/movie/celebrity)
        /// </summary>
        public string fsMEDIA_TYPE { get; set; }
        /// <summary>
        /// LiTV AssetId
        /// </summary>
        public string fsASSET_ID { get; set; }
        /// <summary>
        /// 裝置類型(pc/mobile)
        /// </summary>
        public string fsDEVICE_TYPE { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
    }

    //取得LiTV VOD URL
    public class clsGET_VOD_URL
    {
        /// <summary>
        /// URL List
        /// </summary>
        public List<string> flstURLs { get; set; }
        /// <summary>
        /// 是否要播廣告
        /// </summary>
        public bool fbPLAY_AD { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 廣告破口
        /// </summary>
        public List<int> lstTIME_CODE { get; set; }
        /// <summary>
        /// 未登入允許觀看時間
        /// </summary>
        public int fnALLOWTIME { get; set; }
    }

    //取得LiTV VOD URL
    public class clsGET_VOD_APP_URL
    {
        /// <summary>
        /// URL List
        /// </summary>
        public List<string> flstURLs { get; set; }
        /// <summary>
        /// 是否要播廣告
        /// </summary>
        public bool fbPLAY_AD { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 廣告破口
        /// </summary>
        public List<int> lstTIME_CODE { get; set; }
        /// <summary>
        /// 觀看時間
        /// </summary>
        public int fnALLOWTIME { get; set; }

        public CrownNameInfo CrownName { get; set; }
    }

    //取得戲劇/綜藝類型
    public class clsVOD_TYPE
    {
        /// <summary>
        /// 類型代碼
        /// </summary>
        public string fsCODE { get; set; }
        /// <summary>
        /// 類型名稱
        /// </summary>
        public string fsNAME { get; set; }
        /// <summary>
        /// 是否允許海外
        /// </summary>
        public bool fcOVERSEAS { get; set; }
    }

    public class clsVOD_D_TYPE
    {
        /// <summary>
        /// 類型代碼
        /// </summary>
        public string fsCODE { get; set; }
        /// <summary>
        /// 類型名稱
        /// </summary>
        public string fsNAME { get; set; }
    }


    //取得戲劇年份
    public class clsVOD_YEAR
    {
        public int fnYEAR { get; set; }
    }

    //取得戲劇/綜藝/直擊/文化教育/電影/網路紅什麼首頁資訊
    public class clsVOD_INDEX
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// 主類型
        /// </summary>
        public string fsTYPE1 { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsVIDEO_FROM { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 是否結束播出
        /// </summary>
        public bool fbIS_FINAL { get; set; }
        /// <summary>
        /// 總集數
        /// </summary>
        public int fnTOTAL_EPISODE { get; set; }
        /// <summary>
        /// 上架日期
        /// </summary>
        public string fdDATE { get; set; }
        /// <summary>
        /// 是否下架
        /// </summary>
        public bool fbDOWN { get; set; }
        /// <summary>
        /// 免費
        /// </summary>
        public bool fbIS_FREE { get; set; }
        /// <summary>
        /// 是否允許國外
        /// </summary>
        public bool fcOVERSEAS { get; set; }
        /// <summary>
        /// 類型
        /// </summary>
        public List<clsVOD_EPISODE_DETAIL_TYPE> lstTYPE { get; set; }
        /// <summary>
        /// 影片分級(普遍級:01/保護級:02/輔12級:03/輔15級:04/限制級:05)
        /// </summary>
        public int fnGRADED { get; set; }
        public string fsEnglishTitle { get; set; }
    }

    //取得戲劇/綜藝/直擊/文化教育/電影/網路紅什麼首頁資訊
    public class clsVOD_INDEX_MOBILE
    {
        /// <summary>
        /// 最新VOD列表
        /// </summary>
        public List<string> fsNEW { get; set; }
        /// <summary>
        /// 熱門VOD列表
        /// </summary>
        public List<string> fsHOT { get; set; }
    }
    //取得所有VOD首頁資訊
    public class clsVOD_ALL_INDEX_MOBILE
    {
        /// <summary>
        /// 館別
        /// </summary>
        public string fsTYPE { get; set; }
        /// <summary>
        /// 熱門VOD列表
        /// </summary>
        public List<string> fsHOT { get; set; }
        /// <summary>
        /// 最新VOD列表
        /// </summary>
        public List<string> fsNEW { get; set; }
    }

    //直擊類別
    public class clsVOD_SCENE_CATEGORY
    {
        public string fsVOD_NO { get; set; }
        public string fsTITLE { get; set; }
    }

    //取得直擊系列首頁資訊
    public class clsVOD_SCENE_INDEX_SERIES
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 系列名稱
        /// </summary>
        public string fsPROG_NAME { get; set; }
        /// <summary>
        /// 集數
        /// </summary>
        public int fnEPISODE { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 上架日期
        /// </summary>
        public string fdDATE { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsVIDEO_FROM { get; set; }
    }

    //取得戲劇/綜藝/直擊/文化教育/電影/網路紅什麼集數資訊
    public class clsVOD_EPISODE
    {
        /// <summary>
        /// 節目名稱
        /// </summary>
        public string fsPROG_NAME { get; set; }
        /// <summary>
        /// 是否結束播出
        /// </summary>
        public bool fbIS_FINAL { get; set; }
        /// <summary>
        /// 總集數
        /// </summary>
        public int fnTOTAL_EPISODE { get; set; }
        /// <summary>
        /// 集數集合
        /// </summary>
        public List<clsVOD_EPISODE_LIST> lstEPISODE { get; set; }

        public class clsVOD_EPISODE_LIST
        {
            /// <summary>
            /// VOD 編號
            /// </summary>
            public string fsVOD_NO { get; set; }
            /// <summary>
            /// 序號
            /// </summary>
            public int fnSEQ { get; set; }
            /// <summary>
            /// 集數
            /// </summary>
            public int fnEPISODE { get; set; }
            /// <summary>
            /// 單元名稱
            /// </summary>
            public string fsUNIT_NAME { get; set; }
            /// <summary>
            /// 代表圖
            /// </summary>
            public string fsHEAD_FRAME { get; set; }
            /// <summary>
            /// 播映日期
            /// </summary>
            public string fdPLAY_DATE { get; set; }
            /// <summary>
            /// 類型
            /// </summary>
            public List<clsVOD_EPISODE_DETAIL_TYPE> lstTYPE { get; set; }
        }
    }

    //取得戲劇/綜藝/文化教育/電影/網路紅什麼集數明細
    public class clsVOD_EPISODE_DETAIL
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 集數
        /// </summary>
        public int fnEPISODE { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        public int fnYEAR { get; set; }
        /// <summary>
        /// 導演
        /// </summary>
        public string fsDIRECTOR { get; set; }
        /// <summary>
        /// 演員/人物
        /// </summary>
        public string fsACTOR { get; set; }
        /// <summary>
        /// 配音
        /// </summary>
        public string fsVOICEACTOR { get; set; }
        /// <summary>
        /// 主持人
        /// </summary>
        public string fsHOST { get; set; }
        /// <summary>
        /// 來賓
        /// </summary>
        public string fsGUEST { get; set; }
        /// <summary>
        /// 編劇
        /// </summary>
        public string fsWRITER { get; set; }
        /// <summary>
        /// 類型
        /// </summary>
        public List<clsVOD_EPISODE_DETAIL_TYPE> lstTYPE { get; set; }
        /// <summary>
        /// AssetID或CDN ID
        /// </summary>
        public string fsURL_ID { get; set; }
        /// <summary>
        /// 國別
        /// </summary>
        public string fsCOUNTRY { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string fsDESCRIPTION { get; set; }
        /// <summary>
        /// 單元名稱
        /// </summary>
        public string fsUNIT_NAME { get; set; }
        /// <summary>
        /// 播映日期
        /// </summary>
        public string fdPLAY_DATE { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 分級
        /// </summary>
        public string fsGRADED { get; set; }
        /// <summary>
        /// 影片分級(普遍級:01/保護級:02/輔12級:03/輔15級:04/限制級:05)
        /// </summary>
        public int fnGRADED { get; set; }
        /// <summary>
        /// 供應商
        /// </summary>
        public string fsVENDOR { get; set; }
        /// <summary>
        /// 授權平台
        /// </summary>
        public List<string> fsRIGHT { get; set; }
        /// <summary>
        /// 製作公司
        /// </summary>
        public string fsPRODUCER { get; set; }
        /// <summary>
        /// 片長
        /// </summary>
        public int fnDURATION { get; set; }
        /// <summary>
        /// 來源平台
        /// </summary>
        public string fsPLATFROM { get; set; }
        /// <summary>
        /// 網紅連結1
        /// </summary>
        public string fsHYPERLINK1 { get; set; }
        /// <summary>
        /// 網紅連結2
        /// </summary>
        public string fsHYPERLINK2 { get; set; }
        /// <summary>
        /// 原著
        /// </summary>
        public string fsORIGINAL { get; set; }
        /// <summary>
        /// 建立日期
        /// </summary>
        public string fdCREATED_DATE { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public string fdUPDATED_DATE { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsVIDEO_FROM { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsM3U8_360P { get; set; }

    }

    //取得直擊集數
    public class clsVOD_SCENE_EPISODE
    {
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 是否結束播出
        /// </summary>
        public bool fbIS_FINAL { get; set; }
        /// <summary>
        /// 集數集合
        /// </summary>
        public List<clsVOD_SCENE_EPISODE_LIST> lstEPISODE { get; set; }

        public class clsVOD_SCENE_EPISODE_LIST
        {
            /// <summary>
            /// VOD 編號
            /// </summary>
            public string fsVOD_NO { get; set; }
            /// <summary>
            /// 序號
            /// </summary>
            public int fnSEQ { get; set; }
            /// <summary>
            /// 集數
            /// </summary>
            public int fnEPISODE { get; set; }
            /// <summary>
            /// 單元名稱
            /// </summary>
            public string fsUNIT_NAME { get; set; }
            /// <summary>
            /// 播映日期
            /// </summary>
            public string fdPLAY_DATE { get; set; }
            /// <summary>
            /// 代表圖
            /// </summary>
            public string fsHEAD_FRAME { get; set; }
        }
    }

    //取得直擊集數段落資訊
    public class clsVOD_SCENE_EPISODE_PART
    {
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 集數
        /// </summary>
        public int fnEPISODE { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 段落集合
        /// </summary>
        public List<clsVOD_SCENE_PART_LIST> lstPART { get; set; }

        public class clsVOD_SCENE_PART_LIST
        {
            /// <summary>
            /// VOD編號
            /// </summary>
            public string fsVOD_NO { get; set; }
            /// <summary>
            /// 序號
            /// </summary>
            public int fnSEQ { get; set; }
            /// <summary>
            /// 段落
            /// </summary>
            public string fsPART { get; set; }
        }
    }

    //取得直擊集數段落明細
    public class clsVOD_SCENE_EPISODE_DETAIL
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// 序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// 段落
        /// </summary>
        public string fsPART { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 集數
        /// </summary>
        public int fnEPISODE { get; set; }
        /// <summary>
        /// AssetID或CDN ID
        /// </summary>
        public string fsURL_ID { get; set; }
        /// <summary>
        /// 單元名稱
        /// </summary>
        public string fsUNIT_NAME { get; set; }
        /// <summary>
        /// 單元大綱
        /// </summary>
        public string fsUNIT_DESCRIPTION { get; set; }
        /// <summary>
        /// 活動日期
        /// </summary>
        public string fdDATE { get; set; }
        /// <summary>
        /// 地點
        /// </summary>
        public string fsLOCATION { get; set; }
        /// <summary>
        /// 人物
        /// </summary>
        public string fsPERSON { get; set; }
        /// <summary>
        /// 關鍵字
        /// </summary>
        public string fsKEYWORD { get; set; }
        /// <summary>
        /// 製做公司/主辦單位
        /// </summary>
        public string fsORGANIZE { get; set; }
        /// <summary>
        /// 授權平台
        /// </summary>
        public List<string> fsCOPYRIGHT { get; set; }
        /// <summary>
        /// 影片標籤
        /// </summary>
        public string fsGENER { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 影片分級(普遍級:01/保護級:02/輔12級:03/輔15級:04/限制級:05)
        /// </summary>
        public int fnGRADED { get; set; }
        /// <summary>
        /// 建立日期
        /// </summary>
        public string fdCREATED_DATE { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        public string fdUPDATED_DATE { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsVIDEO_FROM { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsM3U8_360P { get; set; }
    }

    //VOD編號與LiTV Series ID
    public class clsVOD_SERIES
    {
        public string fsVOD_NO { get; set; }
        public int fnSEQ { get; set; }

        public string fsSERIES_ID_LITV { get; set; }
        public int fnEPISODE { get; set; }
    }

    //VOD來源
    public class clsVOD_FROM
    {
        public string fsVOD_NO { get; set; }
        public int fnSEQ { get; set; }
        public string fsFILE_NO { get; set; }
        public string fsFROM { get; set; }
        public string fsVENDOR { get; set; }
        public bool fcOVERSEAS { get; set; }
        public string fsHEAD_FRAME { get; set; }
        public bool fcFREE { get; set; }
        public int fnYEAR { get; set; }
        public string fcCASE_CHT { get; set; }
        public string fsWRITTEN_BY { get; set; }
        public List<string> lstEXCEPT_COUNTRY { get; set; }
    }

    //VOD主檔類型
    public class clsVOD_EPISODE_DETAIL_TYPE
    {
        public string fsCODE { get; set; }
        public string fsNAME { get; set; }
    }

    //VOD主檔(BOX用)
    public class clsVOD_BOX
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
    }

    //取得會員VOD權限參數
    public class clsGET_MEMBER_VOD_AUTH_ARGS
    {
        public string fsACCOUNT_ID { get; set; }
        public string fsVOD_NO { get; set; }
    }

    //取得會員VOD權限
    public class clsGET_MEMBER_VOD_AUTH
    {
        public string fsACCOUNT_ID { get; set; }
        public string fsVOD_NO { get; set; }
        public bool fcAUTH { get; set; }
        public bool fcAD { get; set; }
        public bool fcIS_PAY_MEMBER { get; set; }
    }

    //回傳給APP有更新的VOD與最後更新頻道時間
    public class clsAPP_UPDATE_CONTENT
    {
        public string channel { get; set; }
        public List<clsVOD_INDEX> vod { get; set; }
    }


    public class clsCELEBRITY_PROMO
    {
        //第一層
        /// <summary>
        /// 推薦編號
        /// </summary>
        public int fnID { get; set; }
        /// <summary>
        /// 推薦標題
        /// </summary>
        public string fsPROMO_TITLE { get; set; }
        /// <summary>
        /// 推薦類型
        /// </summary>
        public string fsPROMO_TYPE { get; set; }
        /// <summary>
        /// 更多連結
        /// </summary>
        public string fsMORE_LINK { get; set; }
        /// <summary>
        /// 推件類型與資訊
        /// </summary>
        public List<clsCELEBRITY_PROMO_DETAIL> PROMO_DETAIL { get; set; }
    }

    public class clsCELEBRITY_PROMO_DETAIL
    {
        //第二層
        /// <summary>
        /// 類型代碼
        /// </summary>
        public string fsTYPE { get; set; }
        /// <summary>
        /// 類型名稱
        /// </summary>
        public string fsTYPE_NAME { get; set; }
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public string fdDATE { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        public int fnYEAR { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsIMAGE { get; set; }
        /// <summary>
        /// 總集數
        /// </summary>
        public int fnTOTAL_EPISODE { get; set; }
        /// <summary>
        /// 是否結束播出
        /// </summary>
        public bool fbIS_FINAL { get; set; }
        /// <summary>
        /// 畫質
        /// </summary>
        public string fsQUALITY { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string fsDESCRIPTION { get; set; }
        /// <summary>
        /// 單元名稱
        /// </summary>
        public string fsUNIT_NAME { get; set; }
        /// <summary>
        /// 推件類型與資訊
        /// </summary>
        public List<clsVOD_EPISODE_LIST> clsVOD_EPISODE_LIST { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsVIDEO_FROM { get; set; }
    }


    public class clsCELEBRITY_HOME
    {
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string fsDESCRIPTION { get; set; }
        /// <summary>
        /// 主檔圖
        /// </summary>
        public string fsHEAD_FRAME1_URL { get; set; }
        /// <summary>
        /// 模組
        /// </summary>
        public int fnTEMP_ID { get; set; }
        /// <summary>
        /// 是否允許海外
        /// </summary>
        public bool fcOVERSEAS { get; set; }
        /// <summary>
        /// 模組內容
        /// </summary>
        public clsCELEBRITY_HOME_TEMP_1 clsCELEBRITY_HOME_TEMP_1 { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsVIDEO_FROM { get; set; }
    }
    public class clsCELEBRITY_HOME_TEMP_1
    {
        /// <summary>
        /// 模組名稱
        /// </summary>
        public string fsTEMP_NAME { get; set; }
        /// <summary>
        /// URL
        /// </summary>
        public string fsHOME_TEMP1_URL { get; set; }
        /// <summary>
        /// URL
        /// </summary>
        public string fsHOME_TEMP1_URL_APP{ get; set; }
        /// <summary>
        /// 連結圖
        /// </summary>
        public List<clsCELEBRITY_HOME_LINK> listCELEBRITY_HOME_LINK { get; set; }
    }

    public class clsCELEBRITY_HOME_LINK
    {
        /// <summary>
        /// 類別
        /// </summary>
        public string fsTYPE { get; set; }
        /// <summary>
        /// 類別名稱
        /// </summary>
        public string fsTYPE_NAME { get; set; }
        /// <summary>
        /// 類別連結
        /// </summary>
        public string fsHOME_LINK_URL { get; set; }
    }

    public class clsVOD_CELEBRITY_HOME_INDEX
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 單元名稱
        /// </summary>
        public string fsUNIT_NAME { get; set; }
        /// <summary>
        /// 上架日期
        /// </summary>
        public string fdDATE { get; set; }
        /// <summary>
        /// 類型
        /// </summary>
        public List<clsVOD_EPISODE_DETAIL_TYPE> lstTYPE { get; set; }
    }

    public class clsVOD_CELEBRITY_HOME_DETAIL_PROMO
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 單元名稱
        /// </summary>
        public string fsUNIT_NAME { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsVIDEO_FROM { get; set; }
    }
}