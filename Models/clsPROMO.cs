﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    //首頁推薦
    public class clsPROMO
    {
        //第一層
        /// <summary>
        /// 推薦編號
        /// </summary>
        public int fnID { get; set; }
        /// <summary>
        /// 推薦標題
        /// </summary>
        public string fsPROMO_TITLE { get; set; }
        /// <summary>
        /// 推薦類型
        /// </summary>
        public string fsPROMO_TYPE { get; set; }
        /// <summary>
        /// 更多連結
        /// </summary>
        public string fsMORE_LINK { get; set; }
        /// <summary>
        /// 推件類型與資訊
        /// </summary>
        public List<clsPROMO_DETAIL> PROMO_DETAIL { get; set; }
        
        public class clsPROMO_DETAIL
        {
            //第二層
            /// <summary>
            /// 類型代碼
            /// </summary>
            public string fsTYPE { get; set; }
            /// <summary>
            /// 類型名稱
            /// </summary>
            public string fsTYPE_NAME { get; set; }
            /// <summary>
            /// VOD編號
            /// </summary>
            public string fsVOD_NO { get; set; }
            /// <summary>
            /// 4GTV頻道代碼
            /// </summary>
            public string fs4GTV_ID { get; set; }
            /// <summary>
            /// 頻道編號
            /// </summary>
            public int fnCHANNEL_ID { get; set; }
            /// <summary>
            /// 標題
            /// </summary>
            public string fsTITLE { get; set; }
            /// <summary>
            /// 日期
            /// </summary>
            public string fdDATE { get; set; }
            /// <summary>
            /// 年份
            /// </summary>
            public int fnYEAR { get; set; }
            /// <summary>
            /// 代表圖
            /// </summary>
            public string fsIMAGE { get; set; }
            /// <summary>
            /// 總集數
            /// </summary>
            public int fnTOTAL_EPISODE { get; set; }
            /// <summary>
            /// 是否結束播出
            /// </summary>
            public bool fbIS_FINAL { get; set; }
            /// <summary>
            /// 畫質
            /// </summary>
            public string fsQUALITY { get; set; }
            /// <summary>
            /// 描述
            /// </summary>
            public string fsDESCRIPTION { get; set; }
            /// <summary>
            /// 台徽(PC用)
            /// </summary>
            public string fsCHANNEL_IMAGE_PC { get; set; }
            /// <summary>
            /// 台徽(手機用)
            /// </summary>
            public string fsCHANNEL_IMAGE_MOBILE { get; set; }
            /// <summary>
            /// 影片源
            /// </summary>
            public string fsVIDEO_FROM { get; set; }
        }
    }
}