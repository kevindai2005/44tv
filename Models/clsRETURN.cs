﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static FourgTV_OTT2.Common.Setting.EnumSetting;

namespace FourgTV_OTT2.Api2.Models
{
    //public class clsRETURN
    //{
    //    /// <summary>
    //    /// 結果
    //    /// </summary>
    //    public string result { get; set; }
    //    /// <summary>
    //    /// 訊息
    //    /// </summary>
    //    public string message { get; set; }
    //}

    //public class clsRETURN_ACCOUNT
    //{
    //    /// <summary>
    //    /// 結果
    //    /// </summary>
    //    public string result { get; set; }
    //    /// <summary>
    //    /// 帳號加密值
    //    /// </summary>
    //    public string fsVALUE { get; set; }
    //}


    public class clsRETURN
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string ErrMessage { get; set; }
        /// <summary>
        /// 回傳資訊
        /// </summary>
        public object Data { get; set; }
    }



}