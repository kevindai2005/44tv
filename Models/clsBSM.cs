﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsBSM
    {
    }

    //套餐列表
    public class clsSET_LIST
    {
        /// <summary>
        /// 套餐編號
        /// </summary>
        public string group_id { get; set; }
        /// <summary>
        /// 套餐名稱
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 套餐描述
        /// </summary>
        public string group_description { get; set; }
    }

    //套餐明細資訊
    public class clsSET_PACKAGE_CHT
    {
        public string catalog_id { get; set; }
        public string catalog_description { get; set; }
        public string package_name { get; set; }
        public string package_id { get; set; }
        public string cht_product_id { get; set; }
        public string price_description { get; set; }
        public int price { get; set; }
        public string recurrent { get; set; }
        public List<string> pay_method_list { get; set; }
        public int duration_by_day { get; set; }
        public List<clsSET_PACKAGE_CHT_PAYMENT> payments { get; set; }

        public class clsSET_PACKAGE_CHT_PAYMENT
        {
            public int pay_type_id { get; set; }
            public string pay_type { get; set; }
            public string cash_flow { get; set; }
            public string name { get; set; }
            public string cht_payment_code { get; set; }
        }
    }

    //取得中華支付checksum
    public class clsCHT_CHECHSUM
    {
        public string fsPRODUCT_ID { get; set; }
        public string fsCURL { get; set; }
        public string fsEURL { get; set; }
        public int fnFEE { get; set; }
        public string fsOTHERS { get; set; }

        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //中華支付相關訊息
    public class clsCHT_PAY_INFO
    {
        /// <summary>
        /// 使用者編號
        /// </summary>
        public string fsACCOUNT_ID { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string fsORDER_NO { get; set; }
        /// <summary>
        /// 用戶識別資料
        /// </summary>
        public string fsUID { get; set; }
        /// <summary>
        /// 授權碼
        /// </summary>
        public string fsOTPW { get; set; }
        /// <summary>
        /// 費用
        /// </summary>
        public string fsFEE { get; set; }
        /// <summary>
        /// 交易方式
        /// </summary>
        public string fsAUTHORITY { get; set; }
        /// <summary>
        /// 產品編號
        /// </summary>
        public string fsCHT_PRODUCT_ID { get; set; }
    }

    //查詢會員購買條件是否符合
    public class clsMEMBER_PURCHASE_CONDITION_ARGS
    {
        /// <summary>
        /// 方案編號(如：CH4G03、CH4GSPORT04)
        /// </summary>
        public string fsPACKAGE_ID { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //購買參數
    public class clsPURCHASE_ARGS
    {
        public OrderInfo OrderInfo { get; set; }

        /// <summary>
        /// 綠界這管道進來的fsvalue
        /// </summary>
        public string fsvalue { get; set; }
        public string invoicenumber { get; set; }
        public string fsINV_STATUS { get; set; }

        /// <summary>
        /// 訂單session唯一ID
        /// </summary>
        public string fsSESSION_ID { get; set; }
        /// <summary>
        /// 套餐代號(如CHANNEL4G_FULL)
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
        /// <summary>
        /// 付款來源(如CHT)
        /// </summary>
        public string fsPAY_FROM { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        public int fnPAY_TYPE_ID { get; set; }
        /// <summary>
        /// 產品編號
        /// </summary>
        public string fsCHT_PRODUCT_ID { get; set; }
        /// <summary>
        /// 優惠碼
        /// </summary>
        public string fsPROMO_CODE { get; set; }
        /// <summary>
        /// 是否捐贈發票（true：捐贈; false：不捐贈）
        /// </summary>
        public bool fsINVOICE_GIFT { get; set; }
        /// <summary>
        /// 購買方案ID(如CH4GGOOD04、CH4G01)
        /// </summary>
        public string fsPACKAGE_ID { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsCHT_RESULT { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsCHT_UID { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsCHT_OTPW { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsCHT_FEE { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsCHT_AUTHORITY { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsCHT_OTHERS { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsCHT_CPORDERNO { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsEXPIRATIONDATE { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsBANKCODE { get; set; }
        /// <summary>
        /// 中華支付回傳的資料
        /// </summary>
        public string fsORDERID { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    public class OrderInfo
    {
        public string MerchantTradeDate { get; set; }

        public string MerchantTradeNo { get; set; }

        public int TotalAmount { get; set; }

        public string ReturnURL { get; set; }

        public string TradeDesc { get; set; }

        public string ItemName { get; set; }
        public string TradeNo { get; set; }
        public string TradeAmt { get; set; }
        public string PaymentType { get; set; }
        public string PaymentDate { get; set; }
        public string TradeDate { get; set; }
        public string TradeStatus { get; set; }
        public string ChargeFee { get; set; }
        public string CustomField { get; set; }
        public BarcodeInfo ExpireDate { get; set; }
        public BarcodeInfo Barcode1 { get; set; }
        public BarcodeInfo Barcode2 { get; set; }
        public BarcodeInfo Barcode3 { get; set; }

    }

    public class ATMInfo
    {
        public string ExpireDate { get; set; }
    }

    public class CVSInfo
    {
        public string StoreExpireDate { get; set; }

        public string Desc_1 { get; set; }

        public string Desc_2 { get; set; }

        public string Desc_3 { get; set; }

        public string Desc_4 { get; set; }
    }

    public class BarcodeInfo
    {
        public string StoreExpireDate { get; set; }
        public string ExpireDate { get; set; }
        public string Barcode1 { get; set; }
        public string Barcode2 { get; set; }
        public string Barcode3 { get; set; }
    }

    public class clsORDER_DEVICE_ARGS
    {
        public string fsOrderNo { set; get; }
        public string fsFrom { set; get; }
        public string fsDevice { set; get; }
        public string fsDeviceVendor { set; get; }
        public string fsOS { set; get; }
        public string fsOSVersion { set; get; }
        public string fsBrowser { set; get; }
        public string fsBrowserVersion { set; get; }
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //月付訂單資訊
    public class clsMONTH_PAY_INFO
    {
        /// <summary>
        /// 會員帳號
        /// </summary>
        public string fsACCOUNT_ID { get; set; }
        /// <summary>
        /// 月付授權碼
        /// </summary>
        public string fsMONTH_SUBSCRIBENO { get; set; }
        /// <summary>
        /// 是否已付款
        /// </summary>
        public string fcPAY { get; set; }
        /// <summary>
        /// 中華產品代號
        /// </summary>
        public string fsCHT_PRODUCT_ID { set; get; }
        /// <summary>
        /// 付款方式
        /// </summary>
        public string fsAUTHORITY { get; set; }
        /// <summary>
        /// 訂單開始日期
        /// </summary>
        public string fdSTART_DATE { get; set; }
    }

    //授權2A介面參數
    public class clsCHT_2A_ARGS
    {
        /// <summary>
        /// 認證成功授權碼
        /// </summary>
        public string fsCHT_OTPW { get; set; }
        /// <summary>
        /// 費用
        /// </summary>
        public string fsCHT_FEE { get; set; }
        /// <summary>
        /// 交易方式(即aa-authority)
        /// </summary>
        public string fsCHT_APID { get; set; }
        /// <summary>
        /// 產品編號
        /// </summary>
        public string fsCHT_PRODUCTID { get; set; }
        /// <summary>
        /// 交易方式
        /// </summary>
        public string fsCHT_AUTHORITY { get; set; }
    }

    //授權2A介面回傳
    public class clsCHT_2A
    {
        /// <summary>
        /// 驗證結果(傳回值為"Success"或其它錯誤代碼)
        /// </summary>
        public string fsVALIDATE_A2 { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE_A2 { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        public string fsPAY_BY_A2 { get; set; }
        /// <summary>
        /// 產品狀態
        /// </summary>
        public string fsSTATUS_A2 { get; set; }
        /// <summary>
        /// 中華訂單編號
        /// </summary>
        public string fsORDER_NO_A2 { get; set; }
        /// <summary>
        /// 信用卡交易授權碼
        /// </summary>
        public string fsAPPROVE_NO_A2 { get; set; }
        /// <summary>
        /// XML本文
        /// </summary>
        public string fsRESULT_A2 { get; set; }

    }

    //授權3A介面參數
    public class clsCHT_3A_ARGS
    {
        /// <summary>
        /// 認證成功授權碼
        /// </summary>
        public string fsCHT_OTPW { get; set; }
        /// <summary>
        /// 費用
        /// </summary>
        public string fsCHT_FEE { get; set; }
        /// <summary>
        /// 交易方式(即aa-authority)
        /// </summary>
        public string fsCHT_APID { get; set; }
        /// <summary>
        /// 消費時間(yyyymmddhhmmss)
        /// </summary>
        public string fsCHT_STIME { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string fsCHT_SREMARK { get; set; }
        /// <summary>
        /// 產品編號
        /// </summary>
        public string fsCHT_PRODUCTID { get; set; }
        /// <summary>
        /// 交易方式
        /// </summary>
        public string fsCHT_AUTHORITY { get; set; }
    }

    //授權3A介面回傳
    public class clsCHT_3A
    {
        /// <summary>
        /// 驗證結果(傳回值為"Success"或其它錯誤代碼)
        /// </summary>
        public string fsVALIDATE_A3 { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE_A3 { get; set; }
        /// <summary>
        /// XML本文
        /// </summary>
        public string fsRESULT_A3 { get; set; }

    }

    //攤分參數
    public class clsCHT_MILTIBILL_ARGS
    {
        /// <summary>
        /// 交易方式
        /// </summary>
        public string fsCHT_AUTHORITY { get; set; }
        /// <summary>
        /// 交易授權碼
        /// </summary>
        public string fsCHT_OTPW { get; set; }
        /// <summary>
        /// 交易攤分方式(1^2018010120181231000168)(第1碼:筆數、第3~22碼:起迄日期+金額[6碼，不足補0])
        /// </summary>
        public string fsCHT_MULTIPLE_BILL_PARAM { get; set; }
    }

    //攤分結果
    public class clsCHT_MILTIBILL
    {
        /// <summary>
        /// 驗證結果(傳回值為"Success"或其它錯誤代碼)
        /// </summary>
        public string fsVALIDATE_BILL { get; set; }
        /// <summary>
        /// 攤分結果(00:OK、11:連線IP不正確、12:查無該筆資料、13:資料格式錯誤、14:資料無法更新、15:其他錯誤、20:攤分無回傳、21:結構錯誤)
        /// </summary>
        public string fsMULTIPLE_BILL { get; set; }
    }

    //補送攤分
    public class clsCHT_REMULTIBILL_ARGS
    {
        public string fsORDER_NO { get; set; }
        public string fsUPDATED_BY { get; set; }
    }

    //ATM已繳費參數
    public class clsCHT_ATM_ISPAID_ARGS
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string fsORDER_NO { get; set; }
        /// <summary>
        /// 交易授權碼
        /// </summary>
        public string fsOTPW { get; set; }
        /// <summary>
        /// 付費時間
        /// </summary>
        public string fsATM_TXNDATETIME { get; set; }
        /// <summary>
        /// 回傳碼
        /// </summary>
        public string fsATM_RESPONSE_CODE { get; set; }
        /// <summary>
        /// 回傳字串
        /// </summary>
        public string fsATM_RESULT { get; set; }
    }

    //訂閱參數
    public class clsCHT_SUBSCRIBE_ARGS
    {
        /// <summary>
        /// 交易方式
        /// </summary>
        public string fsCHT_AUTHORITY { get; set; }
        /// <summary>
        /// CP端帳號(AccountID)
        /// </summary>
        public string fsALIAS { get; set; }
        /// <summary>
        /// 產品代碼
        /// </summary>
        public string fsPRODUCT_ID { get; set; }
        /// <summary>
        /// 授權碼(aa-otpw)
        /// </summary>
        public string fsMIXCODE { get; set; }
        /// <summary>
        /// 動作開始日期(yyyyMMddHHmmss)，空值表示馬上開始
        /// </summary>
        public string fsACTIONDATE { get; set; }
        /// <summary>
        /// 包月總月數
        /// </summary>
        public string fsTOTALMONTH { get; set; }
        /// <summary>
        /// 設定費用
        /// </summary>
        public string fsSETTINGCHARGE { get; set; }
        /// <summary>
        /// 安裝費用
        /// </summary>
        public string fsINSTALLCHARGE { get; set; }
        /// <summary>
        /// 首次收費
        /// </summary>
        public string fsFIRSTCHARGE { get; set; }
        /// <summary>
        /// 一般包月帶空值
        /// </summary>
        public string fsAMOUNT { get; set; }
        /// <summary>
        /// 一般包月帶空值
        /// </summary>
        public string fsRENT { get; set; }
        /// <summary>
        /// Check Sum
        /// </summary>
        public string fsCHECKSUM { get; set; }
        /// <summary>
        /// 付費類型
        /// </summary>
        public string fsAPID { get; set; }
        /// <summary>
        /// 一般包月帶空值
        /// </summary>
        public string fsDIFFMSG { get; set; }
        /// <summary>
        /// 未使用，保留欄位
        /// </summary>
        public string fsRESERVED { get; set; }
    }

    //訂閱結果
    public class clsCHT_SUBSCRIBE
    {
        /// <summary>
        /// 驗證結果(傳回值為"Success"或其它錯誤代碼)
        /// </summary>
        public string fsVALIDATE_SUBSCRIBE { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE_SUBSCRIBE { get; set; }
        /// <summary>
        /// 訂閱碼
        /// </summary>
        public string fsSUBSCRIBENO_SUBSCRIBE { get; set; }
        /// <summary>
        /// 起租狀態
        /// </summary>
        public string fsSTATUS_SUBSCRIBE { get; set; }
        /// <summary>
        /// 包月結束日
        /// </summary>
        public string fsENDDATE_SUBSCRIBE { get; set; }
        /// <summary>
        /// XML本文
        /// </summary>
        public string fsRESULT_SUBSCRIBE { get; set; }
    }

    //中華退租參數
    public class clsCHT_UNSUBSCRIBE_ARGS
    {
        /// <summary>
        /// 交易方式
        /// </summary>
        public string fsCHT_AUTHORITY { get; set; }
        /// <summary>
        /// CP端帳號(AccountID)
        /// </summary>
        public string fsALIAS { get; set; }
        /// <summary>
        /// 訂閱碼
        /// </summary>
        public string fsSUBSCRIBENO { get; set; }
        /// <summary>
        /// 產品代碼
        /// </summary>
        public string fsPRODUCT_ID { get; set; }
        /// <summary>
        /// 動作開始日期(yyyyMMddHHmmss)，空值表示馬上開始
        /// </summary>
        public string fsACTIONDATE { get; set; }
        /// <summary>
        /// 一般包月帶空值
        /// </summary>
        public string fsAMOUNT { get; set; }
        /// <summary>
        /// Check Sum
        /// </summary>
        public string fsCHECKSUM { get; set; }
        /// <summary>
        /// 付費類型
        /// </summary>
        public string fsAPID { get; set; }
        /// <summary>
        /// 未使用，保留欄位
        /// </summary>
        public string fsRESERVED { get; set; }
    }

    //中華退租結果
    public class clsCHT_UNSUBSCRIBE
    {
        /// <summary>
        /// 驗證結果(傳回值為"Success"或其它錯誤代碼)
        /// </summary>
        public string fsVALIDATE_UNSUBSCRIBE { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE_UNSUBSCRIBE { get; set; }
        /// <summary>
        /// XML本文
        /// </summary>
        public string fsRESULT_UNSUBSCRIBE { get; set; }
    }

    //退租參數
    public class clsUNSUBSCRIBE_ARGS
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string fsORDER_NO { get; set; }
        /// <summary>
        /// 操作退租者
        /// </summary>
        public string fsUPDATED_BY { get; set; }
    }

    //查詢訂閱參數
    public class clsCHT_SUBSCRIBE_QUERY_ARGS
    {
        /// <summary>
        /// CP端帳號(AccountID)
        /// </summary>
        public string fsALIAS { get; set; }
        /// <summary>
        /// 訂閱碼
        /// </summary>
        public string fsSUBSCRIBENO { get; set; }
        /// <summary>
        /// 產品代碼
        /// </summary>
        public string fsPRODUCT_ID { get; set; }
        /// <summary>
        /// 付費類型
        /// </summary>
        public string fsAPID { get; set; }
        /// <summary>
        /// 未使用，保留欄位
        /// </summary>
        public string fsRESERVED { get; set; }
        /// <summary>
        /// Check Sum
        /// </summary>
        public string fsCHECKSUM { get; set; }
    }

    //查詢訂閱結果
    public class clsCHT_SUBSCRIBE_QUERY
    {
        /// <summary>
        /// 驗證結果(傳回值為"Success"或其它錯誤代碼)
        /// </summary>
        public string fsVALIDATE_SUBSCRIBE_QUERY { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE_SUBSCRIBE_QUERY { get; set; }
    }

    //查詢訂購紀錄參數
    public class clsGET_MEMBER_PURCHASE_INFO_ARGS
    {
        /// <summary>
        /// 授權碼(若傳空值，表示查詢該會員全部訂購紀錄)
        /// </summary>
        public string fsOTPW { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //查詢訂購紀錄參數(APP用)
    public class clsAPP_GET_MEMBER_PURCHASE_INFO_ARGS
    {
        /// <summary>
        /// 授權碼(若傳空值，表示查詢該會員全部訂購紀錄)
        /// </summary>
        public string fsOTPW { get; set; }
        /// <summary>
        /// 購買來源(配合TV版盒子增加，區分不同盒子廠商，如：TV、AGC、TBOX...)
        /// </summary>
        public string fsFROM { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
    }

    //查詢訂購紀錄參數(盒子用)
    public class clsTV_GET_MEMBER_PURCHASE_INFO_ARGS
    {
        /// <summary>
        /// 授權碼(若傳空值，表示查詢該會員全部訂購紀錄)
        /// </summary>
        public string fsOTPW { get; set; }
        /// <summary>
        /// 購買來源(配合TV版盒子增加，區分不同盒子廠商，如：TV、AGC、TBOX...)
        /// </summary>
        public string fsFROM { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
    }

    //查詢訂購紀錄
    public class clsGET_MEMBER_PURCHASE_INFO
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string fsORDER_NO { get; set; }
        /// <summary>
        /// 訂單日期
        /// </summary>
        public string fdORDER_DATE { get; set; }
        /// <summary>
        /// 套餐代碼
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
        /// <summary>
        /// 套餐名稱
        /// </summary>
        public string fsSET_NAME { get; set; }
        /// <summary>
        /// 套餐項目名稱
        /// </summary>
        public string fsSET_ITEM_NAME { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        public string fsPAY_TYPE { get; set; }
        /// <summary>
        /// 付款方式名稱
        /// </summary>
        public string fsPAY_TYPE_NAME { get; set; }
        /// <summary>
        /// 金額
        /// </summary>
        public int fnFEE { get; set; }
        /// <summary>
        /// 繳款狀態
        /// </summary>
        public string fsPAY_STATUS { get; set; }
        /// <summary>
        /// 使用效期
        /// </summary>
        public string fdEND_DATE { get; set; }
        /// <summary>
        /// 繳款期限
        /// </summary>
        public string fsEXPIRATIONDATE { get; set; }
        /// <summary>
        /// 銀行代碼
        /// </summary>
        public string fsBANKCODE { get; set; }
        /// <summary>
        /// 轉帳帳號
        /// </summary>
        public string fsUID { get; set; }
        /// <summary>
        /// 優惠碼
        /// </summary>
        public string fsPROMO_CODE { get; set; }
        /// <summary>
        /// 套餐項目代碼
        /// </summary>
        public string fsPACKAGE_ID { get; set; }
        /// <summary>
        /// 盒子機器號碼
        /// </summary>
        public string fsMACHINE_ID { get; set; }
    }

    //查詢可使用服務參數(盒子用)
    public class clsTV_GET_MEMBER_SERVICE_INFO_ARGS
    {
        /// <summary>
        /// 購買來源(配合TV版盒子增加，區分不同盒子廠商，如：TV、AGC、TBOX...)
        /// </summary>
        public string fsFROM { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
    }

    //查詢可使用服務
    public  class clsGET_MEMBER_SERVICE_INFO
    {
        /// <summary>
        /// 套餐代號
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
        /// <summary>
        /// 套餐項目
        /// </summary>
        public string fsPURCHASE_NAME { get; set; }
        /// <summary>
        /// 起始時間
        /// </summary>
        public string fdSTART_DATE { get; set; }
        /// <summary>
        /// 到期日
        /// </summary>
        public string fdEND_DATE { get; set; }
        /// <summary>
        /// 盒子機器號碼
        /// </summary>
        public string fsMACHINE_ID { get; set; }
    }

    //啟用優惠碼參數
    public class clsREGISTER_COUPON_ARGS
    {
        /// <summary>
        /// 優惠碼
        /// </summary>
        public string fsCOUPON_NO { get; set; }
        /// <summary>
        /// 登入驗證碼
        /// </summary>
        public string fsVALIDATE_CODE { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //啟用優惠碼參數(手機用)
    public class clsAPP_REGISTER_COUPON_ARGS
    {
        /// <summary>
        /// 優惠碼
        /// </summary>
        public string fsCOUPON_NO { get; set; }
        /// <summary>
        /// 驗證碼
        /// </summary>
        public string fsVALIDATE_CODE { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
    }
}