﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{

    public class clsTV
    {
    }

    /// 取得套餐
    public class clsGET_CHANNEL_SET
    {
        
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
        /// <summary>
        /// 來源
        /// </summary>
        public string fsFROM { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }

    }

    /// 會員與裝置綁訂
    public class clsMEMBER_MACHINE_BINDING
    {

        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
        /// <summary>
        /// 來源
        /// </summary>
        public string fsFROM { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }

    }

    /// <summary>
    /// 帳號權限轉移
    /// </summary>
    public class clsACCOUNT_MERGE
    {
        public string fsFROM { get; set; }
        public string fsVALUE_OLD { get; set; }
        public string fsVALUE { get; set; }
        public string fsENC_KEY { get; set; }
    }

    /// 新增訂單
    public class clsPURCHASE
    {

        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
        /// <summary>
        /// 來源
        /// </summary>
        public string fsFROM { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }

    }

    /// <summary>
    /// 訪客登入
    /// </summary>
    public class clsGUEST_SIGN_UP
    {
        //public string fsDEVICE_ID { get; set; }
        public string fsPARTNER_VALUE1 { get; set; }
        public string fsFROM { get; set; }
        public string fsENC_KEY { get; set; }
    }
}
