﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsLOG
    {
    }

    public class clsLOG_ARGS
    {
        public string fsTYPE { get; set; }
        public string fsNAME { get; set; }
        public string fsCONTENT { get; set; }
        public string fsIP { get; set; }
        public string fsCREATED_BY { get; set; }
    }

    public class clsLOG_URL_ARGS
    {
        public string fsFROM { get; set; }
        public string fsNAME { get; set; }
        public string fsUSER_ID { get; set; }
        public string fsASSET_ID { get; set; }
        public string fsIP { get; set; }
        public string fsCOUNTRY { get; set; }
        public string fsUSER_AGENT { get; set; }
    }

    public class clsLOG_SIGNIN_ARGS
    {
        public string fsFROM { get; set; }
        public string fsUSER_ID { get; set; }
        public string fsIP { get; set; }
        public string fsCOUNTRY { get; set; }
        public string fsUSER_AGENT { get; set; }
    }

    public class clsLOG_MEMBER_PROMO_ARGS
    {
        public string fsACCOUNT_ID { get; set; }
        public string fsDEVICE { get; set; }
        public string fsVERSION { get; set; }
        public string fsENC_KEY { get; set; }
        public string fsIP { get; set; }
        public string fsCOUNTRY { get; set; }
        public string fsUSER_AGENT { get; set; }
    }

    public class clsLOG_MEMBER_REGISTER_ARGS
    {
        public string fsUSER_ID { get; set; }
        public string fsDEVICE { get; set; }
        public string fsVERSION { get; set; }
        public string fsENC_KEY { get; set; }
        public string fsIP { get; set; }
        public string fsCOUNTRY { get; set; }
        public string fsUSER_AGENT { get; set; }
    }

    public class clsLOG_BUFFER_TIME_CONDITION
    {
        /// <summary>
        /// ID
        /// </summary>
        public long? fnID { get; set; }
        /// <summary>
        /// 事件
        /// </summary>
        public string fsEvent { get; set; }
        /// <summary>
        /// 使用影像類型(CHANNEL ,VOD)
        /// </summary>
        public string fsAsset_Name { get; set; }
        /// <summary>
        /// 串流ID
        /// </summary>
        public string fsAsset_ID { get; set; }
        /// <summary>
        /// 解析度類別(0=360P,1=480P,2=720P,3=1080P)
        /// </summary>
        public int? fnBitrate_Type { get; set; }
        /// <summary>
        /// 使用無線網路(0=wifi,1=4G,99=其他)
        /// </summary>
        public int? fnNetwork_Type { get; set; }
        /// <summary>
        /// IP
        /// </summary>
        public string fsIP { get; set; }
        /// <summary>
        /// Buffer時間(秒)
        /// </summary>
        public int? fnBufferTime { get; set; }
        /// <summary>
        /// 作業系統
        /// </summary>
        public string fsOS { get; set; }
        /// <summary>
        /// 作業系統版本
        /// </summary>
        public string fsOS_Version { get; set; }
        /// <summary>
        /// 裝置廠商
        /// </summary>
        public string fsDevice_Vendor { get; set; }
        /// <summary>
        /// 裝置
        /// </summary>
        public string fsDevice_Type { get; set; }
        /// <summary>
        /// 瀏覽器
        /// </summary>
        public string fsBrowser { get; set; }
        /// <summary>
        /// 瀏覽器版本
        /// </summary>
        public string fsBrowser_Version { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string fsDesc { get; set; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public DateTime? fdCreated_Date { get; set; }
        /// <summary>
        /// BufferStart
        /// </summary>
        public DateTime? fdBufferStart { get; set; }
    }
    public class clsLOG_BUFFER_TIME_REQ
    {
        /// <summary>
        /// 事件
        /// </summary>
        public string fsEvent { get; set; }
        /// <summary>
        /// 使用影像類型(CHANNEL ,VOD)
        /// </summary>
        [Required(ErrorMessage = "請輸入fsAsset_Name")]
        public string fsAsset_Name { get; set; }
        /// <summary>
        /// 串流ID
        /// </summary>
        [Required(ErrorMessage = "請輸入fsAsset_ID")]
        public string fsAsset_ID { get; set; }
        /// <summary>
        /// 解析度類別(0=360P,1=480P,2=720P,3=1080P)
        /// </summary>
        [Required(ErrorMessage = "請輸入fnBitrate_Type")]
        public int? fnBitrate_Type { get; set; }
        /// <summary>
        /// 使用無線網路(0=wifi,1=4G,99=其他)
        /// </summary>
        [Required(ErrorMessage = "請輸入fnNetwork_Type")]
        public int? fnNetwork_Type { get; set; }
        /// <summary>
        /// Buffer時間(秒)
        /// </summary>
        [Required(ErrorMessage = "請輸入fnBufferTime")]
        public int? fnBufferTime { get; set; }
        /// <summary>
        /// 作業系統
        /// </summary>
        public string fsOS { get; set; }
        /// <summary>
        /// 作業系統版本
        /// </summary>
        public string fsOS_Version { get; set; }
        /// <summary>
        /// 裝置廠商
        /// </summary>
        public string fsDevice_Vendor { get; set; }
        /// <summary>
        /// 裝置
        /// </summary>
        public string fsDevice_Type { get; set; }
        /// <summary>
        /// 瀏覽器
        /// </summary>
        public string fsBrowser { get; set; }
        /// <summary>
        /// 瀏覽器版本
        /// </summary>
        public string fsBrowser_Version { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string fsDesc { get; set; }
        /// <summary>
        /// BufferStart
        /// </summary>
        public long fnBufferStart { get; set; }

    }

    //帳號連結
    public class clsLOG_ACTIVITY_11MINUTE_REQ
    {
        /// <summary>
        /// 驗證身分(WEB)
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
        /// <summary>
        /// 驗證身分(APP)
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }

    }

    //帳號連結
    public class clsLOG_ACTIVITY_11MINUTE_CONDITION
    {
        /// <summary>
        /// 會員ID
        /// </summary>
        public string fsACCOUNT_ID { get; set; }
        /// <summary>
        /// IP
        /// </summary>
        public string fsIP { get; set; }
        /// <summary>
        /// 建立日期
        /// </summary>
        public DateTime? fdCreated_Date { get; set; }
        public DateTime? fdCreated_Date_Start { get; set; }
        public DateTime? fdCreated_Date_End { get; set; }

    }


}