﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{

    //新增Channel PageView
    public class clsINSERT_CHANNEL_PAGEVIEW_ARGS
    {
        public string fs4GTV_ID { get; set; }
    }

    //取得LiTV頻道URL
    public class clsGET_CHANNEL_URL_ARGS
    {
        /// <summary>
        /// 頻道編號
        /// </summary>
        public int fnCHANNEL_ID { get; set; }
        /// <summary>
        /// LiTV AssetId = 4gtv-ID
        /// </summary>
        public string fsASSET_ID { get; set; }
        /// <summary>
        /// 裝置類型(pc/mobile)
        /// </summary>
        public string fsDEVICE_TYPE { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

     //取得LiTV頻道URL
    public class clsGET_CHANNEL_URL_ARGS2
    {
        /// <summary>
        /// AES_value
        /// </summary>
        public string value { get; set; }
  
    }

    //取得LiTV頻道URL(給OVO用)
    public class clsGET_CHANNEL_URL_OVO_ARGS
    {
        /// <summary>
        /// LiTV AssetId = 4gtv-ID
        /// </summary>
        public string fsASSET_ID { get; set; }
        /// <summary>
        /// 裝置類型(pc/mobile)
        /// </summary>
        public string fsDEVICE_TYPE { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //取得LiTV頻道URL(TV用)
    public class clsTV_GET_CHANNEL_URL_ARGS
    {
        /// <summary>
        /// 頻道編號
        /// </summary>
        public int fnCHANNEL_ID { get; set; }
        /// <summary>
        /// LiTV AssetId = 4gtv-ID
        /// </summary>
        public string fsASSET_ID { get; set; }
        /// <summary>
        /// 裝置類型(TV)
        /// </summary>
        public string fsDEVICE_TYPE { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
    }

    //取得LiTV頻道URL(手機用)
    public class clsAPP_GET_CHANNEL_URL_ARGS
    {
        /// <summary>
        /// 頻道編號
        /// </summary>
        public int fnCHANNEL_ID { get; set; }
        /// <summary>
        /// LiTV AssetId = 4gtv-ID
        /// </summary>
        public string fsASSET_ID { get; set; }
        /// <summary>
        /// 裝置類型(pc/mobile)
        /// </summary>
        public string fsDEVICE_TYPE { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
    }

    //取得LiTV CHANNEL URL
    public class clsGET_CHANNEL_URL
    {
        public string fsCHANNEL_NAME { get; set; }
        public List<string> flstURLs { get; set; }
        public string fsHEAD_FRAME { get; set; }
        public bool fbPLAY_AD { get; set; }
        public List<int> flstBITRATE { get; set; }
        public int fnALLOWTIME { get; set; }



    }

    //取得LiTV CHANNEL URL
    public class clsGET_CHANNEL_APP_URL
    {
        public string fsCHANNEL_NAME { get; set; }
        public List<string> flstURLs { get; set; }
        public string fsHEAD_FRAME { get; set; }
        public bool fbPLAY_AD { get; set; }
        public List<int> flstBITRATE { get; set; }
        public int fnALLOWTIME { get; set; }
    }

    //取得LiTV CHANNEL URL (TV用)
    public class clsGET_CHANNEL_TV_URL
    {
        public string fsCHANNEL_NAME { get; set; }
        public List<string> flstURLs { get; set; }
        public string fsHEAD_FRAME { get; set; }
        public bool fbPLAY_AD { get; set; }
        public int fnFREESTARTTIME { get; set; }
        public int fnALLOWTIME { get; set; }
        public List<int> flstBITRATE { get; set; }
    }

    //取得頻道套餐
    public class clsCHANNEL_SET
    {
        /// <summary>
        /// 套餐編號
        /// </summary>
        public int fnID { get; set; }
        /// <summary>
        /// 套餐名稱
        /// </summary>
        public string fsNAME { get; set; }
        /// <summary>
        /// 是否免費
        /// </summary>
        public bool fcFREE { get; set; }
        /// <summary>
        /// 是否允許海外
        /// </summary>
        public bool fcOVERSEAS { get; set; }
        /// <summary>
        /// 價格
        /// </summary>
        public int fnPRICE { get; set; }
        /// <summary>
        /// 此套餐共有幾個頻道
        /// </summary>
        public int fnCOUNT { get; set; }
    }

    //取得頻道
    public class clsCHANNEL
    {
        /// <summary>
        /// 編號
        /// </summary>
        public int fnID { get; set; }
        /// <summary>
        /// 頻道類型
        /// </summary>
		public string fsTYPE { get; set; }
        /// <summary>
        /// 路線
        /// </summary>
        public string fsCDN_ROUTE { get; set; }
        /// <summary>
        /// 類型名稱
        /// </summary>
        public string fsTYPE_NAME { get; set; }
        /// <summary>
        /// 頻道名稱
        /// </summary>
		public string fsNAME { get; set; }
        /// <summary>
        /// 四季台代號
        /// </summary>
		public string fs4GTV_ID { get; set; }
        /// <summary>
        /// 台號
        /// </summary>
        public int fnCHANNEL_NO { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
		public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 電腦版台徽
        /// </summary>
		public string fsLOGO_PC { get; set; }
        /// <summary>
        /// 手機版台徽
        /// </summary>
		public string fsLOGO_MOBILE { get; set; }
        /// <summary>
        /// 是否允許海外
        /// </summary>
        public bool fcOVERSEAS { get; set; }
        /// <summary>
        /// 排除播映國家
        /// </summary>
        public List<string> lstEXCEPT_COUNTRY { get; set; }
        /// <summary>
        /// 排除播映國家
        /// </summary>
        public List<string> lstALL_BITRATE { get; set; }
        /// <summary>
        /// 免費時的Profile
        /// </summary>
        public string fsFREE_PROFILE { get; set; }
        /// <summary>
        /// 所屬套餐
        /// </summary>
        public List<int> lstSETs { get; set; }
        /// <summary>
        /// 是否免費
        /// </summary>
        public bool fcFREE { get; set; }
        /// <summary>
        /// 畫質
        /// </summary>
        public string fsQUALITY { get; set; }
        /// <summary>
        /// 聊天室編號
        /// </summary>
        public int fnCHAT_ID { get; set; }
        /// <summary>
        /// 是否有節目表
        /// </summary>
        public bool fcHAS_PROGLIST { get; set; }
        /// <summary>
        /// 是否強制按讚
        /// </summary>
        public bool fcLIKE { get; set; }
        /// <summary>
        /// 是否為限制級
        /// </summary>
        public bool fcRISTRICT { get; set; }
        /// <summary>
        /// 終止時間
        /// </summary>
        public string fsEXPIRE_DATE { get; set; }
        /// <summary>
        /// 影片分級(普遍級:01/保護級:02/輔12級:03/輔15級:04/限制級:05)
        /// </summary>
        public int fnGRADED { get; set; }
        public string fsVENDOR { get; set; }
        public string fsDESCRIPTION { get; set; }
    }

    //取得頻道套餐介紹
    public class clsCHANNEL_SET_INTRODUCTION
    {
        /// <summary>
        /// 頻道編號
        /// </summary>
        public int fnCHANNEL_ID { get; set; }
        /// <summary>
        /// 頻道名稱
        /// </summary>
        public string fsCHANNEL_NAME { get; set; }
        /// <summary>
        /// 台號
        /// </summary>
        public string fsCHANNEL_NO { get; set; }
        /// <summary>
        /// 畫質
        /// </summary>
        public string fsQUALITY { get; set; }
        /// <summary>
        /// 頻道類型
        /// </summary>
        public string fsTYPE_NAME { get; set; }
        /// <summary>
        /// 是否免費
        /// </summary>
        public bool fcFREE { get; set; }
        /// <summary>
        /// 頻道套餐相關資訊
        /// </summary>
        public List<clsCHANNEL_SET_INFO> lstCHANNEL_SET_INFO { get; set; }

        public class clsCHANNEL_SET_INFO
        {
            /// <summary>
            /// 套餐編號
            /// </summary>
            public int fnSET_ID { get; set; }
            /// <summary>
            /// 是否包含
            /// </summary>
            public bool fbINCLUDE { get; set; }
        }
    }

    //取得會員頻道權限參數
    public class clsGET_MEMBER_CHANNEL_AUTH_ARGS
    {
        public string fsACCOUNT_ID { get; set; }
        public string fs4GTV_ID { get; set; }
    }

    //取得會員頻道權限
    public class clsGET_MEMBER_CHANNEL_AUTH
    {
        public int fnID { get; set; }
        public string fs4GTV_ID { get; set; }
        public bool fcAUTH { get; set; }
        public bool fcAD { get; set; }
        public bool fcIS_PAY_MEMBER { get; set; }
    }

    //取得OVO權限參數
    public class clsGET_CHANNEL_AUTH_FROM_OVO_ARGS
    {
        public string account { get; set; }
        public string channelId { get; set; }
    }

    //取得OVO權限
    public class clsGET_CHANNEL_AUTH_FROM_OVO
    {
        public string code { get; set; }
        public clsGET_CHANNEL_AUTH_FROM_OVO_DATA data { get; set; }

        public class clsGET_CHANNEL_AUTH_FROM_OVO_DATA
        {
            public bool result { get; set; }
        }
    }
}