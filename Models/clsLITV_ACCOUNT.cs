﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsLITV_ACCOUNT
    {
    }

    #region 登入帳號
    public class clsLITV_LOGIN_ARGU
    {
        public string jsonrpc { get; set; }
        public int id { get; set; }
        public string method { get; set; }

        public clsLITV_LOGIN_PARMS Params { get; set; }

        public class clsLITV_LOGIN_PARMS
        {
            public string User { get; set; }
            public string Pass { get; set; }
            public string DeviceId { get; set; }
            public string Swver { get; set; }
            public string ModelInfo { get; set; }
            public string project_num { get; set; }
        }
    }

    public class clsLITV_LOGIN
    {
        public string jsonrpc { get; set; }
        public clsLI_LOGIN_RESULT result { get; set; }
        public clsLI_LOGIN_ERROR error { get; set; }
        public string id { get; set; }


        public class clsLI_LOGIN_RESULT
        {
            public string AccountId { get; set; }
            public string Token { get; set; }
            public string DeviceId { get; set; }
            public clsLI_LOGIN_BSMRESULT BSMResult { get; set; }


            public class clsLI_LOGIN_BSMRESULT
            {
                public string Result_code { get; set; }
                public string Result_message { get; set; }
                public clsLI_LOGIN_PURCHASE Purchase { get; set; }
                public clsLI_LOGIN_CLIENT Client { get; set; }
                public clsLI_LOGIN_MESSAGE message { get; set; }

                public class clsLI_LOGIN_PURCHASE
                {
                    public string Details { get; set; }
                }

                public class clsLI_LOGIN_CLIENT
                {
                    public int Region { get; set; }
                    public string client_id { get; set; }
                    public string mac_address { get; set; }
                    public string client_status { get; set; }
                    public string owner_phone_no { get; set; }
                }

                public class clsLI_LOGIN_MESSAGE
                {
                    public string subject { get; set; }
                    public string body { get; set; }
                }
            }
        }

        public class clsLI_LOGIN_ERROR
        {
            public string code { get; set; }
            public string message { get; set; }
            public string data { get; set; }
        }
    }

    #endregion
}