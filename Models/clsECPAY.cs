﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsECPAY
    {
    }

    //購買參數
    public class clsECPAY_PURCHASE_ARGS
    {
        /// <summary>
        /// 訂單session唯一ID
        /// </summary>
        public string fsSESSION_ID { get; set; }
        /// <summary>
        /// 套餐代號(如CHANNEL4G_FULL)
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
        /// <summary>
        /// 付款來源(如ECPAY)
        /// </summary>
        public string fsPAY_FROM { get; set; }
        /// <summary>
        /// 付款方式編號
        /// </summary>
        public int fnPAY_TYPE_ID { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        public string fsPAY_TYPE_NAME { get; set; }
        /// <summary>
        /// 產品編號
        /// </summary>
        public string fsCHT_PRODUCT_ID { get; set; }
        /// <summary>
        /// 購買方案ID(如CH4GGOOD04、CH4G01)
        /// </summary>
        public string fsPACKAGE_ID { get; set; }
        /// <summary>
        /// 用戶姓名
        /// </summary>
        public string fsINV_NAME { get; set; }
        /// <summary>
        /// 用戶發票寄送地址
        /// </summary>
        public string fsINV_ADDRESS2 { get; set; }
        /// <summary>
        /// 用戶電話
        /// </summary>
        public string fsINV_TEL { get; set; }
        /// <summary>
        /// 發票狀態
        /// </summary>
        public string fsINV_STATUS { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //ECPAY訂單回應結果
    public class clsECPAY_TRADE_RESULT
    {
        /// <summary>
        /// 交易狀態
        /// </summary>
        public string RtnCode { get; set; }
        /// <summary>
        /// 交易訊息
        /// </summary>
        public string RtnMsg { get; set; }
        /// <summary>
        /// 特店交易識別碼
        /// </summary>
        public string SPToken { get; set; }
        /// <summary>
        /// 特店編號
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string MerchantTradeNo { get; set; }
        /// <summary>
        /// 檢查碼
        /// </summary>
        public string CheckMacValue { get; set; }
    }

    //ECPAY ATM取號結果
    public class clsECPAY_ATM_RESULT
    {
        /// <summary>
        /// 特店編號
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string MerchantTradeNo { get; set; }
        /// <summary>
        /// 特店商店代碼
        /// </summary>
        public string StoreID { get; set; }
        /// <summary>
        /// 交易狀態
        /// </summary>
        public int RtnCode { get; set; }
        /// <summary>
        /// 交易訊息
        /// </summary>
        public string RtnMsg { get; set; }
        /// <summary>
        /// 綠界的交易編號
        /// </summary>
        public string TradeNo { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public int TradeAmt { get; set; }
        /// <summary>
        /// 特店選擇的付款方式
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// 訂單成立時間
        /// </summary>
        public string TradeDate { get; set; }
        /// <summary>
        /// 自訂名稱欄位1
        /// </summary>
        public string CustomField1 { get; set; }
        /// <summary>
        /// 自訂名稱欄位2
        /// </summary>
        public string CustomField2 { get; set; }
        /// <summary>
        /// 自訂名稱欄位3
        /// </summary>
        public string CustomField3 { get; set; }
        /// <summary>
        /// 自訂名稱欄位4
        /// </summary>
        public string CustomField4 { get; set; }
        /// <summary>
        /// 檢查碼
        /// </summary>
        public string CheckMacValue { get; set; }
        /// <summary>
        /// 繳費銀行代碼
        /// </summary>
        public string BankCode { get; set; }
        /// <summary>
        /// 繳費虛擬帳號
        /// </summary>
        public string vAccount { get; set; }
        /// <summary>
        /// 繳費期限
        /// </summary>
        public string ExpireDate { get; set; }
    }
    
    //ECPAY付款回應結果
    public class clsECPAY_PAYMENT_RESULT
    {
        /// <summary>
        /// 特店編號
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string MerchantTradeNo { get; set; }
        /// <summary>
        /// 特店商店代碼
        /// </summary>
        public string StoreID { get; set; }
        /// <summary>
        /// 交易狀態
        /// </summary>
        public int RtnCode { get; set; }
        /// <summary>
        /// 交易訊息
        /// </summary>
        public string RtnMsg { get; set; }
        /// <summary>
        /// 綠界的交易編號
        /// </summary>
        public string TradeNo { get; set; }
        /// <summary>
        /// 交易金額
        /// </summary>
        public int TradeAmt { get; set; }
        /// <summary>
        /// 付款時間
        /// </summary>
        public string PaymentDate { get; set; }
        /// <summary>
        /// 特店選擇的付款方式
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// 交易手續費
        /// </summary>
        public int PaymentTypeChargeFee { get; set; }
        /// <summary>
        /// 訂單成立時間
        /// </summary>
        public string TradeDate { get; set; }
        /// <summary>
        /// 是否為模擬付款
        /// </summary>
        public int SimulatePaid { get; set; }
        /// <summary>
        /// 自訂名稱欄位1
        /// </summary>
        public string CustomField1 { get; set; }
        /// <summary>
        /// 自訂名稱欄位2
        /// </summary>
        public string CustomField2 { get; set; }
        /// <summary>
        /// 自訂名稱欄位3
        /// </summary>
        public string CustomField3 { get; set; }
        /// <summary>
        /// 自訂名稱欄位4
        /// </summary>
        public string CustomField4 { get; set; }
        /// <summary>
        /// 檢查碼
        /// </summary>
        public string CheckMacValue { get; set; }


    }

    //ECPAY信用卡付款(補付款用)
    public class clsECPAY_FOR_CREDIT_ARGS
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string fsORDER_NO { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }
}