﻿// ============================================= 
// 描述: Config類別 
// 記錄: <2016/08/31><David.Sin><新增本程式> 
// ============================================= 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsCODE
    {
        public string fsCODE_ID { get; set; }
        public string fsCODE { get; set; }
        public string fsNAME { get; set; }
        public int fnORDER { get; set; }
        public string fsSET { get; set; }
        public string fsDESCRIPTION { get; set; }
        public bool fcACTIVE { get; set; }
        public string fdCREATED_DATE { get; set; }
        public string fsCREATED_BY { get; set; }
        public string fdUPDATED_DATE { get; set; }
        public string fsUPDATED_BY { get; set; }

    }
}