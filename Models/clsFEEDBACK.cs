﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsFEEDBACK
    {
    }

    //新增回饋參數
    public class clsINSERT_FEEDBACK_ARGS
    {
        /// <summary>
        /// USER ID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// 回饋類型
        /// </summary>
        public string fsTYPE { get; set; }
        /// <summary>
        /// 作業系統
        /// </summary>
        public string fsOS { get; set; }
        /// <summary>
        /// 作業系統版本
        /// </summary>
        public string fsOS_VERSION { get; set; }
        /// <summary>
        /// 裝置品牌
        /// </summary>
        public string fsDEVICE_VENDOR { get; set; }
        /// <summary>
        /// 裝置型號
        /// </summary>
        public string fsDEVICE_TYPE { get; set; }
        /// <summary>
        /// 內容
        /// </summary>
        public string fsCONTENT { get; set; }
    }
}