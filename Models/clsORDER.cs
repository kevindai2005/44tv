﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsORDER
    {
        public string fsORDER_NO { get; set; }
        public string fsOTPW { get; set; }
        public string fsAUTHORITY { get; set; }
        public string fsFEE { get; set; }
        public string fsPURCHASE_ID { get; set; }
        public string fsPACKAGE_ID { get; set; }
        public string fsPAY_TYPE_NAME { get; set; }
    }
}