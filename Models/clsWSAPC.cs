﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsWSAPC
    {
      
    }

    public class clsGET_WSAPC_INSERT_MEMBER
    {
        /// <summary>
        /// 帳號/手機號碼
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// 其他登入帳號
        /// </summary>
        public string fsLINK_ID { get; set; }
        /// <summary>
        /// 密碼
        /// </summary>
        public string fsPASSWORD { get; set; }
        /// <summary>
        /// 出生年(西元)
        /// </summary>
        public int fnBIRTH_YEAR { get; set; }
        /// <summary>
        /// 性別(male/female)
        /// </summary>
        public string fsSEX { get; set; }
        /// <summary>
        /// 註冊方式(1:Email、2:手機)
        /// </summary>
        public int? fnREGISTER_TYPE { get; set; }
    }

    public class clsGET_WSAPC_INSERT_ORDER
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// 套餐代碼(豪華餐CHANNELWS_FULL)
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
        /// <summary>
        /// 付款方案(每月自動續訂CHWS01)
        /// </summary>
        public string fsPACKAGE_ID { get; set; }
    }
    public class clsGET_WSAPC_CANCEL_ORDER
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// 套餐代碼(豪華餐CHANNELWS_FULL)
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
    }
   
    

}