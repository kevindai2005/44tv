﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsLITV_SERVICE
    {
    }

    #region 服務位置
    public class clsLI_GET_CONFIG_NO_AUTH_ARGS
    {
        public string jsonrpc { get; set; }
        public int id { get; set; }
        public string method { get; set; }

        public clsLI_GET_CONFIG_NO_AUTH_PARAMS Params { get; set; }

        public class clsLI_GET_CONFIG_NO_AUTH_PARAMS
        {
            public string device_id { get; set; }
            public string project_num { get; set; }
            public string swver { get; set; }
            public List<string> services { get; set; }
        }
    }

    public class clsLI_GET_CONFIG_NO_AUTH
    {
        public string jsonrpc { get; set; }
        public clsLI_GET_CONFIG_NO_AUTH_RESULT result { get; set; }

        public class clsLI_GET_CONFIG_NO_AUTH_RESULT
        {
            public List<string> AccountService { get; set; }
            public List<string> CCCService { get; set; }
            public List<string> ConfigService { get; set; }
            public List<string> LoadService { get; set; }
            public List<string> ClientVarService { get; set; }
            public string detected_ip { get; set; }
            public List<string> epg_sqlite { get; set; }
            public string isp { get; set; }
            public List<string> pics { get; set; }
            public string service_id { get; set; }
            public string session_api { get; set; }
        }
        public string id { get; set; }
    }
    #endregion
}