﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    //取得變數
    public class clsGET_CLIENTVAR_ARGS
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// 帳號資訊
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
        /// <summary>
        /// 是否為海外
        /// </summary>
        public string fcREGION { get; set; }
    }

    //取得變數
    public class clsAPP_GET_CLIENTVAR_ARGS
    {
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// 帳號資訊
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
        /// <summary>
        /// 是否為海外
        /// </summary>
        public string fcREGION { get; set; }
    }

    //取得變數
    public class clsGET_CLIENTVAR
    {
        /// <summary>
        /// VOD類型(drama、show、scene)
        /// </summary>
        public string fsTYPE { get; set; }
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// VOD段落
        /// </summary>
        public string fsPART { get; set; }
        /// <summary>
        /// 觀看時間(秒)
        /// </summary>
        public long fnTIME { get; set; }
        /// <summary>
        /// 代表圖
        /// </summary>
        public string fsHEAD_FRAME { get; set; }
        /// <summary>
        /// 標題
        /// </summary>
        public string fsTITLE { get; set; }
        /// <summary>
        /// 集數
        /// </summary>
        public int fnEPISODE { get; set; }
        /// <summary>
        /// 影片源
        /// </summary>
        public string fsVIDEO_FROM { get; set; }
    }
    
    //設定變數參數
    public class clsSET_CLIENTVAR_ARGS
    {
        /// <summary>
        /// VOD類型(drama、show、scene)
        /// </summary>
        public string fsTYPE { get; set; }
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// VOD段落
        /// </summary>
        public string fsPART { get; set; }
        /// <summary>
        /// 觀看時間(秒)
        /// </summary>
        public long fnTIME { get; set; }
        /// <summary>
        /// 帳號驗證
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //設定變數參數(手機用)
    public class clsAPP_SET_CLIENTVAR_ARGS
    {
        /// <summary>
        /// VOD類型(drama、show、scene)
        /// </summary>
        public string fsTYPE { get; set; }
        /// <summary>
        /// VOD編號
        /// </summary>
        public string fsVOD_NO { get; set; }
        /// <summary>
        /// VOD序號
        /// </summary>
        public int fnSEQ { get; set; }
        /// <summary>
        /// VOD段落
        /// </summary>
        public string fsPART { get; set; }
        /// <summary>
        /// 觀看時間(秒)
        /// </summary>
        public long fnTIME { get; set; }
        /// <summary>
        /// 帳號驗證
        /// </summary>
        public clsAPP_IDENTITY_VALIDATE_ARUS clsAPP_IDENTITY_VALIDATE_ARUS { get; set; }
    }

    //設定變數
    public class clsSET_CLIENTVAR
    {
        /// <summary>
        /// 結果
        /// </summary>
        public bool fbRESULT { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE { get; set; }
    }


}