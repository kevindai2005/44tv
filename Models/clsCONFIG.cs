﻿// ============================================= 
// 描述: Config類別 
// 記錄: <2016/08/31><David.Sin><新增本程式> 
// ============================================= 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsCONFIG
    {
        public string fsKEY { get; set; }
        public string fsVALUE { get; set; }
        public string fsTYPE { get; set; }
        public string fsDESCRIPTION { get; set; }

        public string fdCREATED_DATE { get; set; }
        public string fsCREATED_BY { get; set; }
        public string fdUPDATED_DATE { get; set; }
        public string fsUPDATED_BY { get; set; }
    }
}