﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsACCOUNT
    {
    }

    //驗證登入參數
    public class clsIDENTITY_VALIDATE_ARUS
    {
        /// <summary>
        /// 加密後的Cookie
        /// </summary>
        public string fsVALUE { get; set; }
    }

    //使用者資訊
    public class clsUSER_DATA
    {
        /// <summary>
        /// AccountId
        /// </summary>
        public string fsACCOUNT_ID { get; set; }
        /// <summary>
        /// 帳號/手機
        /// </summary>
        public string fsUSER { get; set; }
    }

    //登入參數
    public class clsLOGIN_ARGS
    {
        /// <summary>
        /// 會員帳號
        /// </summary>
        public string fsUSER { get; set; }
        /// <summary>
        /// 密碼
        /// </summary>
        public string fsPASSWORD { get; set; }
        /// <summary>
        /// 登入驗證碼
        /// </summary>
        public string fsVALIDATE_CODE { get; set; }
    }

    //登入
    public class clsLOGIN
    {
        /// <summary>
        /// 會員編號
        /// </summary>
        public string fsACCOUNT_ID { get; set; }
        /// <summary>
        /// 會員帳號
        /// </summary>
        public string fsUSER { get; set; }
    }

    //取得帳號狀態參數
    public class clsACCOUNT_STATUS_ARGS
    {
        /// <summary>
        /// 帳號/手機號碼
        /// </summary>
        public string fsUSER { get; set; }
    }

    //帳號狀態
    public class clsACCOUNT_STATUS
    {
        /// <summary>
        /// 是否註冊
        /// </summary>
        public bool Registered { get; set; }
        /// <summary>
        /// 是否已驗證
        /// </summary>
        public bool Validated { get; set; }
        /// <summary>
        /// 註冊方式(1:Email、2:手機)
        /// </summary>
        public int Register_type { get; set; }
    }

    //查詢非四季帳號是否已連結
    public class clsACCOUNT_IS_LINK_ARGS
    {
        /// <summary>
        /// 登入類型(02:FB、03:GOOGLE)
        /// </summary>
        public string fsLOGIN_TYPE { get; set; }
        /// <summary>
        /// 非四季其它帳號
        /// </summary>
        public string fsLINK_ID { get; set; }
    }

    //帳號連結
    public class clsINSERT_ACCOUNT_LINK_ARGS
    {
        /// <summary>
        /// 登入類型(02:FB、03:GOOGLE)
        /// </summary>
        public string fsLOGIN_TYPE { get; set; }
        /// <summary>
        /// 非四季其它帳號
        /// </summary>
        public string fsLINK_ID { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //刪除帳號連結
    public class clsDELETE_ACCOUNT_LINK_ARGS
    {
        /// <summary>
        /// 登入類型(01:四季、02:FB、03:GOOGLE)
        /// </summary>
        public string fsLOGIN_TYPE { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }

    }

    //修改密碼
    public class clsEDIT_PASSWORD
    {
        /// <summary>
        /// 舊密碼
        /// </summary>
        public string fsPASSWORD_OLD { get; set; }
        /// <summary>
        /// 新密碼
        /// </summary>
        public string fsPASSWORD_NEW { get; set; }
        /// <summary>
        /// 確認新密碼
        /// </summary>
        public string fsPASSWORD_NEW1 { get; set; }
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //重設密碼
    public class clsRESET_PASSWORD
    {
        /// <summary>
        /// 帳號/手機號碼
        /// </summary>
        public string fsUSER { get; set; }
        /// <summary>
        /// 通行碼
        /// </summary>
        public string fsPASSCODE { get; set; }
        /// <summary>
        /// 新密碼
        /// </summary>
        public string fsPASSWORD { get; set; }
        /// <summary>
        /// 確認密碼
        /// </summary>
        public string fsPASSWORD1 { get; set; }
    }

    //取得通行碼參數
    public class clsGET_PASSCODE_ARGS
    {
        /// <summary>
        /// 註冊方式(1:Email、2:簡訊)
        /// </summary>
        public int fnREGISTER_TYPE { get; set; }
        /// <summary>
        /// EMAIL/手機號碼
        /// </summary>
        public string fsUSER { get; set; }
        /// <summary>
        /// 驗證類型(1:會員註冊，2:忘記密碼)
        /// </summary>
        public string fsVALIDATE_TYPE { get; set; }
    }

    //通行碼
    public class clsPASSCODE
    {
        /// <summary>
        /// 通行碼
        /// </summary>
        public string fsPASSCODE { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE { get; set; }
    }

    //驗證通行碼參數
    public class clsCONFIRM_PASSCODE_ARGS
    {
        /// <summary>
        /// 帳號/手機號碼
        /// </summary>
        public string fsUSER { get; set; }
        /// <summary>
        /// 驗證類型(1:會員註冊，2:忘記密碼)
        /// </summary>
        public string fsVALIDATE_TYPE { get; set; }
        /// <summary>
        /// 通行碼
        /// </summary>
        public string fsPASSCODE { get; set; }
    }
    
    //驗證通行碼
    public class clsCONFIRM_PASSCODE
    {
        /// <summary>
        /// 結果
        /// </summary>
        public bool fbRESULT { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE { get; set; }
    }

    //帳號註冊
    public class clsREGISTER_ARGS : IValidatableObject
    {
        /// <summary>
        /// 註冊方式(1:Email、2:手機)
        /// </summary>
        public int fnREGISTER_TYPE { get; set; }
        /// <summary>
        /// 帳號/手機號碼
        /// </summary>
        public string fsUSER { get; set; }
        /// <summary>
        /// 連結登入類型(02:FB、03:GOOGLE)
        /// </summary>
        public string fsLOGIN_TYPE { get; set; }
        /// <summary>
        /// 其他登入帳號
        /// </summary>
        public string fsLINK_ID { get; set; }
        /// <summary>
        /// 密碼
        /// </summary>
        public string fsPASSWORD { get; set; }
        /// <summary>
        /// 確認密碼
        /// </summary>
        public string fsPASSWORD1 { get; set; }
        /// <summary>
        /// 出生年(西元)
        /// </summary>
        public int fnBIRTH_YEAR { get; set; }
        /// <summary>
        /// 性別(male/female)
        /// </summary>
        public string fsSEX { get; set; }

        //自我驗證商業邏輯
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (fnBIRTH_YEAR <= 0)
                yield return new ValidationResult("請選擇出生年!", new[] { "fnBIRTH_YEAR" });
            else if (string.IsNullOrEmpty(fsSEX))
                yield return new ValidationResult("請選擇生理性別!", new[] { "fsSEX" });
            else if (string.IsNullOrEmpty(fsPASSWORD))
                yield return new ValidationResult("請輸入密碼!", new[] { "fsPASSWORD" });
            else if (string.IsNullOrEmpty(fsPASSWORD1))
                yield return new ValidationResult("請輸入確認密碼!", new[] { "fsPASSWORD1" });
            else if (fsPASSWORD != fsPASSWORD1)
                yield return new ValidationResult("密碼與確認密碼不符!", new[] { "fsPASSWORD" });
            else
                yield return null;
        }
    }

    //註冊結果
    public class clsREGISTER
    {
        /// <summary>
        /// 結果
        /// </summary>
        public bool fbRESULT { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE { get; set; }
    }

    //會員資訊
    public class clsMEMBER_INFO
    {
        /// <summary>
        /// 帳號ID
        /// </summary>
        public string fsACCOUNT_ID { get; set; }
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// 註冊方式
        /// </summary>
        public int fnREGISTER_TYPE { get; set; }
        /// <summary>
        /// 暱稱
        /// </summary>
        public string fsREALNAME { get; set; }
        /// <summary>
        /// 出生年
        /// </summary>
        public int fnBIRTHYEAR { get; set; }
        /// <summary>
        /// 性別(male/female)
        /// </summary>
        public string fsSEX { get; set; }
        /// <summary>
        /// 聯絡電話
        /// </summary>
        public string fsPHONE { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string fsADDRESS { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string fsEMAIL { get; set; }
        /// <summary>
        /// 是否已驗證
        /// </summary>
        public bool fcVALIDATE { get; set; }
        /// <summary>
        /// 剩餘體驗天數
        /// </summary>
        public int fnLEFT_PROMO_DAYS { get; set; }
        /// <summary>
        /// 大頭貼
        /// </summary>
        public string fsPICTURE { get; set; }
        /// <summary>
        /// 已連結的登入類型
        /// </summary>
        public List<string> lstLOGIN_TYPE { get; set; }
    }

    //取得會員資訊
    public class clsGET_ACCOUNT_INFO_ARGS
    {
        /// <summary>
        /// 驗證身分
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //修改會員資料
    public class clsSET_ACCOUNT_INFO_ARGS : IValidatableObject
    {
        /// <summary>
        /// 暱稱
        /// </summary>
        public string fsREALNAME { get; set; }
        /// <summary>
        /// 出生年
        /// </summary>
        public int fnBIRTHYEAR { get; set; }
        /// <summary>
        /// 性別(male/female)
        /// </summary>
        public string fsSEX { get; set; }
        /// <summary>
        /// 聯絡電話
        /// </summary>
        public string fsPHONE { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string fsADDRESS { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string fsEMAIL { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }

        //自我驗證商業邏輯
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (fnBIRTHYEAR <= 0)
                yield return new ValidationResult("請輸入出生年!", new[] { "fnBIRTHYEAR" });
            else if(string.IsNullOrEmpty(fsSEX))
                yield return new ValidationResult("請選擇生理性別!", new[] { "fsSEX" });
            else
                yield return null;
        }
    }

    //修改會員大頭照
    public class clsSET_ACCOUNT_PICTURE 
    {
        /// <summary>
        /// BASE64
        /// </summary>
        public string fsPICTURE_BASE64 { get; set; }
        /// <summary>
        /// URL
        /// </summary>
        public string fsPICTURE_URL { get; set; }
        /// <summary>
        /// 身分驗證
        /// </summary>
        public clsIDENTITY_VALIDATE_ARUS clsIDENTITY_VALIDATE_ARUS { get; set; }
    }

    //修改會員結果
    public class clsSET_ACCOUNT_INFO
    {
        /// <summary>
        /// 結果
        /// </summary>
        public bool fbRESULT { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string fsMESSAGE { get; set; }
    }

    //TV今日免費觀看頻道資訊
    public class clsGET_MEMBER_FREE_TV_WATCH
    {
        /// <summary>
        /// 本日日期
        /// </summary>
        public string fsDATE { get; set; }
        /// <summary>
        /// 開始時間
        /// </summary>
        public DateTime fdFREESTART_DATE { get; set; }
    }
}