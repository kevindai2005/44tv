﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Models
{
    public class clsAGC
    {
    }

    public class clsUPSERT_AGC_MAC_BINDING
    {
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        public string fsPASSWORD { get; set; }
    }

    public class clsDISABLE_AGC_MAC_SERVICE
    {
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
    }
    public class clsGET_AGC_MAC_INFO
    {
        /// <summary>
        /// false 為有效清單 true反之
        /// </summary>
        public bool DISABLED { get; set; }
    }
    public class clsMAC_AGC_INFO
    {
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
        /// <summary>
        /// 綁定USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// MAC
        /// </summary>
        public string fdCREATE_DATE { get; set; }
    }
    public class clsSET_AGC_ACCOUNT_PASSWORD
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// 密碼
        /// </summary>
        public string fsPASSWORD { get; set; }
    }
    public class clsSET_AGC_ACCOUNT_CHANGE_MAC
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// 舊MAC
        /// </summary>
        public string fsMAC_ADDRESS_OLD { get; set; }
        /// <summary>
        /// 新MAC
        /// </summary>
        public string fsMAC_ADDRESS_NEW { get; set; }
    }

    public class clsGET_AGC_MAC_BY_ACCOUNTID
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
    }

    public class clsMAC_AGC_ADDRESS
    {
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
    }
    
    public class clsGET_AGC_INSERT_ORDER
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
        /// <summary>
        /// 套餐代碼(豪華餐CHANNELAGC_FULL;學生餐CHANNELAGC_STU)
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
    }
    public class clsGET_AGC_CANCEL_ORDER
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
        /// <summary>
        /// 套餐代碼(豪華餐CHANNELAGC_FULL;學生餐CHANNELAGC_STU)
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
    }

    public class clsSET_AGC_PURCHASE
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }
        /// <summary>
        /// 舊套餐代碼(豪華餐CHANNELAGC_FULL;學生餐CHANNELAGC_STU)
        /// </summary>
        public string fsPURCHASE_OLD { get; set; }
        /// <summary>
        /// 新套餐代碼(豪華餐CHANNELAGC_FULL;學生餐CHANNELAGC_STU)
        /// </summary>
        public string fsPURCHASE_NEW { get; set; }
    }
    public class clsGET_AGC_ORDER_BY_USERID
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }

    }
    public class clsGET_AGC_ORDER_INFO
    {
        /// <summary>
        /// 訂單
        /// </summary>
        public string fsORDER_NO { get; set; }
        /// <summary>
        /// 訂購日期
        /// </summary>
        public string fdORDER_DATE { get; set; }
        /// <summary>
        /// 套餐ID
        /// </summary>
        public string fsPURCHASE_ID { get; set; }
        /// <summary>
        /// 結束時間
        /// </summary>
        public string fdEND_DATE { get; set; }
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMACHINE_ID { get; set; }

    }

    public class clsCANCEL_AGC_MAC_BINDING
    {
        /// <summary>
        /// USERID
        /// </summary>
        public string fsUSER_ID { get; set; }
        /// <summary>
        /// MAC
        /// </summary>
        public string fsMAC_ADDRESS { get; set; }

    }


}