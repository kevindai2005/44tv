﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace FourgTV_OTT2.Api2.Infrastructure
{
    /// <summary>
    /// Quick comparer for ordering http methods for display.
    /// </summary>
    internal class HttpMethodComparator : IComparer<HttpMethod>
    {
        /// <summary>
        /// The order
        /// </summary>
        private readonly string[] order =
                                  {
                                      "GET",
                                      "POST",
                                      "PUT",
                                      "DELETE",
                                      "PATCH"
                                  };

        /// <summary>
        /// Compares the specified x.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns>System.Int32.</returns>
        public int Compare(HttpMethod x, HttpMethod y)
        {
            return Array.IndexOf(this.order, x.ToString())
                        .CompareTo(Array.IndexOf(this.order, y.ToString()));
        }
    }
}