﻿using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Areas.HelpPage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;

namespace FourgTV_OTT2.Api2.Infrastructure
{
   
        public class PostmanFeatures
        {
            private readonly Regex pathVariableRegEx = new Regex(
                "\\{([A-Za-z0-9-_]+)\\}", RegexOptions.ECMAScript | RegexOptions.Compiled);

            private readonly Regex urlParameterVariableRegEx = new Regex(
                "=\\{([A-Za-z0-9-_]+)\\}", RegexOptions.ECMAScript | RegexOptions.Compiled);

            private string CollectionName { get; set; }

            private string CollectionDesc { get; set; }

            private string NameSpaceFullName { get; set; }

            public PostmanFeatures(string collectionName,
                string collectionDesc,
                string nameSpaceFullName)
            {
                this.CollectionName = collectionName;
                this.CollectionDesc = collectionDesc;
                this.NameSpaceFullName = nameSpaceFullName;
            }

            //-----------------------------------------------------------------------------------------

            public PostmanCollectionGet PostmanCollectionForController(HttpRequestMessage request)
            {
                var requestUri = request.RequestUri;
                var baseUri = requestUri.Scheme + "://" + requestUri.Host + ":" + requestUri.Port +
                              HttpContext.Current.Request.ApplicationPath;

                var postManCollection = new PostmanCollectionGet
                {
                    Id = Guid.NewGuid(),
                    Name = this.CollectionName,
                    Description = this.CollectionDesc,
                    Order = new Collection<Guid>(),
                    Folders = new Collection<PostmanFolderGet>(),
                    Timestamp = DateTime.Now.Ticks,
                    Synced = false,
                    Requests = new Collection<PostmanRequestGet>()
                };

                var configuration = request.GetConfiguration();
                var helpPageSampleGenerator = configuration.GetHelpPageSampleGenerator();
                var apiExplorer = configuration.Services.GetApiExplorer();

                var apiDescriptionsByController =
                    apiExplorer.ApiDescriptions
                               .Where(x => !x.ActionDescriptor.ControllerDescriptor.ControllerName.Contains("Error"))
                               .GroupBy(x => x.ActionDescriptor.ActionBinding.ActionDescriptor.ControllerDescriptor.ControllerType);

                // API Groups
                var apiDescriptions = configuration.Services.GetApiExplorer().ApiDescriptions;

                ILookup<HttpControllerDescriptor, ApiDescription> apiGroups =
                    apiDescriptions.ToLookup(api => api.ActionDescriptor.ControllerDescriptor);

                var documentationProvider = configuration.Services.GetDocumentationProvider();

                foreach (var apiDescriptionsByControllerGroup in apiDescriptionsByController)
                {
                    this.ApiControllers(
                        apiDescriptionsByControllerGroup: apiDescriptionsByControllerGroup,
                        postManCollection: postManCollection,
                        helpPageSampleGenerator: helpPageSampleGenerator,
                        apiGroups: apiGroups,
                        documentationProvider: documentationProvider,
                        baseUri: baseUri,
                        nameSpaceFullName: this.NameSpaceFullName);
                }

                return postManCollection;
            }

            /// <summary>
            /// APIs the folders.
            /// </summary>
            /// <param name="apiDescriptionsByControllerGroup">The API descriptions by controller group.</param>
            /// <param name="postManCollection">The post man collection.</param>
            /// <param name="helpPageSampleGenerator">The help page sample generator.</param>
            /// <param name="baseUri">The base URI.</param>
            /// <param name="nameSpaceFullName">Full name of the name space.</param>
            private void ApiControllers(
                IGrouping<Type, ApiDescription> apiDescriptionsByControllerGroup,
                PostmanCollectionGet postManCollection,
                HelpPageSampleGenerator helpPageSampleGenerator,
                ILookup<HttpControllerDescriptor, ApiDescription> apiGroups,
                IDocumentationProvider documentationProvider,
                string baseUri,
                string nameSpaceFullName)
            {
                var controllerName =
                    apiDescriptionsByControllerGroup.Key.Name.Replace("Controller", string.Empty);

                var apiGroup =
                    apiGroups.Where(x => x.Key.ControllerName.Contains(controllerName)).FirstOrDefault();

                var controllerDocumentation =
                    documentationProvider.GetDocumentation(apiGroup.Key);

                var controllerDisplayName = controllerDocumentation;

                var folderName = string.IsNullOrWhiteSpace(controllerDisplayName)
                                 ? controllerName
                                 : controllerDisplayName;

                var postManFolder = new PostmanFolderGet
                {
                    Id = Guid.NewGuid(),
                    CollectionId = postManCollection.Id,
                    Name = folderName,
                    Description = string.Format("Api Methods for {0}", controllerName),
                    CollectionName = "api",
                    Order = new Collection<Guid>()
                };

                var apiDescriptions =
                    apiDescriptionsByControllerGroup.OrderBy(description => description.HttpMethod, new HttpMethodComparator())
                                                    .ThenBy(description => description.RelativePath);

                ICollection<Guid> requestGuids = new Collection<Guid>();

                foreach (var apiDescription in apiDescriptions)
                {
                    if (!postManFolder.Name.Equals("Error", StringComparison.OrdinalIgnoreCase))
                    {
                        var request = this.ApiActions(
                            postManCollection, helpPageSampleGenerator,
                            baseUri, apiDescription, nameSpaceFullName);

                        requestGuids.Add(request.Id);
                        postManCollection.Requests.Add(request);
                    }
                }

                foreach (var guid in requestGuids)
                {
                    postManFolder.Order.Add(guid);
                }

                postManCollection.Folders.Add(postManFolder);
            }

            private PostmanRequestGet ApiActions(PostmanCollectionGet postManCollection,
                HelpPageSampleGenerator helpPageSampleGenerator,
                string baseUri,
                ApiDescription apiDescription,
                string nameSpaceFullName)
            {
                TextSample sampleData = null;
                var sampleDictionary =
                    helpPageSampleGenerator.GetSample(apiDescription, SampleDirection.Request);

                MediaTypeHeaderValue mediaTypeHeader;

                if (MediaTypeHeaderValue.TryParse("application/json", out mediaTypeHeader) &&
                    sampleDictionary.ContainsKey(mediaTypeHeader))
                {
                    sampleData = sampleDictionary[mediaTypeHeader] as TextSample;
                }

                // scrub curly braces from url parameter values
                var cleanedUrlParameterUrl = this.urlParameterVariableRegEx.Replace(
                    apiDescription.RelativePath, "=$1-value");

                // get pat variables from url
                var pathVariables = this.pathVariableRegEx.Matches(cleanedUrlParameterUrl)
                                        .Cast<Match>()
                                        .Select(m => m.Value)
                                        .Select(s => s.Substring(1, s.Length - 2))
                                        .ToDictionary(s => s, s => string.Format("{0}-value", s));

                // change format of parameters within string to be colon prefixed rather than curly brace wrapped
                var postmanReadyUrl = this.pathVariableRegEx.Replace(cleanedUrlParameterUrl, ":$1");

                // prefix url with base uri
                var url = string.Concat(baseUri.TrimEnd('/'), "/", postmanReadyUrl);

                // Get Controller Action's Description
                var actionDisplayName = apiDescription.Documentation;
                var requestDescription = apiDescription.RelativePath;

                var request = new PostmanRequestGet
                {
                    CollectionId = postManCollection.Id,
                    Id = Guid.NewGuid(),
                    Name = actionDisplayName,
                    Description = requestDescription,
                    Url = url,
                    Method = apiDescription.HttpMethod.Method,
                    Headers = "Content-Type: application/json",
                    Data = sampleData == null ? "" : sampleData.Text,
                    DataMode = "raw",
                    Time = postManCollection.Timestamp,
                    Synced = false,
                    DescriptionFormat = "markdown",
                    Version = "beta",
                    Responses = new Collection<string>(),
                    PathVariables = pathVariables
                };

                return request;
            }
        }
    }
