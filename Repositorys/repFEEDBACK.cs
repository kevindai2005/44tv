﻿using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repFEEDBACK
    {
        //新增回饋
        public string fnINSERT_FEEDBACK(clsINSERT_FEEDBACK_ARGS clsINSERT_FEEDBACK_ARGS)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", clsINSERT_FEEDBACK_ARGS.fsUSER_ID);
            dicParameters.Add("fsTYPE", clsINSERT_FEEDBACK_ARGS.fsTYPE);
            dicParameters.Add("fsOS", clsINSERT_FEEDBACK_ARGS.fsOS);
            dicParameters.Add("fsOS_VERSION", clsINSERT_FEEDBACK_ARGS.fsOS_VERSION);
            dicParameters.Add("fsDEVICE_VENDOR", clsINSERT_FEEDBACK_ARGS.fsDEVICE_VENDOR);
            dicParameters.Add("fsDEVICE_TYPE", clsINSERT_FEEDBACK_ARGS.fsDEVICE_TYPE);
            dicParameters.Add("fsCONTENT", clsINSERT_FEEDBACK_ARGS.fsCONTENT);

            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_FEEDBACK", dicParameters);

            return fsRESULT;
        }
    }
}