﻿//using FourgTV_OTT2.Api2.Common;
using static FourgTV_OTT2.Api2.Common.clsDB;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repECPAY
    {
        //產生付款訂單
        public clsECPAY_TRADE_RESULT fnCREATE_ECPAY_TRADE(string fsORDER_NO, int fnPRICE, string fsSET_NAME, string fsPAY_TYPE)
        {
            //取得特店編號
            string fsMERCHANTID = new repCONFIG().fnGET_CFG("ECPAY_MERCHANTID")[0].fsVALUE;
            //取得建立訂單URL
            string fsTRADE_URL = new repCONFIG().fnGET_CFG("ECPAY_CREATE_TRADE_URL")[0].fsVALUE;
            //取得付款完成接收URL
            string fsRETURN_URL = new repCONFIG().fnGET_CFG("ECPAY_RETURN_URL")[0].fsVALUE;
            //返回商店URL
            string fsCLIENT_RETURN_URL = new repCONFIG().fnGET_CFG("ECPAY_RETURN_CLIENT_URL")[0].fsVALUE;

            clsECPAY_TRADE_RESULT clsECPAY_TRADE_RESULT = new clsECPAY_TRADE_RESULT();

            SortedDictionary<string, string> PostCollection = new SortedDictionary<string, string>();
            PostCollection.Add("MerchantID", fsMERCHANTID);
            PostCollection.Add("MerchantTradeNo", fsORDER_NO);
            PostCollection.Add("MerchantTradeDate", DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss"));
            PostCollection.Add("PaymentType", "aio");
            PostCollection.Add("TotalAmount", fnPRICE.ToString());
            PostCollection.Add("TradeDesc", fsSET_NAME);
            PostCollection.Add("ItemName", fsSET_NAME);
            PostCollection.Add("ReturnURL", fsRETURN_URL);
            PostCollection.Add("ChoosePayment", "ALL");
            PostCollection.Add("EncryptType", "1");
            PostCollection.Add("NeedExtraPaidInfo", "N");
            PostCollection.Add("IgnorePayment", "");
            PostCollection.Add("ClientBackURL", fsCLIENT_RETURN_URL);
            if (fsPAY_TYPE == "ATM")
            {
                //ATM付款有效天數，預設3天
                PostCollection.Add("ExpireDate", new repCONFIG().fnGET_CFG("ECPAY_EXPIRE_DAY")[0].fsVALUE);
                //取得ATM付款資訊
                PostCollection.Add("PaymentInfoURL", new repCONFIG().fnGET_CFG("ECPAY_PAYMENTINFO_URL")[0].fsVALUE);
            }

            string fsPARAMETER = clsSECURITY.GetEcpayParameter(PostCollection);
            string fsRESULT = string.Empty;

            using (WebClient wc = new System.Net.WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                fsRESULT = wc.UploadString(fsTRADE_URL, fsPARAMETER);
            }

            clsECPAY_TRADE_RESULT = JsonConvert.DeserializeObject<clsECPAY_TRADE_RESULT>(fsRESULT);

            if (clsECPAY_TRADE_RESULT.RtnCode == "1")
            {
                SortedDictionary<string, string> PostCollection1 = new SortedDictionary<string, string>();
                PostCollection1.Add("RtnCode", clsECPAY_TRADE_RESULT.RtnCode);
                PostCollection1.Add("RtnMsg", clsECPAY_TRADE_RESULT.RtnMsg);
                PostCollection1.Add("SPToken", clsECPAY_TRADE_RESULT.SPToken);
                PostCollection1.Add("MerchantID", clsECPAY_TRADE_RESULT.MerchantID);
                PostCollection1.Add("MerchantTradeNo", clsECPAY_TRADE_RESULT.MerchantTradeNo);

                //驗證回傳的資訊是否正確
                if (!clsSECURITY.IsEcpayCheckMacValueCorrect(PostCollection1, clsECPAY_TRADE_RESULT.CheckMacValue))
                {
                    return new clsECPAY_TRADE_RESULT() { RtnCode = "10000", RtnMsg = "驗證碼錯誤!" };
                }
            }

            return clsECPAY_TRADE_RESULT;
        }

        //更新ATM取號
        public string fnUPDATE_ECPAY_ATM(clsECPAY_ATM_RESULT clsECPAY_ATM_RESULT)
        {
            string fsRESULT = string.Empty;

            SortedDictionary<string, string> PostCollection = new SortedDictionary<string, string>();
            PostCollection.Add("MerchantID", clsECPAY_ATM_RESULT.MerchantID);
            PostCollection.Add("MerchantTradeNo", clsECPAY_ATM_RESULT.MerchantTradeNo);
            PostCollection.Add("StoreID", clsECPAY_ATM_RESULT.StoreID);
            PostCollection.Add("RtnCode", clsECPAY_ATM_RESULT.RtnCode.ToString());
            PostCollection.Add("RtnMsg", clsECPAY_ATM_RESULT.RtnMsg);
            PostCollection.Add("TradeNo", clsECPAY_ATM_RESULT.TradeNo);
            PostCollection.Add("TradeAmt", clsECPAY_ATM_RESULT.TradeAmt.ToString());
            PostCollection.Add("PaymentType", clsECPAY_ATM_RESULT.PaymentType);
            PostCollection.Add("TradeDate", clsECPAY_ATM_RESULT.TradeDate);
            PostCollection.Add("CustomField1", clsECPAY_ATM_RESULT.CustomField1);
            PostCollection.Add("CustomField2", clsECPAY_ATM_RESULT.CustomField2);
            PostCollection.Add("CustomField3", clsECPAY_ATM_RESULT.CustomField3);
            PostCollection.Add("CustomField4", clsECPAY_ATM_RESULT.CustomField4);
            PostCollection.Add("BankCode", clsECPAY_ATM_RESULT.BankCode);
            PostCollection.Add("vAccount", clsECPAY_ATM_RESULT.vAccount);
            PostCollection.Add("ExpireDate", clsECPAY_ATM_RESULT.ExpireDate);

            //驗證回傳的資訊是否正確
            if (clsSECURITY.IsEcpayCheckMacValueCorrect(PostCollection, clsECPAY_ATM_RESULT.CheckMacValue))
            {
                Dictionary<string, string> dicParameters = new Dictionary<string, string>();
                dicParameters.Add("fsORDER_NO", clsECPAY_ATM_RESULT.MerchantTradeNo.Substring(0,13));
                dicParameters.Add("fnECPAY_RETURN_CODE", clsECPAY_ATM_RESULT.RtnCode.ToString());
                dicParameters.Add("fsECPAY_RETURN_MESSAGE", clsECPAY_ATM_RESULT.RtnMsg);
                dicParameters.Add("fsECPAY_TRADE_NO", clsECPAY_ATM_RESULT.TradeNo);
                dicParameters.Add("fnTRADE_AMT", clsECPAY_ATM_RESULT.TradeAmt.ToString());
                dicParameters.Add("fsECPAY_PAYMENT_TYPE", clsECPAY_ATM_RESULT.PaymentType);
                dicParameters.Add("fdECPAY_CREATED_DATE", clsECPAY_ATM_RESULT.TradeDate);
                dicParameters.Add("fsATM_BANKCODE", clsECPAY_ATM_RESULT.BankCode);
                dicParameters.Add("fsATM_VACCOUNT", clsECPAY_ATM_RESULT.vAccount);
                dicParameters.Add("fdATM_EXPIRE_DATE", clsECPAY_ATM_RESULT.ExpireDate);
                fsRESULT = Do_Tran("spAPI_UPDATE_ORDER_ECPAY_RETURN_ATM", dicParameters);
            }
            else
            {
                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                {
                    fsTYPE = "ECPAY_ATM",
                    fsNAME = "ECPAY ATM取號",
                    fsCONTENT = "驗證碼錯誤!" + Newtonsoft.Json.JsonConvert.SerializeObject(PostCollection),
                    fsCREATED_BY = "ECPAY_ATM"

                });

                fsRESULT = "ERROR:驗證碼錯誤!";
            }

            return fsRESULT;
        }
        
        //更新付款結果
        public string fnUPDATE_ECPAY_PAYMENT(clsECPAY_PAYMENT_RESULT clsECPAY_PAYMENT_RESULT)
        {
            string fsRESULT = string.Empty;

            SortedDictionary<string, string> PostCollection = new SortedDictionary<string, string>();
            PostCollection.Add("MerchantID", clsECPAY_PAYMENT_RESULT.MerchantID);
            PostCollection.Add("MerchantTradeNo", clsECPAY_PAYMENT_RESULT.MerchantTradeNo);
            PostCollection.Add("StoreID", clsECPAY_PAYMENT_RESULT.StoreID);
            PostCollection.Add("RtnCode", clsECPAY_PAYMENT_RESULT.RtnCode.ToString());
            PostCollection.Add("RtnMsg", clsECPAY_PAYMENT_RESULT.RtnMsg);
            PostCollection.Add("TradeNo", clsECPAY_PAYMENT_RESULT.TradeNo);
            PostCollection.Add("TradeAmt", clsECPAY_PAYMENT_RESULT.TradeAmt.ToString());
            PostCollection.Add("PaymentDate", clsECPAY_PAYMENT_RESULT.PaymentDate);
            PostCollection.Add("PaymentType", clsECPAY_PAYMENT_RESULT.PaymentType);
            PostCollection.Add("PaymentTypeChargeFee", clsECPAY_PAYMENT_RESULT.PaymentTypeChargeFee.ToString());
            PostCollection.Add("TradeDate", clsECPAY_PAYMENT_RESULT.TradeDate);
            PostCollection.Add("SimulatePaid", clsECPAY_PAYMENT_RESULT.SimulatePaid.ToString());
            PostCollection.Add("CustomField1", clsECPAY_PAYMENT_RESULT.CustomField1);
            PostCollection.Add("CustomField2", clsECPAY_PAYMENT_RESULT.CustomField2);
            PostCollection.Add("CustomField3", clsECPAY_PAYMENT_RESULT.CustomField3);
            PostCollection.Add("CustomField4", clsECPAY_PAYMENT_RESULT.CustomField4);

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", clsECPAY_PAYMENT_RESULT.MerchantTradeNo.Substring(0, 13));
            dicParameters.Add("fnECPAY_RETURN_CODE", clsECPAY_PAYMENT_RESULT.RtnCode.ToString());
            dicParameters.Add("fsECPAY_RETURN_MESSAGE", clsECPAY_PAYMENT_RESULT.RtnMsg);
            dicParameters.Add("fsECPAY_PAYMENT_TYPE", clsECPAY_PAYMENT_RESULT.PaymentType);
            dicParameters.Add("fdECPAY_CREATED_DATE", clsECPAY_PAYMENT_RESULT.TradeDate);
            dicParameters.Add("fsECPAY_TRADE_NO", clsECPAY_PAYMENT_RESULT.TradeNo);
            dicParameters.Add("fnTRADE_AMT", clsECPAY_PAYMENT_RESULT.TradeAmt.ToString());
            dicParameters.Add("fdPAYMENT_DATE", clsECPAY_PAYMENT_RESULT.PaymentDate);
            dicParameters.Add("fnPAYMENT_FEE", clsECPAY_PAYMENT_RESULT.PaymentTypeChargeFee.ToString().ToString());
            dicParameters.Add("fcSIMULATE_PAID", clsECPAY_PAYMENT_RESULT.SimulatePaid.ToString());

            fsRESULT = Do_Tran("spAPI_UPDATE_ORDER_ECPAY_RETURN_PAYMENT", dicParameters);

            return fsRESULT;
        }
    }
}