﻿// ============================================= 
// 描述: VOD資料介接 
// 記錄: <2017/10/06><David.Sin><新增本程式> 
// ============================================= 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using static FourgTV_OTT2.Api2.Models.clsVOD_EPISODE;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repVOD
    {
        //新增VOD PageView
        public string fnINSERT_VOD_PAGEVIEW(string fsVOD_NO, int fnSEQ)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();

            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());

            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_VOD_PAGEVIEW", dicParameters);

            return fsRESULT;
        }

        //取得戲劇類型
        public List<clsVOD_TYPE> fnGET_VOD_DRAMA_TYPE(string fcREGION)
        {
            List<clsVOD_TYPE> clsVOD_TYPEs = new List<clsVOD_TYPE>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtTYPE = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_TYPE", dicParameters);

            if (dtTYPE != null && dtTYPE.Rows.Count > 0)
            {
                for (int i = 0; i < dtTYPE.Rows.Count; i++)
                {
                    clsVOD_TYPEs.Add(new clsVOD_TYPE()
                    {
                        fsCODE = dtTYPE.Rows[i]["fsCODE"].ToString(),
                        fsNAME = dtTYPE.Rows[i]["fsNAME"].ToString(),
                        fcOVERSEAS = (dtTYPE.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false)
                    });
                }
            }

            return clsVOD_TYPEs;
        }

        //取得戲劇年份
        public List<clsVOD_YEAR> fnGET_VOD_DRAMA_YEAR(string fcREGION)
        {
            List<clsVOD_YEAR> clsVOD_YEARs = new List<clsVOD_YEAR>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtYEAR = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_YEAR", dicParameters);

            if (dtYEAR != null && dtYEAR.Rows.Count > 0)
            {
                for (int i = 0; i < dtYEAR.Rows.Count; i++)
                {
                    clsVOD_YEARs.Add(new clsVOD_YEAR()
                    {
                        fnYEAR = int.Parse(dtYEAR.Rows[i]["fnYEAR"].ToString())
                    });
                }
            }

            return clsVOD_YEARs;
        }

        //取得戲劇首頁資訊
        public List<clsVOD_INDEX> fnGET_VOD_DRAMA_INDEX(string fcFIXED, int fnYEAR, string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcFIXED", fcFIXED);
            dicParameters.Add("fnYEAR", fnYEAR.ToString());
            dicParameters.Add("fsTYPE2", fsTYPE);
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_INDEX2", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEX clsVOD_INDEX = new clsVOD_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                    clsVOD_INDEX.fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString();
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString());
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }

                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }
            return clsVOD_INDEXs;
        }

        //取得戲劇首頁資訊V3
        public List<clsVOD_INDEX> fnGET_VOD_DRAMA_INDEX3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsTYPE2", fsTYPE);
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_INDEX3", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEX clsVOD_INDEX = new clsVOD_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                    clsVOD_INDEX.fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString();
                    clsVOD_INDEX.fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString();
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString());
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }

                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }


            return clsVOD_INDEXs;
        }

        //取得戲劇首頁資訊-手機用
        public clsVOD_INDEX_MOBILE fnGET_VOD_DRAMA_INDEX_MOBILE()
        {
            clsVOD_INDEX_MOBILE clsVOD_INDEX_MOBILE = new clsVOD_INDEX_MOBILE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_INDEX_MOBILE", null);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_INDEX_MOBILE.fsHOT = new List<string>();
                clsVOD_INDEX_MOBILE.fsNEW = new List<string>();
                List<DataRow> drVOD_NEW = dtVOD.Select("fsTYPE = 'new'").ToList();
                List<DataRow> drVOD_HOT = dtVOD.Select("fsTYPE = 'hot'").ToList();
                foreach (var item in drVOD_NEW)
                {
                    clsVOD_INDEX_MOBILE.fsNEW.Add(item["fsVOD_NO"].ToString());
                }
                foreach (var item in drVOD_HOT)
                {
                    clsVOD_INDEX_MOBILE.fsHOT.Add(item["fsVOD_NO"].ToString());
                }

            }


            return clsVOD_INDEX_MOBILE;
        }

        //取得戲劇內頁集數資訊
        public clsVOD_EPISODE fnGET_VOD_DRAMA_EPISODE(string fsVOD_NO)
        {
            clsVOD_EPISODE clsVOD_EPISODE = new clsVOD_EPISODE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_EPISODE", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE.fsPROG_NAME = dtVOD.Rows[0]["fsPROG_NAME"].ToString();
                clsVOD_EPISODE.fbIS_FINAL = (dtVOD.Rows[0]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                clsVOD_EPISODE.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[0]["fnTOTAL_EPISODE"].ToString());
                clsVOD_EPISODE.lstEPISODE = new List<clsVOD_EPISODE.clsVOD_EPISODE_LIST>();
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_EPISODE.lstEPISODE.Add(new clsVOD_EPISODE.clsVOD_EPISODE_LIST()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString()),
                        fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty)
                    });
                }
            }

            return clsVOD_EPISODE;
        }

        //取得戲劇集數明細
        public clsVOD_EPISODE_DETAIL fnGET_VOD_DRAMA_EPISODE_DETAIL(string fsVOD_NO, int fnSEQ)
        {
            clsVOD_EPISODE_DETAIL clsVOD_EPISODE_DETAIL = new clsVOD_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_EPISODE_DETAIL", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_EPISODE_DETAIL.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                clsVOD_EPISODE_DETAIL.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_EPISODE_DETAIL.fsDIRECTOR = dtVOD.Rows[0]["fsDIRECTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsACTOR = dtVOD.Rows[0]["fsACTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                clsVOD_EPISODE_DETAIL.fsCOUNTRY = dtVOD.Rows[0]["fsCOUNTRY"].ToString();
                clsVOD_EPISODE_DETAIL.fsDESCRIPTION = dtVOD.Rows[0]["fsDESCRIPTION"].ToString();
                clsVOD_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_EPISODE_DETAIL.fsGRADED = dtVOD.Rows[0]["fsGRADED"].ToString();
                clsVOD_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_EPISODE_DETAIL.fsVENDOR = dtVOD.Rows[0]["fsVENDOR"].ToString();
                clsVOD_EPISODE_DETAIL.fsRIGHT = dtVOD.Rows[0]["fsRIGHT"].ToString().Split(',').ToList();
                clsVOD_EPISODE_DETAIL.fsPRODUCER = dtVOD.Rows[0]["fsPRODUCER"].ToString();
                clsVOD_EPISODE_DETAIL.fnDURATION = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fnDURATION"].ToString()) ? int.Parse(dtVOD.Rows[0]["fnDURATION"].ToString()) : 0);
                clsVOD_EPISODE_DETAIL.fdCREATED_DATE = DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00");
                clsVOD_EPISODE_DETAIL.fdUPDATED_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00") : DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00"));
                if (!string.IsNullOrEmpty(dtVOD.Rows[0]["fsTYPE"].ToString()))
                {
                    clsVOD_EPISODE_DETAIL.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                    foreach (var item in dtVOD.Rows[0]["fsTYPE"].ToString().Split('/'))
                    {
                        clsVOD_EPISODE_DETAIL.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                    }
                }
                clsVOD_EPISODE_DETAIL.fsVIDEO_FROM = dtVOD.Rows[0]["fsVIDEO_FROM"].ToString();

            }

            return clsVOD_EPISODE_DETAIL;
        }

        //取得戲劇集數明細(By Content Id)
        public clsVOD_EPISODE_DETAIL fnGET_VOD_DRAMA_EPISODE_DETAIL_BY_CONTENT_ID(string fsCONTENT_ID_LI)
        {
            clsVOD_EPISODE_DETAIL clsVOD_EPISODE_DETAIL = new clsVOD_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsCONTENT_ID_LI", fsCONTENT_ID_LI);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_EPISODE_DETAIL_BY_CONTENT_ID", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_EPISODE_DETAIL.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                clsVOD_EPISODE_DETAIL.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_EPISODE_DETAIL.fsDIRECTOR = dtVOD.Rows[0]["fsDIRECTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsACTOR = dtVOD.Rows[0]["fsACTOR"].ToString().Replace("\r\n", "");

                clsVOD_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                clsVOD_EPISODE_DETAIL.fsCOUNTRY = dtVOD.Rows[0]["fsCOUNTRY"].ToString();
                clsVOD_EPISODE_DETAIL.fsDESCRIPTION = dtVOD.Rows[0]["fsDESCRIPTION"].ToString();
                clsVOD_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_EPISODE_DETAIL.fsGRADED = dtVOD.Rows[0]["fsGRADED"].ToString();
                clsVOD_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_EPISODE_DETAIL.fsVENDOR = dtVOD.Rows[0]["fsVENDOR"].ToString();
                clsVOD_EPISODE_DETAIL.fsRIGHT = dtVOD.Rows[0]["fsRIGHT"].ToString().Split(',').ToList();
                clsVOD_EPISODE_DETAIL.fsPRODUCER = dtVOD.Rows[0]["fsPRODUCER"].ToString();
                clsVOD_EPISODE_DETAIL.fnDURATION = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fnDURATION"].ToString()) ? int.Parse(dtVOD.Rows[0]["fnDURATION"].ToString()) : 0);

                if (!string.IsNullOrEmpty(dtVOD.Rows[0]["fsTYPE"].ToString()))
                {
                    clsVOD_EPISODE_DETAIL.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                    foreach (var item in dtVOD.Rows[0]["fsTYPE"].ToString().Split('/'))
                    {
                        clsVOD_EPISODE_DETAIL.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                    }
                }
            }

            return clsVOD_EPISODE_DETAIL;
        }

        //取的戲劇推薦影片
        public List<clsVOD_INDEX> fnGET_VOD_DRAMA_RELATED(string fsVOD_NO, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_DRAMA_PROMO", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString()),
                        fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString(),
                        fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString()
                });
                }
            }


            return clsVOD_INDEXs;
        }

        //取得綜藝類型
        public List<clsVOD_TYPE> fnGET_VOD_SHOW_TYPE(string fcREGION)
        {
            List<clsVOD_TYPE> clsVOD_TYPEs = new List<clsVOD_TYPE>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtTYPE = clsDB.Do_Query("spAPI_GET_VOD_SHOW_TYPE", dicParameters);
            if (dtTYPE != null && dtTYPE.Rows.Count > 0)
            {
                for (int i = 0; i < dtTYPE.Rows.Count; i++)
                {
                    clsVOD_TYPEs.Add(new clsVOD_TYPE()
                    {
                        fsCODE = dtTYPE.Rows[i]["fsCODE"].ToString(),
                        fsNAME = dtTYPE.Rows[i]["fsNAME"].ToString(),
                        fcOVERSEAS = (dtTYPE.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false)
                    });
                }
            }
            return clsVOD_TYPEs;
        }

        //取得綜藝首頁資訊
        public List<clsVOD_INDEX> fnGET_VOD_SHOW_INDEX(string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SHOW_INDEX2", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false),
                        fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false),
                        fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString())
                    });
                }
            }


            return clsVOD_INDEXs;
        }

        //取得綜藝首頁資訊V3
        public List<clsVOD_INDEX> fnGET_VOD_SHOW_INDEX3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsTYPE2", fsTYPE);
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SHOW_INDEX3", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEX clsVOD_INDEX = new clsVOD_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                    clsVOD_INDEX.fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString();
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString());
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }
                    clsVOD_INDEX.fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString();
                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }


            return clsVOD_INDEXs;
        }

        //取得綜藝首頁資訊-手機用
        public clsVOD_INDEX_MOBILE fnGET_VOD_SHOW_INDEX_MOBILE()
        {
            clsVOD_INDEX_MOBILE clsVOD_INDEX_MOBILE = new clsVOD_INDEX_MOBILE();
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SHOW_INDEX_MOBILE", null);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_INDEX_MOBILE.fsHOT = new List<string>();
                clsVOD_INDEX_MOBILE.fsNEW = new List<string>();
                List<DataRow> drVOD_NEW = dtVOD.Select("fsTYPE = 'new'").ToList();
                List<DataRow> drVOD_HOT = dtVOD.Select("fsTYPE = 'hot'").ToList();
                foreach (var item in drVOD_NEW)
                {
                    clsVOD_INDEX_MOBILE.fsNEW.Add(item["fsVOD_NO"].ToString());
                }
                foreach (var item in drVOD_HOT)
                {
                    clsVOD_INDEX_MOBILE.fsHOT.Add(item["fsVOD_NO"].ToString());
                }

            }


            return clsVOD_INDEX_MOBILE;
        }

        //取得綜藝內頁集數資訊
        public clsVOD_EPISODE fnGET_VOD_SHOW_EPISODE(string fsVOD_NO)
        {
            clsVOD_EPISODE clsVOD_EPISODE = new clsVOD_EPISODE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SHOW_EPISODE", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE.fsPROG_NAME = dtVOD.Rows[0]["fsPROG_NAME"].ToString();
                clsVOD_EPISODE.fbIS_FINAL = (dtVOD.Rows[0]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                clsVOD_EPISODE.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[0]["fnTOTAL_EPISODE"].ToString());

                clsVOD_EPISODE.lstEPISODE = new List<clsVOD_EPISODE.clsVOD_EPISODE_LIST>();
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {

                    clsVOD_EPISODE.lstEPISODE.Add(new clsVOD_EPISODE.clsVOD_EPISODE_LIST()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString()),
                        fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty)
                    });
                }
            }

            return clsVOD_EPISODE;
        }

        //取得綜藝集數明細
        public clsVOD_EPISODE_DETAIL fnGET_VOD_SHOW_EPISODE_DETAIL(string fsVOD_NO, int fnSEQ)
        {
            clsVOD_EPISODE_DETAIL clsVOD_EPISODE_DETAIL = new clsVOD_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SHOW_EPISODE_DETAIL", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_EPISODE_DETAIL.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                clsVOD_EPISODE_DETAIL.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_EPISODE_DETAIL.fsHOST = dtVOD.Rows[0]["fsHOST"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsGUEST = dtVOD.Rows[0]["fsGUEST"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                clsVOD_EPISODE_DETAIL.fsCOUNTRY = dtVOD.Rows[0]["fsCOUNTRY"].ToString();
                clsVOD_EPISODE_DETAIL.fsDESCRIPTION = dtVOD.Rows[0]["fsDESCRIPTION"].ToString();
                clsVOD_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_EPISODE_DETAIL.fsGRADED = dtVOD.Rows[0]["fsGRADED"].ToString();
                clsVOD_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_EPISODE_DETAIL.fsVENDOR = dtVOD.Rows[0]["fsVENDOR"].ToString();
                clsVOD_EPISODE_DETAIL.fsRIGHT = dtVOD.Rows[0]["fsRIGHT"].ToString().Split(',').ToList();
                clsVOD_EPISODE_DETAIL.fnDURATION = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fnDURATION"].ToString()) ? int.Parse(dtVOD.Rows[0]["fnDURATION"].ToString()) : 0);
                clsVOD_EPISODE_DETAIL.fdCREATED_DATE = DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00");
                clsVOD_EPISODE_DETAIL.fdUPDATED_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00") : DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00"));

                if (!string.IsNullOrEmpty(dtVOD.Rows[0]["fsTYPE"].ToString()))
                {
                    clsVOD_EPISODE_DETAIL.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                    foreach (var item in dtVOD.Rows[0]["fsTYPE"].ToString().Split('/'))
                    {
                        clsVOD_EPISODE_DETAIL.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                    }
                }
                clsVOD_EPISODE_DETAIL.fsVIDEO_FROM = dtVOD.Rows[0]["fsVIDEO_FROM"].ToString();
            }

            return clsVOD_EPISODE_DETAIL;
        }

        //取得綜藝集數明細(By Content Id)
        public clsVOD_EPISODE_DETAIL fnGET_VOD_SHOW_EPISODE_DETAIL_BY_CONTENT_ID(string fsCONTENT_ID_LI)
        {
            clsVOD_EPISODE_DETAIL clsVOD_EPISODE_DETAIL = new clsVOD_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsCONTENT_ID_LI", fsCONTENT_ID_LI);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SHOW_EPISODE_DETAIL_BY_CONTENT_ID", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_EPISODE_DETAIL.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                clsVOD_EPISODE_DETAIL.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_EPISODE_DETAIL.fsHOST = dtVOD.Rows[0]["fsHOST"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsGUEST = dtVOD.Rows[0]["fsGUEST"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                clsVOD_EPISODE_DETAIL.fsCOUNTRY = dtVOD.Rows[0]["fsCOUNTRY"].ToString();
                clsVOD_EPISODE_DETAIL.fsDESCRIPTION = dtVOD.Rows[0]["fsDESCRIPTION"].ToString();
                clsVOD_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_EPISODE_DETAIL.fsGRADED = dtVOD.Rows[0]["fsGRADED"].ToString();
                clsVOD_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_EPISODE_DETAIL.fsVENDOR = dtVOD.Rows[0]["fsVENDOR"].ToString();
                clsVOD_EPISODE_DETAIL.fsRIGHT = dtVOD.Rows[0]["fsRIGHT"].ToString().Split(',').ToList();
                clsVOD_EPISODE_DETAIL.fnDURATION = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fnDURATION"].ToString()) ? int.Parse(dtVOD.Rows[0]["fnDURATION"].ToString()) : 0);

                if (!string.IsNullOrEmpty(dtVOD.Rows[0]["fsTYPE"].ToString()))
                {
                    clsVOD_EPISODE_DETAIL.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                    foreach (var item in dtVOD.Rows[0]["fsTYPE"].ToString().Split('/'))
                    {
                        clsVOD_EPISODE_DETAIL.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                    }
                }
            }

            return clsVOD_EPISODE_DETAIL;
        }

        //取的綜藝推薦影片
        public List<clsVOD_INDEX> fnGET_VOD_SHOW_RELATED(string fsVOD_NO, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SHOW_PROMO", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString()),
                        fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString(),
                        fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString()
                    });
                }
            }


            return clsVOD_INDEXs;
        }

        //取得直擊分類
        public List<clsVOD_SCENE_CATEGORY> fnGET_VOD_SCENE_CATEGORY(string fcREGION)
        {
            List<clsVOD_SCENE_CATEGORY> clsVOD_SCENE_CATEGORYs = new List<clsVOD_SCENE_CATEGORY>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SCENE_CATEGORY", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_SCENE_CATEGORYs.Add(new clsVOD_SCENE_CATEGORY()
                    {
                        fsVOD_NO = (string.IsNullOrEmpty(dtVOD.Rows[i]["fsVOD_NO"].ToString()) ? string.Empty : dtVOD.Rows[i]["fsVOD_NO"].ToString()),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                    });
                }
            }


            return clsVOD_SCENE_CATEGORYs;
        }

        //取得直擊首頁資訊
        public List<clsVOD_INDEX> fnGET_VOD_SCENE_INDEX(int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SCENE_INDEX", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false),
                        fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false),
                        fdDATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdBEG_SHOW_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdBEG_SHOW_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty)
                    });
                }
            }


            return clsVOD_INDEXs;
        }

        //取得直擊系列資訊
        public List<clsVOD_SCENE_INDEX_SERIES> fnGET_VOD_SCENE_INDEX_SERIES(string fsVOD_NO, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_SCENE_INDEX_SERIES> clsVOD_SCENE_INDEX_SERIESs = new List<clsVOD_SCENE_INDEX_SERIES>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SCENE_INDEX_SERIES", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_SCENE_INDEX_SERIESs.Add(new clsVOD_SCENE_INDEX_SERIES()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsPROG_NAME = dtVOD.Rows[i]["fsPROG_NAME"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString()),
                        fdDATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdBEG_SHOW_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdBEG_SHOW_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty)
                    });
                }
            }

            return clsVOD_SCENE_INDEX_SERIESs;
        }

        //取得直擊集數
        public clsVOD_SCENE_EPISODE fnGET_VOD_SCENE_EPISODE(string fsVOD_NO)
        {
            clsVOD_SCENE_EPISODE clsVOD_SCENE_EPISODE = new clsVOD_SCENE_EPISODE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SCENE_EPISODE", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_SCENE_EPISODE.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_SCENE_EPISODE.fbIS_FINAL = (dtVOD.Rows[0]["fbIS_FINAL"].ToString() == "Y" ? true : false);

                clsVOD_SCENE_EPISODE.lstEPISODE = new List<clsVOD_SCENE_EPISODE.clsVOD_SCENE_EPISODE_LIST>();
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_SCENE_EPISODE.lstEPISODE.Add(new clsVOD_SCENE_EPISODE.clsVOD_SCENE_EPISODE_LIST()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString()),
                        fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString(),
                        fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString()
                    });
                }
            }

            return clsVOD_SCENE_EPISODE;
        }

        //取得直擊集數段落資訊
        public clsVOD_SCENE_EPISODE_PART fnGET_VOD_SCENE_EPISODE_PART(string fsVOD_NO, int fnSEQ)
        {
            clsVOD_SCENE_EPISODE_PART clsVOD_SCENE_EPISODE_PART = new clsVOD_SCENE_EPISODE_PART();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SCENE_EPISODE_PART", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_SCENE_EPISODE_PART.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_SCENE_EPISODE_PART.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_SCENE_EPISODE_PART.lstPART = new List<clsVOD_SCENE_EPISODE_PART.clsVOD_SCENE_PART_LIST>();
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_SCENE_EPISODE_PART.lstPART.Add(new clsVOD_SCENE_EPISODE_PART.clsVOD_SCENE_PART_LIST()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fsPART = dtVOD.Rows[i]["fsPART"].ToString(),

                    });
                }
            }

            return clsVOD_SCENE_EPISODE_PART;
        }

        //取得直擊集數段落明細
        public clsVOD_SCENE_EPISODE_DETAIL fnGET_VOD_SCENE_EPISODE_DETAIL(string fsVOD_NO, int fnSEQ, string fsPART)
        {
            clsVOD_SCENE_EPISODE_DETAIL clsVOD_SCENE_EPISODE_DETAIL = new clsVOD_SCENE_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            dicParameters.Add("fsPART", fsPART);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SCENE_EPISODE_DETAIL", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_SCENE_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_SCENE_EPISODE_DETAIL.fsPART = dtVOD.Rows[0]["fsPART"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_SCENE_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fsUNIT_DESCRIPTION = dtVOD.Rows[0]["fsUNIT_DESCRIPTION"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fdDATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdDATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdDATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_SCENE_EPISODE_DETAIL.fsLOCATION = dtVOD.Rows[0]["fsLOCATION"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fsPERSON = dtVOD.Rows[0]["fsPERSON"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fsKEYWORD = dtVOD.Rows[0]["fsKEYWORD"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fsORGANIZE = dtVOD.Rows[0]["fsORGANIZE"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fsCOPYRIGHT = dtVOD.Rows[0]["fsCOPYRIGHT"].ToString().Split(',').ToList();
                clsVOD_SCENE_EPISODE_DETAIL.fsGENER = dtVOD.Rows[0]["fsGENER"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_SCENE_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_SCENE_EPISODE_DETAIL.fdCREATED_DATE = DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00");
                clsVOD_SCENE_EPISODE_DETAIL.fdUPDATED_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00") : DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00"));
                clsVOD_SCENE_EPISODE_DETAIL.fsVIDEO_FROM = dtVOD.Rows[0]["fsVIDEO_FROM"].ToString();
            }

            return clsVOD_SCENE_EPISODE_DETAIL;
        }

        //取得直擊推薦影片
        public List<clsVOD_INDEX> fnGET_VOD_SCENE_RELATED(string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SCENE_PROMO", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString()),
                        fdDATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdCREATED_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty),
                        fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString()
                    });
                }
            }


            return clsVOD_INDEXs;
        }

        //取得文化教育類型
        public List<clsVOD_TYPE> fnGET_VOD_EDUCATION_TYPE(string fcREGION)
        {
            List<clsVOD_TYPE> clsVOD_TYPEs = new List<clsVOD_TYPE>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtTYPE = clsDB.Do_Query("spAPI_GET_VOD_EDUCATION_TYPE", dicParameters);
            if (dtTYPE != null && dtTYPE.Rows.Count > 0)
            {
                for (int i = 0; i < dtTYPE.Rows.Count; i++)
                {
                    clsVOD_TYPEs.Add(new clsVOD_TYPE()
                    {
                        fsCODE = dtTYPE.Rows[i]["fsCODE"].ToString(),
                        fsNAME = dtTYPE.Rows[i]["fsNAME"].ToString(),
                        fcOVERSEAS = (dtTYPE.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false)
                    });
                }
            }
            return clsVOD_TYPEs;
        }

        //取得文化教育首頁資訊V3
        public List<clsVOD_INDEX> fnGET_VOD_EDUCATION_INDEX3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsTYPE2", fsTYPE);
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_EDUCATION_INDEX3", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEX clsVOD_INDEX = new clsVOD_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                    clsVOD_INDEX.fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString();
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString());
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }
                    clsVOD_INDEX.fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString();
                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }
            return clsVOD_INDEXs;
        }

        //取得文化教育首頁資訊-手機用
        public clsVOD_INDEX_MOBILE fnGET_VOD_EDUCATION_INDEX_MOBILE()
        {
            clsVOD_INDEX_MOBILE clsVOD_INDEX_MOBILE = new clsVOD_INDEX_MOBILE();
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_EDUCATION_INDEX_MOBILE", null);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_INDEX_MOBILE.fsHOT = new List<string>();
                clsVOD_INDEX_MOBILE.fsNEW = new List<string>();
                List<DataRow> drVOD_NEW = dtVOD.Select("fsTYPE = 'new'").ToList();
                List<DataRow> drVOD_HOT = dtVOD.Select("fsTYPE = 'hot'").ToList();
                foreach (var item in drVOD_NEW)
                {
                    clsVOD_INDEX_MOBILE.fsNEW.Add(item["fsVOD_NO"].ToString());
                }
                foreach (var item in drVOD_HOT)
                {
                    clsVOD_INDEX_MOBILE.fsHOT.Add(item["fsVOD_NO"].ToString());
                }

            }


            return clsVOD_INDEX_MOBILE;
        }

        //取得文化教育內頁集數資訊
        public clsVOD_EPISODE fnGET_VOD_EDUCATION_EPISODE(string fsVOD_NO)
        {
            clsVOD_EPISODE clsVOD_EPISODE = new clsVOD_EPISODE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_EDUCATION_EPISODE", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE.fsPROG_NAME = dtVOD.Rows[0]["fsPROG_NAME"].ToString();
                clsVOD_EPISODE.fbIS_FINAL = (dtVOD.Rows[0]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                clsVOD_EPISODE.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[0]["fnTOTAL_EPISODE"].ToString());

                clsVOD_EPISODE.lstEPISODE = new List<clsVOD_EPISODE.clsVOD_EPISODE_LIST>();
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {

                    clsVOD_EPISODE.lstEPISODE.Add(new clsVOD_EPISODE.clsVOD_EPISODE_LIST()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString()),
                        fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty)
                    });
                }
            }

            return clsVOD_EPISODE;
        }

        //取得文化教育集數明細
        public clsVOD_EPISODE_DETAIL fnGET_VOD_EDUCATION_EPISODE_DETAIL(string fsVOD_NO, int fnSEQ)
        {
            clsVOD_EPISODE_DETAIL clsVOD_EPISODE_DETAIL = new clsVOD_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_EDUCATION_EPISODE_DETAIL", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_EPISODE_DETAIL.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                clsVOD_EPISODE_DETAIL.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_EPISODE_DETAIL.fsACTOR = dtVOD.Rows[0]["fsACTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                clsVOD_EPISODE_DETAIL.fsWRITER = dtVOD.Rows[0]["fsWRITER"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsDIRECTOR = dtVOD.Rows[0]["fsDIRECTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsCOUNTRY = dtVOD.Rows[0]["fsCOUNTRY"].ToString();
                clsVOD_EPISODE_DETAIL.fsDESCRIPTION = dtVOD.Rows[0]["fsDESCRIPTION"].ToString();
                clsVOD_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_EPISODE_DETAIL.fsGRADED = dtVOD.Rows[0]["fsGRADED"].ToString();
                clsVOD_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_EPISODE_DETAIL.fsVENDOR = dtVOD.Rows[0]["fsVENDOR"].ToString();
                clsVOD_EPISODE_DETAIL.fsRIGHT = dtVOD.Rows[0]["fsRIGHT"].ToString().Split(',').ToList();
                clsVOD_EPISODE_DETAIL.fnDURATION = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fnDURATION"].ToString()) ? int.Parse(dtVOD.Rows[0]["fnDURATION"].ToString()) : 0);
                clsVOD_EPISODE_DETAIL.fdCREATED_DATE = DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00");
                clsVOD_EPISODE_DETAIL.fdUPDATED_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00") : DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00"));

                if (!string.IsNullOrEmpty(dtVOD.Rows[0]["fsTYPE"].ToString()))
                {
                    clsVOD_EPISODE_DETAIL.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                    foreach (var item in dtVOD.Rows[0]["fsTYPE"].ToString().Split('/'))
                    {
                        clsVOD_EPISODE_DETAIL.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                    }
                }
                clsVOD_EPISODE_DETAIL.fsVIDEO_FROM = dtVOD.Rows[0]["fsVIDEO_FROM"].ToString();
            }

            return clsVOD_EPISODE_DETAIL;
        }

        //取的文化教育推薦影片
        public List<clsVOD_INDEX> fnGET_VOD_EDUCATION_RELATED(string fsVOD_NO, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_EDUCATION_PROMO", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString()),
                        fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString()
                    });
                }
            }


            return clsVOD_INDEXs;
        }

        //取VOD廣告破口
        public List<int> fnGET_VOD_TIME_CODE(string fsASSET_ID)
        {
            List<int> lstTIME_CODE = new List<int>();
            double fnDURATION = 0.0;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsASSET_ID", fsASSET_ID);
            DataTable dtTIME_CODE = clsDB.Do_Query("spAPI_GET_VOD_TIME_CODE", dicParameters);

            //if (dtTIME_CODE != null && dtTIME_CODE.Rows.Count > 0 && !string.IsNullOrEmpty(dtTIME_CODE.Rows[0]["fsTIME_CODE"].ToString()))
            //{
            //    for (int i = 0; i < dtTIME_CODE.Rows[0]["fsTIME_CODE"].ToString().Split(',').Count(); i++)
            //    {
            //        lstTIME_CODE.Add(int.Parse(dtTIME_CODE.Rows[0]["fsTIME_CODE"].ToString().Split(',')[i].ToString()));
            //    }
            //}

            if (dtTIME_CODE != null && dtTIME_CODE.Rows.Count > 0)
            {
                //已有破口
                if (!string.IsNullOrEmpty(dtTIME_CODE.Rows[0]["fsTIME_CODE"].ToString()))
                {
                    for (int i = 0; i < dtTIME_CODE.Rows[0]["fsTIME_CODE"].ToString().Split(',').Count(); i++)
                    {
                        lstTIME_CODE.Add(int.Parse(dtTIME_CODE.Rows[0]["fsTIME_CODE"].ToString().Split(',')[i].ToString()));
                    }
                }
                else
                {
                    if (double.TryParse(dtTIME_CODE.Rows[0]["fnDURATION"].ToString(), out fnDURATION))
                    {
                        int fnTIME_CODE = 900;

                        if (fnDURATION > 0)
                        {
                            //最後一個破口廣告後面至少要有3分鐘正片才可以打
                            while (fnDURATION > fnTIME_CODE + 180)
                            {
                                lstTIME_CODE.Add(fnTIME_CODE);
                                fnTIME_CODE += 900;
                            }
                        }
                        else
                        {
                            while (3 * 60 * 60 > fnTIME_CODE)
                            {
                                lstTIME_CODE.Add(fnTIME_CODE);
                                fnTIME_CODE += 900;
                            }
                        }
                    }
                }
            }

            return lstTIME_CODE;
        }

        //根據Series取得VOD編號
        public clsVOD_SERIES fnGET_VOD_NO_SEQ_BY_SERIES_ID(string fsSERIES_ID_LITV, int fnEPISODE)
        {
            clsVOD_SERIES clsVOD_SERIES = new clsVOD_SERIES();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsSERIES_ID_LITV", fsSERIES_ID_LITV);
            dicParameters.Add("fnEPISODE", fnEPISODE.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_NO_SEQ_BY_SERIES_ID", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_SERIES.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_SERIES.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
            }

            return clsVOD_SERIES;
        }

        //根據VOD編號取得Series
        public clsVOD_SERIES fnGET_SERIES_ID_BY_VOD_NO_SEQ(string fsVOD_NO, int fnSEQ)
        {
            clsVOD_SERIES clsVOD_SERIES = new clsVOD_SERIES();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_SERIES_ID_BY_VOD_NO_SEQ", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_SERIES.fsSERIES_ID_LITV = dtVOD.Rows[0]["fsSERIES_ID_LITV"].ToString();
                clsVOD_SERIES.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
            }

            return clsVOD_SERIES;
        }

        //根據類型取得所有VOD
        public List<clsVOD> fnGET_VOD_BY_TYPE(string fsTYPE, string fcREGION)
        {
            List<clsVOD> clsVODs = new List<clsVOD>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsTYPE", fsTYPE);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_BY_TYPE", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVODs.Add(new clsVOD()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString(),
                        fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString())
                    });
                }
            }

            return clsVODs;
        }

        //取得VOD來源
        public clsVOD_FROM fnGET_VOD_FROM(string fsASSET_ID)
        {
            clsVOD_FROM clsVOD_FROM = new clsVOD_FROM();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsASSET_ID", fsASSET_ID);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_SOURCE_FROM", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_FROM.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_FROM.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_FROM.fsFILE_NO = dtVOD.Rows[0]["fsFILE_NO"].ToString();
                clsVOD_FROM.fsFROM = dtVOD.Rows[0]["fsFROM"].ToString();
                clsVOD_FROM.fsVENDOR = dtVOD.Rows[0]["fsVENDOR"].ToString();
                clsVOD_FROM.fcOVERSEAS = (dtVOD.Rows[0]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                clsVOD_FROM.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_FROM.fcFREE = (dtVOD.Rows[0]["fcFREE"].ToString() == "Y" ? true : false);
                clsVOD_FROM.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                clsVOD_FROM.fcCASE_CHT = dtVOD.Rows[0]["fcCASE_CHT"].ToString();
                clsVOD_FROM.fsWRITTEN_BY = dtVOD.Rows[0]["fsWRITTEN_BY"].ToString();
                clsVOD_FROM.lstEXCEPT_COUNTRY = dtVOD.Rows[0]["fsEXCEPT_COUNTRY"].ToString().Split(',').ToList();
            }
            return clsVOD_FROM;
        }

        //取得VOD在某時間後新增或更新的
        public List<clsVOD_INDEX> fnGET_VOD_BY_UPDATEDATE(double fnSECONDS)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fdDATE", new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(fnSECONDS).ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss"));
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_BY_UPDATEDATE", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEX clsVOD_INDEX = new clsVOD_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_INDEX.fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString();
                    clsVOD_INDEX.fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString();
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fbDOWN = (dtVOD.Rows[i]["fcDOWN"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString());
                    clsVOD_INDEX.fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fsEnglishTitle = dtVOD.Rows[i]["fsATTRIBUTE2"].ToString();
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }

                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }


            return clsVOD_INDEXs;
        }

        //取得VOD列表(BOX用)
        public List<clsVOD_BOX> fnGET_VOD_ALL_BOX(string fsTYPE1, string fcREGION)
        {
            List<clsVOD_BOX> clsVOD_BOXs = new List<clsVOD_BOX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsTYPE1", fsTYPE1);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_BOX_GET_VOD_ALL", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_BOXs.Add(new clsVOD_BOX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString()
                    });
                }
            }

            return clsVOD_BOXs;
        }

        //取得會員VOD權限
        public clsGET_MEMBER_VOD_AUTH fnGET_MEMBER_VOD_AUTH(string fsACCOUNT_ID, string fsVOD_NO)
        {
            clsGET_MEMBER_VOD_AUTH clsGET_MEMBER_VOD_AUTH = new clsGET_MEMBER_VOD_AUTH();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtMEMBER_VOD_AUTH = clsDB.Do_Query("spAPI_GET_MEMBER_VOD_AUTH", dicParameters);
            if (dtMEMBER_VOD_AUTH != null && dtMEMBER_VOD_AUTH.Rows.Count > 0)
            {
                clsGET_MEMBER_VOD_AUTH.fsACCOUNT_ID = dtMEMBER_VOD_AUTH.Rows[0]["fsACCOUNT_ID"].ToString();
                clsGET_MEMBER_VOD_AUTH.fsVOD_NO = dtMEMBER_VOD_AUTH.Rows[0]["fsVOD_NO"].ToString();
                clsGET_MEMBER_VOD_AUTH.fcAUTH = (dtMEMBER_VOD_AUTH.Rows[0]["fcAUTH"].ToString() == "Y" ? true : false);
                clsGET_MEMBER_VOD_AUTH.fcAD = (dtMEMBER_VOD_AUTH.Rows[0]["fcAD"].ToString() == "Y" ? true : false);
                clsGET_MEMBER_VOD_AUTH.fcIS_PAY_MEMBER = (dtMEMBER_VOD_AUTH.Rows[0]["fcIS_PAY_MEMBER"].ToString() == "Y" ? true : false);
            }
            return clsGET_MEMBER_VOD_AUTH;
        }

        //取得網紅類型
        public List<clsVOD_TYPE> fnGET_VOD_CELEBRITY_TYPE(string fcREGION)
        {
            List<clsVOD_TYPE> clsVOD_TYPEs = new List<clsVOD_TYPE>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtTYPE = clsDB.Do_Query("spAPI_GET_VOD_CELEBRITY_TYPE", dicParameters);
            if (dtTYPE != null && dtTYPE.Rows.Count > 0)
            {
                for (int i = 0; i < dtTYPE.Rows.Count; i++)
                {
                    clsVOD_TYPEs.Add(new clsVOD_TYPE()
                    {
                        fsCODE = dtTYPE.Rows[i]["fsCODE"].ToString(),
                        fsNAME = dtTYPE.Rows[i]["fsNAME"].ToString(),
                        fcOVERSEAS = (dtTYPE.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false)
                    });
                }
            }
            return clsVOD_TYPEs;
        }

        //取得網紅首頁資訊V3
        public List<clsVOD_INDEX> fnGET_VOD_CELEBRITY_INDEX3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsTYPE2", fsTYPE);
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_CELEBRITY_INDEX3", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEX clsVOD_INDEX = new clsVOD_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();                  
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                    clsVOD_INDEX.fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString();
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString());
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();

                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }
                    clsVOD_INDEX.fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString();
                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }
            return clsVOD_INDEXs;
        }

        //取得網紅首頁資訊-手機用
        public clsVOD_INDEX_MOBILE fnGET_VOD_CELEBRITY_INDEX_MOBILE()
        {
            clsVOD_INDEX_MOBILE clsVOD_INDEX_MOBILE = new clsVOD_INDEX_MOBILE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_CELEBRITY_INDEX_MOBILE", null);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_INDEX_MOBILE.fsHOT = new List<string>();
                clsVOD_INDEX_MOBILE.fsNEW = new List<string>();
                List<DataRow> drVOD_NEW = dtVOD.Select("fsTYPE = 'new'").ToList();
                List<DataRow> drVOD_HOT = dtVOD.Select("fsTYPE = 'hot'").ToList();
                foreach (var item in drVOD_NEW)
                {
                    clsVOD_INDEX_MOBILE.fsNEW.Add(item["fsVOD_NO"].ToString());
                }
                foreach (var item in drVOD_HOT)
                {
                    clsVOD_INDEX_MOBILE.fsHOT.Add(item["fsVOD_NO"].ToString());
                }
            }
            return clsVOD_INDEX_MOBILE;
        }

        //取得網紅內頁集數資訊
        public clsVOD_EPISODE fnGET_VOD_CELEBRITY_EPISODE(string fsVOD_NO)
        {
            clsVOD_EPISODE clsVOD_EPISODE = new clsVOD_EPISODE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_CELEBRITY_EPISODE", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE.fsPROG_NAME = dtVOD.Rows[0]["fsPROG_NAME"].ToString();
                clsVOD_EPISODE.fbIS_FINAL = (dtVOD.Rows[0]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                clsVOD_EPISODE.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[0]["fnTOTAL_EPISODE"].ToString());
                List<clsVOD_EPISODE.clsVOD_EPISODE_LIST> lstEPISODE = new List<clsVOD_EPISODE.clsVOD_EPISODE_LIST>();
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_EPISODE_LIST clsVOD_EPISODE_LIST = new clsVOD_EPISODE_LIST();
                    clsVOD_EPISODE_LIST.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_EPISODE_LIST.fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString());
                    clsVOD_EPISODE_LIST.fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString());
                    clsVOD_EPISODE_LIST.fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString();
                    clsVOD_EPISODE_LIST.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_EPISODE_LIST.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);

                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        var aaa = dtVOD.Rows[i]["fsTYPE"].ToString();

                        List<clsVOD_EPISODE_DETAIL_TYPE> lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();

                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                clsVOD_EPISODE_DETAIL_TYPE clsVOD_EPISODE_DETAIL_TYPE = new clsVOD_EPISODE_DETAIL_TYPE();
                                clsVOD_EPISODE_DETAIL_TYPE.fsCODE = item.Split(';')[0];
                                clsVOD_EPISODE_DETAIL_TYPE.fsNAME = item.Split(';')[1];
                                lstTYPE.Add(clsVOD_EPISODE_DETAIL_TYPE);
                            }
                        }
                        clsVOD_EPISODE_LIST.lstTYPE = lstTYPE;
                    }
                    lstEPISODE.Add(clsVOD_EPISODE_LIST);
                }
                clsVOD_EPISODE.lstEPISODE = lstEPISODE;
            }
            return clsVOD_EPISODE;
        }

        //取得網紅集數明細
        public clsVOD_EPISODE_DETAIL fnGET_VOD_CELEBRITY_EPISODE_DETAIL(string fsVOD_NO, int fnSEQ)
        {
            clsVOD_EPISODE_DETAIL clsVOD_EPISODE_DETAIL = new clsVOD_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_CELEBRITY_EPISODE_DETAIL", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_EPISODE_DETAIL.fsACTOR = dtVOD.Rows[0]["fsACTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                clsVOD_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                if (!string.IsNullOrEmpty(dtVOD.Rows[0]["fsTYPE"].ToString()))
                {
                    clsVOD_EPISODE_DETAIL.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                    foreach (var item in dtVOD.Rows[0]["fsTYPE"].ToString().Split('/'))
                    {
                        clsVOD_EPISODE_DETAIL.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                    }
                }
                clsVOD_EPISODE_DETAIL.fsCOUNTRY = dtVOD.Rows[0]["fsCOUNTRY"].ToString();
                clsVOD_EPISODE_DETAIL.fsDESCRIPTION = dtVOD.Rows[0]["fsDESCRIPTION"].ToString();
                clsVOD_EPISODE_DETAIL.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_EPISODE_DETAIL.fsGRADED = dtVOD.Rows[0]["fsGRADED"].ToString();
                clsVOD_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_EPISODE_DETAIL.fsRIGHT = dtVOD.Rows[0]["fsRIGHT"].ToString().Split(',').ToList();
                clsVOD_EPISODE_DETAIL.fsPRODUCER = dtVOD.Rows[0]["fsPRODUCER"].ToString();
                clsVOD_EPISODE_DETAIL.fnDURATION = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fnDURATION"].ToString()) ? int.Parse(dtVOD.Rows[0]["fnDURATION"].ToString()) : 0);
                clsVOD_EPISODE_DETAIL.fsPLATFROM = dtVOD.Rows[0]["fsPLATFROM"].ToString();
                clsVOD_EPISODE_DETAIL.fsHYPERLINK1 = dtVOD.Rows[0]["fsHYPERLINK1"].ToString();
                clsVOD_EPISODE_DETAIL.fsHYPERLINK2 = dtVOD.Rows[0]["fsHYPERLINK2"].ToString();
                clsVOD_EPISODE_DETAIL.fdCREATED_DATE = DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00");
                clsVOD_EPISODE_DETAIL.fdUPDATED_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00") : DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00"));
                clsVOD_EPISODE_DETAIL.fsVIDEO_FROM = dtVOD.Rows[0]["fsVIDEO_FROM"].ToString();
            }
            return clsVOD_EPISODE_DETAIL;
        }

        //取的網紅推薦影片
        public List<clsVOD_INDEX> fnGET_VOD_CELEBRITY_RELATED(string fsVOD_NO, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_CELEBRITY_PROMO", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString()),
                        fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString()
                    });
                }
            }
            return clsVOD_INDEXs;
        }

        //取得網紅首頁推薦
        public List<clsCELEBRITY_PROMO> fnGET_CELEBRITY_PROMO(string fcREGION)
        {
            List<clsCELEBRITY_PROMO> clsCELEBRITY_PROMOs = new List<clsCELEBRITY_PROMO>();
            DataTable dtCELEBRITY_PROMO = clsDB.Do_Query("spAPI_GET_CELEBRITY_PROMO", null);
            if (dtCELEBRITY_PROMO != null && dtCELEBRITY_PROMO.Rows.Count > 0)
            {
                for (int i = 0; i < dtCELEBRITY_PROMO.Rows.Count; i++)
                {
                    clsCELEBRITY_PROMO clsCELEBRITY_PROMO = new clsCELEBRITY_PROMO();
                    clsCELEBRITY_PROMO.fnID = int.Parse(dtCELEBRITY_PROMO.Rows[i]["fnID"].ToString());
                    clsCELEBRITY_PROMO.fsPROMO_TYPE = dtCELEBRITY_PROMO.Rows[i]["fsTYPE"].ToString();
                    clsCELEBRITY_PROMO.fsPROMO_TITLE = dtCELEBRITY_PROMO.Rows[i]["fsTITLE"].ToString();
                    clsCELEBRITY_PROMO.fsMORE_LINK = dtCELEBRITY_PROMO.Rows[i]["fsMORE_LINK"].ToString();

                    //取推薦明細
                    Dictionary<string, string> dicParameters = new Dictionary<string, string>();
                    dicParameters.Add("fnID", dtCELEBRITY_PROMO.Rows[i]["fnID"].ToString());
                    dicParameters.Add("fcREGION", fcREGION);
                    DataTable dtPROMO_D = clsDB.Do_Query("spAPI_GET_CELEBRITY_PROMO_D", dicParameters);
                    if (dtPROMO_D != null && dtPROMO_D.Rows.Count > 0)
                    {
                        List<clsCELEBRITY_PROMO_DETAIL> clsPROMO_DETAILs = new List<clsCELEBRITY_PROMO_DETAIL>();
                        for (int j = 0; j < dtPROMO_D.Rows.Count; j++)
                        {
                            clsCELEBRITY_PROMO_DETAIL clsCELEBRITY_PROMO_DETAIL = new clsCELEBRITY_PROMO_DETAIL();
                            clsCELEBRITY_PROMO_DETAIL.fsTYPE = dtPROMO_D.Rows[j]["fsTYPE1"].ToString();
                            clsCELEBRITY_PROMO_DETAIL.fsTYPE_NAME = dtPROMO_D.Rows[j]["fsTYPE_NAME"].ToString();
                            clsCELEBRITY_PROMO_DETAIL.fsVOD_NO = dtPROMO_D.Rows[j]["fsVOD_NO"].ToString();
                            clsCELEBRITY_PROMO_DETAIL.fnSEQ = int.Parse(string.IsNullOrEmpty(dtPROMO_D.Rows[j]["fnSEQ"].ToString()) ? "0": dtPROMO_D.Rows[j]["fnSEQ"].ToString());
                            clsCELEBRITY_PROMO_DETAIL.fsTITLE = dtPROMO_D.Rows[j]["fsTITLE"].ToString();
                            clsCELEBRITY_PROMO_DETAIL.fdDATE = (string.IsNullOrEmpty(dtPROMO_D.Rows[j]["fdSTART_DATE"].ToString()) ? string.Empty : DateTime.Parse(dtPROMO_D.Rows[j]["fdSTART_DATE"].ToString()).ToString("yyyy/MM/dd"));
                            clsCELEBRITY_PROMO_DETAIL.fnYEAR = (string.IsNullOrEmpty(dtPROMO_D.Rows[j]["fnYEAR"].ToString()) ? 0 : int.Parse(dtPROMO_D.Rows[j]["fnYEAR"].ToString()));
                            clsCELEBRITY_PROMO_DETAIL.fsIMAGE = dtPROMO_D.Rows[j]["fsHEAD_FRAME"].ToString();
                            clsCELEBRITY_PROMO_DETAIL.fnTOTAL_EPISODE = (string.IsNullOrEmpty(dtPROMO_D.Rows[j]["fnTOTAL_EPISODE"].ToString()) ? 0 : int.Parse(dtPROMO_D.Rows[j]["fnTOTAL_EPISODE"].ToString()));
                            clsCELEBRITY_PROMO_DETAIL.fbIS_FINAL = (dtPROMO_D.Rows[j]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                            clsCELEBRITY_PROMO_DETAIL.fsQUALITY = dtPROMO_D.Rows[j]["fsQUALITY"].ToString();
                            clsCELEBRITY_PROMO_DETAIL.fsDESCRIPTION = dtPROMO_D.Rows[j]["fsDESCRIPTION"].ToString();
                            clsCELEBRITY_PROMO_DETAIL.fsUNIT_NAME = dtPROMO_D.Rows[j]["fsUNIT_NAME"].ToString();
                            clsCELEBRITY_PROMO_DETAIL.fsVIDEO_FROM = dtPROMO_D.Rows[j]["fsVIDEO_FROM"].ToString();
                            if (clsCELEBRITY_PROMO_DETAIL.fnSEQ==0)
                            {
                                List<clsVOD_EPISODE_LIST> clsVOD_EPISODE_LISTs = new List<clsVOD_EPISODE_LIST>();
                                //取推薦明細
                                Dictionary<string, string> dicParameters_d = new Dictionary<string, string>();
                                dicParameters_d.Add("fsVOD_NO", dtPROMO_D.Rows[j]["fsVOD_NO"].ToString());
                                DataTable dtPROMO_D_D = clsDB.Do_Query("spAPI_GET_CELEBRITY_PROMO_D_2", dicParameters_d);
                                if (dtPROMO_D_D != null && dtPROMO_D_D.Rows.Count > 0)
                                {
                                    for (int k = 0; k < dtPROMO_D_D.Rows.Count; k++)
                                    {
                                        clsVOD_EPISODE_LIST clsVOD_EPISODE_LIST = new clsVOD_EPISODE_LIST();
                                        clsVOD_EPISODE_LIST.fsVOD_NO = dtPROMO_D_D.Rows[k]["fsVOD_NO"].ToString();
                                        clsVOD_EPISODE_LIST.fnSEQ = int.Parse(dtPROMO_D_D.Rows[k]["fnSEQ"].ToString());
                                        clsVOD_EPISODE_LIST.fnEPISODE = int.Parse(dtPROMO_D_D.Rows[k]["fnEPISODE"].ToString());
                                        clsVOD_EPISODE_LIST.fsUNIT_NAME = dtPROMO_D_D.Rows[k]["fsUNIT_NAME"].ToString();
                                        clsVOD_EPISODE_LIST.fsHEAD_FRAME = dtPROMO_D_D.Rows[k]["fsHEAD_FRAME"].ToString();
                                        clsVOD_EPISODE_LIST.fdPLAY_DATE = (!string.IsNullOrEmpty(dtPROMO_D_D.Rows[k]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtPROMO_D_D.Rows[k]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                                        clsVOD_EPISODE_LISTs.Add(clsVOD_EPISODE_LIST);
                                    }
                                    clsCELEBRITY_PROMO_DETAIL.clsVOD_EPISODE_LIST = clsVOD_EPISODE_LISTs;
                                }
                            }
                            clsPROMO_DETAILs.Add(clsCELEBRITY_PROMO_DETAIL);
                            clsCELEBRITY_PROMO.PROMO_DETAIL = clsPROMO_DETAILs;
                        }
                    }
                    else
                    {
                        clsCELEBRITY_PROMO.PROMO_DETAIL = new List<clsCELEBRITY_PROMO_DETAIL>();
                    }
                    clsCELEBRITY_PROMOs.Add(clsCELEBRITY_PROMO);
                }
            }
            return clsCELEBRITY_PROMOs;
        }

        //取得網紅個人首頁
        public clsCELEBRITY_HOME fnGET_CELEBRITY_HOME(string fsVOD_NO)
        {
            clsCELEBRITY_HOME clsCELEBRITY_HOME = new clsCELEBRITY_HOME();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtCELEBRITY_HOME = clsDB.Do_Query("spAPI_GET_CELEBRITY_HOME", dicParameters);
            if (dtCELEBRITY_HOME != null && dtCELEBRITY_HOME.Rows.Count > 0)
            {
                clsCELEBRITY_HOME.fsTITLE = dtCELEBRITY_HOME.Rows[0]["fsTITLE"].ToString();
                clsCELEBRITY_HOME.fsDESCRIPTION = dtCELEBRITY_HOME.Rows[0]["fsDESCRIPTION"].ToString();
                clsCELEBRITY_HOME.fsHEAD_FRAME1_URL = dtCELEBRITY_HOME.Rows[0]["fsHEAD_FRAME1_URL"].ToString();
                clsCELEBRITY_HOME.fcOVERSEAS = (dtCELEBRITY_HOME.Rows[0]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                clsCELEBRITY_HOME.fnTEMP_ID = int.Parse(dtCELEBRITY_HOME.Rows[0]["fnTEMP_ID"].ToString());
                clsCELEBRITY_HOME.fsVIDEO_FROM = dtCELEBRITY_HOME.Rows[0]["fsVIDEO_FROM"].ToString();
                switch (clsCELEBRITY_HOME.fnTEMP_ID)
                {
                    case 1:
                        clsCELEBRITY_HOME_TEMP_1 clsCELEBRITY_HOME_TEMP_1 = new clsCELEBRITY_HOME_TEMP_1();
                        Dictionary<string, string> dicParameters2 = new Dictionary<string, string>();
                        dicParameters2.Add("fsVOD_NO", fsVOD_NO);
                        DataTable dtCELEBRITY_HOME_TEMP_1 = clsDB.Do_Query("spAPI_GET_CELEBRITY_HOME_TEMP_1", dicParameters2);
                        if (dtCELEBRITY_HOME_TEMP_1 != null && dtCELEBRITY_HOME_TEMP_1.Rows.Count > 0)
                        {
                            clsCELEBRITY_HOME_TEMP_1.fsTEMP_NAME = dtCELEBRITY_HOME_TEMP_1.Rows[0]["fsTEMP_NAME"].ToString();
                            clsCELEBRITY_HOME_TEMP_1.fsHOME_TEMP1_URL = dtCELEBRITY_HOME_TEMP_1.Rows[0]["fsHOME_TEMP1_URL"].ToString();
                            clsCELEBRITY_HOME_TEMP_1.fsHOME_TEMP1_URL_APP = dtCELEBRITY_HOME_TEMP_1.Rows[0]["fsHOME_TEMP1_URL_APP"].ToString();
                        }

                        List<clsCELEBRITY_HOME_LINK> listCELEBRITY_HOME_LINK = new List<clsCELEBRITY_HOME_LINK>();
                        Dictionary<string, string> dicParameters3 = new Dictionary<string, string>();
                        dicParameters3.Add("fsVOD_NO", fsVOD_NO);
                        DataTable dtCELEBRITY_HOME_LINK = clsDB.Do_Query("spAPI_GET_CELEBRITY_HOME_LINK", dicParameters3);

                        if (dtCELEBRITY_HOME_LINK != null && dtCELEBRITY_HOME_LINK.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtCELEBRITY_HOME_LINK.Rows.Count; i++)
                            {
                                clsCELEBRITY_HOME_LINK clsCELEBRITY_HOME_LINK = new clsCELEBRITY_HOME_LINK();
                                clsCELEBRITY_HOME_LINK.fsTYPE = dtCELEBRITY_HOME_LINK.Rows[i]["fsTYPE"].ToString();
                                clsCELEBRITY_HOME_LINK.fsTYPE_NAME = dtCELEBRITY_HOME_LINK.Rows[i]["fsTYPE_NAME"].ToString();
                                clsCELEBRITY_HOME_LINK.fsHOME_LINK_URL = dtCELEBRITY_HOME_LINK.Rows[i]["fsHOME_LINK_URL"].ToString();
                                listCELEBRITY_HOME_LINK.Add(clsCELEBRITY_HOME_LINK);
                            }
                            clsCELEBRITY_HOME_TEMP_1.listCELEBRITY_HOME_LINK = listCELEBRITY_HOME_LINK;
                        }
                        clsCELEBRITY_HOME.clsCELEBRITY_HOME_TEMP_1 = clsCELEBRITY_HOME_TEMP_1;
                        break;
                    default:

                        break;
                }
            }
            else
            {
                return null;
            }
            return clsCELEBRITY_HOME;
        }
        //取得網紅子集類別
        public List<clsVOD_D_TYPE> fnGET_VOD_CELEBRITY_HOME_TYPE(string fsVOD_NO)
        {
            List<clsVOD_D_TYPE> clsVOD_D_TYPEs = new List<clsVOD_D_TYPE>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtclsVOD_D_TYPE = clsDB.Do_Query("spAPI_GET_CELEBRITY_HOME_TYPE", dicParameters);

            if (dtclsVOD_D_TYPE != null && dtclsVOD_D_TYPE.Rows.Count > 0)
            {
                for (int i = 0; i < dtclsVOD_D_TYPE.Rows.Count; i++)
                {
                    clsVOD_D_TYPEs.Add(new clsVOD_D_TYPE()
                    {
                        fsCODE = dtclsVOD_D_TYPE.Rows[i]["fsCODE"].ToString(),
                        fsNAME = dtclsVOD_D_TYPE.Rows[i]["fsNAME"].ToString(),
                    });
                }
            }
            return clsVOD_D_TYPEs;
        }
        //取得網紅個人首頁資訊
        public List<clsVOD_CELEBRITY_HOME_INDEX> fnGET_VOD_CELEBRITY_HOME_INDEX(string fsVOD_NO, string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE)
        {
            List<clsVOD_CELEBRITY_HOME_INDEX> clsVOD_INDEXs = new List<clsVOD_CELEBRITY_HOME_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fsTYPE2", fsTYPE);
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());

            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_CELEBRITY_HOME_INDEX", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_CELEBRITY_HOME_INDEX clsVOD_INDEX = new clsVOD_CELEBRITY_HOME_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_INDEX.fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString());
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                  
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString();
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }
                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }
            return clsVOD_INDEXs;
        }

        //取得網紅個人推薦(子檔 類別)
        public List<clsVOD_CELEBRITY_HOME_DETAIL_PROMO> fnGET_CELEBRITY_HOME_DETAIL_PROMO(string fsVOD_NO, int fnSEQ, string fcREGION)
        {
            List<clsVOD_CELEBRITY_HOME_DETAIL_PROMO> clsVOD_INDEXs = new List<clsVOD_CELEBRITY_HOME_DETAIL_PROMO>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_CELEBRITY_HOME_DETAIL_PROMO", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_CELEBRITY_HOME_DETAIL_PROMO()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString(),
                        fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString()
                    });
                }
            }
            return clsVOD_INDEXs;
        }
        //取得電影類型
        public List<clsVOD_TYPE> fnGET_VOD_MOVIE_TYPE(string fcREGION)
        {
            List<clsVOD_TYPE> clsVOD_TYPEs = new List<clsVOD_TYPE>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtTYPE = clsDB.Do_Query("spAPI_GET_VOD_MOVIE_TYPE", dicParameters);

            if (dtTYPE != null && dtTYPE.Rows.Count > 0)
            {
                for (int i = 0; i < dtTYPE.Rows.Count; i++)
                {
                    clsVOD_TYPEs.Add(new clsVOD_TYPE()
                    {
                        fsCODE = dtTYPE.Rows[i]["fsCODE"].ToString(),
                        fsNAME = dtTYPE.Rows[i]["fsNAME"].ToString(),
                        fcOVERSEAS = (dtTYPE.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false)
                    });
                }
            }
            return clsVOD_TYPEs;
        }

        //取得電影首頁資訊V3
        public List<clsVOD_INDEX> fnGET_VOD_MOVIE_INDEX3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsTYPE2", fsTYPE);
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_MOVIE_INDEX3", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEX clsVOD_INDEX = new clsVOD_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                    clsVOD_INDEX.fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString();
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString());
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }
                    clsVOD_INDEX.fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString();
                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }
            return clsVOD_INDEXs;
        }

        //取得電影首頁資訊-手機用
        public clsVOD_INDEX_MOBILE fnGET_VOD_MOVIE_INDEX_MOBILE()
        {
            clsVOD_INDEX_MOBILE clsVOD_INDEX_MOBILE = new clsVOD_INDEX_MOBILE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_MOVIE_INDEX_MOBILE", null);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_INDEX_MOBILE.fsHOT = new List<string>();
                clsVOD_INDEX_MOBILE.fsNEW = new List<string>();
                List<DataRow> drVOD_NEW = dtVOD.Select("fsTYPE = 'new'").ToList();
                List<DataRow> drVOD_HOT = dtVOD.Select("fsTYPE = 'hot'").ToList();
                foreach (var item in drVOD_NEW)
                {
                    clsVOD_INDEX_MOBILE.fsNEW.Add(item["fsVOD_NO"].ToString());
                }
                foreach (var item in drVOD_HOT)
                {
                    clsVOD_INDEX_MOBILE.fsHOT.Add(item["fsVOD_NO"].ToString());
                }
            }
            return clsVOD_INDEX_MOBILE;
        }

        //取得電影內頁集數資訊
        public clsVOD_EPISODE fnGET_VOD_MOVIE_EPISODE(string fsVOD_NO)
        {
            clsVOD_EPISODE clsVOD_EPISODE = new clsVOD_EPISODE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_MOVIE_EPISODE", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE.fsPROG_NAME = dtVOD.Rows[0]["fsPROG_NAME"].ToString();
                clsVOD_EPISODE.fbIS_FINAL = (dtVOD.Rows[0]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                clsVOD_EPISODE.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[0]["fnTOTAL_EPISODE"].ToString());
                clsVOD_EPISODE.lstEPISODE = new List<clsVOD_EPISODE.clsVOD_EPISODE_LIST>();
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_EPISODE.lstEPISODE.Add(new clsVOD_EPISODE.clsVOD_EPISODE_LIST()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString()),
                        fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty)
                    });
                }
            }
            return clsVOD_EPISODE;
        }

        //取得電影集數明細
        public clsVOD_EPISODE_DETAIL fnGET_VOD_MOVIE_EPISODE_DETAIL(string fsVOD_NO, int fnSEQ)
        {
            clsVOD_EPISODE_DETAIL clsVOD_EPISODE_DETAIL = new clsVOD_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_MOVIE_EPISODE_DETAIL", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_EPISODE_DETAIL.fsACTOR = dtVOD.Rows[0]["fsACTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsWRITER = dtVOD.Rows[0]["fsWRITER"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsDIRECTOR = dtVOD.Rows[0]["fsDIRECTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                clsVOD_EPISODE_DETAIL.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                if (!string.IsNullOrEmpty(dtVOD.Rows[0]["fsTYPE"].ToString()))
                {
                    clsVOD_EPISODE_DETAIL.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                    foreach (var item in dtVOD.Rows[0]["fsTYPE"].ToString().Split('/'))
                    {
                        clsVOD_EPISODE_DETAIL.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                    }
                }
                clsVOD_EPISODE_DETAIL.fsCOUNTRY = dtVOD.Rows[0]["fsCOUNTRY"].ToString();
                clsVOD_EPISODE_DETAIL.fsDESCRIPTION = dtVOD.Rows[0]["fsDESCRIPTION"].ToString();
                clsVOD_EPISODE_DETAIL.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_EPISODE_DETAIL.fsGRADED = dtVOD.Rows[0]["fsGRADED"].ToString();
                clsVOD_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_EPISODE_DETAIL.fsRIGHT = dtVOD.Rows[0]["fsRIGHT"].ToString().Split(',').ToList();
                clsVOD_EPISODE_DETAIL.fsPRODUCER = dtVOD.Rows[0]["fsPRODUCER"].ToString();
                clsVOD_EPISODE_DETAIL.fnDURATION = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fnDURATION"].ToString()) ? int.Parse(dtVOD.Rows[0]["fnDURATION"].ToString()) : 0);
                clsVOD_EPISODE_DETAIL.fsORIGINAL = dtVOD.Rows[0]["fsORIGINAL"].ToString();
                clsVOD_EPISODE_DETAIL.fdCREATED_DATE = DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00");
                clsVOD_EPISODE_DETAIL.fdUPDATED_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00") : DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00"));
                clsVOD_EPISODE_DETAIL.fsVIDEO_FROM = dtVOD.Rows[0]["fsVIDEO_FROM"].ToString();
            }
            return clsVOD_EPISODE_DETAIL;
        }

        //取的電影推薦影片
        public List<clsVOD_INDEX> fnGET_VOD_MOVIE_RELATED(string fsVOD_NO, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_MOVIE_PROMO", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString()),
                        fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString(),
                        fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString()
                    });
                }
            }
            return clsVOD_INDEXs;
        }

        //取得動漫類型
        public List<clsVOD_TYPE> fnGET_VOD_ANIME_TYPE(string fcREGION)
        {
            List<clsVOD_TYPE> clsVOD_TYPEs = new List<clsVOD_TYPE>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtTYPE = clsDB.Do_Query("spAPI_GET_VOD_ANIME_TYPE", dicParameters);

            if (dtTYPE != null && dtTYPE.Rows.Count > 0)
            {
                for (int i = 0; i < dtTYPE.Rows.Count; i++)
                {
                    clsVOD_TYPEs.Add(new clsVOD_TYPE()
                    {
                        fsCODE = dtTYPE.Rows[i]["fsCODE"].ToString(),
                        fsNAME = dtTYPE.Rows[i]["fsNAME"].ToString(),
                        fcOVERSEAS = (dtTYPE.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false)
                    });
                }
            }
            return clsVOD_TYPEs;
        }

        //取得動漫首頁資訊V3
        public List<clsVOD_INDEX> fnGET_VOD_ANIME_INDEX3(string fsTYPE, string fcSEARCH_TYPE, int fnINDEX, int fnPAGE_SIZE, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsTYPE2", fsTYPE);
            dicParameters.Add("fcSEARCH_TYPE", fcSEARCH_TYPE);
            dicParameters.Add("fnINDEX", fnINDEX.ToString());
            dicParameters.Add("fnPAGE_SIZE", fnPAGE_SIZE.ToString());
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_ANIME_INDEX3", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEX clsVOD_INDEX = new clsVOD_INDEX();
                    clsVOD_INDEX.fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString();
                    clsVOD_INDEX.fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString();
                    clsVOD_INDEX.fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString();
                    clsVOD_INDEX.fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString();
                    clsVOD_INDEX.fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fbIS_FREE = (dtVOD.Rows[i]["fcFREE"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fcOVERSEAS = (dtVOD.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                    clsVOD_INDEX.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString());
                    if (!string.IsNullOrEmpty(dtVOD.Rows[i]["fsTYPE"].ToString()))
                    {
                        clsVOD_INDEX.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                        foreach (var item in dtVOD.Rows[i]["fsTYPE"].ToString().Split('/'))
                        {
                            clsVOD_INDEX.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                        }
                    }
                    clsVOD_INDEX.fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString();
                    clsVOD_INDEXs.Add(clsVOD_INDEX);
                }
            }
            return clsVOD_INDEXs;
        }

        //取得動漫首頁資訊-手機用
        public clsVOD_INDEX_MOBILE fnGET_VOD_ANIME_INDEX_MOBILE()
        {
            clsVOD_INDEX_MOBILE clsVOD_INDEX_MOBILE = new clsVOD_INDEX_MOBILE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_ANIME_INDEX_MOBILE", null);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_INDEX_MOBILE.fsHOT = new List<string>();
                clsVOD_INDEX_MOBILE.fsNEW = new List<string>();
                List<DataRow> drVOD_NEW = dtVOD.Select("fsTYPE = 'new'").ToList();
                List<DataRow> drVOD_HOT = dtVOD.Select("fsTYPE = 'hot'").ToList();
                foreach (var item in drVOD_NEW)
                {
                    clsVOD_INDEX_MOBILE.fsNEW.Add(item["fsVOD_NO"].ToString());
                }
                foreach (var item in drVOD_HOT)
                {
                    clsVOD_INDEX_MOBILE.fsHOT.Add(item["fsVOD_NO"].ToString());
                }
            }
            return clsVOD_INDEX_MOBILE;
        }

        //取得動漫內頁集數資訊
        public clsVOD_EPISODE fnGET_VOD_ANIME_EPISODE(string fsVOD_NO)
        {
            clsVOD_EPISODE clsVOD_EPISODE = new clsVOD_EPISODE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_ANIME_EPISODE", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE.fsPROG_NAME = dtVOD.Rows[0]["fsPROG_NAME"].ToString();
                clsVOD_EPISODE.fbIS_FINAL = (dtVOD.Rows[0]["fbIS_FINAL"].ToString() == "Y" ? true : false);
                clsVOD_EPISODE.fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[0]["fnTOTAL_EPISODE"].ToString());
                clsVOD_EPISODE.lstEPISODE = new List<clsVOD_EPISODE.clsVOD_EPISODE_LIST>();
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_EPISODE.lstEPISODE.Add(new clsVOD_EPISODE.clsVOD_EPISODE_LIST()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtVOD.Rows[i]["fnSEQ"].ToString()),
                        fnEPISODE = int.Parse(dtVOD.Rows[i]["fnEPISODE"].ToString()),
                        fsUNIT_NAME = dtVOD.Rows[i]["fsUNIT_NAME"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[i]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty)
                    });
                }
            }
            return clsVOD_EPISODE;
        }

        //取得動漫集數明細
        public clsVOD_EPISODE_DETAIL fnGET_VOD_ANIME_EPISODE_DETAIL(string fsVOD_NO, int fnSEQ)
        {
            clsVOD_EPISODE_DETAIL clsVOD_EPISODE_DETAIL = new clsVOD_EPISODE_DETAIL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_ANIME_EPISODE_DETAIL", dicParameters);
            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                clsVOD_EPISODE_DETAIL.fsVOD_NO = dtVOD.Rows[0]["fsVOD_NO"].ToString();
                clsVOD_EPISODE_DETAIL.fnSEQ = int.Parse(dtVOD.Rows[0]["fnSEQ"].ToString());
                clsVOD_EPISODE_DETAIL.fsTITLE = dtVOD.Rows[0]["fsTITLE"].ToString();
                clsVOD_EPISODE_DETAIL.fsUNIT_NAME = dtVOD.Rows[0]["fsUNIT_NAME"].ToString();
                clsVOD_EPISODE_DETAIL.fnEPISODE = int.Parse(dtVOD.Rows[0]["fnEPISODE"].ToString());
                clsVOD_EPISODE_DETAIL.fsACTOR = dtVOD.Rows[0]["fsACTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsVOICEACTOR = dtVOD.Rows[0]["fsVOICEACTOR"].ToString().Replace("\r\n", "");
                clsVOD_EPISODE_DETAIL.fsURL_ID = dtVOD.Rows[0]["fsURL_ID"].ToString();
                clsVOD_EPISODE_DETAIL.fnYEAR = int.Parse(dtVOD.Rows[0]["fnYEAR"].ToString());
                if (!string.IsNullOrEmpty(dtVOD.Rows[0]["fsTYPE"].ToString()))
                {
                    clsVOD_EPISODE_DETAIL.lstTYPE = new List<clsVOD_EPISODE_DETAIL_TYPE>();
                    foreach (var item in dtVOD.Rows[0]["fsTYPE"].ToString().Split('/'))
                    {
                        clsVOD_EPISODE_DETAIL.lstTYPE.Add(new clsVOD_EPISODE_DETAIL_TYPE() { fsCODE = item.Split(';')[0], fsNAME = item.Split(';')[1] });
                    }
                }
                clsVOD_EPISODE_DETAIL.fsCOUNTRY = dtVOD.Rows[0]["fsCOUNTRY"].ToString();
                clsVOD_EPISODE_DETAIL.fsDESCRIPTION = dtVOD.Rows[0]["fsDESCRIPTION"].ToString();
                clsVOD_EPISODE_DETAIL.fdPLAY_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdPLAY_DATE"].ToString()).ToString("yyyy-MM-dd") : string.Empty);
                clsVOD_EPISODE_DETAIL.fsHEAD_FRAME = dtVOD.Rows[0]["fsHEAD_FRAME"].ToString();
                clsVOD_EPISODE_DETAIL.fsGRADED = dtVOD.Rows[0]["fsGRADED"].ToString();
                clsVOD_EPISODE_DETAIL.fnGRADED = int.Parse(dtVOD.Rows[0]["fnGRADED"].ToString());
                clsVOD_EPISODE_DETAIL.fsRIGHT = dtVOD.Rows[0]["fsRIGHT"].ToString().Split(',').ToList();
                clsVOD_EPISODE_DETAIL.fsPRODUCER = dtVOD.Rows[0]["fsPRODUCER"].ToString();
                clsVOD_EPISODE_DETAIL.fnDURATION = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fnDURATION"].ToString()) ? int.Parse(dtVOD.Rows[0]["fnDURATION"].ToString()) : 0);
                clsVOD_EPISODE_DETAIL.fdCREATED_DATE = DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00");
                clsVOD_EPISODE_DETAIL.fdUPDATED_DATE = (!string.IsNullOrEmpty(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()) ? DateTime.Parse(dtVOD.Rows[0]["fdUPDATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00") : DateTime.Parse(dtVOD.Rows[0]["fdCREATED_DATE"].ToString()).ToString("yyyy-MM-ddTHH:mm:ss+08:00"));
                clsVOD_EPISODE_DETAIL.fsVIDEO_FROM = dtVOD.Rows[0]["fsVIDEO_FROM"].ToString();
            }
            return clsVOD_EPISODE_DETAIL;
        }

        //取的動漫推薦影片
        public List<clsVOD_INDEX> fnGET_VOD_ANIME_RELATED(string fsVOD_NO, string fcREGION)
        {
            List<clsVOD_INDEX> clsVOD_INDEXs = new List<clsVOD_INDEX>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_ANIME_PROMO", dicParameters);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                for (int i = 0; i < dtVOD.Rows.Count; i++)
                {
                    clsVOD_INDEXs.Add(new clsVOD_INDEX()
                    {
                        fsVOD_NO = dtVOD.Rows[i]["fsVOD_NO"].ToString(),
                        fsTITLE = dtVOD.Rows[i]["fsTITLE"].ToString(),
                        fsHEAD_FRAME = dtVOD.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fbIS_FINAL = (dtVOD.Rows[i]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                        fnTOTAL_EPISODE = int.Parse(dtVOD.Rows[i]["fnTOTAL_EPISODE"].ToString()),
                        fsVIDEO_FROM = dtVOD.Rows[i]["fsVIDEO_FROM"].ToString(),
                        fsTYPE1 = dtVOD.Rows[i]["fsTYPE1"].ToString()
                    });
                }
            }
            return clsVOD_INDEXs;
        }

        //取得首頁資訊-手機用
        public List<clsVOD_ALL_INDEX_MOBILE> fnGET_VOD_INDEX_MOBILE()
        {
            List<clsVOD_ALL_INDEX_MOBILE> ListVOD_INDEX_MOBILE = new List<clsVOD_ALL_INDEX_MOBILE>();

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            DataTable dtVOD = clsDB.Do_Query("spAPI_GET_VOD_INDEX_MOBILE", null);

            if (dtVOD != null && dtVOD.Rows.Count > 0)
            {
                List<clsCODE> listCODE = new repCODE().fnGET_CODE("VODTYPE", "", "");

                clsVOD_ALL_INDEX_MOBILE clsVOD_ALL_INDEX_MOBILE = new clsVOD_ALL_INDEX_MOBILE();

                clsVOD_ALL_INDEX_MOBILE.fsHOT = new List<string>();
                clsVOD_ALL_INDEX_MOBILE.fsNEW = new List<string>();

                foreach (var CODE in listCODE)
                {
                    clsVOD_ALL_INDEX_MOBILE clsVOD_INDEX_MOBILE = new clsVOD_ALL_INDEX_MOBILE();

                    clsVOD_INDEX_MOBILE.fsTYPE = CODE.fsCODE;

                    List<DataRow> drVOD_NEW = dtVOD.Select("fsTYPE = 'new' AND fsTYPE1 = '" + CODE.fsCODE + "'").ToList();
                    List<DataRow> drVOD_HOT = dtVOD.Select("fsTYPE = 'hot' AND fsTYPE1 = '" + CODE.fsCODE + "'").ToList();
                    List<string> fsHOT = new List<string>();
                    List<string> fsNEW = new List<string>();
                    foreach (var item in drVOD_NEW)
                    {
                        fsNEW.Add(item["fsVOD_NO"].ToString());
                    }
                    clsVOD_INDEX_MOBILE.fsNEW = fsNEW;
                    foreach (var item in drVOD_HOT)
                    {
                        fsHOT.Add(item["fsVOD_NO"].ToString());
                    }
                    clsVOD_INDEX_MOBILE.fsHOT = fsHOT;
                    ListVOD_INDEX_MOBILE.Add(clsVOD_INDEX_MOBILE);
                }
            }
            return ListVOD_INDEX_MOBILE;
        }

    }
}