﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using FourgTV_OTT2.Api2.Common;
using static FourgTV_OTT2.Api2.Common.clsDB;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Web.Configuration;
using FourgTV_OTT2.BLL.Model.Model;
using FourgTV_OTT2.DAL.DAL;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repACCOUNT
    {
        private static readonly string fsENC_KEY = WebConfigurationManager.AppSettings["fsENC_KEY"];

        //LiTV登入驗證
        public clsLITV_LOGIN fnLI_LOGIN(string fsUSER, string fsPASSWORD)
        {
            string fsLITV_SERVICE_URL = new repLITV_SERVICE().fnGET_CONFIG_NO_AUTH("AccountService");
            string fsDEVICE_ID = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
            string fsRESULT = string.Empty;

            clsLITV_LOGIN clsLITV_LOGIN = new clsLITV_LOGIN();

            clsLITV_LOGIN_ARGU clsLITV_LOGIN_ARGU = new clsLITV_LOGIN_ARGU();
            clsLITV_LOGIN_ARGU.jsonrpc = "2.0";
            clsLITV_LOGIN_ARGU.id = 26;
            clsLITV_LOGIN_ARGU.method = "AccountService.Login";
            clsLITV_LOGIN_ARGU.Params = new clsLITV_LOGIN_ARGU.clsLITV_LOGIN_PARMS();
            clsLITV_LOGIN_ARGU.Params.User = fsUSER;
            clsLITV_LOGIN_ARGU.Params.Pass = fsPASSWORD;
            clsLITV_LOGIN_ARGU.Params.DeviceId = fsDEVICE_ID;
            clsLITV_LOGIN_ARGU.Params.Swver = "LTAGP0099999LEP20141118170548";
            clsLITV_LOGIN_ARGU.Params.ModelInfo = "lge|LG-H815|p1|p1";
            clsLITV_LOGIN_ARGU.Params.project_num = "LTIOS04";

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(fsLITV_SERVICE_URL);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = WebRequestMethods.Http.Post;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(JsonConvert.SerializeObject(clsLITV_LOGIN_ARGU));
                streamWriter.Flush();
            }
            using (WebResponse response = httpWebRequest.GetResponse())
            {
                StreamReader sr = new StreamReader(response.GetResponseStream());
                fsRESULT = sr.ReadToEnd();
                sr.Close();
            }

            if (!string.IsNullOrEmpty(fsRESULT))
                clsLITV_LOGIN = JsonConvert.DeserializeObject<clsLITV_LOGIN>(fsRESULT);

            //if (clsLITV_LOGIN.result != null)
            //    fnINSERT_INTO_MEMBER_FROM_LITV(clsLITV_LOGIN.result.AccountId, fsUSER, clsLITV_LOGIN.result.Token, fsDEVICE_ID, string.Empty, string.Empty, string.Empty, string.Empty, 0);

            return clsLITV_LOGIN;
        }

        //登入驗證
        public clsLOGIN fnSIGNIN(string fsUSER_ID, string fsPASSWORD, bool fbRESET)
        {
            clsLOGIN clsLOGIN = new clsLOGIN();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsPASSWORD", clsSECURITY.Encrypt(fsPASSWORD, fsENC_KEY));
            dicParameters.Add("fbRESET", (fbRESET == true ? "Y" : "N"));
            DataTable dtACCOUNT = Do_Query("spAPI_GET_MEMBER_SIGNIN", dicParameters);
            if (dtACCOUNT != null && dtACCOUNT.Rows.Count > 0 && !string.IsNullOrEmpty(dtACCOUNT.Rows[0]["fsACCOUNT_ID"].ToString()))
            {
                clsLOGIN.fsACCOUNT_ID = dtACCOUNT.Rows[0]["fsACCOUNT_ID"].ToString();
                clsLOGIN.fsUSER = dtACCOUNT.Rows[0]["fsUSER_ID"].ToString();
            }
            return clsLOGIN;
        }

        //檢查密碼是否需要重設
        public bool fnGET_MEMBER_PASSWORD_NEED_RESET(string fsUSER_ID)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);

            DataTable dtPASSWORD = Do_Query("spAPI_GET_MEMBER_PASSWORD_EXIST", dicParameters);
            if (dtPASSWORD != null && dtPASSWORD.Rows.Count > 0 && string.IsNullOrEmpty(dtPASSWORD.Rows[0]["fsPASSWORD"].ToString()))
            {
                return true;
            }

            return false;
        }

    
        //取得使用者資料
        public UserInfo fnGET_USER_DATA(string fsCOOKIE_VALUE)
        {

            UserInfo clsUSER_DATA = new UserInfo();
            string fsVALUE = clsSECURITY.CookieDecrypt(fsCOOKIE_VALUE);
            clsUSER_DATA.fsACCOUNT_ID = fsVALUE.Split('-')[0];
            clsUSER_DATA.fsUSER = fsVALUE.Split('-')[2];

            var user = tbmMEMBER_DAL.fnGET_MemberInfoByfsACCOUNT_ID(clsUSER_DATA.fsACCOUNT_ID);
            if (user == null)
                throw new Exception("帳號停權");

            return clsUSER_DATA;
        }

        //取得使用者資料(手機用)
        public UserInfo fnGET_USER_DATA(string fsCOOKIE_VALUE, string fsENC_KEY)
        {
            UserInfo clsUSER_DATA = new UserInfo();
            string fsVALUE = clsSECURITY.CookieDecrypt(fsCOOKIE_VALUE, fsENC_KEY);
            clsUSER_DATA.fsACCOUNT_ID = fsVALUE.Split('-')[0];
            clsUSER_DATA.fsUSER = fsVALUE.Split('-')[2];
            var user = tbmMEMBER_DAL.fnGET_MemberInfoByfsACCOUNT_ID(clsUSER_DATA.fsACCOUNT_ID);
            if (user == null)
                throw new Exception("帳號停權");
            return clsUSER_DATA;
        }

        //查詢帳號狀態
        public clsACCOUNT_STATUS fnGET_ACCOUNT_STATUS(string fsUSER_ID)
        {
            clsACCOUNT_STATUS clsACCOUNT_STATUS = new clsACCOUNT_STATUS();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            DataTable dtACCOUNT = Do_Query("spAPI_GET_MEMBER_STATUS", dicParameters);

            if (dtACCOUNT != null && dtACCOUNT.Rows.Count > 0)
            {
                clsACCOUNT_STATUS.Registered = (dtACCOUNT.Rows[0]["fcREGISTERED"].ToString() == "Y" ? true : false);
                clsACCOUNT_STATUS.Validated = (dtACCOUNT.Rows[0]["fcVALIDATED"].ToString() == "Y" ? true : false);
                clsACCOUNT_STATUS.Register_type = int.Parse(dtACCOUNT.Rows[0]["fnREGISTER_TYPE"].ToString());
            }
            else
            {
                clsACCOUNT_STATUS.Registered = false;
                clsACCOUNT_STATUS.Validated = false;
                clsACCOUNT_STATUS.Register_type = 0;
            }
            return clsACCOUNT_STATUS;
        }

        //查詢非四季帳號是否已連結
        public string fnGET_ACCOUNT_IS_LINK(string fsLOGIN_TYPE,string fsLINK_ID)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsLOGIN_TYPE", fsLOGIN_TYPE);
            dicParameters.Add("fsLINK_ID", fsLINK_ID);
            DataTable dtACCOUNT = Do_Query("spAPI_GET_MEMBER_ACCOUNT_IS_LINK", dicParameters);
            if (dtACCOUNT != null && dtACCOUNT.Rows.Count > 0)
            {
                return dtACCOUNT.Rows[0]["fsRESULT"].ToString();
            }
            else
            {
                return "N";
            }
        }

        //新增非四季帳號連結
        public string fnINSERT_ACCOUNT_LINK(string fsLOGIN_TYPE,string fsACCOUNT_ID, string fsLINK_ID)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsLOGIN_TYPE", fsLOGIN_TYPE);
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsLINK_ID", fsLINK_ID);
            string fsRESULT = Do_Tran("spAPI_INSERT_MEMBER_ACCOUNT_LINK", dicParameters);
            return fsRESULT;
        }

        //非四季帳號連結
        public string fnDELETE_ACCOUNT_LINK(string fsLOGIN_TYPE, string fsACCOUNT_ID)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsLOGIN_TYPE", fsLOGIN_TYPE);
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            string fsRESULT = Do_Tran("spAPI_DELETE_MEMBER_ACCOUNT_LINK", dicParameters);
            return fsRESULT;
        }

        //查詢帳號是否可以試用
        public string fnGET_PROMO_ELIGIBLE(string fsACCOUNT_ID)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            DataTable dtACCOUNT = Do_Query("spAPI_GET_MEMBER_PROMO_ELIGIBLE", dicParameters);
            if (dtACCOUNT != null && dtACCOUNT.Rows.Count > 0)
            {
                return dtACCOUNT.Rows[0]["fsRESULT"].ToString();
            }
            else
            {
                return "N";
            }
        }

        //帳號試用
        public string fnACCOUNT_PROMO(string fsACCOUNT_ID)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            string fsRESULT = Do_Tran("spAPI_INSERT_MEMBER_PROMO", dicParameters);
            return fsRESULT;
        }
        //帳號試用
        public string fnACCOUNT_PROMO(string fsACCOUNT_ID, string fsENC_KEY, string fsDEVICE, string fsVERSION)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            string fsRESULT = Do_Tran("spAPI_INSERT_MEMBER_PROMO", dicParameters);
            return fsRESULT;
        }

        //修改密碼
        public string fnEDIT_PASSWORD(string fsACCOUNT_ID, string fsPASSWORD_OLD, string fsPASSWORD_NEW)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsPASSWORD_OLD", clsSECURITY.Encrypt(fsPASSWORD_OLD, fsENC_KEY));
            dicParameters.Add("fsPASSWORD_NEW", clsSECURITY.Encrypt(fsPASSWORD_NEW, fsENC_KEY));
            string fsRESULT = Do_Tran("spAPI_UPDATE_MEMBER_EDIT_PASSWORD", dicParameters);
            return fsRESULT;
        }

        //重設密碼
        public string fnRESET_PASSWORD(string fsUSER_ID, string fsPASSCODE, string fsPASSWORD)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsPASSCODE", fsPASSCODE);
            dicParameters.Add("fsPASSWORD", clsSECURITY.Encrypt(fsPASSWORD, fsENC_KEY));
            string fsRESULT = Do_Tran("spAPI_UPDATE_MEMBER_RESET_PASSWORD", dicParameters);
            return fsRESULT;
        }

        //取得通行碼
        public clsPASSCODE fnGET_PASSCODE(string fsUSER_ID, string fsVALUDATE_TYPE)
        {
            clsPASSCODE clsPASSCODE = new clsPASSCODE();
            string fsPASSCODE = new Random().Next(1, 99999999).ToString("00000000");
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsVALIDATE_TYPE", fsVALUDATE_TYPE);
            dicParameters.Add("fsVALIDATE_CODE", fsPASSCODE);
            string fsRESULT = Do_Tran("spAPI_INSERT_PASSCODE", dicParameters);
            if (string.IsNullOrEmpty(fsRESULT))
            {
                clsPASSCODE.fsPASSCODE = fsPASSCODE;
                clsPASSCODE.fsMESSAGE = string.Empty;
            }
            else
            {
                clsPASSCODE.fsPASSCODE = string.Empty;
                clsPASSCODE.fsMESSAGE = fsRESULT.Split(':')[1];
            }
            return clsPASSCODE;
        }

        //驗證通行碼與會員帳號是否批配
        public clsCONFIRM_PASSCODE fnCONFIRM_PASSCODE(string fsUSER_ID, string fsVALUDATE_TYPE, string fsPASSCODE)
        {
            string fsRESULT = string.Empty;
            clsCONFIRM_PASSCODE clsCONFIRM_PASSCODE = new clsCONFIRM_PASSCODE();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsVALIDATE_TYPE", fsVALUDATE_TYPE);
            dicParameters.Add("fsVALIDATE_CODE", fsPASSCODE);
            DataTable dtVALIDATE = Do_Query("spAPI_GET_PASSCODE_VALIDATE", dicParameters);
            if (dtVALIDATE != null && dtVALIDATE.Rows.Count > 0)
            {
                if (dtVALIDATE.Rows[0]["fsRESULT"].ToString() == "Y")
                {
                    clsCONFIRM_PASSCODE.fbRESULT = true;
                    clsCONFIRM_PASSCODE.fsMESSAGE = string.Empty;
                }
                else
                {
                    clsCONFIRM_PASSCODE.fbRESULT = false;
                    clsCONFIRM_PASSCODE.fsMESSAGE = dtVALIDATE.Rows[0]["fsMESSAGE"].ToString();
                }
            }
            else
            {
                clsCONFIRM_PASSCODE.fbRESULT = false;
                clsCONFIRM_PASSCODE.fsMESSAGE = "驗證碼錯誤";
            }
            return clsCONFIRM_PASSCODE;
        }

        //註冊帳號
        public clsREGISTER fnREGISTER(int fnREGISTER_TYPE, string fsLOGIN_TYPE, string fsUSER_ID, string fsLINK_ID, int fnBIRTHYEAR, string fsSEX, string fsPASSWORD)
        {
            clsREGISTER clsREGISTER = new clsREGISTER();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fnREGISTER_TYPE", fnREGISTER_TYPE.ToString());
            dicParameters.Add("fsPASSWORD", clsSECURITY.Encrypt(fsPASSWORD, fsENC_KEY));
            dicParameters.Add("fsSEX", fsSEX);
            dicParameters.Add("fnBIRTHYEAR", fnBIRTHYEAR.ToString());
            dicParameters.Add("fsLOGIN_TYPE", fsLOGIN_TYPE);
            dicParameters.Add("fsLINK_ID", fsLINK_ID);

            string fsRESULT = Do_Tran("spAPI_INSERT_MEMBER", dicParameters);
            if (string.IsNullOrEmpty(fsRESULT))
            {
                clsREGISTER.fbRESULT = true;
                clsREGISTER.fsMESSAGE = string.Empty;
            }
            else
            {
                clsREGISTER.fbRESULT = false;
                clsREGISTER.fsMESSAGE = fsRESULT.Split(':')[1];
            }
            return clsREGISTER;

        }

        //取得會員個人資訊
        public clsMEMBER_INFO fnGET_ACCOUNT_INFO(string fsACCOUNT_ID)
        {
            clsMEMBER_INFO clsMEMBER_INFO = new clsMEMBER_INFO();
            clsMEMBER_INFO.lstLOGIN_TYPE = new List<string>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            DataTable dtACCOUNT = Do_Query("spAPI_GET_MEMBER_INFO", dicParameters);
            if (dtACCOUNT != null && dtACCOUNT.Rows.Count > 0)
            {
                clsMEMBER_INFO.fsACCOUNT_ID = dtACCOUNT.Rows[0]["fsACCOUNT_ID"].ToString();
                clsMEMBER_INFO.fsUSER_ID = dtACCOUNT.Rows[0]["fsUSER_ID"].ToString();
                clsMEMBER_INFO.fnREGISTER_TYPE = int.Parse(dtACCOUNT.Rows[0]["fnREGISTER_TYPE"].ToString());
                clsMEMBER_INFO.fsREALNAME = dtACCOUNT.Rows[0]["fsREALNAME"].ToString();
                clsMEMBER_INFO.fnBIRTHYEAR = int.Parse(dtACCOUNT.Rows[0]["fnBIRTHYEAR"].ToString());
                clsMEMBER_INFO.fsSEX = dtACCOUNT.Rows[0]["fsSEX"].ToString();
                clsMEMBER_INFO.fsPHONE = dtACCOUNT.Rows[0]["fsPHONE"].ToString();
                clsMEMBER_INFO.fsADDRESS = dtACCOUNT.Rows[0]["fsADDRESS"].ToString();
                clsMEMBER_INFO.fsEMAIL = dtACCOUNT.Rows[0]["fsEMAIL"].ToString();
                clsMEMBER_INFO.fcVALIDATE = (dtACCOUNT.Rows[0]["fcVALIDATE"].ToString() == "Y" ? true : false);
                clsMEMBER_INFO.fnLEFT_PROMO_DAYS = int.Parse(dtACCOUNT.Rows[0]["fnLEFT_PROMO_DAYS"].ToString());
                clsMEMBER_INFO.fsPICTURE = dtACCOUNT.Rows[0]["fsPICTURE"].ToString();
                if (!string.IsNullOrEmpty(dtACCOUNT.Rows[0]["fsLOGIN_TYPE"].ToString()))
                {
                    foreach (var item in dtACCOUNT.Rows[0]["fsLOGIN_TYPE"].ToString().Split(',').ToList())
                    {
                        clsMEMBER_INFO.lstLOGIN_TYPE.Add(item);
                    }
                }
            }
            return clsMEMBER_INFO;
        }

        //更新會員個人資訊
        public clsSET_ACCOUNT_INFO fnSET_ACCOUNT_INFO(clsMEMBER_INFO clsMEMBER_INFO)
        {
            clsSET_ACCOUNT_INFO clsSET_ACCOUNT_INFO = new clsSET_ACCOUNT_INFO();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", clsMEMBER_INFO.fsACCOUNT_ID);
            dicParameters.Add("fsSEX", clsMEMBER_INFO.fsSEX);
            dicParameters.Add("fnBIRTHYEAR", clsMEMBER_INFO.fnBIRTHYEAR.ToString());
            dicParameters.Add("fsREALNAME", clsMEMBER_INFO.fsREALNAME);
            dicParameters.Add("fsADDRESS", clsMEMBER_INFO.fsADDRESS);
            dicParameters.Add("fsPHONE", clsMEMBER_INFO.fsPHONE);
            dicParameters.Add("fsEMAIL", clsMEMBER_INFO.fsEMAIL);
            string fsRESULT = Do_Tran("spAPI_UPDATE_MEMBER_INFO", dicParameters);
            if (string.IsNullOrEmpty(fsRESULT))
            {
                clsSET_ACCOUNT_INFO.fbRESULT = true;
                clsSET_ACCOUNT_INFO.fsMESSAGE = string.Empty;
            }
            else
            {
                clsSET_ACCOUNT_INFO.fbRESULT = false;
                clsSET_ACCOUNT_INFO.fsMESSAGE = fsRESULT.Split(':')[1];
            }
            return clsSET_ACCOUNT_INFO;
        }

        //更新會員個人資訊大頭貼
        public string fsSET_ACCOUNT_PICTURE(clsMEMBER_INFO clsMEMBER_INFO)
        {
            clsSET_ACCOUNT_INFO clsSET_ACCOUNT_INFO = new clsSET_ACCOUNT_INFO();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", clsMEMBER_INFO.fsACCOUNT_ID);
            dicParameters.Add("fsPICTURE", clsMEMBER_INFO.fsPICTURE);

            string fsRESULT = Do_Tran("spAPI_UPDATE_MEMBER_PICTURE", dicParameters);

            return fsRESULT;
        }

        //新增登入驗證碼
        public string fnINSERT_LOGIN_VALIDATE_CODE(string fsVALIDATE_CODE)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();

            dicParameters.Add("fsVALIDATE_CODE", fsVALIDATE_CODE);
            fsRESULT = Do_Tran("spAPI_INSERT_LOGIN_VALIDATE", dicParameters);

            return fsRESULT;
        }

        //比對登入驗證碼
        public string fnGET_LOGIN_VALIDATE_CODE(string fsVALIDATE_CODE)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsVALIDATE_CODE", fsVALIDATE_CODE);
            DataTable dtVALIDATE_CODE = Do_Query("spAPI_GET_LOGIN_VALIDATE", dicParameters);
            if (dtVALIDATE_CODE != null && dtVALIDATE_CODE.Rows.Count > 0)
            {
                return dtVALIDATE_CODE.Rows[0]["fsRESULT"].ToString();
            }

            return "ERROR:驗證錯誤!";
        }

        //抓取TV今日免費觀看頻道資訊
        public clsGET_MEMBER_FREE_TV_WATCH fnGET_MEMBER_FREE_TV_WATCH(string fsACCOUNT_ID)
        {
            clsGET_MEMBER_FREE_TV_WATCH MEMBER_FREE_TV_WATCH = new clsGET_MEMBER_FREE_TV_WATCH();
        
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            DataTable dtACCOUNT = Do_Query("spAPI_GET_MEMBER_FREE_TV_WATCH", dicParameters);
            if (dtACCOUNT != null && dtACCOUNT.Rows.Count > 0)
            {
                MEMBER_FREE_TV_WATCH.fsDATE = dtACCOUNT.Rows[0]["fsDATE"].ToString();
                MEMBER_FREE_TV_WATCH.fdFREESTART_DATE = DateTime.Parse(dtACCOUNT.Rows[0]["fdFREESTART_DATE"].ToString());
            }
            return MEMBER_FREE_TV_WATCH;
        }

    }
}