﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using FourgTV_OTT2.Api2.Common;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repTV
    {
        /// <summary>fnGET_TV_MACHINE_FROM的取出string</summary>
        public bool fnGET_TV_MACHINE_FROM(string fsFROM, string fsMAC_ADDRESS)
        {
            bool check = false;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsFROM", fsFROM);
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            DataTable dtCHANNEL = clsDB.Do_Query("spAPI_GET_TV_MACHINE_FROM", dicParameters);
            if (dtCHANNEL != null && dtCHANNEL.Rows.Count > 0)
            {
                check = true;
            }
            return check;
        }

        /// <summary>fnGET_TV_MACHINE_FROM的取出string</summary>
        public string fnINSERT_TV_MEMBER_MACHINE_AND_BINDING(string fsACCOUNT_ID, string fsFROM, string fsMAC_ADDRESS)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsFROM", fsFROM);
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_TV_MEMBER_MACHINE_AND_BINDING", dicParameters);

            return fsRESULT;
        }
        
        //查詢可使用服務
        public string fnGET_TV_MEMBER_MACHINE_AND_BINDING(string fsACCOUNT_ID, string fsFROM, string fsMAC_ADDRESS)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsFROM", fsFROM);
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            fsRESULT = clsDB.Do_Tran("spAPI_GET_TV_MEMBER_MACHINE_AND_BINDING", dicParameters);

            return fsRESULT;
        }

    }
}