﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FourgTV_OTT2.Api2.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using FourgTV_OTT2.Api2.Common;
using System.Data;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repCLIENTVAR
    {
        //取得個人變數
        public List<clsGET_CLIENTVAR> fnGET_CLIENT_VAR(string fsACCOUNT_ID, string fsVOD_NO,string fcREGION)
        {
            List<clsGET_CLIENTVAR> clsGET_CLIENTVARs = new List<clsGET_CLIENTVAR>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtCLIENT_VAR = clsDB.Do_Query("spAPI_GET_CLIENT_VAR", dicParameters);
            if (dtCLIENT_VAR != null && dtCLIENT_VAR.Rows.Count > 0)
            {
                for (int i = 0; i < dtCLIENT_VAR.Rows.Count; i++)
                {
                    clsGET_CLIENTVARs.Add(new clsGET_CLIENTVAR()
                    {
                        fsVOD_NO = dtCLIENT_VAR.Rows[i]["fsVOD_NO"].ToString(),
                        fnSEQ = int.Parse(dtCLIENT_VAR.Rows[i]["fnSEQ"].ToString()),
                        fsPART = dtCLIENT_VAR.Rows[i]["fsPART"].ToString(),
                        fnTIME = int.Parse(dtCLIENT_VAR.Rows[i]["fnTIME"].ToString()),
                        fsTITLE = dtCLIENT_VAR.Rows[i]["fsTITLE"].ToString(),
                        fnEPISODE = int.Parse(dtCLIENT_VAR.Rows[i]["fnEPISODE"].ToString()),
                        fsTYPE = dtCLIENT_VAR.Rows[i]["fsTYPE_NAME"].ToString(),
                        fsHEAD_FRAME = dtCLIENT_VAR.Rows[i]["fsHEAD_FRAME"].ToString(),
                        fsVIDEO_FROM = dtCLIENT_VAR.Rows[i]["fsVIDEO_FROM"].ToString()
                    });
                }
            }
            return clsGET_CLIENTVARs;
        }

        //設定個人變數
        public clsSET_CLIENTVAR fnSET_CLIENT_VAR(string fsACCOUNT_ID, string fsVOD_NO, int fnSEQ, string fsPART, long fnTIME)
        {
            clsSET_CLIENTVAR clsSET_CLIENTVAR = new clsSET_CLIENTVAR();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsVOD_NO", fsVOD_NO);
            dicParameters.Add("fnSEQ", fnSEQ.ToString());
            dicParameters.Add("fsPART", fsPART);
            dicParameters.Add("fnTIME", fnTIME.ToString());
            string fsRESULT = clsDB.Do_Tran("spAPI_INSERT_CLIENT_VAR", dicParameters);
            if (string.IsNullOrEmpty(fsRESULT))
            {
                clsSET_CLIENTVAR.fbRESULT = true;
                clsSET_CLIENTVAR.fsMESSAGE = string.Empty;
            }
            else
            {
                clsSET_CLIENTVAR.fbRESULT = false;
                clsSET_CLIENTVAR.fsMESSAGE = fsRESULT.Split(':')[1];
            }
            return clsSET_CLIENTVAR;
        }
    }
}