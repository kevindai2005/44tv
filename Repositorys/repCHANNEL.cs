﻿// ============================================= 
// 描述: Channel資料介接 
// 記錄: <2017/10/06><David.Sin><新增本程式> 
// ============================================= 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using System.Net;
using Newtonsoft.Json;
using System.IO;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repCHANNEL
    {
        //新增Channel PageView
        public string fnINSERT_CHANNEL_PAGEVIEW(string fs4GTV_ID)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();

            dicParameters.Add("fs4GTV_ID", fs4GTV_ID);

            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_CHANNEL_PAGEVIEW", dicParameters);

            return fsRESULT;
        }

        //取得頻道套餐
        public List<clsCHANNEL_SET> fnGET_CHANNEL_SET(string fsSHOW_DEVICE, string fcREGION)
        {
            List<clsCHANNEL_SET> clsCHANNEL_SETs = new List<clsCHANNEL_SET>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsSHOW_DEVICE", fsSHOW_DEVICE);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtSET = clsDB.Do_Query("spAPI_GET_CHANNEL_SET", dicParameters);

            if (dtSET != null && dtSET.Rows.Count > 0)
            {
                for (int i = 0; i < dtSET.Rows.Count; i++)
                {
                    clsCHANNEL_SETs.Add(new clsCHANNEL_SET()
                    {
                        fnID = int.Parse(dtSET.Rows[i]["fnID"].ToString()),
                        fsNAME = dtSET.Rows[i]["fsNAME"].ToString(),
                        fcFREE = (dtSET.Rows[i]["fcFREE"].ToString() == "Y" ? true : false),
                        fcOVERSEAS = (dtSET.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false),
                        fnCOUNT = int.Parse(dtSET.Rows[i]["fnCOUNT"].ToString())
                    });
                }
            }

            return clsCHANNEL_SETs;
        }

        //取得頻道套餐_TV
        public List<clsCHANNEL_SET> fnGET_CHANNEL_SET(string fsSHOW_DEVICE, string fcREGION,string fsPURCHASE_ID)
        {
            List<clsCHANNEL_SET> clsCHANNEL_SETs = new List<clsCHANNEL_SET>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsSHOW_DEVICE", fsSHOW_DEVICE);
            dicParameters.Add("fcREGION", fcREGION);
            dicParameters.Add("fsPURCHASE_ID", fsPURCHASE_ID);
            DataTable dtSET = clsDB.Do_Query("spAPI_GET_CHANNEL_SET", dicParameters);

            if (dtSET != null && dtSET.Rows.Count > 0)
            {
                for (int i = 0; i < dtSET.Rows.Count; i++)
                {
                    clsCHANNEL_SETs.Add(new clsCHANNEL_SET()
                    {
                        fnID = int.Parse(dtSET.Rows[i]["fnID"].ToString()),
                        fsNAME = dtSET.Rows[i]["fsNAME"].ToString(),
                        fcFREE = (dtSET.Rows[i]["fcFREE"].ToString() == "Y" ? true : false),
                        fcOVERSEAS = (dtSET.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false),
                        fnCOUNT = int.Parse(dtSET.Rows[i]["fnCOUNT"].ToString())
                    });
                }
            }

            return clsCHANNEL_SETs;
        }

        //取得套餐下的頻道
        public List<clsCHANNEL> fnGET_CHANNEL_BY_SETID(int fnSET_ID, string fsSHOW_DEVICE, string fcREGION)
        {
            List<clsCHANNEL> clsCHANNELs = new List<clsCHANNEL>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fnSET_ID", fnSET_ID.ToString());
            dicParameters.Add("fsSHOW_DEVICE", fsSHOW_DEVICE);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtSET = clsDB.Do_Query("spAPI_GET_CHANNEL_BY_SET", dicParameters);

            if (dtSET != null && dtSET.Rows.Count > 0)
            {
                for (int i = 0; i < dtSET.Rows.Count; i++)
                {
                    clsCHANNELs.Add(new clsCHANNEL()
                    {
                        fnID = int.Parse(dtSET.Rows[i]["fnID"].ToString()),
                        fsTYPE = dtSET.Rows[i]["fsTYPE"].ToString(),
                        fsTYPE_NAME = dtSET.Rows[i]["fsTYPE_NAME"].ToString(),
                        fsNAME = dtSET.Rows[i]["fsNAME"].ToString(),
                        fs4GTV_ID = dtSET.Rows[i]["fs4GTV_ID"].ToString(),
                        fnCHAT_ID = int.Parse(dtSET.Rows[i]["fnCHAT_ID"].ToString()),
                        fsHEAD_FRAME = dtSET.Rows[i]["fsIMAGE1"].ToString(),
                        fsLOGO_PC = dtSET.Rows[i]["fsIMAGE2"].ToString(),
                        fsLOGO_MOBILE = dtSET.Rows[i]["fsIMAGE3"].ToString(),
                        fsQUALITY = dtSET.Rows[i]["fsQUALITY"].ToString(),
                        fcFREE = (dtSET.Rows[i]["fcFREE"].ToString() == "Y" ? true : false),
                        fnCHANNEL_NO = int.Parse(dtSET.Rows[i]["fnCHANNEL_NO"].ToString()),
                        fcHAS_PROGLIST = (dtSET.Rows[i]["fcHAS_PROGLIST"].ToString() == "Y" ? true : false),
                        fcLIKE = (dtSET.Rows[i]["fcLIKE"].ToString() == "Y" ? true : false),
                        fcRISTRICT = (dtSET.Rows[i]["fcRISTRICT"].ToString() == "Y" ? true : false),
                        fcOVERSEAS = (dtSET.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false),
                        lstEXCEPT_COUNTRY = dtSET.Rows[i]["fsEXCEPT_COUNTRY"].ToString().Split(';').ToList(),
                        fsEXPIRE_DATE = (string.IsNullOrEmpty(dtSET.Rows[i]["fdEXPIRE_DATE"].ToString()) ? string.Empty : DateTime.Parse(dtSET.Rows[i]["fdEXPIRE_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss"))

                    });
                }
            }

            return clsCHANNELs;
        }

        //取得頻道
        public clsCHANNEL fnGET_CHANNEL(int fnID)
        {
            clsCHANNEL clsCHANNEL = new clsCHANNEL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fnID", fnID.ToString());
            DataTable dtCHANNEL = clsDB.Do_Query("spAPI_GET_CHANNEL", dicParameters);

            if (dtCHANNEL != null && dtCHANNEL.Rows.Count > 0)
            {
                clsCHANNEL.fnID = int.Parse(dtCHANNEL.Rows[0]["fnID"].ToString());
                clsCHANNEL.fsTYPE = dtCHANNEL.Rows[0]["fsTYPE"].ToString();
                clsCHANNEL.fsNAME = dtCHANNEL.Rows[0]["fsNAME"].ToString();
                clsCHANNEL.fs4GTV_ID = dtCHANNEL.Rows[0]["fs4GTV_ID"].ToString();
                clsCHANNEL.fnCHANNEL_NO = int.Parse(dtCHANNEL.Rows[0]["fnCHANNEL_NO"].ToString());
                clsCHANNEL.fsHEAD_FRAME = dtCHANNEL.Rows[0]["fsIMAGE1"].ToString();
                clsCHANNEL.fsLOGO_PC = dtCHANNEL.Rows[0]["fsIMAGE2"].ToString();
                clsCHANNEL.fsLOGO_MOBILE = dtCHANNEL.Rows[0]["fsIMAGE3"].ToString();
                clsCHANNEL.fcOVERSEAS = (dtCHANNEL.Rows[0]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                clsCHANNEL.fcFREE = (dtCHANNEL.Rows[0]["fcFREE"].ToString() == "Y" ? true : false);
                clsCHANNEL.fsQUALITY = dtCHANNEL.Rows[0]["fsQUALITY"].ToString();
                clsCHANNEL.fnCHAT_ID = int.Parse(dtCHANNEL.Rows[0]["fnCHAT_ID"].ToString());
                clsCHANNEL.fcHAS_PROGLIST = (dtCHANNEL.Rows[0]["fcHAS_PROGLIST"].ToString() == "Y" ? true : false);
                clsCHANNEL.fcLIKE = (dtCHANNEL.Rows[0]["fcLIKE"].ToString() == "Y" ? true : false);
                clsCHANNEL.fcRISTRICT = (dtCHANNEL.Rows[0]["fcRISTRICT"].ToString() == "Y" ? true : false);
                clsCHANNEL.lstEXCEPT_COUNTRY = dtCHANNEL.Rows[0]["fsEXCEPT_COUNTRY"].ToString().Split(';').ToList();
                clsCHANNEL.lstALL_BITRATE = dtCHANNEL.Rows[0]["fsALL_BITRATE"].ToString().Split(';').ToList();
                clsCHANNEL.fsFREE_PROFILE = dtCHANNEL.Rows[0]["fsFREE_PROFILE"].ToString();
                clsCHANNEL.fsEXPIRE_DATE = (string.IsNullOrEmpty(dtCHANNEL.Rows[0]["fdEXPIRE_DATE"].ToString()) ? null : DateTime.Parse(dtCHANNEL.Rows[0]["fdEXPIRE_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss"));
                clsCHANNEL.fnGRADED = int.Parse(dtCHANNEL.Rows[0]["fnGRADED"].ToString());
                clsCHANNEL.fsVENDOR= dtCHANNEL.Rows[0]["fsVENDOR"].ToString();
                clsCHANNEL.fsDESCRIPTION = dtCHANNEL.Rows[0]["fsDESCRIPTION"].ToString();
            }

            return clsCHANNEL;
        }

        //取得頻道
        public clsCHANNEL fnGET_CHANNEL(string fs4GTV_ID)
        {
            clsCHANNEL clsCHANNEL = new clsCHANNEL();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fs4GTV_ID", fs4GTV_ID);
            DataTable dtCHANNEL = clsDB.Do_Query("spAPI_GET_CHANNEL_BY_4GTV_ID", dicParameters);

            if (dtCHANNEL != null && dtCHANNEL.Rows.Count > 0)
            {
                clsCHANNEL.fnID = int.Parse(dtCHANNEL.Rows[0]["fnID"].ToString());
                clsCHANNEL.fsTYPE = dtCHANNEL.Rows[0]["fsTYPE"].ToString();
                clsCHANNEL.fsCDN_ROUTE = dtCHANNEL.Rows[0]["fsCDN_ROUTE"].ToString();
                clsCHANNEL.fsNAME = dtCHANNEL.Rows[0]["fsNAME"].ToString();
                clsCHANNEL.fs4GTV_ID = dtCHANNEL.Rows[0]["fs4GTV_ID"].ToString();
                clsCHANNEL.fnCHANNEL_NO = int.Parse(dtCHANNEL.Rows[0]["fnCHANNEL_NO"].ToString());
                clsCHANNEL.fsHEAD_FRAME = dtCHANNEL.Rows[0]["fsIMAGE1"].ToString();
                clsCHANNEL.fsLOGO_PC = dtCHANNEL.Rows[0]["fsIMAGE2"].ToString();
                clsCHANNEL.fsLOGO_MOBILE = dtCHANNEL.Rows[0]["fsIMAGE3"].ToString();
                clsCHANNEL.fcOVERSEAS = (dtCHANNEL.Rows[0]["fcOVERSEAS"].ToString() == "Y" ? true : false);
                clsCHANNEL.fcFREE = (dtCHANNEL.Rows[0]["fcFREE"].ToString() == "Y" ? true : false);
                clsCHANNEL.fsQUALITY = dtCHANNEL.Rows[0]["fsQUALITY"].ToString();
                clsCHANNEL.fnCHAT_ID = int.Parse(dtCHANNEL.Rows[0]["fnCHAT_ID"].ToString());
                clsCHANNEL.fcHAS_PROGLIST = (dtCHANNEL.Rows[0]["fcHAS_PROGLIST"].ToString() == "Y" ? true : false);
                clsCHANNEL.fcLIKE = (dtCHANNEL.Rows[0]["fcLIKE"].ToString() == "Y" ? true : false);
                clsCHANNEL.fcRISTRICT = (dtCHANNEL.Rows[0]["fcRISTRICT"].ToString() == "Y" ? true : false);
                clsCHANNEL.lstEXCEPT_COUNTRY = dtCHANNEL.Rows[0]["fsEXCEPT_COUNTRY"].ToString().Split(';').ToList();
                clsCHANNEL.lstALL_BITRATE = dtCHANNEL.Rows[0]["fsALL_BITRATE"].ToString().Split(';').ToList();
                clsCHANNEL.fsFREE_PROFILE = dtCHANNEL.Rows[0]["fsFREE_PROFILE"].ToString();
                clsCHANNEL.fsEXPIRE_DATE = (string.IsNullOrEmpty(dtCHANNEL.Rows[0]["fdEXPIRE_DATE"].ToString()) ? null : DateTime.Parse(dtCHANNEL.Rows[0]["fdEXPIRE_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss"));

            }

            return clsCHANNEL;
        }

        //取得所有頻道
        public List<clsCHANNEL> fnGET_ALL_CHANNEL(string fsSHOW_DEVICE, string fcREGION)
        {
            List<clsCHANNEL> clsCHANNELs = new List<clsCHANNEL>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsSHOW_DEVICE", fsSHOW_DEVICE);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtCHANNEL = clsDB.Do_Query("spAPI_GET_ALL_CHANNEL", dicParameters);

            if (dtCHANNEL != null && dtCHANNEL.Rows.Count > 0)
            {
                for (int i = 0; i < dtCHANNEL.Rows.Count; i++)
                {
                    clsCHANNELs.Add(new clsCHANNEL()
                    {
                        fnID = int.Parse(dtCHANNEL.Rows[i]["fnID"].ToString()),
                        fsTYPE = dtCHANNEL.Rows[i]["fsTYPE"].ToString(),
                        fsTYPE_NAME = dtCHANNEL.Rows[i]["fsTYPE_NAME"].ToString(),
                        fsNAME = dtCHANNEL.Rows[i]["fsNAME"].ToString(),
                        fs4GTV_ID = dtCHANNEL.Rows[i]["fs4GTV_ID"].ToString(),
                        fnCHAT_ID = int.Parse(dtCHANNEL.Rows[i]["fnCHAT_ID"].ToString()),
                        fsHEAD_FRAME = dtCHANNEL.Rows[i]["fsIMAGE1"].ToString(),
                        fsLOGO_PC = dtCHANNEL.Rows[i]["fsIMAGE2"].ToString(),
                        fsLOGO_MOBILE = dtCHANNEL.Rows[i]["fsIMAGE3"].ToString(),
                        fsQUALITY = dtCHANNEL.Rows[i]["fsQUALITY"].ToString(),
                        fcFREE = (dtCHANNEL.Rows[i]["fcFREE"].ToString() == "Y" ? true : false),
                        fnCHANNEL_NO = int.Parse(dtCHANNEL.Rows[i]["fnCHANNEL_NO"].ToString()),
                        fcHAS_PROGLIST = (dtCHANNEL.Rows[i]["fcHAS_PROGLIST"].ToString() == "Y" ? true : false),
                        fcLIKE = (dtCHANNEL.Rows[i]["fcLIKE"].ToString() == "Y" ? true : false),
                        fcRISTRICT = (dtCHANNEL.Rows[i]["fcRISTRICT"].ToString() == "Y" ? true : false),
                        fcOVERSEAS = (dtCHANNEL.Rows[i]["fcOVERSEAS"].ToString() == "Y" ? true : false),
                        lstEXCEPT_COUNTRY = dtCHANNEL.Rows[i]["fsEXCEPT_COUNTRY"].ToString().Split(';').ToList(),
                        lstSETs = (string.IsNullOrEmpty(dtCHANNEL.Rows[i]["fsSETs"].ToString()) ? null : dtCHANNEL.Rows[i]["fsSETs"].ToString().Split(';').Select(Int32.Parse).ToList()),
                        fsEXPIRE_DATE = (string.IsNullOrEmpty(dtCHANNEL.Rows[i]["fdEXPIRE_DATE"].ToString()) ? string.Empty : DateTime.Parse(dtCHANNEL.Rows[i]["fdEXPIRE_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss")),
                        fsVENDOR = dtCHANNEL.Rows[i]["fsVENDOR"].ToString(),
                        fsDESCRIPTION = dtCHANNEL.Rows[i]["fsDESCRIPTION"].ToString()
                });
                }
            }

            return clsCHANNELs;
        }

        //取得套餐頻道關聯
        public List<clsCHANNEL_SET_INTRODUCTION> fnGET_CHANNEL_SET_INTRODUCTION(string fcREGION)
        {
            List<clsCHANNEL_SET_INTRODUCTION> clsCHANNEL_SET_INTRODUCTIONs = new List<clsCHANNEL_SET_INTRODUCTION>();
            //取得套餐頻道相關聯
            DataTable dtSET_CHANNEL = clsDB.Do_Query("spAPI_GET_CHANNEL_SET_INTRODUCTION", null);
            //取得所有頻道
            List<clsCHANNEL> clsCHANNELs = fnGET_ALL_CHANNEL(string.Empty, fcREGION);

            if (clsCHANNELs != null && clsCHANNELs.Count > 0)
            {
                foreach (var channel in clsCHANNELs)
                {
                    clsCHANNEL_SET_INTRODUCTION clsCHANNEL_SET_INTRODUCTION = new clsCHANNEL_SET_INTRODUCTION();
                    clsCHANNEL_SET_INTRODUCTION.fnCHANNEL_ID = channel.fnID;
                    clsCHANNEL_SET_INTRODUCTION.fsCHANNEL_NAME = channel.fsNAME;
                    clsCHANNEL_SET_INTRODUCTION.fsCHANNEL_NO = channel.fnCHANNEL_NO.ToString();
                    clsCHANNEL_SET_INTRODUCTION.fsQUALITY = channel.fsQUALITY;
                    clsCHANNEL_SET_INTRODUCTION.fsTYPE_NAME = channel.fsTYPE_NAME;
                    clsCHANNEL_SET_INTRODUCTION.fcFREE = channel.fcFREE;
                    clsCHANNEL_SET_INTRODUCTION.lstCHANNEL_SET_INFO = new List<clsCHANNEL_SET_INTRODUCTION.clsCHANNEL_SET_INFO>();
                    for (int i = 0; i < dtSET_CHANNEL.Rows.Count; i++)
                    {
                        clsCHANNEL_SET_INTRODUCTION.clsCHANNEL_SET_INFO clsCHANNEL_SET_INFO = new clsCHANNEL_SET_INTRODUCTION.clsCHANNEL_SET_INFO();
                        clsCHANNEL_SET_INFO.fnSET_ID = int.Parse(dtSET_CHANNEL.Rows[i]["fnSET_ID"].ToString());
                        //clsCHANNEL_SET_INFO.fsSET_NAME = dtSET_CHANNEL.Rows[i]["fsSET_NAME"].ToString();
                        clsCHANNEL_SET_INFO.fbINCLUDE = (dtSET_CHANNEL.Rows[i]["fsCHANNELs"].ToString().Split(',').ToList().FirstOrDefault(f => f == channel.fnID.ToString()) == null ? false : true);
                        clsCHANNEL_SET_INTRODUCTION.lstCHANNEL_SET_INFO.Add(clsCHANNEL_SET_INFO);
                    }

                    //若全部套餐都沒有，則就不用顯示了
                    if (clsCHANNEL_SET_INTRODUCTION.lstCHANNEL_SET_INFO.FirstOrDefault(f => f.fbINCLUDE) != null)
                    {
                        clsCHANNEL_SET_INTRODUCTIONs.Add(clsCHANNEL_SET_INTRODUCTION);
                    }
                }
            }
            return clsCHANNEL_SET_INTRODUCTIONs;
        }

        //取得會員頻道權限
        public clsGET_MEMBER_CHANNEL_AUTH fnGET_MEMBER_CHANNEL_AUTH(string fsACCOUNT_ID, string fs4GTV_ID)
        {
            clsGET_MEMBER_CHANNEL_AUTH clsGET_MEMBER_CHANNEL_AUTH = new clsGET_MEMBER_CHANNEL_AUTH();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fs4GTV_ID", fs4GTV_ID);
            DataTable dtMEMBER_CHANNEL_AUTH = clsDB.Do_Query("spAPI_GET_MEMBER_CHANNEL_AUTH", dicParameters);
            if (dtMEMBER_CHANNEL_AUTH != null && dtMEMBER_CHANNEL_AUTH.Rows.Count > 0)
            {
                clsGET_MEMBER_CHANNEL_AUTH.fnID = int.Parse(dtMEMBER_CHANNEL_AUTH.Rows[0]["fnID"].ToString());
                clsGET_MEMBER_CHANNEL_AUTH.fs4GTV_ID = dtMEMBER_CHANNEL_AUTH.Rows[0]["fs4GTV_ID"].ToString();
                clsGET_MEMBER_CHANNEL_AUTH.fcAUTH = (dtMEMBER_CHANNEL_AUTH.Rows[0]["fcAUTH"].ToString() == "Y" ? true : false);
                clsGET_MEMBER_CHANNEL_AUTH.fcAD = (dtMEMBER_CHANNEL_AUTH.Rows[0]["fcAD"].ToString() == "Y" ? true : false);
                clsGET_MEMBER_CHANNEL_AUTH.fcIS_PAY_MEMBER = (dtMEMBER_CHANNEL_AUTH.Rows[0]["fcIS_PAY_MEMBER"].ToString() == "Y" ? true : false);
            }
            return clsGET_MEMBER_CHANNEL_AUTH;
        }

        //從OVO取得是否有權限播放
        public bool fnGET_CHANNEL_AUTH_FROM_OVO(string fsUSER_ID, string fsASSET_ID)
        {
            string fsURL = new repCONFIG().fnGET_CFG("OVO_API_URL")[0].fsVALUE;
            string fsRESULT = string.Empty;

            try
            {
                clsGET_CHANNEL_AUTH_FROM_OVO clsGET_CHANNEL_AUTH_FROM_OVO = new clsGET_CHANNEL_AUTH_FROM_OVO();


                clsGET_CHANNEL_AUTH_FROM_OVO_ARGS clsGET_CHANNEL_AUTH_FROM_OVO_ARGS = new clsGET_CHANNEL_AUTH_FROM_OVO_ARGS();
                clsGET_CHANNEL_AUTH_FROM_OVO_ARGS.account = fsUSER_ID;
                clsGET_CHANNEL_AUTH_FROM_OVO_ARGS.channelId = fsASSET_ID;

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(fsURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = WebRequestMethods.Http.Post;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(JsonConvert.SerializeObject(clsGET_CHANNEL_AUTH_FROM_OVO_ARGS));
                    streamWriter.Flush();
                }
                using (WebResponse response = httpWebRequest.GetResponse())
                {
                    StreamReader sr = new StreamReader(response.GetResponseStream());
                    fsRESULT = sr.ReadToEnd();
                    sr.Close();
                }

                if (!string.IsNullOrEmpty(fsRESULT))
                    clsGET_CHANNEL_AUTH_FROM_OVO = JsonConvert.DeserializeObject<clsGET_CHANNEL_AUTH_FROM_OVO>(fsRESULT);

                return clsGET_CHANNEL_AUTH_FROM_OVO.data.result;
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }
    }
}