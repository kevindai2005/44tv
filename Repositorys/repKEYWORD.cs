﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using System.Data;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repKEYWORD
    {
        //取得熱門關鍵字
        public List<clsKEYWORD> fnGET_HOT_KEYWORD()
        {
            List<clsKEYWORD> clsKEYWORDs = new List<clsKEYWORD>();
            DataTable dtKEYWORD = clsDB.Do_Query("spAPI_GET_HOT_KEYWORD", null);
            if (dtKEYWORD != null && dtKEYWORD.Rows.Count > 0)
            {
                for (int i = 0; i < dtKEYWORD.Rows.Count; i++)
                {
                    clsKEYWORDs.Add(new clsKEYWORD() { fsKEYWORD = dtKEYWORD.Rows[i]["fsKEYWORD"].ToString() });
                }
            }
            return clsKEYWORDs;
        }
    }
}