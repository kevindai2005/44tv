﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
//using FourgTV_OTT2.Api2.Common;
using static FourgTV_OTT2.Api2.Common.clsDB;
using FourgTV_OTT2.Common;
using FourgTV_OTT2.Api2.Models;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repAGC
    {
        private static readonly string fsENC_KEY = WebConfigurationManager.AppSettings["fsENC_KEY"];
        
        //啟用/新增Mac白名單
        public string fnGET_MEMBER_MACHINE_AND_BINDING(string fsMAC_ADDRESS, string fsPASSWORD, string fnREGISTER_TYPE)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            dicParameters.Add("fsPASSWORD", clsSECURITY.Encrypt(fsPASSWORD, fsENC_KEY));
            dicParameters.Add("fnREGISTER_TYPE", fnREGISTER_TYPE);
            fsRESULT = Do_Tran("spAPI_ACG_INSERT_MEMBER_MACHINE_BINDING", dicParameters);

            return fsRESULT;
        }
   

        //改變MAC 狀態
        public string fnUPDATE_MAC_STATUS(string fsMAC_ADDRESS, string fcSTATUS)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            dicParameters.Add("fcSTATUS", fcSTATUS);
            fsRESULT = Do_Tran("spAPI_ACG_UPDATE_MAC_STATUS", dicParameters);

            return fsRESULT;
        }

        //改變MAC綁定狀態
        public string fnUPDATE_MAC_BINDING_STATUS(string fsUSER_ID,string fsMAC_ADDRESS, string fcSTATUS)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            dicParameters.Add("fcSTATUS", fcSTATUS);
            fsRESULT = Do_Tran("spAPI_ACG_UPDATE_MEMBER_MACHINE_STATUS", dicParameters);

            return fsRESULT;
        }

        //ACG MAC INFO
        public List<clsMAC_AGC_INFO> fnGET_AGC_MAC_INFO(string fcSTATUS)
        {
            List<clsMAC_AGC_INFO> clsMAC_INFOs = new List<clsMAC_AGC_INFO>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcSTATUS", fcSTATUS);
            DataTable fsRESULT = Do_Query("spAPI_AGC_GET_MAC_INFO", dicParameters);
            if (fsRESULT != null && fsRESULT.Rows.Count > 0)
            {
                for (int i = 0; i < fsRESULT.Rows.Count; i++)
                {
                    clsMAC_AGC_INFO clsMAC_INFO = new clsMAC_AGC_INFO();
                    clsMAC_INFO.fsMAC_ADDRESS = fsRESULT.Rows[i]["fsMACHINE_ID"].ToString();
                    clsMAC_INFO.fsUSER_ID = fsRESULT.Rows[i]["fsUSER_ID"].ToString();
                    clsMAC_INFO.fdCREATE_DATE = (string.IsNullOrEmpty(fsRESULT.Rows[i]["fdCREATED_DATE"].ToString()) == true ? string.Empty : DateTime.Parse(fsRESULT.Rows[i]["fdCREATED_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss"));
                    clsMAC_INFOs.Add(clsMAC_INFO);
                }
            }
            return clsMAC_INFOs;
        }
   
        //修改密碼
        public string fnUPDATE_ACG_ACCOUNT_PASSWORD(string fsUSER_ID, string fsPASSWORD)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsPASSWORD", clsSECURITY.Encrypt(fsPASSWORD, fsENC_KEY));
            fsRESULT = Do_Tran("spAPI_ACG_UPDATE_ACCOUNT_PASSWORD", dicParameters);

            return fsRESULT;
        }

        //換機
        public string fnUPDATE_ACCOUNT_CHANGE_MAC(string fsUSER_ID, string fsMAC_ADDRESS_OLD, string fsMAC_ADDRESS_NEW)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsMAC_ADDRESS_OLD", fsMAC_ADDRESS_OLD);
            dicParameters.Add("fsMAC_ADDRESS_NEW", fsMAC_ADDRESS_NEW);
            fsRESULT = Do_Tran("spAPI_ACG_UPDATE_ACCOUNT_CHANGE_MAC", dicParameters);

            return fsRESULT;
        }

        //取得使用者綁定的機器
        public List<clsMAC_AGC_ADDRESS> fnGET_MAC_BY_ACCOUNTID(string fsUSER_ID)
        {

            List<clsMAC_AGC_ADDRESS> clsMAC_INFOs = new List<clsMAC_AGC_ADDRESS>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            DataTable fsRESULT = Do_Query("spAPI_ACG_GET_MAC_BY_ACCOUNTID", dicParameters);
            if (fsRESULT != null && fsRESULT.Rows.Count > 0)
            {
                for (int i = 0; i < fsRESULT.Rows.Count; i++)
                {
                    clsMAC_AGC_ADDRESS clsMAC_INFO = new clsMAC_AGC_ADDRESS();
                    clsMAC_INFO.fsMAC_ADDRESS = fsRESULT.Rows[i]["fsMACHINE_ID"].ToString();
                    clsMAC_INFOs.Add(clsMAC_INFO);
                }
            }
            return clsMAC_INFOs;
        }
        //查詢是否為AGC使用者
        public string fnGET_CHECK_USER_ID(string fsUSER_ID)
        {

            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);

             fsRESULT = Do_Tran("spAPI_ACG_CHECK_USER_ID", dicParameters);

            return fsRESULT;
       }
        //取消訂單
        public string fnUPDATE_CANCEL_ORDER(string fsUSER_ID, string fsMAC_ADDRESS, string fsPURCHASE_ID)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            dicParameters.Add("fsPURCHASE_ID", fsPURCHASE_ID);
            fsRESULT = Do_Tran("spAPI_ACG_UPDATE_CANCEL_ORDER", dicParameters);

            return fsRESULT;
        }
        //新增訂單
        public string fnINSERT_ORDER(string fsUSER_ID, string fsMAC_ADDRESS, string fsPURCHASE_ID, string fsPACKAGE_ID, string fsORDER_NO)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            dicParameters.Add("fsPURCHASE_ID", fsPURCHASE_ID);
            dicParameters.Add("fsPACKAGE_ID", fsPACKAGE_ID);
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            fsRESULT = Do_Tran("spAPI_ACG_INSERT_ORDER", dicParameters);

            return fsRESULT;
        }

        //變更套餐
        public string fnUPDATE_CHANGE_PURCHASE(string fsUSER_ID, string fsMAC_ADDRESS, string fsPURCHASE_OLD, string fsPURCHASE_NEW, string fsPACKAGE_ID, string fsORDER_NO)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            dicParameters.Add("fsPURCHASE_OLD", fsPURCHASE_OLD);
            dicParameters.Add("fsPURCHASE_NEW", fsPURCHASE_NEW);
            dicParameters.Add("fsPACKAGE_ID", fsPACKAGE_ID);
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            fsRESULT = Do_Tran("spAPI_ACG_UPDATE_CHANGE_PURCHASE", dicParameters);

            return fsRESULT;
        }

        
        //ACG ORDER INFO
        public clsGET_AGC_ORDER_INFO fnGET_AGC_ORDER_INFO(string fsACCOUNTID, string fsMAC_ADDRESS)
        {
            clsGET_AGC_ORDER_INFO clsGET_AGC_ORDER_INFO = new clsGET_AGC_ORDER_INFO();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNTID", fsACCOUNTID);
            dicParameters.Add("fsMAC_ADDRESS", fsMAC_ADDRESS);
            DataTable fsRESULT = Do_Query("spAPI_AGC_GET_ORDER_INFO", dicParameters);
            if (fsRESULT != null && fsRESULT.Rows.Count > 0)
            {
                clsGET_AGC_ORDER_INFO.fsORDER_NO = fsRESULT.Rows[0]["fsORDER_NO"].ToString();
                clsGET_AGC_ORDER_INFO.fsPURCHASE_ID = fsRESULT.Rows[0]["fsPURCHASE_ID"].ToString();
                clsGET_AGC_ORDER_INFO.fsMACHINE_ID = fsRESULT.Rows[0]["fsMACHINE_ID"].ToString();
                clsGET_AGC_ORDER_INFO.fdORDER_DATE = (string.IsNullOrEmpty(fsRESULT.Rows[0]["fdORDER_DATE"].ToString()) == true ? string.Empty : DateTime.Parse(fsRESULT.Rows[0]["fdORDER_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss"));
                clsGET_AGC_ORDER_INFO.fdEND_DATE = (string.IsNullOrEmpty(fsRESULT.Rows[0]["fdEND_DATE"].ToString()) == true ? string.Empty : DateTime.Parse(fsRESULT.Rows[0]["fdEND_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss"));
            }
            return clsGET_AGC_ORDER_INFO;
        }

    }
}