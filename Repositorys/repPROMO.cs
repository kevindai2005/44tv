﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using System.Data;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repPROMO
    {
        //取得首頁推薦
        public List<clsPROMO> fnGET_PROMO(string fcREGION)
        {
            List<clsPROMO> clsPROMOs = new List<clsPROMO>();
            DataTable dtPROMO = clsDB.Do_Query("spAPI_GET_PROMO", null);
            if (dtPROMO != null && dtPROMO.Rows.Count > 0)
            {
                for (int i = 0; i < dtPROMO.Rows.Count; i++)
                {
                    clsPROMO clsPROMO = new clsPROMO();
                    clsPROMO.fnID = int.Parse(dtPROMO.Rows[i]["fnID"].ToString());
                    clsPROMO.fsPROMO_TYPE = dtPROMO.Rows[i]["fsTYPE"].ToString();
                    clsPROMO.fsPROMO_TITLE = dtPROMO.Rows[i]["fsTITLE"].ToString();
                    clsPROMO.fsMORE_LINK = dtPROMO.Rows[i]["fsMORE_LINK"].ToString();

                    //取推薦明細
                    Dictionary<string, string> dicParameters = new Dictionary<string, string>();
                    dicParameters.Add("fnID", dtPROMO.Rows[i]["fnID"].ToString());
                    dicParameters.Add("fcREGION", fcREGION);
                    DataTable dtPROMO_D = clsDB.Do_Query("spAPI_GET_PROMO_D", dicParameters);
                    if (dtPROMO_D != null && dtPROMO_D.Rows.Count > 0)
                    {
                        List<clsPROMO.clsPROMO_DETAIL> clsPROMO_DETAILs = new List<clsPROMO.clsPROMO_DETAIL>();

                        for (int j = 0; j < dtPROMO_D.Rows.Count; j++)
                        {
                            clsPROMO_DETAILs.Add(new clsPROMO.clsPROMO_DETAIL()
                            {
                                fsTYPE = dtPROMO_D.Rows[j]["fsTYPE1"].ToString(),
                                fsTYPE_NAME = dtPROMO_D.Rows[j]["fsTYPE_NAME"].ToString(),
                                fsVOD_NO = dtPROMO_D.Rows[j]["fsVOD_NO"].ToString(),
                                fsVIDEO_FROM = dtPROMO_D.Rows[j]["fsVIDEO_FROM"].ToString(),
                                fs4GTV_ID = dtPROMO_D.Rows[j]["fs4GTV_ID"].ToString(),
                                fnCHANNEL_ID = int.Parse(dtPROMO_D.Rows[j]["fnCHANNEL_ID"].ToString()),
                                fsTITLE = dtPROMO_D.Rows[j]["fsTITLE"].ToString(),
                                fdDATE = (string.IsNullOrEmpty(dtPROMO_D.Rows[j]["fdSTART_DATE"].ToString()) ? string.Empty : DateTime.Parse(dtPROMO_D.Rows[j]["fdSTART_DATE"].ToString()).ToString("yyyy/MM/dd")),
                                fnYEAR = (string.IsNullOrEmpty(dtPROMO_D.Rows[j]["fnYEAR"].ToString()) ? 0 : int.Parse(dtPROMO_D.Rows[j]["fnYEAR"].ToString())),
                                fsIMAGE = dtPROMO_D.Rows[j]["fsHEAD_FRAME"].ToString(),
                                fnTOTAL_EPISODE = (string.IsNullOrEmpty(dtPROMO_D.Rows[j]["fnTOTAL_EPISODE"].ToString()) ? 0 : int.Parse(dtPROMO_D.Rows[j]["fnTOTAL_EPISODE"].ToString())),
                                fbIS_FINAL = (dtPROMO_D.Rows[j]["fbIS_FINAL"].ToString() == "Y" ? true : false),
                                fsQUALITY = dtPROMO_D.Rows[j]["fsQUALITY"].ToString(),
                                fsDESCRIPTION = dtPROMO_D.Rows[j]["fsDESCRIPTION"].ToString(),
                                fsCHANNEL_IMAGE_PC = dtPROMO_D.Rows[j]["fsCHANNEL_IMAGE_PC"].ToString(),
                                fsCHANNEL_IMAGE_MOBILE = dtPROMO_D.Rows[j]["fsCHANNEL_IMAGE_MOBILE"].ToString(),
                            });

                            clsPROMO.PROMO_DETAIL = clsPROMO_DETAILs;
                        }
                    }
                    else
                    {
                        clsPROMO.PROMO_DETAIL = new List<clsPROMO.clsPROMO_DETAIL>();
                    }
                    clsPROMOs.Add(clsPROMO);
                }
            }


            return clsPROMOs;
        }


    }
}