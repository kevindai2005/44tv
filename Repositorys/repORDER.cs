﻿using FourgTV_OTT2.Api2.Models;
using FourgTV_OTT2.Api2.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repORDER
    {
        //取得訂單資訊
        public clsORDER fnGET_ORDER_INFO(string fsORDER_NO)
        {
            clsORDER clsORDER = new clsORDER();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            DataTable dtORDER = clsDB.Do_Query("spAPI_GET_ORDER_INFO", dicParameters);
            if(dtORDER != null && dtORDER.Rows.Count > 0)
            {
                clsORDER.fsORDER_NO = dtORDER.Rows[0]["fsORDER_NO"].ToString();
                clsORDER.fsOTPW = dtORDER.Rows[0]["fsOTPW"].ToString();
                clsORDER.fsAUTHORITY = dtORDER.Rows[0]["fsAUTHORITY"].ToString();
                clsORDER.fsFEE = dtORDER.Rows[0]["fsFEE"].ToString();
                clsORDER.fsPURCHASE_ID = dtORDER.Rows[0]["fsPURCHASE_ID"].ToString();
                clsORDER.fsPACKAGE_ID = dtORDER.Rows[0]["fsPACKAGE_ID"].ToString();
                clsORDER.fsPAY_TYPE_NAME = dtORDER.Rows[0]["fsPAY_TYPE_NAME"].ToString();
            }
            return clsORDER;
        }


    }
}