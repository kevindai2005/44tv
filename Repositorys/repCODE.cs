﻿// ============================================= 
// 描述: Config資料介接 
// 記錄: <2016/08/31><David.Sin><新增本程式> 
// ============================================= 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repCODE
    {

        public List<clsCODE> fnGET_CODE(string fsCODE_ID, string fsCODE, string fsNAME)
        {
            List<clsCODE> clsCODEs = new List<clsCODE>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();

            dicParameters.Add("fsCODE_ID", fsCODE_ID);
            dicParameters.Add("fsCODE", fsCODE);
            dicParameters.Add("fsNAME", fsNAME);

            DataTable dtCODE = clsDB.Do_Query("spGET_CODE", dicParameters);

            if (dtCODE != null && dtCODE.Rows.Count > 0)
            {
                for (int i = 0; i < dtCODE.Rows.Count; i++)
                {
                    clsCODEs.Add(new clsCODE
                    {
                        fsCODE_ID = dtCODE.Rows[i]["fsCODE_ID"].ToString(),
                        fsCODE = dtCODE.Rows[i]["fsCODE"].ToString(),
                        fsNAME = dtCODE.Rows[i]["fsNAME"].ToString(),
                        fnORDER = int.Parse(dtCODE.Rows[i]["fnORDER"].ToString()),
                        fsSET = dtCODE.Rows[i]["fsSET"].ToString(),
                        fsDESCRIPTION = dtCODE.Rows[i]["fsDESCRIPTION"].ToString(),
                        fcACTIVE = (dtCODE.Rows[i]["fcACTIVE"].ToString() == "Y" ? true : false),
                        fdCREATED_DATE = (string.IsNullOrEmpty(dtCODE.Rows[i]["fdCREATED_DATE"].ToString()) == true ? string.Empty : DateTime.Parse(dtCODE.Rows[i]["fdCREATED_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss")),
                        fsCREATED_BY = dtCODE.Rows[i]["fsCREATED_BY"].ToString(),
                        fdUPDATED_DATE = (string.IsNullOrEmpty(dtCODE.Rows[i]["fdUPDATED_DATE"].ToString()) == true ? string.Empty : DateTime.Parse(dtCODE.Rows[i]["fdUPDATED_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss")),
                        fsUPDATED_BY = dtCODE.Rows[i]["fsUPDATED_BY"].ToString()
                    });
                }
            }

            return clsCODEs;
        }

    }
}