﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Data;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repBANNER
    {
        //取得Banner By位置
        public List<clsBANNER> fnGET_BANNER(string fsPOSITION, string fcREGION)
        {
            List<clsBANNER> clsBANNERs = new List<clsBANNER>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsPOSITION", fsPOSITION);
            dicParameters.Add("fcREGION", fcREGION);
            DataTable dtBANNER = clsDB.Do_Query("spAPI_GET_BANNER", dicParameters);

            if(dtBANNER != null && dtBANNER.Rows.Count > 0)
            {
                for (int i = 0; i < dtBANNER.Rows.Count; i++)
                {
                    clsBANNERs.Add(new clsBANNER()
                    {
                        fsIMAGE1 = dtBANNER.Rows[i]["fsIMAGE1"].ToString(),
                        fsIMAGE2 = dtBANNER.Rows[i]["fsIMAGE2"].ToString(),
                        fbIS_TARGET = (dtBANNER.Rows[i]["fcTARGET"].ToString() == "Y" ? true : false),
                        fsURL = dtBANNER.Rows[i]["fsURL"].ToString(),
                        fsEMBED = dtBANNER.Rows[i]["fsEMBED"].ToString()
                    });
                }
            }
            
            return clsBANNERs;
        }

    }
}