﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FourgTV_OTT2.Api2.Models;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repLITV_SERVICE
    {
        private static readonly string fsLiTV_API_URL = WebConfigurationManager.AppSettings["fsLiTV_API_URL"];
        
        //取得服務位置
        public string fnGET_CONFIG_NO_AUTH(string fsSERVICE_NAME)
        {
            string fsRESULT = string.Empty;

            if (fsLiTV_API_URL == "LITV_API_URL_TEST")
            {
                fsRESULT = new repCONFIG().fnGET_CFG(fsLiTV_API_URL)[0].fsVALUE.Split(';')[0];
            }
            else
            {
                List<string> fsURLs = new repCONFIG().fnGET_CFG(fsLiTV_API_URL)[0].fsVALUE.Split(';').ToList();
                if (fsURLs != null && fsURLs.Count > 0)
                {
                    clsLI_GET_CONFIG_NO_AUTH_ARGS clsLI_GET_CONFIG_NO_AUTH_ARGS = new clsLI_GET_CONFIG_NO_AUTH_ARGS();
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.jsonrpc = "2.0";
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.id = 2;
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.method = "ConfigService.GetConfigNoAuth";
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.Params = new clsLI_GET_CONFIG_NO_AUTH_ARGS.clsLI_GET_CONFIG_NO_AUTH_PARAMS();
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.Params.device_id = "F895C7F2F5AD";
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.Params.project_num = "LTIOS04";
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.Params.swver = "LTAGP0099999LEP20141118170548";
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.Params.services = new List<string>();
                    clsLI_GET_CONFIG_NO_AUTH_ARGS.Params.services.Add(fsSERVICE_NAME);

                    for (int i = 0; i < fsURLs.Count; i++)
                    {
                        HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(fsURLs[i]);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = WebRequestMethods.Http.Post;

                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            streamWriter.Write(JsonConvert.SerializeObject(clsLI_GET_CONFIG_NO_AUTH_ARGS));
                            streamWriter.Flush();
                        }
                        using (WebResponse response = httpWebRequest.GetResponse())
                        {
                            StreamReader sr = new StreamReader(response.GetResponseStream());
                            fsRESULT = sr.ReadToEnd();
                            sr.Close();
                        }

                        if (!string.IsNullOrEmpty(fsRESULT))
                        {
                            clsLI_GET_CONFIG_NO_AUTH clsLI_GET_CONFIG_NO_AUTH = JsonConvert.DeserializeObject<clsLI_GET_CONFIG_NO_AUTH>(fsRESULT);
                            if (clsLI_GET_CONFIG_NO_AUTH != null && clsLI_GET_CONFIG_NO_AUTH.result != null)
                                switch (fsSERVICE_NAME)
                                {
                                    case "AccountService":
                                        fsRESULT = clsLI_GET_CONFIG_NO_AUTH.result.AccountService[0];
                                        break;
                                    case "CCCService":
                                        fsRESULT = clsLI_GET_CONFIG_NO_AUTH.result.CCCService[0];
                                        break;
                                    case "LoadService":
                                        fsRESULT = clsLI_GET_CONFIG_NO_AUTH.result.LoadService[0];
                                        break;
                                    case "ClientVarService":
                                        fsRESULT = clsLI_GET_CONFIG_NO_AUTH.result.ClientVarService[0];
                                        break;
                                    default:
                                        fsRESULT = clsLI_GET_CONFIG_NO_AUTH.result.CCCService[0];
                                        break;
                                }


                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }


            return fsRESULT;
        }
    }


}