﻿// ============================================= 
// 描述: Config資料介接 
// 記錄: <2016/08/31><David.Sin><新增本程式> 
// ============================================= 

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repCONFIG
    {
        
        /// <summary>CONFIG的標準取出函數</summary>
        public List<clsCONFIG> fnGET_CFG(string fsKEY)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            List<clsCONFIG> clsCONFIGs = new List<clsCONFIG>();

            dicParameters.Add("fsKEY", fsKEY);
            DataTable dtCFG = clsDB.Do_Query("spGET_CONFIG", dicParameters);

            if (dtCFG != null && dtCFG.Rows.Count > 0)
            {
                for (int i = 0; i < dtCFG.Rows.Count; i++)
                {
                    clsCONFIGs.Add(new clsCONFIG {
                        fsKEY = dtCFG.Rows[i]["fsKEY"].ToString(),
                        fsVALUE = dtCFG.Rows[i]["fsVALUE"].ToString(),
                        fsDESCRIPTION = dtCFG.Rows[i]["fsDESCRIPTION"].ToString(),

                        fdCREATED_DATE = (string.IsNullOrEmpty(dtCFG.Rows[i]["fdCREATED_DATE"].ToString()) == true ? string.Empty : DateTime.Parse(dtCFG.Rows[i]["fdCREATED_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss")),
                        fsCREATED_BY = dtCFG.Rows[i]["fsCREATED_BY"].ToString(),
                        fdUPDATED_DATE = (string.IsNullOrEmpty(dtCFG.Rows[i]["fdUPDATED_DATE"].ToString()) == true ? string.Empty : DateTime.Parse(dtCFG.Rows[i]["fdUPDATED_DATE"].ToString()).ToString("yyyy/MM/dd HH:mm:ss")),
                        fsUPDATED_BY = dtCFG.Rows[i]["fsUPDATED_BY"].ToString()
                    });
                }
            }
            return clsCONFIGs;
        }
    }
}