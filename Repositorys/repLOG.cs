﻿using Dapper;
using FourgTV_OTT2.Api2.Common;
using FourgTV_OTT2.Api2.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repLOG
    {
        private static readonly string fsSQL_CONN = WebConfigurationManager.ConnectionStrings["fsSQL_CONN"].ToString();

        //新增LOG
        public string fnINSERT_LOG(clsLOG_ARGS clsLOG_ARGS)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();

            dicParameters.Add("fsTYPE", clsLOG_ARGS.fsTYPE);
            dicParameters.Add("fsNAME", clsLOG_ARGS.fsNAME);
            dicParameters.Add("fsCONTENT", clsLOG_ARGS.fsCONTENT);
            dicParameters.Add("fsIP", clsLOG_ARGS.fsIP);
            dicParameters.Add("fsCREATED_BY", clsLOG_ARGS.fsCREATED_BY);

            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_LOG", dicParameters);

            return fsRESULT;
        }

        //新增取用連結LOG
        public string fnINSERT_LOG_URL(clsLOG_URL_ARGS clsLOG_URL_ARGS)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();

            dicParameters.Add("fsFROM", clsLOG_URL_ARGS.fsFROM);
            dicParameters.Add("fsNAME", clsLOG_URL_ARGS.fsNAME);
            dicParameters.Add("fsUSER_ID", clsLOG_URL_ARGS.fsUSER_ID);
            dicParameters.Add("fsASSET_ID", clsLOG_URL_ARGS.fsASSET_ID);
            dicParameters.Add("fsIP", clsLOG_URL_ARGS.fsIP);
            dicParameters.Add("fsCOUNTRY", clsLOG_URL_ARGS.fsCOUNTRY);
            dicParameters.Add("fsUSER_AGENT", clsLOG_URL_ARGS.fsUSER_AGENT);

            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_LOG_URL", dicParameters);

            return fsRESULT;
        }

        //新增會員登入LOG
        public string fnINSERT_LOG_SIGNIN(clsLOG_SIGNIN_ARGS clsLOG_SIGNIN_ARGS)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();

            dicParameters.Add("fsFROM", clsLOG_SIGNIN_ARGS.fsFROM);
            dicParameters.Add("fsUSER_ID", clsLOG_SIGNIN_ARGS.fsUSER_ID);
            dicParameters.Add("fsIP", clsLOG_SIGNIN_ARGS.fsIP);
            dicParameters.Add("fsCOUNTRY", clsLOG_SIGNIN_ARGS.fsCOUNTRY);
            dicParameters.Add("fsUSER_AGENT", clsLOG_SIGNIN_ARGS.fsUSER_AGENT);

            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_LOG_SIGNIN", dicParameters);

            return fsRESULT;
        }

        //新增取用連結LOG
        public string fnINSERT_MEMBER_PROMO_LOG(clsLOG_MEMBER_PROMO_ARGS clsLOG_MEMBER_PROMO_ARGS)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", clsLOG_MEMBER_PROMO_ARGS.fsACCOUNT_ID);
            dicParameters.Add("fsDEVICE", clsLOG_MEMBER_PROMO_ARGS.fsDEVICE);
            dicParameters.Add("fsVERSION", clsLOG_MEMBER_PROMO_ARGS.fsVERSION);
            dicParameters.Add("fsENC_KEY", clsLOG_MEMBER_PROMO_ARGS.fsENC_KEY);
            dicParameters.Add("fsIP", clsLOG_MEMBER_PROMO_ARGS.fsIP);
            dicParameters.Add("fsCOUNTRY", clsLOG_MEMBER_PROMO_ARGS.fsCOUNTRY);
            dicParameters.Add("fsUSER_AGENT", clsLOG_MEMBER_PROMO_ARGS.fsUSER_AGENT);

            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_MEMBER_PROMO_LOG", dicParameters);

            return fsRESULT;
        }

        //新增註冊LOG
        public string fnINSERT_MEMBER_REGISTER_LOG(clsLOG_MEMBER_REGISTER_ARGS clsLOG_MEMBER_REGISTER_ARGS)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", clsLOG_MEMBER_REGISTER_ARGS.fsUSER_ID);
            dicParameters.Add("fsDEVICE", clsLOG_MEMBER_REGISTER_ARGS.fsDEVICE);
            dicParameters.Add("fsVERSION", clsLOG_MEMBER_REGISTER_ARGS.fsVERSION);
            dicParameters.Add("fsENC_KEY", clsLOG_MEMBER_REGISTER_ARGS.fsENC_KEY);
            dicParameters.Add("fsIP", clsLOG_MEMBER_REGISTER_ARGS.fsIP);
            dicParameters.Add("fsCOUNTRY", clsLOG_MEMBER_REGISTER_ARGS.fsCOUNTRY);
            dicParameters.Add("fsUSER_AGENT", clsLOG_MEMBER_REGISTER_ARGS.fsUSER_AGENT);

            fsRESULT = clsDB.Do_Tran("spAPI_INSERT_MEMBER_REGISTER_LOG", dicParameters);

            return fsRESULT;
        }

        //新增LOG
        public void fnINSERT_BUFFER_TIME_LOG(clsLOG_BUFFER_TIME_CONDITION _condition)
        {
            using (SqlConnection _conn = new SqlConnection(fsSQL_CONN))
            {
                string sql = "INSERT INTO tblLOG_BUFFER_TIME(fsEvent,fsAsset_Name,fsAsset_ID,fnBitrate_Type,fnNetwork_Type,fsIP,fnBufferTime,fsOS,fsOS_Version,fsDevice_Vendor,fsDevice_Type,fsBrowser,fsBrowser_Version,fsDesc,fdCreated_Date,fdBufferStart) VALUES (@fsEvent,@fsAsset_Name,@fsAsset_ID,@fnBitrate_Type,@fnNetwork_Type,@fsIP,@fnBufferTime,@fsOS,@fsOS_Version,@fsDevice_Vendor,@fsDevice_Type,@fsBrowser,@fsBrowser_Version,@fsDesc,@fdCreated_Date,@fdBufferStart)";

                _conn.Execute(sql, _condition);
            }
        }

        //新增LOG
        public void fnINSERT_ACTIVITY_11MINUTE_LOG(clsLOG_ACTIVITY_11MINUTE_CONDITION _condition)
        {
            using (SqlConnection _conn = new SqlConnection(fsSQL_CONN))
            {
                string sql = "INSERT INTO tblLOG_ACTIVITY_11MINUTE(fsACCOUNT_ID,fsIP,fdCREATED_DATE) VALUES (@fsACCOUNT_ID,@fsIP,@fdCREATED_DATE)";

                _conn.Execute(sql, _condition);
            }
        }

        //查LOG
        public clsLOG_ACTIVITY_11MINUTE_CONDITION fnGET_ACTIVITY_11MINUTE_LOG_TODAY_INFO(clsLOG_ACTIVITY_11MINUTE_CONDITION _condition)
        {
            using (SqlConnection _conn = new SqlConnection(fsSQL_CONN))
            {
                string sql = @" 
                                SELECT *
                                FROM tblLOG_ACTIVITY_11MINUTE
                                WHERE 	
                                 (fsACCOUNT_ID  = @fsACCOUNT_ID) AND
								 (fdCreated_Date >= @fdCreated_Date_Start) AND
		                         (fdCreated_Date <= @fdCreated_Date_End)
                              ";
                return _conn.QueryFirstOrDefault<clsLOG_ACTIVITY_11MINUTE_CONDITION>(sql, _condition);
            }
        }
    }
}