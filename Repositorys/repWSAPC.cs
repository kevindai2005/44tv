﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
//using FourgTV_OTT2.Api2.Common;
using static FourgTV_OTT2.Api2.Common.clsDB;
using FourgTV_OTT2.Common;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repWSAPC
    {

        private static readonly string fsENC_KEY = WebConfigurationManager.AppSettings["fsENC_KEY"];
        //新增會員
        public string fnINSERT_MEMBER(string fsUSER_ID, string fsLINK_ID, string fsPASSWORD, int fnBIRTH_YEAR, string fsSEX, int fnREGISTER_TYPE)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsLINK_ID", fsLINK_ID);
            dicParameters.Add("fsPASSWORD", clsSECURITY.Encrypt(fsPASSWORD, fsENC_KEY));
            dicParameters.Add("fnBIRTH_YEAR", fnBIRTH_YEAR.ToString());
            dicParameters.Add("fsSEX", fsSEX);
            dicParameters.Add("fnREGISTER_TYPE", fnREGISTER_TYPE.ToString());
            fsRESULT = Do_Tran("spAPI_WSAPC_INSERT_MEMBER", dicParameters);

            return fsRESULT;
        }

        //新增訂單
        public string fnINSERT_ORDER(string fsUSER_ID, string fsPURCHASE_ID, string fsPACKAGE_ID,string fsORDER_NO)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsPURCHASE_ID", fsPURCHASE_ID);
            dicParameters.Add("fsPACKAGE_ID", fsPACKAGE_ID);
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            fsRESULT = Do_Tran("spAPI_WSAPC_INSERT_ORDER", dicParameters);

            return fsRESULT;
        }

        //取消訂單
        public string fnUPDATE_CANCEL_ORDER(string fsUSER_ID, string fsPURCHASE_ID)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsUSER_ID", fsUSER_ID);
            dicParameters.Add("fsPURCHASE_ID", fsPURCHASE_ID);
            fsRESULT = Do_Tran("spAPI_WSAPC_UPDATE_CANCEL_ORDER", dicParameters);

            return fsRESULT;
        }
    }
}