﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using FourgTV_OTT2.Api2.Common;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repL_NO
    {
        /// <summary>L_NO的取出函數</summary>
        public string fnGET_L_NO(string fsTYPE, string fsNAME, string fsHEAD, string fsBODY, int fnNO_L, string fsCREATED_BY)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            string fsRESULT = "";

            dicParameters.Add("fsTYPE", fsTYPE);
            dicParameters.Add("fsNAME", fsNAME);
            dicParameters.Add("fsHEAD", fsHEAD);
            dicParameters.Add("fsBODY", fsBODY);
            dicParameters.Add("fnNO_L", fnNO_L.ToString());
            dicParameters.Add("BY", fsCREATED_BY);

            DataTable dtNO = clsDB.Do_Query("spGET_NO", dicParameters);

            if (dtNO != null && dtNO.Rows.Count > 0)
            {
                fsRESULT = dtNO.Rows[0]["NEW_NO"].ToString();
            }

            return fsRESULT;
        }
    }
}