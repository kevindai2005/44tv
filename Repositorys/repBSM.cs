﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FourgTV_OTT2.Api2.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;
//using FourgTV_OTT2.Api2.Common;
using static FourgTV_OTT2.Api2.Common.clsDB;
using FourgTV_OTT2.Common;
using System.Data;
using System.Xml;
using System.Text;
using System.Web.Configuration;

namespace FourgTV_OTT2.Api2.Repositorys
{
    public class repBSM
    {
        //取得套餐列表
        public List<clsSET_LIST> fnGET_SET_LIST(string fcREGION, string fsSHOW_DEVICE)
        {
            List<clsSET_LIST> clsSET_LISTs = new List<clsSET_LIST>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fcREGION", fcREGION);
            dicParameters.Add("fsSHOW_DEVICE", fsSHOW_DEVICE);
            DataTable dtSET = Do_Query("spAPI_GET_SET_LIST", dicParameters);

            if (dtSET != null && dtSET.Rows.Count > 0)
            {
                for (int i = 0; i < dtSET.Rows.Count; i++)
                {
                    clsSET_LISTs.Add(new clsSET_LIST()
                    {
                        group_id = dtSET.Rows[i]["fsPURCHASE_ID"].ToString(),
                        title = dtSET.Rows[i]["fsNAME"].ToString(),
                        group_description = dtSET.Rows[i]["fsDESCRIPTION"].ToString()
                    });
                }
            }

            return clsSET_LISTs;
        }

        //取得套餐明細資訊
        public List<clsSET_PACKAGE_CHT> fnGET_PACKAGE_CHT(string fsPURCHASE_ID, string fsPACKAGE_ID)
        {
            List<clsSET_PACKAGE_CHT> clsSET_PACKAGE_CHTs = new List<clsSET_PACKAGE_CHT>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsPURCHASE_ID", fsPURCHASE_ID);
            dicParameters.Add("fsPACKAGE_ID", fsPACKAGE_ID);
            DataTable dtPACKAGE = Do_Query("spAPI_GET_SET_PACKAGE_CHT", dicParameters);
            if (dtPACKAGE != null && dtPACKAGE.Rows.Count > 0)
            {
                for (int i = 0; i < dtPACKAGE.Rows.Count; i++)
                {
                    clsSET_PACKAGE_CHT clsSET_PACKAGE_CHT = new clsSET_PACKAGE_CHT();
                    clsSET_PACKAGE_CHT.pay_method_list = new List<string>();
                    clsSET_PACKAGE_CHT.payments = new List<clsSET_PACKAGE_CHT.clsSET_PACKAGE_CHT_PAYMENT>();

                    clsSET_PACKAGE_CHT.catalog_id = dtPACKAGE.Rows[i]["fsPURCHASE_ID"].ToString();
                    clsSET_PACKAGE_CHT.catalog_description = dtPACKAGE.Rows[i]["fsSET_NAME"].ToString();
                    clsSET_PACKAGE_CHT.package_name = dtPACKAGE.Rows[i]["fsNAME"].ToString();
                    clsSET_PACKAGE_CHT.package_id = dtPACKAGE.Rows[i]["fsPACKAGE_ID"].ToString();
                    clsSET_PACKAGE_CHT.cht_product_id = dtPACKAGE.Rows[i]["fsCHT_PRODUCT_ID"].ToString();
                    clsSET_PACKAGE_CHT.price_description = dtPACKAGE.Rows[i]["fnPRICE"].ToString() + "元";
                    clsSET_PACKAGE_CHT.price = int.Parse(dtPACKAGE.Rows[i]["fnPRICE"].ToString());
                    clsSET_PACKAGE_CHT.recurrent = (int.Parse(dtPACKAGE.Rows[i]["fnPRICE"].ToString()) == 0 ? "R" : "0");
                    clsSET_PACKAGE_CHT.duration_by_day = int.Parse(dtPACKAGE.Rows[i]["fnDURATION_DAY"].ToString());

                    if (!string.IsNullOrEmpty(dtPACKAGE.Rows[i]["fsPAY_TYPE"].ToString()))
                    {
                        foreach (string item in dtPACKAGE.Rows[i]["fsPAY_TYPE"].ToString().Split(','))
                        {
                            //clsSET_PACKAGE_CHT.clsSET_PACKAGE_CHT_PAYMENT clsSET_PACKAGE_CHT_PAYMENT = fnGET_PAY_TYPE_CHT(int.Parse(item), dtPACKAGE.Rows[i]["fsCASH_FLOW"].ToString());
                            clsSET_PACKAGE_CHT.clsSET_PACKAGE_CHT_PAYMENT clsSET_PACKAGE_CHT_PAYMENT = fnGET_PAY_TYPE_CHT(int.Parse(item), string.Empty);
                            clsSET_PACKAGE_CHT.pay_method_list.Add(clsSET_PACKAGE_CHT_PAYMENT.pay_type);
                            clsSET_PACKAGE_CHT.payments.Add(clsSET_PACKAGE_CHT_PAYMENT);
                        }
                    }
                    

                    clsSET_PACKAGE_CHTs.Add(clsSET_PACKAGE_CHT);
                }
            }

            return clsSET_PACKAGE_CHTs;
        }

        //取得中華支付方式
        public clsSET_PACKAGE_CHT.clsSET_PACKAGE_CHT_PAYMENT fnGET_PAY_TYPE_CHT(int fnID, string fsCASH_FLOW = "CHT")
        {
            clsSET_PACKAGE_CHT.clsSET_PACKAGE_CHT_PAYMENT clsSET_PACKAGE_CHT_PAYMENT = new clsSET_PACKAGE_CHT.clsSET_PACKAGE_CHT_PAYMENT();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fnID", fnID.ToString());
            dicParameters.Add("fsCASH_FLOW", fsCASH_FLOW);
            DataTable dtPAY_TYPE = Do_Query("spAPI_GET_PAY_TYPE_CHT", dicParameters);
            if (dtPAY_TYPE != null && dtPAY_TYPE.Rows.Count > 0)
            {
                clsSET_PACKAGE_CHT_PAYMENT.pay_type_id = int.Parse(dtPAY_TYPE.Rows[0]["fnID"].ToString());
                clsSET_PACKAGE_CHT_PAYMENT.pay_type = dtPAY_TYPE.Rows[0]["fsPAY_TYPE"].ToString();
                clsSET_PACKAGE_CHT_PAYMENT.cash_flow = dtPAY_TYPE.Rows[0]["fsCASH_FLOW"].ToString();
                clsSET_PACKAGE_CHT_PAYMENT.name = dtPAY_TYPE.Rows[0]["fsDISPLAY_NAME"].ToString();
                clsSET_PACKAGE_CHT_PAYMENT.cht_payment_code = dtPAY_TYPE.Rows[0]["fsCHT_PAYCODE"].ToString();
            }
            return clsSET_PACKAGE_CHT_PAYMENT;
        }

        //取得中華支付要用的checksum
        public string fnGET_CHT_CHECHSUM(string fsPRODUCT_ID, string fsCURL, string fsEURL, int fnFEE, string fsOTHERS)
        {
            string fsRESULT = string.Empty;
            string fsVERSION = new repCONFIG().fnGET_CFG("CHT_CHECKSUM_VERSION")[0].fsVALUE;
            fsRESULT = clsSECURITY.GetChtCheckSum(fsVERSION, fsPRODUCT_ID, fsCURL, fsEURL, fnFEE, fsOTHERS);

            return fsRESULT;
        }

        //取得訂單中華支付資訊
        public clsCHT_PAY_INFO fnGET_CHT_PAY_INFO(string fsORDER_NO, string fsOTPW)
        {
            clsCHT_PAY_INFO clsCHT_PAY_INFO = new clsCHT_PAY_INFO();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            dicParameters.Add("fsOTPW", fsOTPW);
            DataTable dtPAY_INFO = Do_Query("spAPI_GET_CHT_PAY_INFO", dicParameters);
            if (dtPAY_INFO != null && dtPAY_INFO.Rows.Count > 0)
            {
                clsCHT_PAY_INFO.fsORDER_NO = dtPAY_INFO.Rows[0]["fsORDER_NO"].ToString();
                clsCHT_PAY_INFO.fsUID = dtPAY_INFO.Rows[0]["fsUID"].ToString();
                clsCHT_PAY_INFO.fsOTPW = dtPAY_INFO.Rows[0]["fsOTPW"].ToString();
                clsCHT_PAY_INFO.fsFEE = dtPAY_INFO.Rows[0]["fsFEE"].ToString();
                clsCHT_PAY_INFO.fsAUTHORITY = dtPAY_INFO.Rows[0]["fsAUTHORITY"].ToString();
                clsCHT_PAY_INFO.fsCHT_PRODUCT_ID = dtPAY_INFO.Rows[0]["fsCHT_PRODUCT_ID"].ToString();
                clsCHT_PAY_INFO.fsACCOUNT_ID = dtPAY_INFO.Rows[0]["fsACCOUNT_ID"].ToString();
            }
            return clsCHT_PAY_INFO;
        }

        //取得會員是否尚有在有效期的訂單
        public string fnGET_MEMBER_ORDER_INDATE(string fsACCOUNT_ID, string fsPACKAGE_ID)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsPACKAGE_ID", fsPACKAGE_ID);
            DataTable dtMEMBER_ORDER_INDATE = Do_Query("spAPI_GET_MEMBER_ORDER_INDATE", dicParameters);

            if (dtMEMBER_ORDER_INDATE != null && dtMEMBER_ORDER_INDATE.Rows.Count > 0)
            {
                return dtMEMBER_ORDER_INDATE.Rows[0][0].ToString();
            }

            return string.Empty;
        }

        //新增訂單
        public string fnINSERT_ORDER(string fsACCOUNT_ID, string fsORDER_NO, string fsSESSION_ID, string fsPURCHASE_ID, string fsPACKAGE_ID, string fsPAY_FROM, int fnPAY_TYPE_ID, string fsCHT_PRODUCT_ID,
            string fsPROMO_CODE, string fs1A_RESULT, string fsUID, string fsOTPW, string fsFEE, string fsAUTHORITY, string fsOTHERS, string fsCPORDERNO, string fsEXPIRATIONDATE, string fsBANKCODE, string fsORDERID,
            bool fcSTOP_PAY, bool fcINV_GIFT, string fsINV_NAME, string fsINV_ADDRESS, string fsINV_ADDRESS2, string fsINV_TEL, string fsINV_SERIAL, string fsINV_SERIAL_DATE, string fsSTATUS, string fsCREATED_BY, string fsINV_STATUS)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            dicParameters.Add("fsSESSION_ID", fsSESSION_ID);
            dicParameters.Add("fsPURCHASE_ID", fsPURCHASE_ID);
            dicParameters.Add("fsPACKAGE_ID", fsPACKAGE_ID);
            dicParameters.Add("fsPAY_FROM", fsPAY_FROM);
            dicParameters.Add("fnPAY_TYPE_ID", fnPAY_TYPE_ID.ToString());
            dicParameters.Add("fsCHT_PRODUCT_ID", fsCHT_PRODUCT_ID);
            dicParameters.Add("fsRESULT", fs1A_RESULT);
            dicParameters.Add("fsUID", fsUID);
            dicParameters.Add("fsOTPW", fsOTPW);
            dicParameters.Add("fsFEE", fsFEE);
            dicParameters.Add("fsAUTHORITY", fsAUTHORITY);
            dicParameters.Add("fsOTHERS", fsOTHERS);
            dicParameters.Add("fsCPORDERNO", fsCPORDERNO);
            dicParameters.Add("fsEXPIRATIONDATE", fsEXPIRATIONDATE);
            dicParameters.Add("fsBANKCODE", fsBANKCODE);
            dicParameters.Add("fsORDERID", fsORDERID);
            dicParameters.Add("fcSTOP_PAY", (fcSTOP_PAY ? "Y" : "N"));
            dicParameters.Add("fsPROMO_CODE", fsPROMO_CODE);
            dicParameters.Add("fcINV_GIFT", (fcINV_GIFT ? "Y" : "N"));
            dicParameters.Add("fsINV_NAME", fsINV_NAME);
            dicParameters.Add("fsINV_ADDRESS", fsINV_ADDRESS);
            dicParameters.Add("fsINV_ADDRESS2", fsINV_ADDRESS2);
            dicParameters.Add("fsINV_TEL", fsINV_TEL);
            dicParameters.Add("fsINV_SERIAL", fsINV_SERIAL);
            dicParameters.Add("fsINV_SERIAL_DATE", fsINV_SERIAL_DATE);
            dicParameters.Add("fsSTATUS", fsSTATUS);
            dicParameters.Add("fsCREATED_BY", fsCREATED_BY);
            dicParameters.Add("fsINV_STATUS", fsINV_STATUS);

            fsRESULT = Do_Tran("spAPI_INSERT_ORDER", dicParameters);

            return fsRESULT;
        }

        public string fnINSERT_ORDER_DEVICE(string fsACCOUNT_ID, string fsOrderNo, string fsFrom, string fsDeviceVendor, string fsDevice, string fsIP, string fsOS, string fsOSVersion, string fsBrowser,string fsBrowserVersion)
        {
            string fsRESULT = string.Empty;

            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsOrderNo", fsOrderNo);
            dicParameters.Add("fsFrom", fsFrom);
            dicParameters.Add("fsDeviceVendor", fsDeviceVendor);
            dicParameters.Add("fsDevice", fsDevice);
            dicParameters.Add("fsIP", fsIP);
            dicParameters.Add("fsOS", fsOS);
            dicParameters.Add("fsOSVersion", fsOSVersion);
            dicParameters.Add("fsBrowser", fsBrowser);
            dicParameters.Add("fsBrowserVersion", fsBrowserVersion);

            fsRESULT = Do_Tran("spAPI_INSERT_ORDER_DEVICE", dicParameters);

            return fsRESULT;
        }

        //取得月付訂單資訊
        public clsMONTH_PAY_INFO fnGET_MONTH_PAY_INFO(string fsORDER_NO)
        {
            clsMONTH_PAY_INFO clsMONTH_PAY_INFO = new clsMONTH_PAY_INFO();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            DataTable dtMONTH_PAY_INFO = Do_Query("spAPI_GET_MONTH_PAY_INFO", dicParameters);

            if (dtMONTH_PAY_INFO != null && dtMONTH_PAY_INFO.Rows.Count > 0)
            {
                clsMONTH_PAY_INFO.fsACCOUNT_ID = dtMONTH_PAY_INFO.Rows[0]["fsACCOUNT_ID"].ToString();
                clsMONTH_PAY_INFO.fcPAY = dtMONTH_PAY_INFO.Rows[0]["fcPAY"].ToString();
                clsMONTH_PAY_INFO.fsMONTH_SUBSCRIBENO = dtMONTH_PAY_INFO.Rows[0]["fsMONTH_SUBSCRIBENO"].ToString();
                clsMONTH_PAY_INFO.fsCHT_PRODUCT_ID = dtMONTH_PAY_INFO.Rows[0]["fsCHT_PRODUCT_ID"].ToString();
                clsMONTH_PAY_INFO.fsAUTHORITY = dtMONTH_PAY_INFO.Rows[0]["fsAUTHORITY"].ToString();
                clsMONTH_PAY_INFO.fdSTART_DATE = dtMONTH_PAY_INFO.Rows[0]["fdSTART_DATE"].ToString();
            }
            return clsMONTH_PAY_INFO;
        }

        //進行2A授權
        public clsCHT_2A fnGET_CHT_2A(clsCHT_2A_ARGS clsCHT_2A_ARGS)
        {
            string fsRESULT = string.Empty;
            int fnRETRY_COUNT = 1;
            clsCHT_2A clsCHT_2A = new clsCHT_2A();
            string fsCHT_2A_URL = new repCONFIG().fnGET_CFG("BSM_CHT_2A")[0].fsVALUE.Replace("{$aa-authority}", clsCHT_2A_ARGS.fsCHT_AUTHORITY);
            string fsPARAM = "aa-otpw=" + clsCHT_2A_ARGS.fsCHT_OTPW + "&aa-fee=" + clsCHT_2A_ARGS.fsCHT_FEE + "&aa-apid=" + clsCHT_2A_ARGS.fsCHT_APID + "&aa-productid=" + clsCHT_2A_ARGS.fsCHT_PRODUCTID + "&aa-authority=" + clsCHT_2A_ARGS.fsCHT_AUTHORITY;
            byte[] fbPARAM = System.Text.Encoding.UTF8.GetBytes(fsPARAM);

            XmlDocument xDOC = new XmlDocument();

            try
            {
                while (fnRETRY_COUNT <= 3 && (string.IsNullOrEmpty(fsRESULT) ||
                            (xDOC.SelectSingleNode("/Result/response-code") != null && xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value != "Success")))
                {
                    if (fnRETRY_COUNT >= 2) System.Threading.Thread.Sleep(3000);

                    fsRESULT = string.Empty;

                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(fsCHT_2A_URL);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = fbPARAM.Length;

                    using (Stream reqStream = httpWebRequest.GetRequestStream())
                    {
                        reqStream.Write(fbPARAM, 0, fbPARAM.Length);
                    }
                    using (StreamReader sr = new StreamReader(httpWebRequest.GetResponse().GetResponseStream(), Encoding.Default))
                    {
                        fsRESULT = sr.ReadToEnd();

                        xDOC.LoadXml(fsRESULT);
                    }

                    fnRETRY_COUNT += 1;
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    clsCHT_2A.fsVALIDATE_A2 = "Error";
                    clsCHT_2A.fsMESSAGE_A2 = "取得2A授權失敗";
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "CHT_2A",
                        fsNAME = "2A授權",
                        fsCONTENT = "取得2A授權失敗【fsOTPW:" + clsCHT_2A_ARGS.fsCHT_OTPW + "】",
                        fsCREATED_BY = "fnGET_CHT_2A"
                    });
                }
                else
                {
                    //拆解XML
                    if (xDOC.SelectSingleNode("/Result/response-code").FirstChild != null)
                    {
                        if (xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value == "Success")
                        {
                            XmlNodeList xNodeList = xDOC.GetElementsByTagName("policy-response");

                            clsCHT_2A.fsVALIDATE_A2 = "Success";
                            clsCHT_2A.fsMESSAGE_A2 = (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty);
                            foreach (XmlNode item in xNodeList)
                            {
                                if (item.SelectSingleNode("Attribute").FirstChild.Value == "aa-payby")
                                    clsCHT_2A.fsPAY_BY_A2 = item.SelectSingleNode("value").FirstChild.Value;
                                if (item.SelectSingleNode("Attribute").FirstChild.Value == "OrderNo")
                                    clsCHT_2A.fsORDER_NO_A2 = item.SelectSingleNode("value").FirstChild.Value;
                                if (item.SelectSingleNode("Attribute").FirstChild.Value == "ApproveNo")
                                    clsCHT_2A.fsAPPROVE_NO_A2 = item.SelectSingleNode("value").FirstChild.Value;
                                if (item.SelectSingleNode("Attribute").FirstChild.Value == "Status")
                                    clsCHT_2A.fsSTATUS_A2 = item.SelectSingleNode("value").FirstChild.Value;
                            }
                            clsCHT_2A.fsRESULT_A2 = fsRESULT;

                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_2A",
                                fsNAME = "2A授權",
                                fsCONTENT = "授權成功【fsOTPW:" + clsCHT_2A_ARGS.fsCHT_OTPW + "】",
                                fsCREATED_BY = "fnGET_CHT_2A"
                            });

                        }
                        else
                        {
                            clsCHT_2A.fsVALIDATE_A2 = "Error";
                            clsCHT_2A.fsMESSAGE_A2 = (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty);
                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_2A",
                                fsNAME = "2A授權",
                                fsCONTENT = "授權失敗【fsOTPW:" + clsCHT_2A_ARGS.fsCHT_OTPW + "】" + (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty),
                                fsCREATED_BY = "fnGET_CHT_2A"
                            });
                        }
                    }
                    else
                    {
                        clsCHT_2A.fsVALIDATE_A2 = "Error";
                        clsCHT_2A.fsMESSAGE_A2 = "回傳結構錯誤";
                        new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                        {
                            fsTYPE = "CHT_2A",
                            fsNAME = "2A授權",
                            fsCONTENT = "回傳結構錯誤【fsOTPW:" + clsCHT_2A_ARGS.fsCHT_OTPW + "】" + fsRESULT,
                            fsCREATED_BY = "fnGET_CHT_2A"
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                clsCHT_2A.fsVALIDATE_A2 = "Error";
                clsCHT_2A.fsMESSAGE_A2 = ex.Message;
                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                {
                    fsTYPE = "CHT_2A",
                    fsNAME = "2A授權",
                    fsCONTENT = ex.Message + "【fsOTPW:" + clsCHT_2A_ARGS.fsCHT_OTPW + "】",
                    fsCREATED_BY = "fnGET_CHT_2A"
                });
            }

            return clsCHT_2A;
        }

        //更新2A授權
        public string fnUPDATE_CHT_2A(string fsORDER_NO, string fsVALIDATE_A2, string fsMESSAGE_A2, string fsPAY_BY_A2, string fsSTATUS_A2, string fsORDER_NO_A2, string fsAPPROVE_NO_A2, string fsRESULT_A2)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            dicParameters.Add("fsVALIDATE_A2", fsVALIDATE_A2);
            dicParameters.Add("fsMESSAGE_A2", fsMESSAGE_A2);
            dicParameters.Add("fsPAY_BY_A2", fsPAY_BY_A2);
            dicParameters.Add("fsSTATUS_A2", fsSTATUS_A2);
            dicParameters.Add("fsORDER_NO_A2", fsORDER_NO_A2);
            dicParameters.Add("fsAPPROVE_NO_A2", fsAPPROVE_NO_A2);
            dicParameters.Add("fsRESULT_A2", fsRESULT_A2);

            fsRESULT = Do_Tran("spAPI_UPDATE_ORDER_CHT_2A", dicParameters);

            return fsRESULT;
        }

        //進行3A計費
        public clsCHT_3A fnGET_CHT_3A(clsCHT_3A_ARGS clsCHT_3A_ARGS)
        {
            //aa-otpw=string&aa-fee=string&aa-apid=string&aa-stime=string&aa-sremark=string&aa-productid=string& aa-authority=string
            string fsRESULT = string.Empty;
            int fnRETRY_COUNT = 1;
            clsCHT_3A clsCHT_3A = new clsCHT_3A();
            string fsCHT_3A_URL = new repCONFIG().fnGET_CFG("BSM_CHT_3A")[0].fsVALUE.Replace("{$aa-authority}", clsCHT_3A_ARGS.fsCHT_AUTHORITY);
            string fsPARAM = "aa-otpw=" + clsCHT_3A_ARGS.fsCHT_OTPW + "&aa-fee=" + clsCHT_3A_ARGS.fsCHT_FEE + "&aa-apid=" + clsCHT_3A_ARGS.fsCHT_APID + "&aa-stime=" + clsCHT_3A_ARGS.fsCHT_STIME + "&aa-sremark=" + clsCHT_3A_ARGS.fsCHT_SREMARK + "&aa-productid=" + clsCHT_3A_ARGS.fsCHT_PRODUCTID + "&aa-authority=" + clsCHT_3A_ARGS.fsCHT_AUTHORITY;
            byte[] fbPARAM = System.Text.Encoding.UTF8.GetBytes(fsPARAM);

            XmlDocument xDOC = new XmlDocument();

            try
            {
                while (fnRETRY_COUNT <= 3 && (string.IsNullOrEmpty(fsRESULT) || (xDOC.SelectSingleNode("/Result/response-code").FirstChild != null && xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value != "Success")))
                {
                    if (fnRETRY_COUNT >= 2) System.Threading.Thread.Sleep(3000);

                    fsRESULT = string.Empty;

                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(fsCHT_3A_URL);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = fbPARAM.Length;

                    using (Stream reqStream = httpWebRequest.GetRequestStream())
                    {
                        reqStream.Write(fbPARAM, 0, fbPARAM.Length);
                    }
                    using (StreamReader sr = new StreamReader(httpWebRequest.GetResponse().GetResponseStream(), Encoding.Default))
                    {
                        fsRESULT = sr.ReadToEnd();

                        xDOC.LoadXml(fsRESULT);
                    }

                    fnRETRY_COUNT += 1;
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    clsCHT_3A.fsVALIDATE_A3 = "Error";
                    clsCHT_3A.fsMESSAGE_A3 = "取得3A計費失敗";
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "CHT_3A",
                        fsNAME = "3A計費",
                        fsCONTENT = "取得3A計費失敗【fsOTPW:" + clsCHT_3A_ARGS.fsCHT_OTPW + "】",
                        fsCREATED_BY = "fnGET_CHT_3A"
                    });
                }
                else
                {

                    if (xDOC.SelectSingleNode("/Result/response-code").FirstChild != null)
                    {
                        if (xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value == "Success")
                        {
                            XmlNodeList xNodeList = xDOC.GetElementsByTagName("policy-response");

                            clsCHT_3A.fsVALIDATE_A3 = "Success";
                            clsCHT_3A.fsMESSAGE_A3 = (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty);
                            clsCHT_3A.fsRESULT_A3 = fsRESULT;

                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_3A",
                                fsNAME = "3A授權",
                                fsCONTENT = "授權成功【fsOTPW:" + clsCHT_3A_ARGS.fsCHT_OTPW + "】",
                                fsCREATED_BY = "fnGET_CHT_3A"
                            });

                        }
                        else
                        {
                            clsCHT_3A.fsVALIDATE_A3 = "Error";
                            clsCHT_3A.fsMESSAGE_A3 = (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty);
                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_3A",
                                fsNAME = "3A授權",
                                fsCONTENT = "授權失敗【fsOTPW:" + clsCHT_3A_ARGS.fsCHT_OTPW + "】" + (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty),
                                fsCREATED_BY = "fnGET_CHT_3A"
                            });
                        }
                    }
                    else
                    {
                        clsCHT_3A.fsVALIDATE_A3 = "Error";
                        clsCHT_3A.fsMESSAGE_A3 = "回傳結構錯誤";
                        new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                        {
                            fsTYPE = "CHT_3A",
                            fsNAME = "3A授權",
                            fsCONTENT = "回傳結構錯誤【fsOTPW:" + clsCHT_3A_ARGS.fsCHT_OTPW + "】" + fsRESULT,
                            fsCREATED_BY = "fnGET_CHT_3A"
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                clsCHT_3A.fsVALIDATE_A3 = "Error";
                clsCHT_3A.fsMESSAGE_A3 = ex.Message;
                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                {
                    fsTYPE = "CHT_3A",
                    fsNAME = "3A計費",
                    fsCONTENT = ex.Message + "【fsOTPW:" + clsCHT_3A_ARGS.fsCHT_OTPW + "】",
                    fsCREATED_BY = "fnGET_CHT_3A"
                });
            }

            return clsCHT_3A;
        }

        //更新3A計費
        public string fnUPDATE_CHT_3A(string fsORDER_NO, string fsVALIDATE_A3, string fsMESSAGE_A3, string fsRESULT_A3)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            dicParameters.Add("fsVALIDATE_A3", fsVALIDATE_A3);
            dicParameters.Add("fsMESSAGE_A3", fsMESSAGE_A3);
            dicParameters.Add("fsRESULT_A3", fsRESULT_A3);

            fsRESULT = Do_Tran("spAPI_UPDATE_ORDER_CHT_3A", dicParameters);

            return fsRESULT;
        }

        //進行攤分
        public clsCHT_MILTIBILL fnGET_CHT_MULTIBILL(clsCHT_MILTIBILL_ARGS clsCHT_MILTIBILL_ARGS)
        {
            string fsRESULT = string.Empty;
            int fnRETRY_COUNT = 1;
            clsCHT_MILTIBILL clsCHT_MILTIBILL = new clsCHT_MILTIBILL();
            string fsCHT_MULTIBILL_URL = string.Empty;
            //根據不同付費方式，不同的攤分URL
            if (clsCHT_MILTIBILL_ARGS.fsCHT_AUTHORITY.ToLower() == "credit")
                fsCHT_MULTIBILL_URL = new repCONFIG().fnGET_CFG("BSM_CHT_CREDIT_MULTIBILL")[0].fsVALUE;
            else if (clsCHT_MILTIBILL_ARGS.fsCHT_AUTHORITY.ToLower() == "cvs")
                fsCHT_MULTIBILL_URL = new repCONFIG().fnGET_CFG("BSM_CHT_CVS_MULTIBILL")[0].fsVALUE;
            else if (clsCHT_MILTIBILL_ARGS.fsCHT_AUTHORITY.ToLower() == "atm")
                fsCHT_MULTIBILL_URL = new repCONFIG().fnGET_CFG("BSM_CHT_ATM_MULTIBILL")[0].fsVALUE;

            string fsPARAM = "aa-otpw=" + clsCHT_MILTIBILL_ARGS.fsCHT_OTPW + "&multiplebills=" + clsCHT_MILTIBILL_ARGS.fsCHT_MULTIPLE_BILL_PARAM;
            byte[] fbPARAM = System.Text.Encoding.UTF8.GetBytes(fsPARAM);


            XmlDocument xDOC = new XmlDocument();

            try
            {
                while (fnRETRY_COUNT <= 3 && (string.IsNullOrEmpty(fsRESULT) || (xDOC.SelectSingleNode("/Result/response-code").FirstChild != null && xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value != "00")))
                {
                    if (fnRETRY_COUNT >= 2) System.Threading.Thread.Sleep(3000);

                    fsRESULT = string.Empty;

                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(fsCHT_MULTIBILL_URL);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = fbPARAM.Length;

                    using (Stream reqStream = httpWebRequest.GetRequestStream())
                    {
                        reqStream.Write(fbPARAM, 0, fbPARAM.Length);
                    }
                    using (StreamReader sr = new StreamReader(httpWebRequest.GetResponse().GetResponseStream(), Encoding.Default))
                    {
                        fsRESULT = sr.ReadToEnd();

                        xDOC.LoadXml(fsRESULT);
                    }

                    fnRETRY_COUNT += 1;
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    clsCHT_MILTIBILL.fsVALIDATE_BILL = "Error";
                    clsCHT_MILTIBILL.fsMULTIPLE_BILL = "20";
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "CHT_MULTIBILL",
                        fsNAME = clsCHT_MILTIBILL_ARGS.fsCHT_AUTHORITY + "攤分",
                        fsCONTENT = "取得攤分失敗【fsOTPW:" + clsCHT_MILTIBILL_ARGS.fsCHT_OTPW + "】",
                        fsCREATED_BY = "fnGET_CHT_MULTIBILL"
                    });
                }
                else
                {
                    if (xDOC.SelectSingleNode("/Result/response-code").FirstChild != null)
                    {
                        if (xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value == "00")
                        {
                            clsCHT_MILTIBILL.fsVALIDATE_BILL = "Success";
                            clsCHT_MILTIBILL.fsMULTIPLE_BILL = xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value;

                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_BILL",
                                fsNAME = clsCHT_MILTIBILL_ARGS.fsCHT_AUTHORITY + "攤分",
                                fsCONTENT = "取得攤分成功【fsOTPW:" + clsCHT_MILTIBILL_ARGS.fsCHT_OTPW + "】",
                                fsCREATED_BY = "fnGET_CHT_MULTIBILL"
                            });
                        }
                        else
                        {
                            clsCHT_MILTIBILL.fsVALIDATE_BILL = "Error";
                            clsCHT_MILTIBILL.fsMULTIPLE_BILL = xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value;

                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_BILL",
                                fsNAME = clsCHT_MILTIBILL_ARGS.fsCHT_AUTHORITY + "攤分",
                                fsCONTENT = "取得攤分失敗【fsOTPW:" + clsCHT_MILTIBILL_ARGS.fsCHT_OTPW + "】" + fsRESULT,
                                fsCREATED_BY = "fnGET_CHT_MULTIBILL"
                            });
                        }
                    }
                    else
                    {
                        clsCHT_MILTIBILL.fsVALIDATE_BILL = "Error";
                        clsCHT_MILTIBILL.fsMULTIPLE_BILL = "21";
                        new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                        {
                            fsTYPE = "CHT_BILL",
                            fsNAME = clsCHT_MILTIBILL_ARGS.fsCHT_AUTHORITY + "攤分",
                            fsCONTENT = "回傳結構錯誤【fsOTPW:" + clsCHT_MILTIBILL_ARGS.fsCHT_OTPW + "】" + fsRESULT,
                            fsCREATED_BY = "fnGET_CHT_MULTIBILL"
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsCHT_MILTIBILL.fsVALIDATE_BILL = "Error";
                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                {
                    fsTYPE = "CHT_BILL",
                    fsNAME = clsCHT_MILTIBILL_ARGS.fsCHT_AUTHORITY + "攤分",
                    fsCONTENT = ex.Message + "【fsOTPW:" + clsCHT_MILTIBILL_ARGS.fsCHT_OTPW + "】",
                    fsCREATED_BY = "fnGET_CHT_MULTIBILL"
                });
            }

            return clsCHT_MILTIBILL;
        }

        //更新攤分
        public string fnUPDATE_CHT_MULTIBILL(string fsOTPW, string fsMULTIPLE_BILL, string fsUPDATED_BY)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsOTPW", fsOTPW);
            dicParameters.Add("fsMULTIPLE_BILL", fsMULTIPLE_BILL);
            dicParameters.Add("fsUPDATED_BY", fsUPDATED_BY);

            fsRESULT = Do_Tran("spAPI_UPDATE_ORDER_CHT_MULTIBILL", dicParameters);

            return fsRESULT;
        }

        //接收中華通知ATM繳費
        public string fnUPDATE_CHT_ATM_ISPAID(clsCHT_ATM_ISPAID_ARGS clsCHT_ATM_ISPAID_ARGS)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsOTPW", clsCHT_ATM_ISPAID_ARGS.fsOTPW);
            dicParameters.Add("fsATM_RESPONSE_CODE", clsCHT_ATM_ISPAID_ARGS.fsATM_RESPONSE_CODE);
            dicParameters.Add("fsATM_TXNDATETIME", clsCHT_ATM_ISPAID_ARGS.fsATM_TXNDATETIME);
            dicParameters.Add("fsATM_RESULT", clsCHT_ATM_ISPAID_ARGS.fsATM_RESULT);

            fsRESULT = Do_Tran("spAPI_UPDATE_ORDER_CHT_ATM_ISPAID", dicParameters);

            return fsRESULT;
        }

        //進行訂閱
        public clsCHT_SUBSCRIBE fnGET_CHT_SUBSCRIBE(clsCHT_SUBSCRIBE_ARGS clsCHT_SUBSCRIBE_ARGS)
        {
            string fsRESULT = string.Empty;
            int fnRETRY_COUNT = 1;
            clsCHT_SUBSCRIBE clsCHT_SUBSCRIBE = new clsCHT_SUBSCRIBE();
            string fsCHT_SUBSCRIBE_URL = string.Empty;
            //根據不同付費方式，不同的月租URL
            if (clsCHT_SUBSCRIBE_ARGS.fsCHT_AUTHORITY.ToLower() == "hinet")
                fsCHT_SUBSCRIBE_URL = new repCONFIG().fnGET_CFG("BSM_CHT_SUBSCRIBE_HN")[0].fsVALUE;
            else if (clsCHT_SUBSCRIBE_ARGS.fsCHT_AUTHORITY.ToLower() == "chtld")
                fsCHT_SUBSCRIBE_URL = new repCONFIG().fnGET_CFG("BSM_CHT_SUBSCRIBE_MOBILE")[0].fsVALUE;
            else if (clsCHT_SUBSCRIBE_ARGS.fsCHT_AUTHORITY.ToLower() == "chtn")
                fsCHT_SUBSCRIBE_URL = new repCONFIG().fnGET_CFG("BSM_CHT_SUBSCRIBE_PHONE")[0].fsVALUE;

            string fsPARAM = "Alias=" + clsCHT_SUBSCRIBE_ARGS.fsALIAS + "&ProductID=" + clsCHT_SUBSCRIBE_ARGS.fsPRODUCT_ID + "&MixCode=" + clsCHT_SUBSCRIBE_ARGS.fsMIXCODE + "&ActionDate=" + clsCHT_SUBSCRIBE_ARGS.fsACTIONDATE +
                "&TotalMonth=" + clsCHT_SUBSCRIBE_ARGS.fsTOTALMONTH + "&SettingCharge=" + clsCHT_SUBSCRIBE_ARGS.fsSETTINGCHARGE + "&InstallCharge=" + clsCHT_SUBSCRIBE_ARGS.fsINSTALLCHARGE + "&FirstCharge=" + clsCHT_SUBSCRIBE_ARGS.fsFIRSTCHARGE +
                "&Amount=" + clsCHT_SUBSCRIBE_ARGS.fsAMOUNT + "&Rent=" + clsCHT_SUBSCRIBE_ARGS.fsRENT + "&CheckSum=" + clsCHT_SUBSCRIBE_ARGS.fsCHECKSUM + "&ApID=" + clsCHT_SUBSCRIBE_ARGS.fsAPID + "&DiffMsg=" + clsCHT_SUBSCRIBE_ARGS.fsDIFFMSG + "&Reserved=" + clsCHT_SUBSCRIBE_ARGS.fsRESERVED;

            byte[] fbPARAM = System.Text.Encoding.UTF8.GetBytes(fsPARAM);

            XmlDocument xDOC = new XmlDocument();

            try
            {
                while (fnRETRY_COUNT <= 3 && (string.IsNullOrEmpty(fsRESULT) || (xDOC.SelectSingleNode("/Result/response-code") != null && xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value != "Success")))
                {
                    if (fnRETRY_COUNT >= 2) System.Threading.Thread.Sleep(3000);

                    fsRESULT = string.Empty;

                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(fsCHT_SUBSCRIBE_URL);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = fbPARAM.Length;

                    using (Stream reqStream = httpWebRequest.GetRequestStream())
                    {
                        reqStream.Write(fbPARAM, 0, fbPARAM.Length);
                    }
                    using (StreamReader sr = new StreamReader(httpWebRequest.GetResponse().GetResponseStream(), Encoding.Default))
                    {
                        fsRESULT = sr.ReadToEnd();
                        xDOC.LoadXml(fsRESULT);
                    }

                    fnRETRY_COUNT += 1;
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    clsCHT_SUBSCRIBE.fsVALIDATE_SUBSCRIBE = "Error";
                    clsCHT_SUBSCRIBE.fsMESSAGE_SUBSCRIBE = "訂閱失敗";
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "CHT_SUBSCRIBE",
                        fsNAME = "月租訂閱",
                        fsCONTENT = "訂閱失敗【fsOTPW:" + clsCHT_SUBSCRIBE_ARGS.fsMIXCODE + "】",
                        fsCREATED_BY = "fnGET_CHT_SUBSCRIBE"
                    });
                }
                else
                {
                    if (xDOC.SelectSingleNode("/Result/response-code") != null)
                    {
                        if (xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value == "Success")
                        {
                            if (xDOC.SelectSingleNode("/Result/subscribeno")!= null && !string.IsNullOrEmpty(xDOC.SelectSingleNode("/Result/subscribeno").FirstChild.Value))
                            {
                                clsCHT_SUBSCRIBE.fsVALIDATE_SUBSCRIBE = "Success";
                                clsCHT_SUBSCRIBE.fsSUBSCRIBENO_SUBSCRIBE = (xDOC.SelectSingleNode("/Result/subscribeno") != null ? xDOC.SelectSingleNode("/Result/subscribeno").FirstChild.Value : string.Empty);
                                //clsCHT_SUBSCRIBE.fsMESSAGE_SUBSCRIBE = (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty);
                                //XmlNodeList xNodeList = xDOC.GetElementsByTagName("policy-response");
                                //foreach (XmlNode item in xNodeList)
                                //{
                                //    if (item.SelectSingleNode("Attribute").FirstChild.Value == "EndDate")
                                //        clsCHT_SUBSCRIBE.fsENDDATE_SUBSCRIBE = item.SelectSingleNode("value").FirstChild.Value;
                                //}
                                clsCHT_SUBSCRIBE.fsRESULT_SUBSCRIBE = fsRESULT;


                                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                                {
                                    fsTYPE = "CHT_SUBSCRIBE",
                                    fsNAME = "月租訂閱",
                                    fsCONTENT = "訂閱成功【fsOTPW:" + clsCHT_SUBSCRIBE_ARGS.fsMIXCODE + "】",
                                    fsCREATED_BY = "fnGET_CHT_SUBSCRIBE"
                                });
                            }
                            else
                            {
                                clsCHT_SUBSCRIBE.fsVALIDATE_SUBSCRIBE = "Error";
                                clsCHT_SUBSCRIBE.fsMESSAGE_SUBSCRIBE = "無回傳訂閱碼";
                                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                                {
                                    fsTYPE = "CHT_SUBSCRIBE",
                                    fsNAME = "月租訂閱",
                                    fsCONTENT = "無回傳訂閱碼【fsOTPW:" + clsCHT_SUBSCRIBE_ARGS.fsMIXCODE + "】" + fsRESULT,
                                    fsCREATED_BY = "fnGET_CHT_SUBSCRIBE"
                                });
                            }
                        }
                        else
                        {
                            clsCHT_SUBSCRIBE.fsVALIDATE_SUBSCRIBE = "Error";
                            clsCHT_SUBSCRIBE.fsMESSAGE_SUBSCRIBE = (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty);
                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_SUBSCRIBE",
                                fsNAME = "月租訂閱",
                                fsCONTENT = (xDOC.SelectSingleNode("/Result/response-message") != null ? (xDOC.SelectSingleNode("/Result/response-message").FirstChild != null ? xDOC.SelectSingleNode("/Result/response-message").FirstChild.Value : string.Empty) : string.Empty) + "【fsOTPW:" + clsCHT_SUBSCRIBE_ARGS.fsMIXCODE + "】" + fsRESULT,
                                fsCREATED_BY = "fnGET_CHT_SUBSCRIBE"
                            });
                        }
                    }
                    else
                    {
                        clsCHT_SUBSCRIBE.fsVALIDATE_SUBSCRIBE = "Error";
                        clsCHT_SUBSCRIBE.fsMESSAGE_SUBSCRIBE = "回傳結構錯誤";
                        new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                        {
                            fsTYPE = "CHT_SUBSCRIBE",
                            fsNAME = "月租訂閱",
                            fsCONTENT = "回傳結構錯誤【fsOTPW:" + clsCHT_SUBSCRIBE_ARGS.fsMIXCODE + "】" + fsRESULT,
                            fsCREATED_BY = "fnGET_CHT_SUBSCRIBE"
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                clsCHT_SUBSCRIBE.fsVALIDATE_SUBSCRIBE = "Error";
                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                {
                    fsTYPE = "CHT_SUBSCRIBE",
                    fsNAME = clsCHT_SUBSCRIBE_ARGS.fsCHT_AUTHORITY + "訂閱",
                    fsCONTENT = ex.Message + "【fsOTPW:" + clsCHT_SUBSCRIBE_ARGS.fsMIXCODE + "】" + fsRESULT,
                    fsCREATED_BY = "fnGET_CHT_SUBSCRIBE"
                });
            }


            return clsCHT_SUBSCRIBE;
        }

        //更新訂閱
        public string fnUPDATE_CHT_SUBSCRIBE(string fsORDER_NO, string fsMONTH_RESPONSE_CODE, string fsMONTH_SUBSCRIBENO, string fsMONTH_RESULT)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            dicParameters.Add("fsMONTH_RESPONSE_CODE", fsMONTH_RESPONSE_CODE);
            dicParameters.Add("fsMONTH_SUBSCRIBENO", fsMONTH_SUBSCRIBENO);
            dicParameters.Add("fsMONTH_RESULT", fsMONTH_RESULT);

            fsRESULT = Do_Tran("spAPI_UPDATE_ORDER_CHT_SUBSCRIBE", dicParameters);

            return fsRESULT;
        }
        private static readonly string fsLOG_PATH = WebConfigurationManager.AppSettings["fsLOG_PATH"];
        //進行退租
        public clsCHT_UNSUBSCRIBE fnGET_CHT_UNSUBSCRIBE(clsCHT_UNSUBSCRIBE_ARGS clsCHT_UNSUBSCRIBE_ARGS)
        {

            System.IO.File.AppendAllText(fsLOG_PATH + DateTime.Now.ToString("yyyyMMdd") + ".txt", "發生時間:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 退租開始" + "\r\n");


            string fsRESULT = string.Empty;
            int fnRETRY_COUNT = 1;
            clsCHT_UNSUBSCRIBE clsCHT_UNSUBSCRIBE = new clsCHT_UNSUBSCRIBE();
            string fsCHT_UNSUBSCRIBE_URL = string.Empty;
            //根據不同付費方式，不同的月租URL
            if (clsCHT_UNSUBSCRIBE_ARGS.fsCHT_AUTHORITY.ToLower() == "hinet")
                fsCHT_UNSUBSCRIBE_URL = new repCONFIG().fnGET_CFG("BSM_CHT_UNSUBSCRIBE_HN")[0].fsVALUE;
            else if (clsCHT_UNSUBSCRIBE_ARGS.fsCHT_AUTHORITY.ToLower() == "chtld")
                fsCHT_UNSUBSCRIBE_URL = new repCONFIG().fnGET_CFG("BSM_CHT_UNSUBSCRIBE_MOBILE")[0].fsVALUE;
            else if (clsCHT_UNSUBSCRIBE_ARGS.fsCHT_AUTHORITY.ToLower() == "chtn")
                fsCHT_UNSUBSCRIBE_URL = new repCONFIG().fnGET_CFG("BSM_CHT_UNSUBSCRIBE_PHONE")[0].fsVALUE;

            //Alias=string&SubscribeNo=string&ProductID=string&ActionDate=string&Amount=string&CheckSum=string&ApID=string&Reserved=string
            string fsPARAM = "Alias=" + clsCHT_UNSUBSCRIBE_ARGS.fsALIAS + "&SubscribeNo=" + clsCHT_UNSUBSCRIBE_ARGS.fsSUBSCRIBENO + "&ProductID=" + clsCHT_UNSUBSCRIBE_ARGS.fsPRODUCT_ID + "&ActionDate=" + clsCHT_UNSUBSCRIBE_ARGS.fsACTIONDATE + "&Amount=" + clsCHT_UNSUBSCRIBE_ARGS.fsAMOUNT +
                    "&CheckSum=" + clsCHT_UNSUBSCRIBE_ARGS.fsCHECKSUM + "&ApID=" + clsCHT_UNSUBSCRIBE_ARGS.fsAPID + "&Reserved=" + clsCHT_UNSUBSCRIBE_ARGS.fsRESERVED;
            byte[] fbPARAM = System.Text.Encoding.UTF8.GetBytes(fsPARAM);
            System.IO.File.AppendAllText(fsLOG_PATH + DateTime.Now.ToString("yyyyMMdd") + ".txt", "發生時間:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " fsCHT_UNSUBSCRIBE_URL:" + fsCHT_UNSUBSCRIBE_URL + "\r\n");

            System.IO.File.AppendAllText(fsLOG_PATH + DateTime.Now.ToString("yyyyMMdd") + ".txt", "發生時間:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " fsPARAM:" + fsPARAM + "\r\n");
            XmlDocument xDOC = new XmlDocument();

            try
            {
                while (fnRETRY_COUNT <= 3 && (string.IsNullOrEmpty(fsRESULT) || (xDOC.SelectSingleNode("/Result/response-code") != null && xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value != "Success")))
                {
                    if (fnRETRY_COUNT >= 2) System.Threading.Thread.Sleep(3000);

                    fsRESULT = string.Empty;

                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(fsCHT_UNSUBSCRIBE_URL);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = fbPARAM.Length;

                    using (Stream reqStream = httpWebRequest.GetRequestStream())
                    {
                        reqStream.Write(fbPARAM, 0, fbPARAM.Length);
                    }
                    using (StreamReader sr = new StreamReader(httpWebRequest.GetResponse().GetResponseStream(), Encoding.Default))
                    {
                        fsRESULT = sr.ReadToEnd();

                        xDOC.LoadXml(fsRESULT);
                    }

                    fnRETRY_COUNT += 1;
                }





                if (string.IsNullOrEmpty(fsRESULT))
                {
                    clsCHT_UNSUBSCRIBE.fsVALIDATE_UNSUBSCRIBE = "Error";
                    clsCHT_UNSUBSCRIBE.fsMESSAGE_UNSUBSCRIBE = "退租失敗";
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "CHT_UNSUBSCRIBE",
                        fsNAME = "訂閱退租",
                        fsCONTENT = "退租失敗【fsSUBSCRIBENO:" + clsCHT_UNSUBSCRIBE_ARGS.fsSUBSCRIBENO + "】",
                        fsCREATED_BY = "fnGET_CHT_UNSUBSCRIBE"
                    });
                }
                else
                {
                    System.IO.File.AppendAllText(fsLOG_PATH + DateTime.Now.ToString("yyyyMMdd") + ".txt", "發生時間:" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " CHT:" + fsRESULT + "\r\n");


                    if (xDOC.SelectSingleNode("/Result/response-code").FirstChild != null)
                    {
                        if (xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value == "Success")
                        {
                            clsCHT_UNSUBSCRIBE.fsVALIDATE_UNSUBSCRIBE = "Success";
                            clsCHT_UNSUBSCRIBE.fsMESSAGE_UNSUBSCRIBE = string.Empty;
                            clsCHT_UNSUBSCRIBE.fsRESULT_UNSUBSCRIBE = fsRESULT;

                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_UNSUBSCRIBE",
                                fsNAME = "訂閱退租",
                                fsCONTENT = "退租成功【fsSUBSCRIBENO:" + clsCHT_UNSUBSCRIBE_ARGS.fsSUBSCRIBENO + "】" + fsRESULT,
                                fsCREATED_BY = "fnGET_CHT_UNSUBSCRIBE"
                            });
                        }
                        else
                        {
                            clsCHT_UNSUBSCRIBE.fsVALIDATE_UNSUBSCRIBE = "Error";
                            clsCHT_UNSUBSCRIBE.fsMESSAGE_UNSUBSCRIBE = "退租失敗";
                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_UNSUBSCRIBE",
                                fsNAME = "訂閱退租",
                                fsCONTENT = "退租失敗【fsSUBSCRIBENO:" + clsCHT_UNSUBSCRIBE_ARGS.fsSUBSCRIBENO + "】" + fsRESULT,
                                fsCREATED_BY = "fnGET_CHT_UNSUBSCRIBE"
                            });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                clsCHT_UNSUBSCRIBE.fsVALIDATE_UNSUBSCRIBE = "Error";
                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                {
                    fsTYPE = "CHT_UNSUBSCRIBE",
                    fsNAME = "訂閱退租",
                    fsCONTENT = ex.Message + "【fsSUBSCRIBENO:" + clsCHT_UNSUBSCRIBE_ARGS.fsSUBSCRIBENO + "】" + fsRESULT,
                    fsCREATED_BY = "fnGET_CHT_UNSUBSCRIBE"
                });
            }

            return clsCHT_UNSUBSCRIBE;
        }

        //查詢租用狀況
        public clsCHT_SUBSCRIBE_QUERY fnGET_CHT_SUBSCRIBE_QUERY(clsCHT_SUBSCRIBE_QUERY_ARGS clsCHT_SUBSCRIBE_QUERY_ARGS)
        {
            string fsRESULT = string.Empty;
            int fnRETRY_COUNT = 1;
            clsCHT_SUBSCRIBE_QUERY clsCHT_SUBSCRIBE_QUERY = new clsCHT_SUBSCRIBE_QUERY();
            string fsCHT_SUBSCRIBE_QUERY_URL = new repCONFIG().fnGET_CFG("BSM_CHT_SUBSCRIBE_QUERY")[0].fsVALUE;

            string fsPARAM = "Alias=" + clsCHT_SUBSCRIBE_QUERY_ARGS.fsALIAS + "&SubscribeNo=" + clsCHT_SUBSCRIBE_QUERY_ARGS.fsSUBSCRIBENO + "&ProductID=" + clsCHT_SUBSCRIBE_QUERY_ARGS.fsPRODUCT_ID +
                    "&CheckSum=" + clsCHT_SUBSCRIBE_QUERY_ARGS.fsCHECKSUM + "&ApID=" + clsCHT_SUBSCRIBE_QUERY_ARGS.fsAPID + "&Reserved=" + string.Empty;
            byte[] fbPARAM = System.Text.Encoding.UTF8.GetBytes(fsPARAM);
            XmlDocument xDOC = new XmlDocument();

            try
            {
                while (fnRETRY_COUNT <= 3 && (string.IsNullOrEmpty(fsRESULT) || (xDOC.SelectSingleNode("/Result/response-code") == null)))
                {
                    if (fnRETRY_COUNT >= 2) System.Threading.Thread.Sleep(3000);

                    fsRESULT = string.Empty;

                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(fsCHT_SUBSCRIBE_QUERY_URL);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = fbPARAM.Length;

                    using (Stream reqStream = httpWebRequest.GetRequestStream())
                    {
                        reqStream.Write(fbPARAM, 0, fbPARAM.Length);
                    }
                    using (StreamReader sr = new StreamReader(httpWebRequest.GetResponse().GetResponseStream(), Encoding.Default))
                    {
                        fsRESULT = sr.ReadToEnd();

                        xDOC.LoadXml(fsRESULT);
                    }

                    fnRETRY_COUNT += 1;
                }

                if (string.IsNullOrEmpty(fsRESULT))
                {
                    clsCHT_SUBSCRIBE_QUERY.fsVALIDATE_SUBSCRIBE_QUERY = "Error";
                    clsCHT_SUBSCRIBE_QUERY.fsMESSAGE_SUBSCRIBE_QUERY = "查詢訂閱失敗";
                    new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                    {
                        fsTYPE = "CHT_SUBSCRIBE_QUERY",
                        fsNAME = "查詢訂閱",
                        fsCONTENT = "查詢訂閱失敗【fsSUBSCRIBENO:" + clsCHT_SUBSCRIBE_QUERY_ARGS.fsSUBSCRIBENO + "】",
                        fsCREATED_BY = "fnGET_CHT_SUBSCRIBE_QUERY"
                    });
                }
                else
                {
                    if (xDOC.SelectSingleNode("/Result/response-code") != null)
                    {
                        if (xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value == "Success" || xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value == "Queued" ||
                            xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value == "s241")
                        {
                            clsCHT_SUBSCRIBE_QUERY.fsVALIDATE_SUBSCRIBE_QUERY = xDOC.SelectSingleNode("/Result/response-code").FirstChild.Value;
                            clsCHT_SUBSCRIBE_QUERY.fsMESSAGE_SUBSCRIBE_QUERY = string.Empty;
                        }
                        else
                        {
                            clsCHT_SUBSCRIBE_QUERY.fsVALIDATE_SUBSCRIBE_QUERY = "Error";
                            clsCHT_SUBSCRIBE_QUERY.fsMESSAGE_SUBSCRIBE_QUERY = "查詢訂閱失敗";
                            new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                            {
                                fsTYPE = "CHT_SUBSCRIBE_QUERY",
                                fsNAME = "查詢訂閱",
                                fsCONTENT = "查詢訂閱失敗【fsSUBSCRIBENO:" + clsCHT_SUBSCRIBE_QUERY_ARGS.fsSUBSCRIBENO + "】" + fsRESULT,
                                fsCREATED_BY = "fnGET_CHT_SUBSCRIBE_QUERY"
                            });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                clsCHT_SUBSCRIBE_QUERY.fsVALIDATE_SUBSCRIBE_QUERY = "Error";
                new repLOG().fnINSERT_LOG(new clsLOG_ARGS()
                {
                    fsTYPE = "CHT_SUBSCRIBE_QUERY",
                    fsNAME = "查詢訂閱",
                    fsCONTENT = ex.Message + "【fsSUBSCRIBENO:" + clsCHT_SUBSCRIBE_QUERY_ARGS.fsSUBSCRIBENO + "】" + fsRESULT,
                    fsCREATED_BY = "fnGET_CHT_SUBSCRIBE_QUERY"
                });
            }

            return clsCHT_SUBSCRIBE_QUERY;
        }

        //更新退租
        public string fnUPDATE_CHT_UNSUBSCRIBE(string fsORDER_NO, string fsMONTH_RESPONSE_CODE, string fsMONTH_SUBSCRIBENO, string fsMONTH_UNSUBSCRIBE, string fsUPDATED_BY)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            dicParameters.Add("fsMONTH_RESPONSE_CODE", fsMONTH_RESPONSE_CODE);
            dicParameters.Add("fsMONTH_SUBSCRIBENO", fsMONTH_SUBSCRIBENO);
            dicParameters.Add("fsMONTH_UNSUBSCRIBE", fsMONTH_UNSUBSCRIBE);
            dicParameters.Add("fsUPDATED_BY", fsUPDATED_BY);
            fsRESULT = Do_Tran("spAPI_UPDATE_ORDER_CHT_UNSUBSCRIBE", dicParameters);

            return fsRESULT;
        }

        //查詢訂購明細
        public List<clsGET_MEMBER_PURCHASE_INFO> fnGET_MEMBER_PURCHASE_INFO(string fsACCOUNT_ID, string fsOTPW)
        {
            List<clsGET_MEMBER_PURCHASE_INFO> clsGET_MEMBER_PURCHASE_INFOs = new List<clsGET_MEMBER_PURCHASE_INFO>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsOTPW", fsOTPW);
            DataTable dtPURCHASE_INFO = Do_Query("spAPI_GET_MEMBER_PURCHASE_INFO", dicParameters);
            if (dtPURCHASE_INFO != null && dtPURCHASE_INFO.Rows.Count > 0)
            {
                for (int i = 0; i < dtPURCHASE_INFO.Rows.Count; i++)
                {
                    clsGET_MEMBER_PURCHASE_INFOs.Add(new clsGET_MEMBER_PURCHASE_INFO()
                    {
                        fsORDER_NO = dtPURCHASE_INFO.Rows[i]["fsORDER_NO"].ToString(),
                        fdORDER_DATE = DateTime.Parse(dtPURCHASE_INFO.Rows[i]["fdORDER_DATE"].ToString()).ToString("yyyy/MM/dd"),
                        fsPURCHASE_ID = dtPURCHASE_INFO.Rows[i]["fsPURCHASE_ID"].ToString(),
                        fsSET_NAME = dtPURCHASE_INFO.Rows[i]["fsSET_NAME"].ToString(),
                        fsSET_ITEM_NAME = dtPURCHASE_INFO.Rows[i]["fsSET_ITEM_NAME"].ToString(),
                        fsPAY_TYPE = dtPURCHASE_INFO.Rows[i]["fsPAY_TYPE"].ToString(),
                        fsPAY_TYPE_NAME = dtPURCHASE_INFO.Rows[i]["fsPAY_TYPE_NAME"].ToString(),
                        fnFEE = int.Parse(dtPURCHASE_INFO.Rows[i]["fsFEE"].ToString()),
                        fsPAY_STATUS = dtPURCHASE_INFO.Rows[i]["fsPAY_STATUS"].ToString(),
                        fdEND_DATE = dtPURCHASE_INFO.Rows[i]["fdEND_DATE"].ToString(),
                        fsEXPIRATIONDATE = dtPURCHASE_INFO.Rows[i]["fsEXPIRATIONDATE"].ToString(),
                        fsBANKCODE = dtPURCHASE_INFO.Rows[i]["fsBANKCODE"].ToString(),
                        fsUID = dtPURCHASE_INFO.Rows[i]["fsUID"].ToString(),
                        fsPROMO_CODE = dtPURCHASE_INFO.Rows[i]["fsPROMO_CODE"].ToString(),
                        fsPACKAGE_ID = dtPURCHASE_INFO.Rows[i]["fsPACKAGE_ID"].ToString(),
                        fsMACHINE_ID = dtPURCHASE_INFO.Rows[i]["fsMACHINE_ID"].ToString()
                    });
                }
            }
            return clsGET_MEMBER_PURCHASE_INFOs;
        }

        //查詢可使用服務
        public List<clsGET_MEMBER_SERVICE_INFO> fnGET_MEMBER_SERVICE_INFO(string fsACCOUNT_ID, string fsFROM = "")
        {
            List<clsGET_MEMBER_SERVICE_INFO> clsGET_MEMBER_SERVICE_INFOs = new List<clsGET_MEMBER_SERVICE_INFO>();
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsFROM", fsFROM);
            DataTable dtPURCHASE_INFO = Do_Query("spAPI_GET_MEMBER_SERVICE_INFO", dicParameters);
            if (dtPURCHASE_INFO != null && dtPURCHASE_INFO.Rows.Count > 0)
            {
                for (int i = 0; i < dtPURCHASE_INFO.Rows.Count; i++)
                {
                    clsGET_MEMBER_SERVICE_INFOs.Add(new clsGET_MEMBER_SERVICE_INFO()
                    {
                        fsPURCHASE_ID = dtPURCHASE_INFO.Rows[i]["fsPURCHASE_ID"].ToString(),
                        fsPURCHASE_NAME = dtPURCHASE_INFO.Rows[i]["fsPURCHASE_NAME"].ToString(),
                        fdSTART_DATE = dtPURCHASE_INFO.Rows[i]["fdSTART_DATE"].ToString(),
                        fdEND_DATE = dtPURCHASE_INFO.Rows[i]["fdEND_DATE"].ToString(),
                        fsMACHINE_ID = dtPURCHASE_INFO.Rows[i]["fsMACHINE_ID"].ToString()
                    });
                }
            }
            return clsGET_MEMBER_SERVICE_INFOs;
        }

        //刪除失敗訂單
        public string fnDELETE_FAIL_ORDER(string fsORDER_NO, string fsACCOUNT_ID)
        {
            string fsRESULT = string.Empty;
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsORDER_NO", fsORDER_NO);
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            fsRESULT = Do_Tran("spAPI_DELETE_FAIL_ORDER", dicParameters);

            return fsRESULT;
        }

        //取得優惠碼狀態
        public string fnGET_COUPON_STATUS(string fsCOUPON_NO)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsCOUPON_NO", fsCOUPON_NO);
            DataTable dtCOUPON = Do_Query("spAPI_GET_COUPON_STATUS", dicParameters);

            if (dtCOUPON != null && dtCOUPON.Rows.Count > 0)
            {
                return dtCOUPON.Rows[0][0].ToString();
            }

            return "伺服器錯誤!";
        }

        //啟用優惠碼
        public string fnREGISTER_COUPON(string fsACCOUNT_ID, string fsCOUPON_NO)
        {
            Dictionary<string, string> dicParameters = new Dictionary<string, string>();
            dicParameters.Add("fsACCOUNT_ID", fsACCOUNT_ID);
            dicParameters.Add("fsCOUPON_NO", fsCOUPON_NO);
            DataTable dtCOUPON = Do_Query("spAPI_INSERT_USE_COUPON", dicParameters);

            if (dtCOUPON != null && dtCOUPON.Rows.Count > 0)
            {
                return dtCOUPON.Rows[0][0].ToString();
            }

            return "ERROR:伺服器錯誤!";
        }
    }
}